ktap/ktap
azat/ktap
d2bb7dec7876b6128ef24227f04e2e563b632014
runtime/ktap.h
187,#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 19, 0)
188,#define TRACE_SEQ_LEN(s) ((s)->len)
189,#define TRACE_SEQ_READPOS(s) ((s)->readpos)
190,#else
191,#define TRACE_SEQ_LEN(s) ((s)->seq.len) /** XXX: trace_seq_used() */
192,#define TRACE_SEQ_READPOS(s) ((s)->seq.readpos)
193,#endif
