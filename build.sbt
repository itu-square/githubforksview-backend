name := "GithubForkViews"
organization := "dk.itu.gfv"
version := "1.0"

scalaVersion := "2.12.2"
//scapegoatVersion := "1.1.0"


scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")
//libraryDependencies += "com.twitter" %% "finagle-http" % "6.44.0"
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.25"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "ch.qos.logback" % "logback-core" % "1.2.3"
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.40"
libraryDependencies += "com.typesafe.akka" % "akka-slf4j_2.12" % "2.5.1"
libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.5"
libraryDependencies += "org.apache.commons" % "commons-email" % "1.4"
libraryDependencies += "org.apache.poi" % "poi" % "3.14"
libraryDependencies += "org.apache.poi" % "poi-ooxml" % "3.15-beta2"
libraryDependencies += "org.eclipse.jgit" % "org.eclipse.jgit" % "4.8.0.201705170830-rc1"
// https://mvnrepository.com/artifact/org.scalatest/scalatest_2.12
libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.2.0-SNAP6"

// https://mvnrepository.com/artifact/org.scalaj/scalaj-http_2.12
libraryDependencies += "org.scalaj" % "scalaj-http_2.12" % "2.3.0"


// https://mvnrepository.com/artifact/org.parboiled/parboiled-scala_2.12
//libraryDependencies += "org.parboiled" % "parboiled-scala_2.12" % "1.1.8"
libraryDependencies += "org.parboiled" %% "parboiled" % "2.1.4"
libraryDependencies += "org.jsoup" % "jsoup" % "1.10.2"
libraryDependencies += "org.mariadb.jdbc" % "mariadb-java-client" % "2.0.1"

libraryDependencies += "com.jcraft" % "jsch" % "0.1.54"

libraryDependencies += "com.google.code.gson" % "gson" % "2.7"
libraryDependencies += "com.github.pathikrit" % "better-files_2.12" % "3.0.0"
libraryDependencies += "com.typesafe" % "config" % "1.3.1"
libraryDependencies += "com.typesafe.scala-logging" % "scala-logging_2.12" % "3.5.0"

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.0",
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.3.0",
  "joda-time" % "joda-time" % "2.7",
  "org.joda" % "joda-convert" % "1.7"
)

// https://mvnrepository.com/artifact/com.typesafe.play/play-json_2.12
//libraryDependencies += "com.typesafe.play" % "play-json_2.12" % "2.6.0-M7"
//resolvers += Resolver.bintrayRepo("hseeberger", "maven")
//
//libraryDependencies ++= List(
//  "de.heikoseeberger" %% "akka-http-play-json" % "2.6.0-M7"
//)

// https://mvnrepository.com/artifact/io.argonaut/argonaut_2.12
//libraryDependencies += "io.argonaut" % "argonaut_2.12" % "6.2"

libraryDependencies += "info.debatty" % "java-string-similarity" % "0.18"

libraryDependencies += "commons-io" % "commons-io" % "2.5"
libraryDependencies += "au.com.bytecode" % "opencsv" % "2.4"

libraryDependencies += "org.mockito" % "mockito-core" % "2.8.9"

// https://mvnrepository.com/artifact/com.typesafe.akka/akka-actor_2.12
libraryDependencies += "com.typesafe.akka" % "akka-http-spray-json_2.12" % "10.0.6"
libraryDependencies += "com.typesafe.akka" % "akka-actor_2.12" % "2.5.1"
libraryDependencies += "com.typesafe.akka" % "akka-http-core_2.12" % "10.0.6"
libraryDependencies += "com.typesafe.akka" % "akka-http_2.12" % "10.0.6"
libraryDependencies += "com.typesafe.akka" % "akka-stream_2.12" % "2.5.1"
// https://mvnrepository.com/artifact/com.typesafe.akka/akka-slf4j_2.12



//libraryDependencies ++= Seq(
//  "com.typesafe.akka" %% "akka-http-core" % "10.0.5",
//  "com.typesafe.akka" %% "akka-http" % "10.0.5",
//  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.5"
//)
//libraryDependencies ++= {
//  val akkaV = "2.4.17"
//  Seq(
//    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
//    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test",
//    "com.typesafe.akka"   %%  "akka-slf4j"    % akkaV
//  )
//}

//
//resolvers ++= Seq("RoundEights" at "http://maven.spikemark.net/roundeights")
//
//libraryDependencies ++= Seq("com.roundeights" %% "hasher" % "1.2.0")

//Revolver.settings

test in assembly := {}
//mainClass in assembly := Some("dk.itu.gfv.server.Boot")
//assemblyJarName in assembly := "server.jar"
mainClass in assembly := Some("dk.itu.gfv.monitor.ServiceRunner")
assemblyJarName in assembly := "gms.jar"
//mainClass in assembly := Some("INFOX_data")
//assemblyJarName in assembly := "infox_forks.jar"