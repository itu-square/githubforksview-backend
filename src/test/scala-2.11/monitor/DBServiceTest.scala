package monitor

import dk.itu.gfv.model.Model.RepoFork
import dk.itu.gfv.monitor.db.Models.Project
import dk.itu.gfv.monitor.db.{DBService, DatabaseSchema}
import dk.itu.gfv.repo.GithubExtractor
import org.joda.time.DateTime
import org.scalatest.{FunSuite, Matchers}
import argonaut._
import Argonaut._
import concurrent.ExecutionContext.Implicits.global

/**
  * Created by scas on 07-01-2017.
  */
class DBServiceTest extends FunSuite with DatabaseSchema with DBService with Matchers {


  test("get forks of project") {
    val forks = getForksOfProject("scas-mdd/fork-test")
    forks.size shouldBe 3
//    forks shouldBe Seq(
//      Project(2, "mining-repos1/fork-test", 3, true, 1, DateTime.parse("2016-06-11T19:36:06Z"), DateTime.parse("2016-07-13T21:17:38Z"), DateTime.parse("2016-06-11T19:36:08Z")),
//      Project(3, "mining-repositories/fork-test", 5, true, 1, DateTime.parse("2016-06-11T19:38:01Z"), DateTime.parse("2016-07-10T23:04:21Z"), DateTime.parse("2016-06-11T19:38:02Z")),
//      Project(4, "proof-ro/fork-test", 7, true, 3, DateTime.parse("2017-01-05T19:46:26Z"), DateTime.parse("2016-07-10T23:04:21Z"), DateTime.parse("2017-01-05T19:46:29Z"))
//    )
  }

  test("cloning project"){



  }
  test("cloning existing project"){
    val project = "scas-mdd/fork-test"
    val p = GithubExtractor.getRepositoryJson(project).decodeOption[RepoFork].get

    cloneProject(project,p) shouldBe true
  }

  test("is git repository cloned"){
    isGitRepositoryCloned("F:/gms/test/fork-test-no-checkout/.git") shouldBe true
  }
}
