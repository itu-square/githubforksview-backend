package monitor

import java.io.File

import dk.itu.gfv.model.Model.{RepoFork, RepoMain}
import org.scalatest.{FunSuite, Matchers}
import dk.itu.gfv.monitor.db.NightFetcher._
import argonaut._
import Argonaut._
import better.files.{File => ScalaFile, _}
import dk.itu.gfv.repo.RepositoryOperations
import dk.itu.gfv.util.{MonitorProperties, Util}
import org.eclipse.jgit.api.Git

import scala.io.Source
import scala.util.Success

/**
  * Created by scas on 12-12-2016.
  */
class NightFetcherTest extends FunSuite with Matchers{

  val projects_folder = "test-files/monitor/projects"
  val project = "scas-mdd/redundant-test"
  val metadata_folder = projects_folder + "/" + project + "/metadata-gfv/"

  test("pushed_to_forks") {
    //    pushed_to_for ks
    val list_of_forks_json = Source.fromURL(getClass.getResource("/monitor/pushed_to_forks_new.json")).mkString
    val old_list_of_forks_json = Source.fromURL(getClass.getResource("/monitor/pushed_to_forks_old.json")).mkString
    val list_of_forks = list_of_forks_json.decodeOption[Seq[RepoMain]].get
    val old_list_of_forks = old_list_of_forks_json.decodeOption[Seq[RepoMain]].get

    pushed_to_forks(list_of_forks, old_list_of_forks) shouldBe Seq(list_of_forks.head)
    pushed_to_forks(old_list_of_forks, old_list_of_forks) shouldBe Seq()

  }

  test("new_forks") {
    val list_of_forks_json = Source.fromURL(getClass.getResource("/monitor/forks_new.json")).mkString
    val old_list_of_forks_json = Source.fromURL(getClass.getResource("/monitor/forks_old.json")).mkString
    val exp_json = Source.fromURL(getClass.getResource("/monitor/forks_new_exp.json")).mkString
    val list_of_forks = list_of_forks_json.decodeOption[Seq[RepoMain]].get
    list_of_forks.size shouldBe 2
    val old_list_of_forks = old_list_of_forks_json.decodeOption[Seq[RepoMain]].get
    old_list_of_forks.size shouldBe 1
    val exp = Seq(exp_json.decodeOption[RepoFork].get)
    exp.size shouldBe 1
    val new_forks_res = new_forks(list_of_forks, old_list_of_forks)
    new_forks_res shouldBe exp
  }

  test("new_branches") {
    //    new_branches
    val list_of_forks_json = Source.fromURL(getClass.getResource("/monitor/pushed_to_forks_new.json")).mkString
    val list_of_forks = list_of_forks_json.decodeOption[Seq[RepoMain]].get
    val old_forks_commits_updated = Seq(list_of_forks.head)
    val project = "scas-mdd/redundant-test"
    val old_forks_branches = Util.readFromFileToString("test-files/monitor/projects/" + project + "/metadata-gfv/" + old_branch_heads).decodeOption[Seq[ProjectBranch]].get
    // we simulate fetch by inserting a branch in one of the projects.
    // prepare -- make a branch

    val git = Git.open(new File("test-files/monitor/projects/" + project + "/repositories/" + old_forks_commits_updated.head.full_name + "/.git"))
    val branch_name = "testing_branch"
    try {
      git.branchCreate
        .setName(branch_name)
        .call
    } catch {
      case _: Throwable => None
    }
    // now we get a new list of branches
    val old_forks_branches_after_fetch = old_forks_commits_updated.map(x => RepositoryOperations.getBranches(Git.open(new File(projects_folder + "/" + project + "/repositories/" + x.full_name + "/.git"))).map(a => ProjectBranch(x.full_name, a.getName, a.getObjectId.toString))).flatten

    // we cleanup by deleting that branch
    git.branchDelete
      .setBranchNames(branch_name)
      .setForce(true)
      .call

     new_branches(old_forks_branches, old_forks_branches_after_fetch) shouldBe old_forks_branches_after_fetch
     new_branches(old_forks_branches, old_forks_branches) shouldBe Seq()

  }

  test("Parse branch heads") {
    val json = Util.readFromFileToString("test-files/monitor/projects/scas-mdd/redundant-test/metadata-gfv/new_branch_heads.json")
    val exp = Seq(
      ProjectBranch("scas-mdd/redundant-test", "master", "7fd042e7043d7e26e0bfc6965251bda67bedb683"),
      ProjectBranch("mining-repositories/redundant-test", "master", "874beac03380683730cd32679c0efd81909a685d"),
      ProjectBranch("mining-repos1/redundant-test", "master", "f34a255b5736a4d8d334b6f8e41040b20781d906")
    )
    json shouldBe exp.asJson.spaces2
    json.decodeOption[Seq[ProjectBranch]].get shouldBe exp
  }

  test("Overwrite old jsons") {

//    def prepare_json = {
//      val old_branch_heads_json = ScalaFile(metadata_folder + "new_branch_heads.json")
//      val old_forks_json = ScalaFile(metadata_folder + "new_branch_heads.json")
//      val old_main_json = ScalaFile(metadata_folder + "new_branch_heads.json")
//
//      val old_branch_heads1 = old_branch_heads_json.contentAsString.decodeOption[Seq[ProjectBranch]].get
//      val old_forks = old_forks_json.contentAsString.decodeOption[Seq[RepoMain]].get
//      val old_main = old_main_json.contentAsString.decodeOption[RepoMain].get
//
//      val new_branch_heads1 = old_branch_heads1.map(x => ProjectBranch(x.project, x.branch, "newer_head")).asJson.spaces2
//      val new_forks = old_forks.map(x => RepoMain(x.id,x.full_name,x.fork, x.created_at, x.updated_at, x.pushed_at, x.default_branch)).asJson.spaces2
//      val new_main = RepoMain(old_main.id,old_main.full_name,old_main.fork, old_main.created_at, old_main.updated_at, org.joda.time.DateTime.now.toString, old_main.default_branch).asJson.spaces2
//
//      ScalaFile(metadata_folder + old_branch_heads) < old_branch_heads_json.contentAsString
//      ScalaFile(metadata_folder + old_main_project_json) < old_main_json.contentAsString
//      ScalaFile(metadata_folder + old_forks_project_json) < old_forks_json.contentAsString
//
//      ScalaFile(metadata_folder + new_branch_heads).write(new_branch_heads1)
//      ScalaFile(metadata_folder + new_main_project_json).write(new_main)
//      ScalaFile(metadata_folder + new_forks_project_json).write(new_forks)
//    }

    val old_branch_heads_json = ScalaFile(metadata_folder + old_branch_heads)
    val old_main_json = ScalaFile(metadata_folder + old_main_project_json)
    val old_forks_json = ScalaFile(metadata_folder + old_forks_project_json)
    val new_branch_heads_file =  ScalaFile(metadata_folder + new_branch_heads)
    val new_main_json = ScalaFile(metadata_folder + new_main_project_json)
    val new_forks_project_json_file = ScalaFile(metadata_folder + new_forks_project_json)
    overwrite_old_jsons(project)(metadata_folder) shouldBe Success()

    old_branch_heads_json.contentAsString shouldBe ScalaFile(metadata_folder + "bckp/new_branch_heads.json").contentAsString
    old_main_json.contentAsString shouldBe ScalaFile(metadata_folder + "bckp/new_main.json").contentAsString
    old_forks_json.contentAsString shouldBe ScalaFile(metadata_folder + "bckp/new_forks.json").contentAsString

    new_branch_heads_file.contentAsString shouldBe ""
    new_main_json.contentAsString shouldBe ""
    new_forks_project_json_file.contentAsString shouldBe ""

    old_branch_heads_json < ScalaFile(metadata_folder + "bckp/branch_heads.json").contentAsString
    old_main_json < ScalaFile(metadata_folder + "bckp/main.json").contentAsString
    old_forks_json < ScalaFile(metadata_folder + "bckp/forks.json").contentAsString

    new_branch_heads_file < ScalaFile(metadata_folder + "bckp/new_branch_heads.json").contentAsString
    new_main_json < ScalaFile(metadata_folder + "bckp/new_main.json").contentAsString
    new_forks_project_json_file < ScalaFile(metadata_folder + "bckp/new_forks.json").contentAsString

  }

  test("load old branches and heads") {

  }

  test("update branches head") {
    val old_branches_and_heads = Seq()
    val old_forks_branches_after_fetch = Seq()
    val old_main_forks_new_branches = Seq()
    val added_forks = Seq()
    update_branches_head_json(old_branches_and_heads,old_forks_branches_after_fetch,old_main_forks_new_branches,added_forks) shouldBe Seq()
  }
}
