package patchparser

import java.util.regex.Pattern

import dk.itu.gfv.repo.RepositoryOperations
import dk.itu.gfv.repo.RepositoryOperations._
import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 28-03-2017.
  */
class RegexTest extends FunSuite with Matchers{


  test("empty line check"){
    val s = "+  "
    RepositoryOperations.emptylineCheck(s) shouldBe true

    val s1 = "+ n"
    RepositoryOperations.emptylineCheck(s) shouldBe false


  }

  test("break regex"){
    val s2 = "+ break;\n"
    Pattern.matches(breakKeywordRegex,s2) shouldBe true
    val s3 = "+ break;"
    Pattern.matches(breakKeywordRegex,s3) shouldBe true

    val s4 = "+         break "
    Pattern.matches(breakKeywordRegex,s4) shouldBe true
  }
}
