package patchparser

import org.scalatest.{FunSuite, Matchers}
import dk.itu.gfv.repo.PatchParser._
import dk.itu.gfv.repo.RepositoryOperations

/**
  * Created by scas on 03-11-2016.
  */
class PatchParserTest extends FunSuite with Matchers{
  val patch = """diff --git a/system/modules/backend/drivers/DC_Folder.php b/system/modules/backend/drivers/DC_Folder.php
                |index adeafd9..30d4119 100644
                |--- a/system/modules/backend/drivers/DC_Folder.php
                |+++ b/system/modules/backend/drivers/DC_Folder.php
                |@@ -75,6 +75,12 @@ class DC_Folder extends \DataContainer implements \listable, \editable
                | 	 */
                | 	protected $arrMessages = array();
                |
                |+	/**
                |+	 * True if a new version has to be created
                |+	 * @param boolean
                |+	 */
                |+	protected $blnCreateNewVersion = false;
                |+
                |
                | 	/**
                | 	 * Initialize the object
                |@@ -203,6 +209,10 @@ public function __get($strKey)
                | 				return $this->strExtension;
                | 				break;
                |
                |+			case 'createNewVersion':
                |+				return $this->blnCreateNewVersion;
                |+        break;
                |+
                | 			default:
                | 				return parent::__get($strKey);
                | 				break;
                |@@ -998,6 +1008,55 @@ public function edit()
                | 			$this->redirect('contao/main.php?act=error');
                | 		}
                |
                |+		// Get the DB entry
                |+		$objFile = \FilesModel::findByPath($this->intId);
                |+		$this->objActiveRecord = $objFile;
                |+
                |+		$this->blnCreateNewVersion = false;
                |""".stripMargin

  test("Parsing patch to hunk"){


    val p = splitPatchByFiles(patch)
    val hunks = splitPatchByHunks(p.get.head.asInstanceOf[DiffFile].patch)
    hunks.get.size shouldBe 3
//    val hunkExpected = """--- /dev/null
//                         |+++ b/system/library/PhpDiff/Diff/Renderer/Text/Unified.php
//                         |@@ -0,0 +1,87 @@
//                         |+<?php
//                         |+	public function render()
//                         |+			$diff .= '@@ -'.($i1 + 1).','.($i2 - $i1).' +'.($j1 + 1).','.($j2 - $j1)." @@\n";
//                         |""".stripMargin
//    val expected = Some(Vector(Hunk(hunkExpected)))

//    hunks shouldBe expected
  }

  //Positive
  test("Parsing patch to hunk1"){
    val patch = """diff --git a/system/library/PhpDiff/Diff/Renderer/Text/Unified.php b/system/library/PhpDiff/Diff/Renderer/Text/Unified.php
                  |new file mode 100644
                  |index 0000000..e94d951
                  |--- /dev/null
                  |+++ b/system/library/PhpDiff/Diff/Renderer/Text/Unified.php
                  |@@ -0,0 +1,87 @@
                  |+<?php
                  |+public function render()
                  |+$diff .= ""
                  |""".stripMargin

    val p = splitPatchByFiles(patch)
    val hunks = splitPatchByHunks(p.get.head.asInstanceOf[DiffFile].patch)

    val hunkExpected = """--- /dev/null
                         |+++ b/system/library/PhpDiff/Diff/Renderer/Text/Unified.php
                         |@@ -0,0 +1,87 @@
                         |+<?php
                         |+public function render()
                         |+$diff .= ""
                         |""".stripMargin
    val expected = Some(Vector(Hunk(hunkExpected)))

    hunks shouldBe expected
  }

  test("patch to added lines"){
    val p = RepositoryOperations.patchToAddedLinesInternal(patch,"test",removeStopWords = true,project=0)
    p foreach println
    p.size shouldBe 6
  }

}
