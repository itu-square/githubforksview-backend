package githubextractor

import dk.itu.gfv.model.Model.User
import dk.itu.gfv.repo.GithubExtractor
import org.joda.time.DateTime
import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 05-01-2017.
  */
class GithubExtractorTest extends FunSuite with Matchers{

  val json = """{
               |  "login": "ErikZalm",
               |  "id": 882374,
               |  "avatar_url": "https://avatars.githubusercontent.com/u/882374?v=3",
               |  "gravatar_id": "",
               |  "url": "https://api.github.com/users/ErikZalm",
               |  "html_url": "https://github.com/ErikZalm",
               |  "followers_url": "https://api.github.com/users/ErikZalm/followers",
               |  "following_url": "https://api.github.com/users/ErikZalm/following{/other_user}",
               |  "gists_url": "https://api.github.com/users/ErikZalm/gists{/gist_id}",
               |  "starred_url": "https://api.github.com/users/ErikZalm/starred{/owner}{/repo}",
               |  "subscriptions_url": "https://api.github.com/users/ErikZalm/subscriptions",
               |  "organizations_url": "https://api.github.com/users/ErikZalm/orgs",
               |  "repos_url": "https://api.github.com/users/ErikZalm/repos",
               |  "events_url": "https://api.github.com/users/ErikZalm/events{/privacy}",
               |  "received_events_url": "https://api.github.com/users/ErikZalm/received_events",
               |  "type": "User",
               |  "site_admin": false,
               |  "name": "Erik van der Zalm",
               |  "company": "Ultimaker",
               |  "blog": "www.ultimaker.com",
               |  "location": "The Netherlands",
               |  "email": null,
               |  "hireable": null,
               |  "bio": null,
               |  "public_repos": 11,
               |  "public_gists": 0,
               |  "followers": 186,
               |  "following": 0,
               |  "created_at": "2011-06-28T20:31:04Z",
               |  "updated_at": "2016-10-10T20:04:57Z"
               |}
               |""".stripMargin

  test("get user from json")  {
    val exp = User("ErikZalm",882374, Some("Erik van der Zalm"), None,DateTime.parse("2011-06-28T20:31:04Z"))
    GithubExtractor.getUserFromJSON(json) shouldBe Some(exp)
  }

}
