package redundant

import java.io.FileWriter

import dk.itu.gfv.redundant.Backend
import org.scalatest.{Matchers, FunSuite}

/**
  * Created by scas on 25-08-2016.
  */
class SimilarityMainCheckTest extends FunSuite with Matchers{

  test("Similarity check1"){
    val str1 = "    function moveLine(editor, direction) {"
    val str2 = "    function _moveLineBy(instance, from, moveBy) {"

    println("n-levenshtein: " + Backend.checkSimilarityOfStrings(str1,str2, "n-levenshtein"))
    println("ngram: " + Backend.checkSimilarityOfStrings(str1,str2, "ngram"))
    println("cosine: " + Backend.checkSimilarityOfStrings(str1,str2, "cosine"))
    println("jaro-winkler: " + Backend.checkSimilarityOfStrings(str1,str2, "jaro-winkler"))
  }

  test("Similarity check2"){
    val str1 = "ele.val(get(this,'value'));"
    val str2 = "var value = get(this, 'value');"

    println("n-levenshtein: " + Backend.checkSimilarityOfStrings(str1,str2, "n-levenshtein"))
    println("ngram: " + Backend.checkSimilarityOfStrings(str1,str2, "ngram"))
    println("cosine: " + Backend.checkSimilarityOfStrings(str1,str2, "cosine"))
    println("jaro-winkler: " + Backend.checkSimilarityOfStrings(str1,str2, "jaro-winkler"))

  }





  test("Complex check"){
    val newcode = """    var ele = this.$();
                    |    var pos = ele.prop('selectionStart');
                    |    ele.val(get(this,'value'));
                    |    ele.prop({ selectionStart: pos, selectionEnd: pos });""".stripMargin.split("""\r?\n""").toList

    val origcode = """    // We do this check so cursor position doesn't get affected in IE
                     |    var value = get(this, 'value');
                     |    if (value !== this.$().val()) {
                     |      this.$().val(value);
                     |    }""".stripMargin.split("""\r?\n""").toList


    println("Levenshtein     , NLevenshtein      , N-gram       , Cosine        , Jaro-Winkler     , Jaccard ")
    val headers = "new-row;old-row,Levenshtein     , NLevenshtein      , N-gram       , Cosine        , Jaro-Winkler     , Jaccard "
    val str = new StringBuilder()
    str.append(headers + "\n")
    for (i <- 0 to newcode.size-1){
      for (j <- 0 to origcode.size-1){
        val r = (i + "-" + j + " ," + Backend.checkSimilarityOfStrings(newcode(i),origcode(j), "levenshtein") ++ " ," + Backend.checkSimilarityOfStrings(newcode(i),origcode(j), "n-levenshtein") + " ," + Backend.checkSimilarityOfStrings(newcode(i),origcode(j), "ngram")  + " ," + Backend.checkSimilarityOfStrings(newcode(i),origcode(j), "cosine") + " ," + Backend.checkSimilarityOfStrings(newcode(i),origcode(j), "jaro-winkler") + " ," + Backend.checkSimilarityOfStrings(newcode(i),origcode(j), "jaccard"))
        str.append(r + "\n")
        print(r)
        print("\n")

      }
    }
    val fw = new FileWriter("sim.csv")
    fw.write(str.toString())
    fw.close()
//    newcode foreach(x => {
//      origcode.foreach(y => {
//        println("n-levenshtein: " + Backend.checkSimilarityOfStrings(x,y, "n-levenshtein"))
//        println("ngram: " + Backend.checkSimilarityOfStrings(str1,str2, "ngram"))
//        println("cosine: " + Backend.checkSimilarityOfStrings(str1,str2, "cosine"))
//        println("jaro-winkler: " + Backend.checkSimilarityOfStrings(str1,str2, "jaro-winkler"))
//      })
//    })

  }
}
