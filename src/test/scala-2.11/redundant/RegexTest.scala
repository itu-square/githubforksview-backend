//package redundant
//
//import dk.itu.gfv.redundant.model.LineChangeModel
//import LineChangeModel.LineChange
//import dk.itu.gfv.redundant.model.LineChangeModel
//import org.scalatest.{Matchers, FunSuite}
//
///**
//  * Created by scas on 17-09-2016.
//  */
//class RegexTest extends FunSuite with Matchers {
//
//  test("start of comment") {
//    val c0 = """+ /* """
//    val c1 = """+ /* comment"""
//    val c4 = """+// """
//    val c5 = """+* """
//    val c6 = """+ *""" // ActionScript, AutoHotkey, C, C++, C#, D,[13] Go, Java, JavaScript, Objective-C, PHP, PL/I, Rust (can be nested), Scala (can be nested), SASS, SQL, Swift, Visual Prolog, CSS
//    val c7 =
//      """+ #""" // Bourne shell and other UNIX shells, Cobra, Perl, Python, Ruby, Seed7, Windows PowerShell, PHP, R, Make, Maple, Nimrod
//    val c8 =
//      """+ --""" // Euphoria, Haskell, SQL, Ada, AppleScript, Eiffel, Lua, VHDL, SGML
//    val c9 =
//      """+ %""" // Euphoria, Haskell, SQL, Ada, AppleScript, Eiffel, Lua, VHDL, SGML
//    val c10 =
//      """+ ;""" // AutoHotkey, AutoIt, Lisp, Common Lisp, Clojure, Rebol, Scheme, many assemblers
//    val c11 =
//      """+ /**""" //
//
//    LineChangeModel.commentsRegex.reset(c0).matches() shouldBe true
//    LineChangeModel.commentsRegex.reset(c1).matches() shouldBe true
//    LineChangeModel.commentsRegex.reset(c4).matches() shouldBe true
//    LineChangeModel.commentsRegex.reset(c7).matches() shouldBe true
//    LineChangeModel.commentsRegex.reset(c8).matches() shouldBe true
//    LineChangeModel.commentsRegex.reset(c9).matches() shouldBe true
//    LineChangeModel.commentsRegex.reset(c11).matches() shouldBe true
//
//    LineChangeModel.commentsRegex.reset(c5).matches() shouldBe true
//    LineChangeModel.commentsRegex.reset(c6).matches() shouldBe true
//  }
//
//  test("end of comment") {
//    val c2 = """+ comment */  """
//    val c3 = """+ */ """
//    LineChangeModel.commentsRegex.reset(c2).matches() shouldBe true
//    LineChangeModel.commentsRegex.reset(c3).matches() shouldBe true
//  }
//
//  test("imports") {
//    // JAVA / SCALA / SWIFT / D / GO / HASKELL/ Matlab / Python
//    // import
//
//    val java = """+ import java.net"""
//    LineChangeModel.importRegex.reset(java).matches shouldBe true
//
//    // C#
//    val csharp =
//      """+ using System.text"""
//    LineChangeModel.importRegex.reset(csharp).matches shouldBe true
//    // C/C++/ASP
//    val c = "+ #include filename"
//    LineChangeModel.importRegex.reset(c).matches shouldBe true
//    //#include
//    // Obj C,
//    // #import / @import
//    val objc = "+ #import filename"
//    val objc1 = "+ @import filename"
//    LineChangeModel.importRegex.reset(objc).matches shouldBe true
//    LineChangeModel.importRegex.reset(objc1).matches shouldBe true
//
//    // Python class imports
//    // from module import class
//
//    val python = "+ from module import class"
//    LineChangeModel.importRegex.reset(python).matches shouldBe true
//    val others = "+ require "
//    val others1 = "+ use "
//    LineChangeModel.importRegex.reset(others).matches shouldBe true
//    LineChangeModel.importRegex.reset(others1).matches shouldBe true
//
//  }
//
//  test("Random test") {
//
//    val s =
//      """--- a/Frontend/src/main/scala/de/fosd/typechef/ProductGeneration.scala
//        |+++ b/Frontend/src/main/scala/de/fosd/typechef/ProductGeneration.scala
//        |@@ -14,7 +14,7 @@
//        | import collection.mutable.{ListBuffer, HashSet, BitSet}
//        | import io.Source
//        | import java.util.regex.Pattern
//        |-import java.util.ArrayList
//        |+import java.util.{Calendar, ArrayList}
//        | import java.lang.SuppressWarnings
//        | import java.io._
//        | import util.Random
//        |@@ -209,7 +209,7 @@
//        |         /**Starting with no tasks */
//        |         var tasks: List[Task] = List()
//        |
//        |-        val useSerialization = false
//        |+        val useSerialization = true
//        |         if (useSerialization &&
//        |             configSerializationDir.exists() &&
//        |             new File(configSerializationDir, "FeatureHashmap.ser").exists()) {
//        |@@ -251,7 +251,7 @@
//        |         /**Henard CSV configurations */
//        |
//        |         {
//        |-            if (tasks.find(_._1.equals("csv")).isDefined) {
//        |+            if (tasks.find(_._1.equals("henard")).isDefined) {
//        |                 msg = "omitting henard loading, because a serialized version was loaded from serialization"
//        |             } else {
//        |                 var productsDir: File = null
//        |@@ -303,7 +303,7 @@
//        |         /**Coverage Configurations - no Header files*/
//        |
//        |         {
//        |-            if (tasks.find(_._1.equals("coverage")).isDefined) {
//        |+            if (tasks.find(_._1.equals("coverage_noHeader")).isDefined) {
//        |                 msg = "omitting coverage_noHeader generation, because a serialized version was loaded"
//        |             } else {
//        |                 startTime = System.currentTimeMillis()
//        |@@ -322,6 +322,7 @@
//        |             if (tasks.find(_._1.equals("coverage")).isDefined) {
//        |                 msg = "omitting coverage generation, because a serialized version was loaded"
//        |             } else {
//        |+                System.out.println("generating code coverage with header - " + Calendar.getInstance().getTime)
//        |                 startTime = System.currentTimeMillis()
//        |                 val (configs, logmsg) = configurationCoverage(family_ast, fm, features, List(),
//        |                     preferDisabledFeatures = false, includeVariabilityFromHeaderFiles = true)
//        |@@ -371,6 +372,18 @@
//        |                       )
//        |         */
//        |         (log, tasks)
//        |+    }
//        |+
//        |+    private def countNumberOfASTElements(ast: AST): Long = {
//        |+      def countNumberOfASTElementsHelper(a: Any): Long = {
//        |+        a match {
//        |+          case l: List[_] => l.map(countNumberOfASTElementsHelper).sum
//        |+          case _: FeatureExpr => 0
//        |+          case p: Product => 1 + p.productIterator.toList.map(countNumberOfASTElementsHelper).sum
//        |+          case _ => 1
//        |+        }
//        |+      }
//        |+      countNumberOfASTElementsHelper(ast)
//        |     }
//        |
//        |     private def varAwareAnalysisSetup(fm_ts: FeatureModel, ast: AST, opt: FrontendOptions): (TranslationUnit, List[Task], String, String) = {
//        |@@ -486,8 +499,7 @@
//        |         }
//        |
//        |         val fm = fm_ts // I got false positives while using the other fm
//        |-        //val family_ast = prepareAST[TranslationUnit](ast.asInstanceOf[TranslationUnit])
//        |-        val family_ast = ast.asInstanceOf[TranslationUnit]
//        |+        val family_ast = prepareAST[TranslationUnit](ast.asInstanceOf[TranslationUnit])
//        |
//        |         println("starting product checking.")
//        |
//        |@@ -503,44 +515,30 @@
//        |                                 family_ast: TranslationUnit, fm: FeatureModel, ast: AST,
//        |                                 fileID: String, startLog: String = "") {
//        |         val log: String = startLog
//        |+        val checkXTimes = 3
//        |         println("starting product checking.")
//        |
//        |-        // Warmup: we do a complete separate run of all tasks for warmup
//        |-        // typechecking
//        |-        {
//        |-            val tsWarmup = new CTypeSystemFrontend(family_ast, fm)
//        |-            val startTimeWarmup: Long = System.currentTimeMillis()
//        |-            tsWarmup.checkASTSilent
//        |-            println("warmupTime_Family_tc" + ": " + (System.currentTimeMillis() - startTimeWarmup))
//        |-            for ((taskDesc: String, configs: List[SimpleConfiguration]) <- typecheckingTasks) {
//        |-                for (configID: Int <- 0 until configs.size - 1) {
//        |-                    val product: TranslationUnit = ProductDerivation.deriveProd[TranslationUnit](family_ast,
//        |-                        new Configuration(configs(configID).toFeatureExpr, fm))
//        |-                    val ts = new CTypeSystemFrontend(product, FeatureExprFactory.default.featureModelFactory.empty)
//        |-                    val startTime: Long = System.currentTimeMillis()
//        |+      // family base checking
//        |+      println("family-based checking: (" + countNumberOfASTElements(family_ast) + ")")
//        |+      // analysis initialization and warmup
//        |+      val ts = new CTypeSystemFrontend(family_ast, fm)
//        |                     ts.checkASTSilent
//        |-                    println("warmupTime_tc_" + taskDesc + "_" + (configID + 1) + ": " + (System.currentTimeMillis() - startTime))
//        |-                }
//        |-            }
//        |-        }
//        |-      // dataflow analysis
//        |-      {
//        |-        val dfWarmup = new CAnalysisFrontend(family_ast, fm)
//        |-        val startTimeWarmup: Long = System.currentTimeMillis()
//        |-        dfWarmup.checkDataflow()
//        |-        println("warmupTime_Family_df" + ": " + (System.currentTimeMillis() - startTimeWarmup))
//        |-        for ((taskDesc: String, configs: List[SimpleConfiguration]) <- typecheckingTasks) {
//        |-          for (configID: Int <- 0 until configs.size - 1) {
//        |-            val product: TranslationUnit = ProductDerivation.deriveProd[TranslationUnit](family_ast,
//        |-              new Configuration(configs(configID).toFeatureExpr, fm))
//        |-            val df = new CAnalysisFrontend(product, FeatureExprFactory.default.featureModelFactory.empty)
//        |-            val startTime: Long = System.currentTimeMillis()
//        |-            df.checkDataflow()
//        |-            println("warmupTime_df_" + taskDesc + "_" + (configID + 1) + ": " + (System.currentTimeMillis() - startTime))
//        |-          }
//        |-        }
//        |-      }
//        |
//        |+      // measurement
//        |+      val startTime : Long = System.currentTimeMillis()
//        |+      var noErrors: Boolean = false
//        |+      for (_ <- 0 until checkXTimes) noErrors = ts.checkASTSilent
//        |+      val familyTime: Long = (System.currentTimeMillis() - startTime) / checkXTimes
//        |+
//        |+      var timeDfFamily: Long = -1
//        |+      if (noErrors) {
//        |+        // analysis initalization and warmup
//        |+        val df = new CAnalysisFrontend(family_ast, fm)
//        |+        df.checkDataflow()
//        |+        val startTimeDf: Long = System.currentTimeMillis()
//        |+        for (_ <- 0 until checkXTimes) df.checkDataflow()
//        |+        timeDfFamily = (System.currentTimeMillis() - startTimeDf) / checkXTimes
//        |+      }
//        |
//        |       if (typecheckingTasks.size > 0) println("start task - checking (" + (typecheckingTasks.size) + " tasks)")
//        |         // results (taskName, (NumConfigs, errors, timeSum))
//        |@@ -553,20 +551,38 @@
//        |             var dfProductTimes: List[Long] = List()
//        |             for (config <- configs) {
//        |                 current_config += 1
//        |-                println("checking configuration " + current_config + " of " + configs.size + " (" + fileID + " , " + taskDesc + ")")
//        |+
//        |+                // product deriviation
//        |                 val product: TranslationUnit = ProductDerivation.deriveProd[TranslationUnit](family_ast,
//        |                     new Configuration(config.toFeatureExpr, fm))
//        |+                println("checking configuration " + current_config + " of " + configs.size + " (" + fileID + " , " + taskDesc + ")" + "(" + countNumberOfASTElements(product) + ")")
//        |+
//        |+                // analysis initialization
//        |                 val ts = new CTypeSystemFrontend(product, FeatureExprFactory.default.featureModelFactory.empty)
//        |+
//        |+                // warmup
//        |+                ts.checkASTSilent
//        |+
//        |+                // measurment
//        |                 val startTime: Long = System.currentTimeMillis()
//        |-                val noErrors: Boolean = ts.checkAST
//        |-                val productTime: Long = System.currentTimeMillis() - startTime
//        |+                var noErrors: Boolean = false
//        |+                for (_ <- 0 until checkXTimes) noErrors = ts.checkASTSilent
//        |+                val productTime: Long = (System.currentTimeMillis() - startTime) / checkXTimes
//        |+
//        |                 tcProductTimes ::= productTime // append to the beginning of tcProductTimes
//        |                 if (noErrors) {
//        |+                  // analysis initialization
//        |                   val df = new CAnalysisFrontend(product, FeatureExprFactory.empty)
//        |-                  val startTimeDataFlowProduct: Long = System.currentTimeMillis()
//        |+
//        |+                  // warmup
//        |                   df.checkDataflow()
//        |-                  val timeDataFlowProduct = System.currentTimeMillis() - startTimeDataFlowProduct
//        |-                  dfProductTimes ::= timeDataFlowProduct
//        |+
//        |+                  // measurement
//        |+                  val startTimeDataFlowProduct: Long = System.currentTimeMillis()
//        |+                  for (_ <- 0 until checkXTimes) df.checkDataflow()
//        |+                  val timeDataFlowProduct = (System.currentTimeMillis() - startTimeDataFlowProduct) / checkXTimes
//        |+
//        |+                  dfProductTimes ::= timeDataFlowProduct // add to the head - reverse later
//        |                 } else {
//        |                   dfProductTimes ::= -1 // we add -1 to mark that we did not checkDataflow for a product with type errors
//        |                     var fw: FileWriter = null
//        |@@ -598,20 +614,6 @@
//        |             }
//        |             // reverse tcProductTimes to get the ordering correct
//        |             configCheckingResults ::=(taskDesc, (configs.size, configurationsWithErrors, dfProductTimes.reverse, tcProductTimes.reverse))
//        |-        }
//        |-        // family base checking
//        |-        println("family-based checking:")
//        |-        val ts = new CTypeSystemFrontend(family_ast, fm)
//        |-        val startTime : Long = System.currentTimeMillis()
//        |-        val noErrors: Boolean = ts.checkAST
//        |-        val familyTime: Long = System.currentTimeMillis() - startTime
//        |-
//        |-        var timeDfFamily: Long = -1
//        |-        if (noErrors) {
//        |-          val df = new CAnalysisFrontend(family_ast, fm)
//        |-          val startTimeDf: Long = System.currentTimeMillis()
//        |-          df.checkDataflow()
//        |-          timeDfFamily = System.currentTimeMillis() - startTimeDf
//        |         }
//        |
//        |         val file: File = new File(outFilePrefix + "_report.txt")
//        |""".stripMargin
//
//      val hunkLines = s.split("\r?\n").toList.drop(2)
//      val hunkInfo = hunkLines.head
//      import dk.itu.gfv.redundant.model.LineChangeModel._
//      val rangeHunk = LineChangeModel.stringToRangeHunk(hunkInfo)
//      val hunkText = hunkLines.tail.filter(h => h.startsWith("+") && !h.startsWith("++") && !h.matches("""\+\s*""")).filterNot(x => LineChangeModel.commentsRegex.reset(x).matches || importRegex.reset(x).matches || bracketsKeyword.reset(x).matches || returnKeyword.reset(x).matches || semicolon.reset(x).matches)
//
//    println(hunkText.map(x => LineChange(Commit("","","",0), rangeHunk, x.substring(1).trim, "")))
//
//  }
//
//}
