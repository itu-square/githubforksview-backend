package redundant

import dk.itu.gfv.redundant.functionparsers.JavaParser.{FunctionDefJava, GenParameter}
import dk.itu.gfv.redundant.functionparsers.ParseLine
import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 02-12-2016.
  */
class FunctionParserJavaTest extends FunSuite with Matchers{


  test("Parse Java functions "){

    val f1 = """if (msg instanceof TextMessage) {""" // if block
    val f2 = """public String encrypt(Message msg) {"""   ;// function def
    val f3 = """blogin.addActionListener(new ActionListener() {""" // function call
    val f4 = """public Log(String content, String filepath) {""" // constructor
    val f5 = """public TextMessage(String content) {""" // constructor
    val f6 = """public  LoginAuth() {""" // constructor
    val f7 = """addWindowListener(new WindowAdapter() {""" // method
    val f8 = """for (File f : plugins) {""" // for loop

    ParseLine.parseLine(f1,"java") shouldBe None
    ParseLine.parseLine(f3,"java") shouldBe None
    ParseLine.parseLine(f2,"java") shouldBe Some(FunctionDefJava(Some("public"), Some("String"), "encrypt", List(GenParameter("msg", Some("Message")))))
    ParseLine.parseLine(f4,"java") shouldBe None
    ParseLine.parseLine(f5,"java") shouldBe None
    ParseLine.parseLine(f6,"java") shouldBe None
    ParseLine.parseLine(f7,"java") shouldBe None
    ParseLine.parseLine(f8,"java") shouldBe None

  }

}
