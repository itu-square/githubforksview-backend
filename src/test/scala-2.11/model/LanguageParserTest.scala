package model

import dk.itu.gfv.redundant.functionparsers
import dk.itu.gfv.redundant.functionparsers._
import PHPParser.PHPParameter
import ScalaParser.{FunctionDefScala,parseScalaCode}
import JavaParser.{FunctionDefJava,parseJavaCode}
import JavascriptParser.{FunctionDefJavascript, parseJavascriptCode}
import org.scalatest.{FunSuite, Matchers}


/**
  * Created by scas on 9/6/2016.
  */
class LanguageParserTest extends FunSuite with Matchers with  GeneralLanguageAST{

  // SCALA
  test("Scala different function definitions") {
    import ScalaParser.GenParameter
    parseScalaCode("override def run(){") shouldBe Some(FunctionDefScala(Some("override"), "", None, "run", List()))
    parseScalaCode("def construct(args: Array[Object]): Unit = {") shouldBe Some(FunctionDefScala(None, "", Some("Unit"), "construct", List(functionparsers.ScalaParser.GenParameter("args", (Some("Array[Object]"))))))
    parseScalaCode("def construct(args: Array[Object]) = {") shouldBe Some(FunctionDefScala(None, "", None, "construct", List(GenParameter("args", Some("Array[Object]")))))
    parseScalaCode("def construct(args: Array[Object]){") shouldBe Some(FunctionDefScala(None, "", None, "construct", List(GenParameter("args", Some("Array[Object]")))))
    parseScalaCode("def construct(args: Array[Object])") shouldBe Some(FunctionDefScala(None, "", None, "construct", List(GenParameter("args", Some("Array[Object]")))))
    parseScalaCode("private def construct(args: Array[Object])") shouldBe Some(FunctionDefScala(Some("private"), "", None, "construct", List(GenParameter("args", Some("Array[Object]")))))
    parseScalaCode("private def construct(args: Array[Object], implicit newParam: String)") shouldBe Some(FunctionDefScala(Some("private"), "", None, "construct", List(GenParameter("args", Some("Array[Object]")), GenParameter("implicit newParam", Some("String")))))
    parseScalaCode("private def construct(args: Array[Object], newParam: Boolean = false)") shouldBe Some(FunctionDefScala(Some("private"), "", None, "construct", List(GenParameter("args", Some("Array[Object]")), GenParameter("newParam", Some("Boolean = false")))))
    parseScalaCode("def construct: Boolean = test") shouldBe Some(FunctionDefScala(None, "", Some("Boolean"), "construct", Nil))
    parseScalaCode("def construct test") shouldBe None
  }


  // JAVA
  test("Java parser test function definitions"){
    import JavaParser.GenParameter
    parseJavaCode("public double computePayment(){") shouldBe Some(FunctionDefJava(Some("public"),Some("double"),"computePayment",List()))
    parseJavaCode("public static void combination(int a)") shouldBe Some(FunctionDefJava(Some("public"),Some("void"),"combination",List(GenParameter("a",Some("int")))))
    parseJavaCode("public CallAdapter<?> get(Annotation[] annotations, int a){") shouldBe  Some(FunctionDefJava(Some("public"),Some("CallAdapter<?>"),"get",List(GenParameter("annotations",Some("Annotation[]")), GenParameter("a",Some("int")))))
    parseJavaCode("public static void print(int[] combination, Object[] elements)") shouldBe     Some(FunctionDefJava(Some("public"),Some("void"),"print",List(GenParameter("combination",Some("int[]")), GenParameter("elements",Some("Object[]")))))
    parseJavaCode("public CallAdapter<?> get(int a)") shouldBe     Some(FunctionDefJava(Some("public"),Some("CallAdapter<?>"),"get",List(GenParameter("a",Some("int")))))

    parseJavaCode("return x/y;") shouldBe None
  }

  test("Javascript function parser"){
    import JavascriptParser.GenParameter
    val def1 = "function test(){"
    val def2 = "function test(self){"
    val def3 = "function test(a,b)"
    val def4 = "function test(a,b,c){"

    parseJavascriptCode(def1) shouldBe Some(FunctionDefJavascript("test", List()))
    parseJavascriptCode(def2) shouldBe Some(FunctionDefJavascript("test", List(GenParameter("self", None))))
    parseJavascriptCode(def3) shouldBe Some(FunctionDefJavascript("test", List(GenParameter("a", None),GenParameter("b", None))))
    parseJavascriptCode(def4) shouldBe Some(FunctionDefJavascript("test", List(GenParameter("a", None),GenParameter("b", None),GenParameter("c", None))))
  }

  test("PHP function parser"){
    import PHPParser.GenParameter
    import PHPParser.FunctionDefPhP
    import PHPParser.parsePhPCode
    val def1 = "function test(){"
    val def2 = "function test($self){"
    val def3 = "function test($a,$b)"
    val def4 = """function test($a,$b,$c="test",$d){"""

    parsePhPCode(def1) shouldBe Some(FunctionDefPhP("test", List()))
    parsePhPCode(def2) shouldBe Some(FunctionDefPhP("test", List(GenParameter("$self", None))))
    parsePhPCode(def3) shouldBe Some(FunctionDefPhP("test", List(GenParameter("$a", None), GenParameter("$b", None))))
    parsePhPCode(def4) shouldBe Some(FunctionDefPhP("test", List(GenParameter("$a", None), GenParameter("$b", None),PHPParameter("$c", None, Some(""""test"""")),GenParameter("$d", None))))
  }

  test("Python function parser"){
    import PythonParser._
    val def1 = "def test():"
    val def2 = "def test(self):"
    val def3 = "def test(a: int):"
    val def4 = "def test:"

    parsePythonCode(def1) shouldBe Some(FunctionDefPython("test", List()))
    parsePythonCode(def2) shouldBe Some(FunctionDefPython("test", List(GenParameter("self", None))))
    parsePythonCode(def3) shouldBe Some(FunctionDefPython("test", List(GenParameter("a", Some("int")))))
    parsePythonCode(def4) shouldBe Some(FunctionDefPython("test", List()))
  }


}
