package model

import java.util.regex.Pattern

import dk.itu.gfv.redundant.model.LineChangeModel
import LineChangeModel.RangeHunk
import LineChangeModel.stringToRangeHunk
import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 18-09-2016.
  */
class HunkTest extends FunSuite with Matchers{



  test("Range hunk"){
    val hunkRange = "@@ -1,1 +2,2 @@ public void"
    val hunkRange1 = "@@ -1 +1,2 @@ public void "
    val hunkRange2 = "@@ -1 +1 @@ public void "
    val hunkRange3 = "@@ -1,1 +1 @@ public void "

    stringToRangeHunk(hunkRange) shouldBe RangeHunk(1,1,2,2)
    stringToRangeHunk(hunkRange1) shouldBe RangeHunk(1,1,1,2)
    stringToRangeHunk(hunkRange2) shouldBe RangeHunk(1,1,1,1)
    stringToRangeHunk(hunkRange3) shouldBe RangeHunk(1,1,1,1)

  }

  test("Determing if change was in the range hunk "){
    val rangeHunk = RangeHunk(12,8,12,10)

    def proximityChange( lineNumber: Int, rangeHunk: RangeHunk): Boolean = {
      lineNumber >= rangeHunk.start && lineNumber <= rangeHunk.end + rangeHunk.linesAfterChange
    }
    proximityChange(15,rangeHunk) shouldBe true
    proximityChange(18,rangeHunk) shouldBe true
    proximityChange(11,rangeHunk) shouldBe false
  }

}
