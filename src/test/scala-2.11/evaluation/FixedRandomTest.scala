package evaluation

import dk.itu.gfv.evaluation.setup.FixedRandomCommits._
import dk.itu.gfv.evaluation.setup.{FixedRandomCommits, RegexPatternsNormalization}
import dk.itu.gfv.redundant.TokenFrequency.LineFrequency
import dk.itu.gfv.repo.PatchParser.{BinaryFile, DiffFile}
import dk.itu.gfv.repo.PatchParser
import dk.itu.gfv.repo.RepositoryOperations.AddedLine2
import dk.itu.gfv.util.Util._
import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 18-11-2016.
  */
class FixedRandomTest extends FunSuite with Matchers with RegexPatternsNormalization {

  test("Added lines 1") {
    val test =
      """diff --git a/runtime/lib_net.c b/runtime/lib_net.c
        |new file mode 100644
        |index 0000000..7dfb7da
        |--- /dev/null
        |+++ b/runtime/lib_net.c
        |@@ -0,0 +1,7 @@
        |+test
        |+
        |+test
        |+
        |+
        |+test
        |+
        | """.stripMargin

    val expected = Vector(
      AddedLine2("", "runtime/lib_net.c", 1, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 2, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 3, "test", "test"))

    val patchFiles = PatchParser.splitPatchByFiles(test).getOrElse(List()).flatMap {
      case _: BinaryFile => None
      case diff: DiffFile => Some(diff)
    }
    val r = FixedRandomCommits.patchToAddedLines(patchFiles, "").get
    r shouldBe expected
  }

  test("Added lines for mixed patch") {
    val test =
      """diff --git a/runtime/lib_net.c b/runtime/lib_net.c
        |new file mode 100644
        |index 0000000..7dfb7da
        |--- /dev/null
        |+++ b/runtime/lib_net.c
        |@@ -0,0 +1,7 @@
        |+test
        |+
        |-test
        |+test1
        |-
        |-
        |+test
        |+
        | """.stripMargin

    val expected = Vector(
      AddedLine2("", "runtime/lib_net.c", 1, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 2, "test1", "test1"),
      AddedLine2("", "runtime/lib_net.c", 3, "test", "test"))

    val patchFiles = PatchParser.splitPatchByFiles(test).getOrElse(List()).flatMap {
      case _: BinaryFile => None
      case diff: DiffFile => Some(diff)
    }
    val r = FixedRandomCommits.patchToAddedLines(patchFiles, "").get
    r shouldBe expected
  }

  test("Added lines for mixed patch and removed empty lines") {
    val test =
      """diff --git a/runtime/lib_net.c b/runtime/lib_net.c
        |new file mode 100644
        |index 0000000..7dfb7da
        |--- /dev/null
        |+++ b/runtime/lib_net.c
        |@@ -0,0 +1,7 @@
        |+test
        |+
        |-test
        |+test1
        |-
        |-
        |+test
        |+
        |+
        |test
        |+test
        | """.stripMargin

    val expected = Vector(
      AddedLine2("", "runtime/lib_net.c", 1, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 2, "test1", "test1"),
      AddedLine2("", "runtime/lib_net.c", 3, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 5, "test", "test"))


    val patchFiles = PatchParser.splitPatchByFiles(test).getOrElse(List()).flatMap {
      case _: BinaryFile => None
      case diff: DiffFile => Some(diff)
    }
    val r = FixedRandomCommits.patchToAddedLines(patchFiles, "").get

    r shouldBe expected
//    val nokewyords = FixedRandomCommits.removeKeywordsAndEmptyLinesTailRec(r)
//    nokewyords shouldBe expected
  }

  test("Added lines for mixed patch and removed empty lines, keywords, brackets") {
    val test =
      """diff --git a/runtime/lib_net.c b/runtime/lib_net.c
        |new file mode 100644
        |index 0000000..7dfb7da
        |--- /dev/null
        |+++ b/runtime/lib_net.c
        |@@ -0,0 +1,7 @@
        |+test
        |+break
        |-test
        |+test1
        |-
        |-
        |+test
        |+}
        |+else
        |test
        |+test
        | """.stripMargin

    val expected = Vector(
      AddedLine2("", "runtime/lib_net.c", 1, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 2, "break", "break"),
      AddedLine2("", "runtime/lib_net.c", 3, "test1", "test1"),
      AddedLine2("", "runtime/lib_net.c", 4, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 5, "}", "}"),
      AddedLine2("", "runtime/lib_net.c", 6, "else", "else"),
      AddedLine2("", "runtime/lib_net.c", 8, "test", "test"))

    val patchFiles = PatchParser.splitPatchByFiles(test).getOrElse(List()).flatMap {
      case _: BinaryFile => None
      case diff: DiffFile => Some(diff)
    }

    val r = FixedRandomCommits.patchToAddedLines(patchFiles, "").get

    r shouldBe expected
//    val nokewyordsrec = FixedRandomCommits.removeKeywordsAndEmptyLinesTailRec(r)
    val expected1 = Vector(
      AddedLine2("", "runtime/lib_net.c", 1, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 2, "test1", "test1"),
      AddedLine2("", "runtime/lib_net.c", 3, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 5, "test", "test"))

//    nokewyordsrec shouldBe expected1

  }

  test("Added lines for mixed patch and removed empty lines, keywords, brackets using tailrec") {
    val test =
      """diff --git a/runtime/lib_net.c b/runtime/lib_net.c
        |new file mode 100644
        |index 0000000..7dfb7da
        |--- /dev/null
        |+++ b/runtime/lib_net.c
        |@@ -0,0 +1,7 @@
        |+test
        |+break
        |-test
        |+test1
        |-
        |-
        |+test
        |+}
        |+else
        |test
        |+test
        | """.stripMargin

    val expected = Vector(
      AddedLine2("", "runtime/lib_net.c", 1, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 2, "break", "break"),
      AddedLine2("", "runtime/lib_net.c", 3, "test1", "test1"),
      AddedLine2("", "runtime/lib_net.c", 4, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 5, "}", "}"),
      AddedLine2("", "runtime/lib_net.c", 6, "else", "else"),
      AddedLine2("", "runtime/lib_net.c", 8, "test", "test"))

    val patchFiles = PatchParser.splitPatchByFiles(test).getOrElse(List()).flatMap {
      case _: BinaryFile => None
      case diff: DiffFile => Some(diff)
    }

    val r = FixedRandomCommits.patchToAddedLines(patchFiles, "").get

    r shouldBe expected
//    val nokewyords = FixedRandomCommits.removeKeywordsAndEmptyLinesTailRec(r)
    val expected1 = Vector(
      AddedLine2("", "runtime/lib_net.c", 1, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 2, "test1", "test1"),
      AddedLine2("", "runtime/lib_net.c", 3, "test", "test"),
      AddedLine2("", "runtime/lib_net.c", 5, "test", "test"))

//    println(nokewyords)
//    nokewyords shouldBe expected1

  }

  test("Remove common lines") {
    val string = "public class TextMessage implements Serializable, Message {"
    val frequentLinesInBaseCode = readFromFileLines(repodir + "chatprojects/17708-gabrielcsf/" + linefrequency).toVector.map(x => {
      val lastComma = x.lastIndexOf(',')
      val str = x.substring(0, lastComma)
      val nrOfAppearances = Integer.parseInt(x.substring(lastComma + 1, x.size))
      LineFrequency(str, nrOfAppearances)
    })
    val frequentlines  = frequentLinesInBaseCode.filter(_.numberOfAppearances >= 15).map(_.line).toList
    println(frequentlines)
    frequentlines.size shouldBe 13
    frequentlines.intersect(List(string)) shouldBe List()
    frequentlines.intersect(List("package edu.cmu.cs.server;")).nonEmpty shouldBe true

  }

}
