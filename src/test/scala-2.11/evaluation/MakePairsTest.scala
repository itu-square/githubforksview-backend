package evaluation

import dk.itu.gfv.repo.RepositoryOperations.AddedLine2
import org.scalatest.{FunSuite, Matchers}
import dk.itu.gfv.util.Util.time
/**
  * Created by scas on 21-11-2016.
  */
class MakePairsTest extends FunSuite with Matchers {


  val file1 = List(
    AddedLine2("aaa", "file1", 1,"test", "test"),
    AddedLine2("aaa", "file1", 2,"test", "test"),
    AddedLine2("aaa", "file1", 3,"test", "test"),
    AddedLine2("aaa", "file1", 4,"test", "test"),
    AddedLine2("aaa", "file1", 5,"test", "test"),
    AddedLine2("aaa", "file1", 8,"test", "test"),
    AddedLine2("aaa", "file1", 9,"test", "test"),
    AddedLine2("aaa", "file1", 10,"test", "test"))

  val file2 = List(
    AddedLine2("aaa", "file2", 1,"file2-test", "file2-test"),
    AddedLine2("aaa", "file2", 2,"file2-test", "file2-test"),
    AddedLine2("aaa", "file2", 3,"file2-test", "file2-test"),
    AddedLine2("aaa", "file2", 4,"file2-test", "file2-test"),
    AddedLine2("aaa", "file2", 5,"file2-test", "file2-test"),
    AddedLine2("aaa", "file2", 7,"file2-test", "file2-test"),
    AddedLine2("aaa", "file2", 8,"file2-test", "file2-test"),
    AddedLine2("aaa", "file2", 9,"file2-test", "file2-test"),
    AddedLine2("aaa", "file2", 10,"file2-test", "file2-test"))

  val file3 = List(
    AddedLine2("bbb", "file1", 5,"test", "test"),
    AddedLine2("bbb", "file1", 6,"test", "test"),
    AddedLine2("bbb", "file1", 7,"test", "test"),
    AddedLine2("bbb", "file1", 11,"test", "test"),
    AddedLine2("bbb", "file1", 12,"test", "test"),
    AddedLine2("bbb", "file1", 15,"test", "test"),
    AddedLine2("bbb", "file1", 16,"test", "test"),
    AddedLine2("bbb", "file1", 17,"test", "test"))


  val addedLines = file1 ++ file2 ++ file3


  def makePairs(input: List[AddedLine2], pairSize: Int) = {

    val sameSHA = input.groupBy(_.sha)
    val sameFilesInSHA = sameSHA.map(x => x._1 -> x._2.groupBy(_.file))

    def keepOnlyConsecutiveLines(input: List[List[AddedLine2]]): List[List[AddedLine2]] = {

      input match {
        case h :: t if input.size > 1 => {
          var s = true
          var p = h.head.position
          h.takeRight(pairSize - 1).foreach(a => {
            if (p + 1 != a.position)
              s = false
            p = a.position
          })
          if (s)
            h :: keepOnlyConsecutiveLines(t)
          else
            keepOnlyConsecutiveLines(t)
        }
        case List(h) => {
          var s = true
          var p = h.head.position
          h.takeRight(pairSize - 1).foreach(a => {
            if (p + 1 != a.position)
              s = false
            p = a.position
          })
          if (s)
            List(h)
          else
            List()
        }
      }
    }

    val pairs = (sameFilesInSHA).par.map(x => x._1 -> x._2.map(y => y._1 -> keepOnlyConsecutiveLines(y._2.sliding(pairSize).toList).flatten))
//    pairs.values.
  }

  test("Test1") {
    time("make pairs", makePairs(addedLines,2))
  }
}
