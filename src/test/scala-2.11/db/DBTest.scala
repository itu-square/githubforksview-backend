package db

import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 10-01-2017.
  */
class DBTest extends FunSuite with Matchers{

  case class AddedLine(id: Long, filename: String, line_number: Int, text: String, text_n: String)

  test("test insertion of 8 million rows"){
    import java.sql.{Connection,DriverManager}

      // connect to the database named "mysql" on port 8889 of localhost
      val url = "jdbc:mysql://localhost:3306/scala_test?rewriteBatchedStatements=true"
      val driver = "com.mysql.jdbc.Driver"
      val username = "stefan"
      val password = "stefan"

      try {
        Class.forName(driver)
        val connection = DriverManager.getConnection(url, username, password)
        connection.setAutoCommit(false)

        val statement = connection.createStatement
        statement.execute("""DROP TABLE `scala_test`.`added_lines`;""")
        val table = """CREATE TABLE `added_lines` (
                      |  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                      |  `filename` text NOT NULL,
                      |  `line_number` int(11) NOT NULL,
                      |  `text` longtext NOT NULL,
                      |  `text_normalized` longtext NOT NULL,
                      |  PRIMARY KEY (`id`)
                      |)ENGINE=MyISAM AUTO_INCREMENT=187722 DEFAULT CHARSET=utf8;""".stripMargin
        statement.execute(table)

        val data = Range.apply(1,8000000,1).map(x => AddedLine(x,"test"+x,0,"test_string", "text_n"))

        val sql_insert =
          """INSERT INTO added_lines
            |(id, filename,line_number,text,text_normalized) VALUES (?,?,?,?,?)""".stripMargin
        val st = connection.prepareStatement(sql_insert);
        var b = 0
        var x = 1
        data.map{ d => {
          st.setLong(1,d.id)
          st.setString(2,d.filename)
          st.setInt(3,d.line_number)
          st.setString(4,d.text)
          st.setString(5,d.text_n)
          st.addBatch()
          b = b+1
          if(b == 20000){
            println(b * x)
            st.executeBatch()
            st.clearBatch()
            b = 0
          }
        }
        }

        val sb = new StringBuilder
        sb.append("INSERT INTO added_lines (id, filename,line_number,text,text_normalized) VALUES ")
//


//        for (d <-data){
//          st.setLong(1,d.id)
//          st.setString(2,d.filename)
//          st.setInt(3,d.line_number)
//          st.setString(4,d.text)
//          st.setString(5,d.text_n)
//          st.addBatch()
//          b = b+1
//          if(b == 10000){
//            println(b * x)
//             x = x + 1
//            st.executeBatch()
//            st.clearBatch()
//            b = 0
//          }
//        }
//        if (b>0){
//          st.executeBatch
//        }
        connection.commit()
//        val rs = statement.executeQuery("SELECT host, user FROM user")
//        while (rs.next) {
//          val host = rs.getString("host")
//          val user = rs.getString("user")
//          println("host = %s, user = %s".format(host,user))
//        }
        connection.close
      } catch {
        case e: Exception => e.printStackTrace
      }

    }
}
