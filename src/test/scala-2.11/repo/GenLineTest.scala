package repo

import java.io.File

import dk.itu.gfv.repo.GithubExtractor._
import dk.itu.gfv.repo.{RepositoryOperations, AnalyzeFile}
import dk.itu.gfv.util.ProjectsPaths
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.scalatest.{Matchers, FunSuite}


/**
  * Created by scas on 11-07-2016.
  */
class GenLineTest extends FunSuite with Matchers with ProjectsPaths {


  test("Simple test") {
    val file = "test.c"
    val branch = "master"
    val repositoryPath = repodir + "/" + "scas-mdd" + "-" + "fork-test"
    val git = new Git(new FileRepository(repositoryPath + "/.git"))
    val lineTable = AnalyzeFile.genLineTable(git, file, "master",  "scas-mdd/fork-test/master")
//    println(lineTable.toColouredSplitHTMLTable())

  }


  test("Simple typechef test") {
    val file = "PartialPreprocessor/src/main/scala/de/fosd/typechef/lexer/Preprocessor.java"
    val username = "ckaestne"
    val reponame = "TypeChef"
    val branch = "master"
    val repositoryPath = repodir + "/" + username + "-" + reponame
    val git = new Git(new FileRepository(repositoryPath + "/.git"))
    val lineTable = AnalyzeFile.genLineTable(git, file, branch,  username + "/" + reponame + "/" + branch)
    println(lineTable)
    println(lineTable.toIfdef())

  }
  }
