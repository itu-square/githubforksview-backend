import dk.itu.gfv.repo.GithubExtractor
import dk.itu.gfv.repo.GithubExtractor._
import dk.itu.gfv.model.Model.RepoMain
import dk.itu.gfv.repo.{AnalyzeFile, RepositoryOperations}
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.ListBranchCommand.ListMode
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.scalatest.{Matchers, FunSuite}
import collection.JavaConverters._
import argonaut._
import Argonaut._

/**
  * Created by scas on 13-07-2016.
  */
class MainTest extends FunSuite with Matchers {


  val url = "https://github.com/scas-mdd/fork-test/blob/master/test.c"
  val githubURL = GithubExtractor.transformURL(url).get
  val decodedForks = getJSONPagination(githubURL.username + "/" + githubURL.reponame, "forks").decodeOption[List[RepoMain]].get
  val git = new Git(new FileRepository("repositories/" + githubURL.username + "-" + githubURL.reponame + "/.git"))
  RepositoryOperations.cloneRepository(githubURL.username + "/" + githubURL.reponame, githubURL.branch)
  RepositoryOperations.addForksAsRemotes(git, decodedForks)
//  git.remoteList().call.asScala.foreach(x => println(x.getName))
  RepositoryOperations.fetchForksRemotes(git, decodedForks)
//  git.branchList().setListMode(ListMode.ALL).call.asScala.foreach(x => println(x.getName))
  val lineTable = AnalyzeFile.genLineTable(git, githubURL.filename, githubURL.branch, githubURL.username + "/" + githubURL.reponame + "/" + githubURL.branch)
  println(lineTable.toIfdef())


  test("Test file with blob stuff"){

  }
}
