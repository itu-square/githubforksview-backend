package repo

import dk.itu.gfv.monitor.db.Models.AddedLine
import dk.itu.gfv.redundant.model.LineChangeModel.{RangeHunk, stringToRangeHunk1}
import org.scalatest.{FlatSpec, Matchers}

import scala.io.Source.fromURL
import dk.itu.gfv.repo.RepositoryOperations.patchToAddedLines
import org.eclipse.jgit.api.Git

/**
  * Created by scas on 29-05-2017.
  */
class RepositoryOperationsTest extends FlatSpec with Matchers {

  it should "parse" in {
    val git = Git.open(new java.io.File("test-files/dhis2-core/.git"))
    val patch = patchToAddedLines(git, "31898dc")
    patch.get.foreach(println)
  }
  it should "return a start" in {
    val hunk = "@@ -29,43 +29,51 @@"
    stringToRangeHunk1(hunk) shouldBe RangeHunk(29, 43, 29, 51)

  }

  it should "" in {
    val hunk = "@@ -0,0 +1 @@"
  }

  it should " correctly parse the files " in {
    val git = Git.open(new java.io.File("test-files/dhis2-core/.git"))
    val patch = patchToAddedLines(git, "abf51a249289d1a9985fd5de7201f2ede534ad35")
    patch.get === Vector(
      AddedLine(0, 0, "abf51a249289d1a9985fd5de7201f2ede534ad35", "dhis-2/dhis-services/dhis-service-dxf2/src/main/java/org/hisp/dhis/dxf2/metadata/DefaultMetadataExportService.java",
        327, "if ( Dashboard.class.isInstance( object ) ) return handleDashboard( metadata, (Dashboard) object );", "if ( Dashboard.class.isInstance( object ) ) return handleDashboard( metadata, (Dashboard) object );"
      ),
      AddedLine(0, 0, "abf51a249289d1a9985fd5de7201f2ede534ad35", "dhis-2/dhis-services/dhis-service-dxf2/src/main/java/org/hisp/dhis/dxf2/metadata/DefaultMetadataExportService.java",
        328, "if ( DataElementGroup.class.isInstance( object ) ) return handleDataElementGroup( metadata, (DataElementGroup) object );", "if ( DataElementGroup.class.isInstance( object ) ) return handleDataElementGroup( metadata, (DataElementGroup) object );"
      ),
      AddedLine(0, 0, "abf51a249289d1a9985fd5de7201f2ede534ad35", "dhis-2/dhis-services/dhis-service-dxf2/src/main/java/org/hisp/dhis/dxf2/metadata/DefaultMetadataExportService.java",
        756, "handleLegendSet( metadata, dataElementGroup.getLegendSets() );", "handleLegendSet( metadata, dataElementGroup.getLegendSets() );"
      )
    )
  }
}
