package jgit

import dk.itu.gfv.model.Model.{Branch, CommitGithub}
import dk.itu.gfv.repo.GithubExtractor._
import dk.itu.gfv.repo.RepositoryOperations.{cloneRepository, createBranchAndPush, pushAllChanges}
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.scalatest._

import scala.collection.JavaConverters._
import scala.sys.process._
/**
  * Created by scas on 24-05-2017.
  */
class JGitTest extends FunSpec with Matchers {
  // before -> setup
  val reponame = "scala-app-repo"
  val repoowner = "proof-ro"
  val repo = repoowner + "/" + reponame
  val testDirectory = "tests-data"
  val authToken = better.files.File("github.txt").lines.map(_.split(";")(0)).last


  class AddCommit extends FlatSpec {
    it should "create a commit in the git repository" in {
      val helloFile = new java.io.File(testDirectory + "/" + repo, "hello.txt")
      helloFile.createNewFile
      val fw = new java.io.FileWriter(helloFile)
      fw.write("hello world")
      fw.flush
      fw.close
      val git = Git.open(new java.io.File(s"tests-data/$repo/.git"))
      git.add.addFilepattern("hello.txt").call
      // Now, we do the commit with a message
      val rev = git.commit.setAuthor("scas", "scas@scas-mdd.com").setMessage("My first commit").call
      val logCommits = git.log.call().asScala
      git.getRepository.close()
      git.close
      assert(logCommits.size === 2)
    }
  }

  class PushCommit extends FlatSpec {
    it should "push the commit to github" in {
      val git = Git.open(new java.io.File(s"tests-data/$repo/.git"))
      git.push.setPushAll().setCredentialsProvider(new UsernamePasswordCredentialsProvider("${token}", authToken)).call()
      git.getRepository.close()
      git.close
      //      git.close
      val commits = getCommits(repo, "master")
      commits shouldBe a[Some[_]] // Seq[CommitGithub]
      commits.get.size shouldEqual 2
      commits.get.head.commit.message shouldEqual "My first commit"
      commits.get.head.commit.author.name shouldEqual "scas"
      commits.get.head.commit.author.email shouldEqual "scas@scas-mdd.com"
    }
  }

  class CreateBranchAndPush extends FlatSpec {

    it should "create a branch and push it on github" in {
      val git = Git.open(new java.io.File(s"tests-data/$repo/.git"))
      createBranchAndPush(git, "patch-1", authToken)
      val branchesGithub = getRepositoryBranches(repo)
      val branches = git.branchList().call().asScala.map(_.getName.replaceAll("refs/heads/", ""))
      git.getRepository.close
      git.close
      branchesGithub shouldBe a[Some[Seq[Branch]]]
      branchesGithub.get.map(_.name).toSet.diff(branches.toSet).size shouldEqual 0
    }
  }

  class PushCommitsOnNewBranch extends FlatSpec {

    it should "push the commit on the new branch patch-1" in {
      val git = Git.open(new java.io.File(s"tests-data/$repo/.git"))
      git.checkout().addPath("patch-1").call()
      val helloFile = new java.io.File(testDirectory + "/" + repo, "hello.txt")
      val fw = new java.io.FileWriter(helloFile)

      fw.append("testing the implementation")
      fw.flush
      fw.close
      val rev = git.commit.setAuthor("scas", "scas@scas-mdd.com").setMessage("New branch for a fix").call
      val logCommits = git.log.call().asScala.toSeq

      logCommits shouldBe a[Seq[RevCommit]]
      logCommits.size shouldEqual 3
      logCommits.head.getFullMessage shouldEqual "New branch for a fix"
      logCommits.head.getAuthorIdent.getName shouldEqual "scas"
      logCommits.head.getAuthorIdent.getEmailAddress shouldEqual "scas@scas-mdd.com"

      pushAllChanges(git, authToken)
      git.getRepository.close
      git.close
      val commits = getCommits(repo, "patch-1")
      commits shouldBe a[Some[Seq[CommitGithub]]]
      commits.get.size shouldEqual 3
      commits.get.head.commit.message shouldEqual "New branch for a fix"
      commits.get.head.commit.author.name shouldEqual "scas"
      commits.get.head.commit.author.email shouldEqual "scas@scas-mdd.com"
    }
  }

  class DeleteBranchAndPush extends FlatSpec {

  }

  class TestSuite extends Suites(
    new AddCommit(),
    new PushCommit(),
    new CreateBranchAndPush(),
    new PushCommitsOnNewBranch()
  ) with BeforeAndAfterAll {

    override def beforeAll() = {
      val t = "C:\\Program Files\\Git\\bin\\bash.exe removedir.sh".!!
      deleteGithubRepository(authToken, repoowner, reponame)
      println("creating folder")
      val f = better.files.File(testDirectory)
      f.createIfNotExists(true)
      val githubRepo = createGithubRepository(authToken, reponame, "testing jgit", false, false)
      cloneRepository(repo, "master", testDirectory)
    }

    override def afterAll() = {

    }
  }

  val test = new TestSuite()
  test.execute()

}
