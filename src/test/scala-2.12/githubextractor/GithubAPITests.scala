package githubextractor

import dk.itu.gfv.model.Model._
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers, Sequential}

import scala.util.Success

/**
  * Created by scas on 23-05-2017.
  */

import dk.itu.gfv.repo.GithubExtractor._

class GithubAPITests extends FlatSpec with Matchers {

  // mining-repos1
  // mining-repos
  // proof-ro
  type Name = String
  type Token = String
  val accs: Map[Name, Token] = better.files.File("github.txt").lines.tail.tail.map(_.split(";")).map(x => x(1).trim -> x(0).trim).toMap
  val tokens = better.files.File("github.txt").lines.tail.tail.map(_.split(";")(0)).toSet
  val authToken = tokens.last
  val reponame = "scala-app-repo"
  //{"name" : reponame, "has_issues" : false, "private" : false, "description" : "testing", "has_wiki" : true, "auto_init" : true}

  class CreateRepository extends FlatSpec {
    it should "create a repository for the user proof-ro and return the object" in {
      assert(createGithubRepository(authToken, reponame, "testing", false, false).isInstanceOf[Some[GithubRepository]])
    }
  }

  class FailWhenRepositoryExists extends FlatSpec {
    it should "produce a None when repository already exists" in {
      assert(createGithubRepository(authToken, reponame, "testing", false, false).isDefined === false)
    }
  }

  class ForkRepository extends FlatSpec {
    it should s"create a fork of the proof-ro/$reponame repository" in {
      val fork = forkGithubRepository(tokens.head, "proof-ro", reponame)
      fork shouldBe a [Success[Some[GithubRepository]]]
      assert(fork.get.get.full_name === s"mining-repos1/$reponame")
    }
  }

  class DeleteForkedRepository extends FlatSpec {
    it should s"delete the fork (mining-repos1/$reponame) of the proof-ro/$reponame-repo repository" in {
      assert(deleteGithubRepository(tokens.head, "mining-repos1", reponame) === Success(s"Repository mining-repos1/$reponame deleted successfully"))
    }
  }

  class CheckNoForksAfterDeletingForkedRepository extends FlatSpec {
    val sleepTime = 2 // seconds
    it should "show that the count of forks is 0" in {
      info(s"sleeping for $sleepTime s to wait for Github to update its meta-data, which does not happen instantenously")
      Thread.sleep(1000L * sleepTime)
      assert(getRepository("proof-ro/scala-app-repo").get.forks_count === 0)
    }
  }

  class DeleteRepository extends FlatSpec {
    it should "delete the github repository" in {
      assert(deleteGithubRepository(authToken, "proof-ro", reponame) === Success(s"Repository proof-ro/$reponame deleted successfully"))
    }
  }

  class FailsWhenRepositoryDoesNotExist extends FlatSpec {
    it should "throw a failure when the repository does not exist" in {
      assert(deleteGithubRepository(authToken, "proof-ro", reponame).isFailure)
    }
  }

  class MyTestSuite extends Sequential(
    new CreateRepository,
    new FailWhenRepositoryExists,
    new ForkRepository,
    new DeleteForkedRepository,
    new CheckNoForksAfterDeletingForkedRepository, // it takes a some time before this is updated in Github's meta-data
    new DeleteRepository,
    new FailsWhenRepositoryDoesNotExist
  ) with BeforeAndAfterAll {
    override def beforeAll = {
      accs.foreach(x => deleteGithubRepository(x._2, x._1, reponame))
    }
  }

  new MyTestSuite().execute()
  //
  //  class CreateTest extends Sequential (
  //    new CreateRepository
  //  )
//    new CreateRepository().execute()
  //
//    new ForkRepository().execute()
//    new DeleteForkedRepository().execute()
//    new DeleteRepository().execute()

}
