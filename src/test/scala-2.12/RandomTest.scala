import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 29-05-2017.
  */
class RandomTest extends FunSuite with Matchers {

  def findPairs (inputArray: Array[Int], sum: Int): Seq[(Int,Int)] = {

    val complementMap = collection.mutable.Map[Int,Int]()
    val result = collection.mutable.Seq[(Int,Int)]()
    for(v <- inputArray){
      complementMap.get(v) match {
        case Some(value) => result.+:((v,value))
        case None => complementMap.put(sum-v, v)
      }

    }
    result.toSeq
  }


  test("test"){
    val inputArray = Array(12,24,28,32,44,56,88,90)
    findPairs(inputArray,100) === Seq((12,88), (44,56))
    findPairs(inputArray,100) === Seq((12,88), (44,56))
  }

}
