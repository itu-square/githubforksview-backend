package json

import dk.itu.gfv.model.Model.{CommitGithub, GithubRepository, Owner, Parent}
import org.scalatest.{FunSuite, Matchers}

import scala.io.Source.fromURL
import spray.json._
import DefaultJsonProtocol._
import dk.itu.gfv.repo.GithubExtractor
import org.joda.time.format.ISODateTimeFormat

/**
  * Created by scas on 28-04-2017.
  */
class ModelsTest extends FunSuite with Matchers{

  def joda_time(str: String) = org.joda.time.DateTime.parse(str,ISODateTimeFormat.dateTimeNoMillis())

  test("repo main"){
    //final case class GithubRepository(id: Int, full_name: String, fork: Boolean,
    // created_at: org.joda.time.DateTime, updated_at: org.joda.time.DateTime, pushed_at: org.joda.time.DateTime,
    // language: Option[String], forks_count: Int,default_branch: String)
    val exp =
      GithubRepository(4164482,
        "django/django",
        Owner("django"),
        false,
        joda_time("2012-04-28T02:47:18Z"),
        joda_time("2017-04-28T19:12:19Z"),
        joda_time("2017-04-28T17:18:21Z"),
        25297,
        Some("Python"),
        10333,
        "master",
        None,
        Some(1524))

    val json_string = fromURL(getClass.getResource("/json/project.json")).getLines().mkString("\n")
    val parsed_json = json_string.parseJson
    parsed_json shouldBe a [JsObject]
    parsed_json.convertTo[GithubRepository] shouldBe a [GithubRepository]
    parsed_json.convertTo[GithubRepository] shouldBe exp

    val exp1 =
      GithubRepository(12670444,
        "petkaantonov/bluebird",
        Owner("petkaantonov"),
        false,
        joda_time("2013-09-07T19:39:57Z"),
        joda_time("2017-04-28T22:04:45Z"),
        joda_time("2017-04-25T14:48:17Z"),
        14449,
        Some("JavaScript"),
        1782,
        "master",
        None,
        Some(366))

    val json_string1 = fromURL(getClass.getResource("/json/project1.json")).getLines().mkString("\n")
    val parsed_json1 = json_string1.parseJson
    parsed_json1 shouldBe a [JsObject]
    parsed_json1.convertTo[GithubRepository] shouldBe a [GithubRepository]
    parsed_json1.convertTo[GithubRepository] shouldBe exp1

    val exp2 =
      GithubRepository(60923354,
        "scas-mdd/fork-test",
        Owner("scas-mdd"),
        false,
        joda_time("2016-06-11T19:04:02Z"),
        joda_time("2016-06-11T19:35:17Z"),
        joda_time("2017-03-02T04:56:55Z"),
        0,
        Some("C"),
        4,
        "master",
        None,
        Some(1))

    val json_string2 = fromURL(getClass.getResource("/json/project2.json")).getLines().mkString("\n")
    val parsed_json2 = json_string2.parseJson
    parsed_json2 shouldBe a [JsObject]
    parsed_json2.convertTo[GithubRepository] shouldBe a [GithubRepository]
    parsed_json2.convertTo[GithubRepository] shouldBe exp2

    val exp3 =
      GithubRepository(28866595,
        "ErikZalm/Marlin",
        Owner("ErikZalm"),
        true,
        joda_time("2015-01-06T14:19:20Z"),
        joda_time("2017-04-11T14:39:11Z"),
        joda_time("2017-02-18T14:52:25Z"),
        64,
        Some("C"),
        39,
        "Stable",
        Some(Parent("MarlinFirmware/Marlin")),
        Some(31))

    val json_string3 = fromURL(getClass.getResource("/json/project3.json")).getLines().mkString("\n")
    val parsed_json3 = json_string3.parseJson
    parsed_json3 shouldBe a [JsObject]
    parsed_json3.convertTo[GithubRepository] shouldBe a [GithubRepository]
    parsed_json3.convertTo[GithubRepository] shouldBe exp3
  }
  test("parsing forks json") {
    val json_string = fromURL(getClass.getResource("/json/bluebird_forks.json")).getLines.mkString("\n")
    val parsed_json = json_string.parseJson
    parsed_json shouldBe a [JsValue]
    parsed_json.convertTo[Option[Seq[GithubRepository]]] shouldBe a [Option[Seq[GithubRepository]]]
  }

  test("parsing commits json of github"){

    val json_string = fromURL(getClass.getResource("/json/commits.json")).getLines.mkString("\n")
    println(json_string)
    val parsed_json = json_string.parseJson
    println(parsed_json.convertTo[Option[Seq[CommitGithub]]])
    parsed_json.convertTo[Option[Seq[CommitGithub]]] shouldBe a [Some[Seq[CommitGithub]]]
  }

  test("parsing stefan"){
    val json_string = fromURL(getClass.getResource("/json/commits.json")).getLines.mkString("\n")
    val parsed_json = json_string.parseJson
    parsed_json.convertTo[CommitGithub] shouldBe a [CommitGithub]
    parsed_json.convertTo[Option[CommitGithub]] shouldBe a [Some[CommitGithub]]
  }

  test("parsing github commits from API"){
    GithubExtractor.getCommits("scas-mdd/fork-test", "new_branch_to_test") shouldBe a [Some[Seq[CommitGithub]]]
  }

  test("parsing github commits from API - 1 commit"){
    GithubExtractor.getCommits("proof-ro/scala-app-repo", "master") shouldBe a [Some[Seq[CommitGithub]]]
  }
}
