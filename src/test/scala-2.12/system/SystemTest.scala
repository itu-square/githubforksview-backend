package system

import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.model.Model.{GithubRepository, Parent}
import dk.itu.gfv.monitor.InsertData
import dk.itu.gfv.monitor.db.Models.{Branch, Project}
import dk.itu.gfv.monitor.db.NightFetcher._
import dk.itu.gfv.repo.GithubExtractor._
import dk.itu.gfv.repo.RepositoryOperations
import dk.itu.gfv.repo.RepositoryOperations.{cloneRepository, pushAllChanges}
import dk.itu.gfv.util.MonitorProperties
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.RevCommit
import org.scalatest._

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.sys.process._
import scala.util.Success
import spray.json._
import DefaultJsonProtocol._
import org.eclipse.jgit.api.CreateBranchCommand.SetupUpstreamMode

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by scas on 25-05-2017.
  */
class SystemTest extends FlatSpec with Matchers with MonitorProperties with LazyLogging {


  type Name = String
  type Token = String
  val tokens: Map[Name, Token] = better.files.File("github.txt").lines.map(x => x.split(";")).map(x => x(1).trim -> (x(0).trim)).toMap
  val accs = Seq("proof-ro", "mining-repos", "mining-repos1")

  val reponame = "scala-app-repo"
  val testsFolder = properties.getProperty("data")

  /**
    *
    * @param repo repository name e.g., scas-mdd/MyRepository
    * @param file file including any directories e.g., hello.txt
    * @param msg commit message
    * @param commitChange string to be added to the file
    * @param token github token to be used for authorizing the change
    */

  def makeCommit(repo: String, file: String, msg: String, commitChange: String, token: String) = {
    val git = Git.open(new java.io.File(s"$testsFolder/$repo/.git"))
    val helloFile = better.files.File(s"$testsFolder/$repo/$file")
    helloFile.append(s"\n$commitChange")
    // Now, we do the commit with a message
    git.add.addFilepattern(file).call
    val rev = git.commit.setAuthor("scas", "scas@scas-mdd.com").setMessage(msg).call
    //    val logCommits = git.log.call().asScala
    pushAllChanges(git, token)
    git.getRepository.close()
    git.close
  }

  class MainRepo extends FlatSpec {
    it should "create the main project repository for proof-ro" in {
      val r = createGithubRepository(tokens.get(accs.head).getOrElse(""), reponame, "testing GMS tool", false, false)
      r shouldBe a[Some[_]] //GithubRepository
    }
  }

  class CommitsInMainRepo extends FlatSpec {
    it should "create a commit in the main repository and push it" in {
      cloneRepository(s"${accs.head}/$reponame", "master", testsFolder)
      val git = Git.open(new java.io.File(s"$testsFolder/${accs.head}/$reponame/.git"))
      val helloFile = new java.io.File(s"$testsFolder/${accs.head}/$reponame", "hello.txt")
      helloFile.createNewFile
      val fw = new java.io.FileWriter(helloFile)
      fw.write("hello world\n")
      fw.flush
      fw.close
      git.add.addFilepattern("hello.txt").call
      // Now, we do the commit with a message
      val rev = git.commit.setAuthor("scas", "scas@scas-mdd.com").setMessage("My first commit").call
      val logCommits = git.log.call().asScala
      pushAllChanges(git, tokens.get(accs.head).get)
      git.getRepository.close()
      git.close
      val commitsGithub = getCommits(s"${accs.head}/$reponame", "master")
      assert(logCommits.size === 2)
      assert(commitsGithub.get.size === 2)
    }
  }

  class MiningRepos1Fork extends FlatSpec {
    it should s"create a fork of the main's project repository in ${accs.last} from proof-ro" in {
      val fork = forkGithubRepository(tokens.get(accs.last).getOrElse(""), accs.head, reponame)
      fork shouldBe a[Success[_]] //Some[GithubRepository]
      fork.get.get.fork shouldEqual true
      fork.get.get.forks_count shouldEqual 0
      fork.get.get.parent shouldBe (Some(Parent(s"${accs.head}/$reponame")))
      fork.get.get.full_name shouldEqual s"${accs.last}/$reponame"
      fork.get.get.owner.login shouldEqual s"${accs.last}"
    }
  }

  class MakeCommitInFork extends FlatSpec {
    it should "clone the fork repository, create a commit in the fork, push the commit" in {
      Thread.sleep(1000 * 10) // wait 10 seconds before trying to clone the repository. Forking takes a bit of time
      cloneRepository(s"${accs.last}/$reponame", "master", testsFolder)
      val git = Git.open(new java.io.File(s"$testsFolder/${accs.last}/$reponame/.git"))
      val helloFile = better.files.File(s"$testsFolder/${accs.last}/$reponame/hello.txt")
      helloFile.append(s"commit from ${accs.last}/$reponame fork\n")
      // Now, we do the commit with a message
      git.add.addFilepattern("hello.txt").call
      val rev = git.commit.setAuthor("scas", "scas@scas-mdd.com").setMessage(s"Add commit in ${accs.last}/$reponame fork ").call
      val logCommits = git.log.call().asScala
      pushAllChanges(git, tokens.get(accs.last).get)
      git.getRepository.close()
      git.close
      val commitsGithub = getCommits(s"${accs.last}/$reponame", "master")
      assert(logCommits.size === 3)
      assert(commitsGithub.get.size === 3)
    }
  }

  class InsertDataIntoDatabase extends FlatSpec {
    it should "insert the two repositories into the database" in {
      InsertData.main(Array(s"${accs.head}/$reponame"))
    }
  }

  class CheckIfDataIsInsertedCorrectlyInDB extends FlatSpec {

    it should "check that there are two rows and the correct ones in the projects table" in {
      val projectsDB = Await.result(getProjectByIds(Seq(1, 2)), Duration.Inf)
      val projectsGithub = accs.flatMap(x => getRepository(s"$x/$reponame")) // this will give one error on the console because it tries to get a repository that does not exist.
      val usersGithub = Await.result(getAllUsers, Duration.Inf)

      val p1 = projectsGithub.head
      val p2 = projectsGithub.last
      projectsDB.size shouldBe projectsGithub.size
      projectsDB.head shouldBe Project(1, p1.full_name, 1, p1.language.getOrElse(""), p1.default_branch, p1.fork, -1, -1, p1.forks_count, p1.stargazers_count, p1.subscribers_count, p1.created_at, p1.pushed_at, p1.updated_at)
      projectsDB.last shouldBe Project(2, p2.full_name, usersGithub.filter(_.login == p2.owner.login).head.id, p2.language.getOrElse(""), p2.default_branch, p2.fork, 1, 1, p2.forks_count, p2.stargazers_count, p2.subscribers_count, p2.created_at, p2.pushed_at, p2.updated_at)
    }

    it should "check that the branches are correct in the branches table" in {
      val branchesDB = Await.result(getBranches, Duration.Inf)
      val p1Branch = {
        val git = Git.open(new java.io.File(s"$testsFolder/${accs.head}/$reponame/.git"))
        val branches = git.branchList().call
        git.getRepository.close
        git.close
        branches
      }
      val p2Branch = {
        val git = Git.open(new java.io.File(s"$testsFolder/${accs.last}/$reponame/.git"))
        val branches = git.branchList().call
        git.getRepository.close
        git.close
        branches
      }
      branchesDB.size shouldEqual 2
      branchesDB.head shouldBe Branch(1, p1Branch.get(0).getName.replace("heads", "remotes/origin"), p1Branch.get(0).getObjectId.getName, 1)
      branchesDB.last shouldBe Branch(2, p2Branch.get(0).getName.replace("heads", "remotes/origin"), p2Branch.get(0).getObjectId.getName, 2)
    }

    it should "check that there are 2 commits inserted into the commits table" in {
      val commitsDB = Await.result(getAllCommits, Duration.Inf)
      commitsDB.size shouldEqual 2 // first commit of the repositories is ignored when inserting data into the database
    }

    it should "check that there are three branch_commits into the branch_commits table" in {
      val branchCommits = Await.result(getBranchCommitsOfProjects(Seq(1, 2)), Duration.Inf)
      branchCommits.size shouldEqual 3
    }

    it should "check that there are two added lines in the added_lines table" in {
      val addedLines = Await.result(getAllAddedLines, Duration.Inf)
      addedLines.size shouldEqual 2
    }
  }

  class InsertDuplicateData extends FlatSpec {
    it should "try to insert same branch with new commits the same commits as before" in {

      import slick.driver.JdbcProfile
      import slick.jdbc.MySQLProfile.api._

      val git = Git.open(new java.io.File(s"$testsFolder/${accs.last}/$reponame/.git"))
      val branch_commits = git.log().call().asScala.toSeq
      val addedLines = transformRevCommits(branch_commits).par.map(x => RepositoryOperations.patchToAddedLines(git, x.sha, true, 2)).flatten.flatten.seq
      val branch = Await.result(getBranches, Duration.Inf)

      val insertion = db.run(DBIO.from(insert_new_commits(branch_commits, branch_commits, addedLines, 2, git, branch.last, false)).transactionally)
      try {
        Await.result(insertion, Duration.Inf)
      }
      catch {
        case ex: Exception => logger.error("tried to insert duplicates, but it failed;" + ex.getMessage)
      }
    }
  }

  class CheckTransactionalityOfDB extends FlatSpec {
    it should "fail when inserting a forced branch with same commits as new commits, and it should rollback" in {
      new CheckIfDataIsInsertedCorrectlyInDB()
    }
  }

  class InsertSameBranchAgain extends FlatSpec {
    it should "try to insert same branch -> tests the transactionality of insertNonExistingBranch method" in {

      val main_git = Git.open(new java.io.File(s"$testsFolder/${accs.last}/$reponame/.git"))
      val x = main_git.branchList().call().asScala.head
      val gitCommits = main_git.log.all().call().asScala.toSeq
      println(s"insert same branch again git commits: $gitCommits")
      println(gitCommits.size)
      val repo = Await.result(getProjectById(2), Duration.Inf)
      val commitsInBranches = collection.mutable.HashSet[String]()
      val commitsToExclude = collection.mutable.HashSet[String]()
      val new_commits_to_be_added_to_commits = new collection.mutable.ListBuffer[RevCommit]()
      val main_repo = false
      val project = "test"
      insertNonExistingBranchWithRollback(project,x, repo, main_git, main_repo, commitsInBranches, commitsToExclude, new_commits_to_be_added_to_commits)
    }
  }

  class MiningReposFork extends FlatSpec {
    it should s"create a fork of the main's project repository in ${accs.tail.head} from proof-ro" in {
      val fork = forkGithubRepository(tokens.get(accs.tail.head).getOrElse(""), accs.head, reponame)
      fork shouldBe a[Success[_]] //Some[GithubRepository]
      fork.get.get.fork shouldEqual true
      fork.get.get.forks_count shouldEqual 0
      fork.get.get.parent shouldBe (Some(Parent(s"${accs.head}/$reponame")))
      fork.get.get.full_name shouldEqual s"${accs.tail.head}/$reponame"
      fork.get.get.owner.login shouldEqual s"${accs.tail.head}"
    }
  }

  class CheckEtags extends FlatSpec {
    it should "make a commit and check that in indeed the get updated forks has changed" in {
      val cachedETAGs = better.files.File(cache_file).contentAsString.parseJson.convertTo[Option[List[CachedETAG]]].get.map(x => x.project -> x).toMap
      val (newCachedETAGs, forks) = getUpdatedProjectsWithETAGs(accs.head+"/" + reponame)

      cachedETAGs === newCachedETAGs
      forks.isEmpty shouldBe true
    }
  }

  class CheckGetUpdatedForksWithEtags extends FlatSpec {
    it should "make a commit and push it and return that a repository has been modified" in {
      val cachedETAGs = better.files.File(s"$testsFolder/cache.txt").contentAsString.parseJson.convertTo[Option[List[CachedETAG]]].get.map(x => x.project -> x).toMap
      val (newCachedETAGs, forks) = getUpdatedProjectsWithETAGs(accs.head + "/" + reponame)

      makeCommit(accs.last + "/" + reponame, "hello.txt", "new change to test getUpdatedForks", "making change\n", tokens.get(accs.last).get)

      Thread.sleep(10000) // wait until the commit updates the metadata.
      val (newCachedETAGs1, forks1) = getUpdatedProjectsWithETAGs(accs.head + "/" + reponame)

      forks1.size shouldEqual 1
      forks1.head shouldBe a [GithubRepository]
      forks1.head.full_name shouldBe accs.last + "/" + reponame

      // repo forks_etag should be different
      assert(! newCachedETAGs.get(accs.head + "/" + reponame).get.forks_etag.equalsIgnoreCase(newCachedETAGs1.get(accs.head + "/" + reponame).get.forks_etag))
      // forks_etag should be the same as the repository does not have any forks
      assert(newCachedETAGs.get(accs.last + "/" + reponame).get.forks_etag.equalsIgnoreCase(newCachedETAGs1.get(accs.last + "/" + reponame).get.forks_etag))
      // etag should be different as a new commit was just pushed
      assert(!newCachedETAGs.get(accs.last + "/" + reponame).get.etag.equalsIgnoreCase(newCachedETAGs1.get(accs.last + "/" + reponame).get.etag))

//      writeUpdatedCacheETAGs(newCachedETAGs1)
//
//      better.files.File(s"$testsFolder/cache.txt").contentAsString.parseJson.convertTo[Option[List[CachedETAG]]].get.map(x => x.project -> x).toMap === newCachedETAGs1
    }
  }

  class MonitorProject extends FlatSpec {
    it should "run the monitor_project method and insert new data" in {
      implicit val analyze = false
      monitor_project(accs.head + "/" + reponame)
      Thread.sleep(5000) // wait until the commit updates the metadata.
    }
  }

  class CheckNewInsertedData extends FlatSpec {
    it should "check that there are two rows and the correct ones in the projects table" in {
      val projectsDB = Await.result(getProjectByIds(Seq(1, 2)), Duration.Inf)
      val projectsGithub = accs.flatMap(x => getRepository(s"$x/$reponame")) // this will give one error on the console because it tries to get a repository that does not exist.
      val usersGithub = Await.result(getAllUsers, Duration.Inf)

      val p1 = projectsGithub.head
      val p2 = projectsGithub.last
      projectsDB.size shouldBe projectsGithub.size
      projectsDB.head shouldBe Project(1, p1.full_name, 1, p1.language.getOrElse(""), p1.default_branch, p1.fork, -1, -1, p1.forks_count, p1.stargazers_count, p1.subscribers_count, p1.created_at, p1.pushed_at, p1.updated_at)
      projectsDB.last shouldBe Project(2, p2.full_name, usersGithub.filter(_.login == p2.owner.login).head.id, p2.language.getOrElse(""), p2.default_branch, p2.fork, 1, 1, p2.forks_count, p2.stargazers_count, p2.subscribers_count, p2.created_at, p2.pushed_at, p2.updated_at)
    }

    it should "check that the branches are correct in the branches table" in {
      val branchesDB = Await.result(getBranches, Duration.Inf)
      val p1Branch = {
        val git = Git.open(new java.io.File(s"$testsFolder/${accs.head}/$reponame/.git"))
        val branches = git.branchList().call
        git.getRepository.close
        git.close
        branches
      }
      val p2Branch = {
        val git = Git.open(new java.io.File(s"$testsFolder/${accs.last}/$reponame/.git"))
        val branches = git.branchList().call
        git.getRepository.close
        git.close
        branches
      }
      branchesDB.size shouldEqual 2
      branchesDB.head shouldBe Branch(1, p1Branch.get(0).getName.replace("heads", "remotes/origin"), p1Branch.get(0).getObjectId.getName, 1)
      branchesDB.last shouldBe Branch(2, p2Branch.get(0).getName.replace("heads", "remotes/origin"), p2Branch.get(0).getObjectId.getName, 2)
    }

    it should "check that there are 3 commits inserted into the commits table" in {
      val commitsDB = Await.result(getAllCommits, Duration.Inf)
      commitsDB.size shouldEqual 3 // first commit of the repositories is ignored when inserting data into the database
    }

    it should "check that there are four branch_commits into the branch_commits table" in {
      val branchCommits = Await.result(getBranchCommitsOfProjects(Seq(1, 2)), Duration.Inf)
      branchCommits.size shouldEqual 4
    }

    it should "check that there are two added lines in the added_lines table" in {
      val addedLines = Await.result(getAllAddedLines, Duration.Inf)
      addedLines.size shouldEqual 3
    }

  }

  class AddANewBranch extends FlatSpec {
    it should "create a new branch, push it and run monitor project method to update the DB" in {
      val git = Git.open(new java.io.File(s"$testsFolder/${accs.last}/$reponame/.git"))
      git.branchCreate().setName("testing_branch").setUpstreamMode(SetupUpstreamMode.TRACK).call()
      pushAllChanges(git,tokens.get(accs.last).get)
      monitor_project(accs.head + "/" + reponame)(false)
    }
  }

  class CheckNewBranchInDB extends FlatSpec {

    it should "check that it has three branches in the DB" in {
      val branches = Await.result(getBranches, Duration.Inf)
      branches.size shouldEqual 3
    }

    it should "check that it has mapped the existing commits on this branch to the branch_commits table" in {
      val branchCommits = Await.result(getBranchCommits, Duration.Inf)
      branchCommits.size shouldEqual 7
    }
  }

  class ProjectUpdated extends FlatSpec {
    it should "check that a project has been updated and report it" in {

      makeCommit(s"${accs.head}/$reponame","hello.txt", s"Add commit in ${accs.head}/$reponame from ProjectUpdated test ", s"commit from projectUpdated test for ${accs.head}/$reponame \n", tokens.filter(_._1.equalsIgnoreCase(accs.head)).head._2)
      Thread.sleep(1000)
      val (cachedETAGS, forks) = getUpdatedProjectsWithETAGs(accs.head + "/" + reponame)
      forks.size shouldEqual 2 // a previous change to last fork was done, thus it is reported here because the etags were not saved!
      forks.map(_.full_name) === Seq("proof-ro/scala-app-repo", "mining-repos1/scala-app-repo")

      writeUpdatedCacheETAGs(cachedETAGS)
    }
  }

//  class ProjectsUpdated extends FlatSpec {
//    it should "check that a fork has been updated and report it" in {
//      makeCommit(s"${accs.last}/$reponame","hello.txt", s"Add commit in ${accs.last}/$reponame from ProjectsUpdated test ", s"commit from projectUpdated test for ${accs.last}/$reponame \n", tokens.filter(_._1.equalsIgnoreCase(accs.last)).head._2)
//      val (cachedETAGS, forks) = getUpdatedProjectsWithETAGs(accs.head + "/" + reponame)
//      println(forks)
//      forks.size shouldEqual 2
//    }
//  }

  class AllTests extends Sequential(
    new MainRepo(),
    new CommitsInMainRepo(),
    new MiningRepos1Fork(),
    new MakeCommitInFork(),
    new InsertDataIntoDatabase(),
    new CheckIfDataIsInsertedCorrectlyInDB(),
    new InsertDuplicateData(),
    new CheckIfDataIsInsertedCorrectlyInDB(),
    new InsertSameBranchAgain(),
    new CheckIfDataIsInsertedCorrectlyInDB(),
//    new MiningReposFork(),
    new CheckEtags(),
    new CheckGetUpdatedForksWithEtags(),
    new MonitorProject(),
    new CheckNewInsertedData(),
    new AddANewBranch(),
    new CheckNewBranchInDB(),
    new ProjectUpdated()
//    new ProjectsUpdated()
    //    new CheckTransactionalityOfDB()
  ) with BeforeAndAfterAll {

    override def beforeAll = {
      // drop database schema
      resetDb
      createSchemaIfNotExists
      // remove all github projects
      tokens.foreach(x => deleteGithubRepository(x._2, x._1, reponame))
      // remove all git repositories on hdd
      val testFolder = properties.getProperty("data")
      s"C:\\Program Files\\Git\\bin\\bash.exe removedir_tests.sh $testFolder".!!
    }

    override def afterAll = {

    }
  }

  class DBTests extends Suites {
    new CheckIfDataIsInsertedCorrectlyInDB()
  }


  new AllTests().execute(stats = true)
  //  new DBTests().execute(stats = true)

}
