package db

import dk.itu.gfv.monitor.db.{DBService, DatabaseSchema}
import dk.itu.gfv.monitor.db.Models.{Branch, Project}
import dk.itu.gfv.util.MonitorProperties
import org.scalatest.{FunSuite, Matchers}
import org.mockito.Mockito.mock
import org.mockito.ArgumentMatchers._
import org.mockito.InjectMocks
import org.mockito.Mockito._
import org.scalatest.FunSuite
import org.scalatest.mockito.MockitoSugar

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
/**
  * Created by scas on 21-05-2017.
  */
class InsertDataTest extends FunSuite with MockitoSugar{

  @InjectMocks
  val dbService = mock[DBService]
  test("should return branch") {
    when(dbService.getBranch(1, "test")).thenReturn(Future(Branch(1, "test", "123", 1)))
    verify(dbService).getBranch(1,"test")
  }
  test("should return project") {
    when(dbService.getProjectById(1)).thenReturn(Future(Project(1,"test",1,"C","master",true,-1,-1,0,0,None,org.joda.time.DateTime.now(),org.joda.time.DateTime.now(),org.joda.time.DateTime.now())))
  }

  test("should return a seq of projects") {
    when(dbService.getProjectById(1)).thenReturn(Future(Project(1,"test",1,"C","master",true,-1,-1,0,0,None,org.joda.time.DateTime.now(),org.joda.time.DateTime.now(),org.joda.time.DateTime.now())))
  }
}
