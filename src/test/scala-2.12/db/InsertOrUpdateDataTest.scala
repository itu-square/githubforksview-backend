package db

import dk.itu.gfv.monitor.{InsertOrUpdateData, Utils}
import dk.itu.gfv.monitor.db.Models.CommitParent
import org.scalatest.{FunSuite, Matchers}

/**
  * Created by scas on 29-04-2017.
  */
class InsertOrUpdateDataTest extends FunSuite with Matchers with Utils{

  test ("Have the same commit parents"){
    val cp = Seq(CommitParent("a", "b"), CommitParent("a", "c"))

    val cp1 = Seq(CommitParent("a", "b"), CommitParent("a", "c")).groupBy(_.sha)
    sameCommitParents(cp, cp1) shouldBe true

    val cp2 = Seq(CommitParent("a", "c"), CommitParent("a", "c")).groupBy(_.sha)
    sameCommitParents(cp, cp2) shouldBe false

    val cp3 = Seq(CommitParent("a", "b"), CommitParent("a", "d")).groupBy(_.sha)
    sameCommitParents(cp, cp3) shouldBe false

    val cp4 = Seq(CommitParent("a", "c"), CommitParent("a", "b")).groupBy(_.sha)
    sameCommitParents(cp, cp4) shouldBe true

    val cp5 = Seq(CommitParent("b", "c"), CommitParent("a", "b")).groupBy(_.sha)
    sameCommitParents(cp, cp5) shouldBe false
  }
}
