package pre.evaluation

import java.io.{FileWriter, File}
import java.util.regex.Pattern

import dk.itu.gfv.redundant.Backend
import dk.itu.gfv.util.CsvToXls

/**
  * Created by scas on 04-09-2016.
  */
object EvaluationPREQS {

  val whitespace = Pattern.compile("""\+\s*""").matcher("")

  val brackets = """\+\s*(\{|\}|\(|\))\s*"""
  val pattern = Pattern.compile(brackets).matcher("")


  def evaluateProject(folder: String) = {
    new File(folder + "/result.csv").delete
    new File(folder + "/result.xlsx").delete
    new File(folder + "/after.txt").delete
    new File(folder + "/other.txt").delete
    val patches = getPatches(folder)
    val str = new StringBuilder()
    val headers = "new-row-old-row,Levenshtein     , NLevenshtein      , N-gram       , Cosine        , Jaro-Winkler     , Jaccard "
    str.append(headers + "\n")

    val before = patches.last
    val after = patches.init.last
    val otherPatches = List(before) ++ patches.init.init

    println(before)
    println(after)
    println(otherPatches)

    val afterAdditions = patchAdditions(scala.io.Source.fromFile(after.getAbsolutePath).getLines.toList)

    val otherAdditionsForRetrieval = (for (
      p <- otherPatches;
      oA = patchAdditions(scala.io.Source.fromFile(p.getAbsolutePath).getLines.toList) map (x => p + "***" + x)
    )yield oA).flatten
    val otherAdditions = (for (
            p <- otherPatches;
            oA = patchAdditions(scala.io.Source.fromFile(p.getAbsolutePath).getLines.toList)
          )yield oA).flatten

    for (i <- 0 to afterAdditions.size-1) {
      for (j <- 0 to otherAdditions.size - 1) {
        str.append(computeSimilarity(afterAdditions(i).trim, otherAdditions(j).trim, i,j))
      }
    }
    writeToFile(afterAdditions.mkString("\n"),folder + "/after.txt")
    writeToFile(otherAdditionsForRetrieval.mkString("\n"),folder + "/other.txt")
    writeToFile(str.toString,folder + "/result.csv")

    val xls = CsvToXls.convertCSVToXLS(folder + "/result.csv", folder + "/result.xlsx")
  }

  def getPatches(folder: String) = {
    new File(folder).listFiles().toList
  }
  
  def computeSimilarity(str1: String, str2: String, i: Int, j: Int) = {
    (i+1) + "-" + (j+1) + "," + Backend.checkSimilarityOfStrings(str1,str2, "levenshtein") ++ " ," + Backend.checkSimilarityOfStrings(str1,str2, "n-levenshtein") + " ," + Backend.checkSimilarityOfStrings(str1,str2, "ngram")  + " ," + Backend.checkSimilarityOfStrings(str1,str2, "cosine") + " ," + Backend.checkSimilarityOfStrings(str1,str2, "jaro-winkler") + " ," + Backend.checkSimilarityOfStrings(str1,str2, "jaccard") + "\n"
  }

  def patchAdditions(str: List[String]) = {
    str.filter(x => x.startsWith("+") && !x.startsWith("+++") && !whitespace.reset(x).matches && !pattern.reset(x).matches && (x.replaceAll("""\s*""","").size > 4))
  }

  def writeToFile(str: String, file: String) = {
    val fw1 = new FileWriter(file)
    fw1.write(str)
    fw1.close
  }

  def main(args: Array[String]) {
    val biolab = "preqs/biolab-orange3-1111-1107"
    val conta_core_bundle = "preqs/conta-core-bundle-444-433"
    val spark = "preqs/spark-5479-5451"

    evaluateProject(biolab)
    evaluateProject(conta_core_bundle)
    evaluateProject(spark)
  }
}
