package dk.itu.gfv.redundant.model

import java.util.regex.{Matcher, Pattern}

import dk.itu.gfv.repo.PatchParser
import dk.itu.gfv.repo.PatchParser.{BinaryFile, DiffFile}
import spray.json._
import spray.json.DefaultJsonProtocol._


/**
  * Created by scas on 15-09-2016.
  */
object LineChangeModel {

  case class Commit(fork: String, branch: String, sha: String, commitTime: Int)
  implicit val commitCodec = jsonFormat4(Commit)
  case class RangeHunk(start: Int, linesBeforeChange: Int, end: Int, linesAfterChange: Int)
  implicit val rangeHunkCodec = jsonFormat4(RangeHunk)
  case class LineChange(commit: Commit, rangeHunk: RangeHunk, line: String, file: String)
  implicit val LineChangeCodec = jsonFormat4(LineChange)

  val bracketsKeyword = Pattern.compile("""\+\s*(\{|\})\s*""").matcher("")
  val returnKeyword = Pattern.compile("""\+\s*return\s*;?\s*""").matcher("")
  val semicolon = Pattern.compile("""\+\s*(\}\s*;|;|\}\s*,)\s*""").matcher("")
  // // ActionScript, AutoHotkey, C, C++, C#, D,[13] Go, Java, JavaScript, Objective-C, PHP, PL/I, Rust (can be nested), Scala (can be nested), SASS, SQL, Swift, Visual Prolog, CSS
  // # Bourne shell and other UNIX shells, Cobra, Perl, Python, Ruby, Seed7, Windows PowerShell, PHP, R, Make, Maple, Nimrod
  // -- Euphoria, Haskell, SQL, Ada, AppleScript, Eiffel, Lua, VHDL, SGML
  // % TeX, Prolog, MATLAB,[10] Erlang, S-Lang, Visual Prolog
  // ; AutoHotkey, AutoIt, Lisp, Common Lisp, Clojure, Rebol, Scheme, many assemblers
  // * COBOL (if fixed-form and * in column 7), PAW, many assemblers, Fortran (if fixed-form and * in column 1)
  val commentStart = """(\+\s*(\/\/|\/\*\*?|--|%|#).*)"""
  val multiLineComment = """(\+\s*\*.*)"""
  val commentEnd = """(\+.*?(?=\*\/)\*\/\s*)"""
  val commentsRegex = Pattern.compile(commentStart + "|" + multiLineComment + "|" + commentEnd).matcher("")

  val javaScalaImport = """(\+\s*import .*)"""
  val csharpImport = """(\+\s*using .*)"""
  val objCImport = """(\+\s*(#|@)import .*)"""
  val cImport = """(\+\s*#include .*)"""
  val pythonImport = """(\+\s*from .*)"""
  val otherImport = """(\+\s*(use|require) .*)"""
  val importRegex = Pattern.compile(javaScalaImport + "|" + csharpImport + "|" + cImport  + "|" + objCImport + "|" + otherImport  + "|" + pythonImport).matcher("")

  var c = 1

  /**
    * Return a RangeHunk object that contains information about the hunks' region (start, end, and nr of lines before/after the change)
    *
    * @param input
    * @return
    */
  def stringToRangeHunk(input: String) = {
//    println(input.toList.groupBy(identity).get('@').get.size)
    if (input.toList.groupBy(identity).get('@').get.size >= 4){
      val p = input.trim.replaceAll(" @@.*","").replace("@@ ","").replace("-","").replace("+","")
      val s = p.split(" ").toList
      if(s(0).contains(',') && s(1).contains(',')){
        val start = s(0).split(",")
        val end = s(1).split(",")
//        println(input)
//        println(p)
        RangeHunk(Integer.parseInt(start(0)), Integer.parseInt(start(1)),Integer.parseInt( end(0)), Integer.parseInt(end(1)))
      }
      else if (s(0).contains(',') && !s(1).contains(',')){
        val start = s(0).split(",")
        val end = s(1)
        RangeHunk(Integer.parseInt(start(0)), Integer.parseInt(start(1)),Integer.parseInt( end), Integer.parseInt(end))
      }
      else if(!s(0).contains(',') && s(1).contains(',')){
        val start = s(0)
        val end = s(1).split(",")
        RangeHunk(Integer.parseInt(start), Integer.parseInt(start),Integer.parseInt( end(0)), Integer.parseInt(end(1)))
      }
      else {
        RangeHunk(Integer.parseInt(s(0)), Integer.parseInt(s(0)),Integer.parseInt(s(1)), Integer.parseInt(s(1)))
      }
    }
    else {
      RangeHunk(0, 0,0, 0)
    }


  }

  def stringToRangeHunk1(input: String) = {
    //    println(input.toList.groupBy(identity).get('@').get.size)
    if (input.toList.groupBy(identity).get('@').get.size >= 4){

      val p = input.trim.replaceAll(" @@.*","").replace("@@ ","").replace("-","").replace("+","")
      val s = p.split(" ").toList
      if(s(0).contains(',') && s(1).contains(',')){
        val start = s(0).split(",")
        val end = s(1).split(",")
        //        println(input)
        //        println(p)
        RangeHunk(Integer.parseInt(start(0)), Integer.parseInt(start(1)),Integer.parseInt( end(0)), Integer.parseInt(end(1)))
      }
      else if (s(0).contains(',') && !s(1).contains(',')){
        val start = s(0).split(",")
        val end = s(1)
        RangeHunk(Integer.parseInt(start(0)), Integer.parseInt(start(1)),Integer.parseInt( end), Integer.parseInt(end))
      }
      else if(!s(0).contains(',') && s(1).contains(',')){
        val start = s(0)
        val end = s(1).split(",")
        RangeHunk(Integer.parseInt(start), Integer.parseInt(start),Integer.parseInt( end(0)), Integer.parseInt(end(1)))
      }
      else {
        RangeHunk(Integer.parseInt(s(0)), Integer.parseInt(s(0)),Integer.parseInt(s(1)), Integer.parseInt(s(1)))
      }
    }
    else {
      RangeHunk(0, 0,0, 0)
    }


  }

  /**
    * Given a patch with no information of commit, committer, time (starting with diff -- git a/test.txt etc)
    * it will return a list of lines that were added in that patch
    *
    * @param patch
    * @param commit
    * @return
    */

  def patchToLineChange(patch: String, commit: Commit): List[LineChange] = {
    c = c + 1
    // filter out binary diffs
    val patchFiles = PatchParser.splitPatchByFiles(patch).getOrElse(List()).flatMap{
      case _: BinaryFile => None
      case diff: DiffFile => Some(diff)
    }
    val addedLines = collection.mutable.MutableList[LineChange]()
    patchFiles.foreach(patchFile => {
      val hunks = PatchParser.splitPatchByHunks(patchFile.patch)
      hunks match {
        case Some(hunkList) => hunkList.size match {
          case 0 => Nil
          case _ => hunkList.foreach(x => {
            try{
              val hunkLines = x.patch.split("\r?\n").toList.drop(2)
              val hunkInfo = hunkLines.head
              val rangeHunk = stringToRangeHunk(hunkInfo)
//              println(hunkInfo)
//              println(rangeHunk)
              val hunkText = hunkLines.tail.filter(h => h.startsWith("+") && !h.startsWith("++") && !h.matches("""\+\s*""")).filterNot(x => commentsRegex.reset(x).matches || importRegex.reset(x).matches || bracketsKeyword.reset(x).matches || returnKeyword.reset(x).matches || semicolon.reset(x).matches)
//              println(hunkText.size)
              addedLines.++=(hunkText.map(x => LineChange(commit, rangeHunk, x.substring(1).trim, patchFile.filename)))
            }
            catch {
              case a: java.lang.StringIndexOutOfBoundsException => sys.error("Error: " + x + "\n" + a.getMessage)
////              case a: Exception => sys.error("Error: " + x + "\n" + a.getMessage)
//              case a: Exception => sys.error("Error: " + patch + "\n" + a.getMessage)
            }

          })
        }
        case None => Nil
      }
    })
    addedLines.toList
  }

  def patchToLineChange(patch: String, commit: Commit,commentsRegex: Pattern, importRegex: Pattern, returnKeyword: Pattern, semicolon: Pattern): List[LineChange] = {
    c = c + 1
    // filter out binary diffs
    val patchFiles = PatchParser.splitPatchByFiles(patch).getOrElse(List()).flatMap{
      case _: BinaryFile => None
      case diff: DiffFile => Some(diff)
    }
    val addedLines = collection.mutable.MutableList[LineChange]()

    patchFiles.foreach(patchFile => {
      val hunks = PatchParser.splitPatchByHunks(patchFile.patch)
      hunks match {
        case Some(hunkList) => hunkList.size match {
          case 0 => Nil
          case _ => hunkList.foreach(x => {
            try{
              val hunkLines = x.patch.split("\r?\n").toList.drop(2)
              val hunkInfo = hunkLines.head
              val rangeHunk = stringToRangeHunk(hunkInfo)
              //              println(hunkInfo)
              //              println(rangeHunk)
              val hunkText = hunkLines.tail.filter(h => h.startsWith("+") && !h.startsWith("++") && !h.matches("""\+\s*""")).filterNot(x => commentsRegex.matcher(x).matches() || importRegex.matcher(x).matches() ||  returnKeyword.matcher(x).matches()|| semicolon.matcher(x).matches())
              //              println(hunkText.size)
              addedLines.++=(hunkText.map(x => LineChange(commit, rangeHunk, x.substring(1).trim, patchFile.filename)))
            }
            catch {
              case a: java.lang.StringIndexOutOfBoundsException => println("Error: " + x + "\n" + a.getMessage)
              ////              case a: Exception => sys.error("Error: " + x + "\n" + a.getMessage)
              //              case a: Exception => sys.error("Error: " + patch + "\n" + a.getMessage)
              case ex: java.lang.NumberFormatException => println("Error: " + x + "\n" + ex.getMessage)
            }

          })
        }
        case None => Nil
      }
    })
    addedLines.toList
  }

}
