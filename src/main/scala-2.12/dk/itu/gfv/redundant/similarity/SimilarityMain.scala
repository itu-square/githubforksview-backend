package dk.itu.gfv.redundant.similarity

import dk.itu.gfv.redundant.model.LineChangeModel
import LineChangeModel.LineChange
import dk.itu.gfv.evaluation.setup.LevenshteinDistance
import info.debatty.java.stringsimilarity._

/**
  * Created by scas on 19-09-2016.
  */
object SimilarityMain {

  case class Sim(simscore: Double, linechange: LineChange)
  case class ParsedFunctionSim(simscore: Double, linechange: LineChange)

  def checkSimilarityOfStrings(str1: String, str2: String) = {
    val nl = new NormalizedLevenshtein;
    nl.distance(str1,str2)
  }

  def checkSimilarityOfStrings(str1: String, str2: String, alg: String) = alg match{
    case "levenshtein" => new Levenshtein().distance(str1,str2)
    case "n-levenshtein" => LevenshteinDistance.similarityNormalized(str1,str2)//new NormalizedLevenshtein().similarity(str1,str2)
    case "ngram" => new NGram().distance(str1,str2)
    case "cosine" => new Cosine().similarity(str1,str2)
    case "jaro-winkler" => new JaroWinkler().similarity(str1,str2)
    case "jaccard" => new Jaccard().similarity(str1,str2)

  }

}
