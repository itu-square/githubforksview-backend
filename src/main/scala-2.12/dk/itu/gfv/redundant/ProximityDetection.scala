package dk.itu.gfv.redundant

import dk.itu.gfv.redundant.model.LineChangeModel.RangeHunk

/**
  * Created by scas on 9/8/2016.
  */
object ProximityDetection {

    //@@ -127,11 +127,12 @@ private function etc

  def getHunksLines(patch: String): List[Int] = {
    patch.split("\n").toList.filter(x => x.startsWith("@@ ")) map (x => Integer.parseInt(x.split(" ")(1).split(",")(0)))
  }

  def proximityChange(lineNumber: Int, rangeHunk: RangeHunk): Boolean = {
    lineNumber >= rangeHunk.start && lineNumber <= rangeHunk.end + rangeHunk.linesAfterChange
  }

  def findSimilarLocation(texteditorLine: Int, patchLine: List[Int], range: Int) = {
    patchLine.filter(x => {
      ((texteditorLine <= (x + range)) && (texteditorLine >= (x - range)))
    })
  }
}


