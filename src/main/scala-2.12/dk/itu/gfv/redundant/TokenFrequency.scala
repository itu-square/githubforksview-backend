package dk.itu.gfv.redundant

import java.io.File

import dk.itu.gfv.util.Util
import dk.itu.gfv.util.Util._

import scala.collection.immutable.WrappedString

/**
  * Created by scas on 9/30/2016.
  */
object TokenFrequency {

  case class LineFrequency(line: String, numberOfAppearances: Int) {
    override def toString = line + "," + numberOfAppearances
  }

  val fileExtensions = readFromFileLines("file-extensions-corrected.txt")

  def linesFrequency(projectPath: String): List[LineFrequency] = {
    val files = Util.files(projectPath, true)
    val filteredFilesByExtension = files.filter(x => fileExtensions.contains(fileExtension(x)))
    val lines = filteredFilesByExtension.par
      .map(x => Util.readFromFileLines(x))
      .flatten
      .map(x => x.trim)
      .toList
    lines.groupBy(identity).par.map(x => x._1 -> x._2.size).toList.sortWith(_._2 > _._2).map(x => LineFrequency(x._1, x._2))
  }

}
