package dk.itu.gfv.redundant

import java.io.{FileWriter, File}
import java.util.regex.Pattern

import dk.itu.gfv.util.ProjectsPaths
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevCommit
//import com.roundeights.hasher.Implicits._
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import scala.language.postfixOps
import scala.collection.JavaConversions._
import scala.sys.process.{ProcessLogger, Process}
import info.debatty.java.stringsimilarity._
;
/**
  * Created by scas on 17-08-2016.
  */
object Backend extends ProjectsPaths{

  val mainrepo = repodir + "scas-mdd-redundant-test"
  val hashesFile = repodir + "/hashes.txt"
//  val mainRepository = Git.open(new File(mainrepo))
  val forks = List("mining-repos1-redundant-test","mining-repositories-redundant-test")
  val whitespaces = Pattern.compile("""^\s*|\r?\n$""").matcher("")

  case class Hash(hash: String)
  case class HashData(repository: String, sha: String, hash: Hash)

//  def hashLine(input: String): Hash = Hash(input.sha1)

  def run(in: String, folder: String): (List[String], List[String], Int) = {
    var out = List[String]()
    var err = List[String]()
    val process = Process(in, new File(folder))
    val exit = process ! ProcessLogger((s) => out ::= s, (s) => err ::= s)
    (out.reverse, err.reverse, exit)
  }

  def readHashes = scala.io.Source.fromFile(hashesFile).getLines.toList map (x => {
    val s = x.split(",")
    HashData(s(0),s(1),Hash(s(2)))
  })

  def checkBySameHash(str: String) = {
    val result = new StringBuilder()
    val hashes = readHashes
//    hashes.foreach(x => {
//      if (x.hash.hash.equals(hashLine(str).hash))
//        result.append("This code --" + str + " -- exists already in repo: " +  x.repository + " at commit " + x.sha + "\n")
//    })
    val resultString = result.toString
    if(resultString.isEmpty)
      "False"
    else
      resultString
  }

  def checkSimilarityOfStrings(str1: String, str2: String) = {
    val nl = new NormalizedLevenshtein;
    nl.distance(str1,str2)
  }

  def checkSimilarityOfStrings(str1: String, str2: String, alg: String) = alg match{
    case "levenshtein" => new Levenshtein().distance(str1,str2)
    case "n-levenshtein" => new NormalizedLevenshtein().similarity(str1,str2)
    case "ngram" => new NGram().distance(str1,str2)
    case "cosine" => new Cosine().similarity(str1,str2)
    case "jaro-winkler" => new JaroWinkler().similarity(str1,str2)
    case "jaccard" => new Jaccard().similarity(str1,str2)

  }

}
