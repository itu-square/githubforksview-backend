package dk.itu.gfv.redundant.functionparsers

/**
  * Created by scas on 07-09-2016.
  */
trait GeneralLanguageAST {

  trait Parameter {
    val name: String
    val typeName: Option[String]
  }
  trait FunctionDef {
    val name: String
    val parameters: List[Parameter]
  }

}
