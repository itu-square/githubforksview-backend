package dk.itu.gfv.redundant.functionparsers

/**
  * Created by scas on 08-09-2016.
  */
object ParseLine  extends GeneralLanguageAST{

  def parseLine(src: String, language: String)= language match {
    case "java" => JavaParser.parseJavaCode(src)
    case "js" => JavascriptParser.parseJavascriptCode(src)
    case "php" => PHPParser.parsePhPCode(src)
    case "py" => PythonParser.parsePythonCode(src)
    case "scala" => ScalaParser.parseScalaCode(src)
//    case "txt" => {
//      println(src + ";" + language)
//      JavaParser.parseJavaCode(src)
//    }
    case _ => None
  }
}
