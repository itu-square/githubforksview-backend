package dk.itu.gfv.redundant.functionparsers

import org.parboiled2._
import scala.util.Failure
import scala.util.Success
/**
  * Created by scas on 07-09-2016.
  */
object JavaParser extends GeneralLanguageAST{

  case class GenParameter(name: String, typeName: Option[String]) extends Parameter
  case class FunctionDefJava(modifier: Option[String], returnType: Option[String], name: String, parameters: List[Parameter]) extends FunctionDef
  case class FunctionCall(obj: String, functionCall: String)

  class LanguageParser(val input: ParserInput) extends Parser {

    def whitespace = rule {" " | "  "}

    def startRule: Rule1[FunctionDefJava] = rule {
      inputLine ~ EOI
    }

    def inputLine = rule {
      zeroOrMore(whitespace) ~ capture(modifier) ~ oneOrMore(whitespace) ~ optional("static" ~ oneOrMore(whitespace)) ~ zeroOrMore(whitespace) ~ returnType ~ zeroOrMore(whitespace) ~ funName ~ zeroOrMore(whitespace) ~ funParameters ~ zeroOrMore(whitespace) ~ optional("{") ~>
        ((a: String, returnType: Option[String], name: String, parameters: List[Parameter] ) => FunctionDefJava(Some(a), returnType, name, parameters))
    }

    def returnType = rule {
      returnSimpleType | returnArray | returnParametricType
    }

    def returnSimpleType = rule {capture(oneOrMore(!(" "|"(") ~ ANY)) ~> ((a: String) => Some(a))}
    def returnArray = rule {
      zeroOrMore(whitespace) ~ capture(oneOrMore(!("]") ~ ANY) ~ "]") ~> ((a: String) => Some(a))
    }
    def returnParametricType = rule {
      zeroOrMore(whitespace) ~ capture(oneOrMore(!(">") ~ ANY) ~ ">") ~> ((a: String) => Some(a))
    }

    def modifier = rule {
      "public" | "protected" | "private"
    }
    def funName = rule {
      capture(oneOrMore(!("(") ~ ANY))
    }

    def funParameters = rule {
      noParameters | parameters
    }

    def parameters = rule {
       "(" ~ zeroOrMore(whitespace) ~ parameter.+(",") ~ zeroOrMore(whitespace) ~ ")" ~ zeroOrMore(whitespace) ~> ((a: Seq[Parameter]) => a.toList)
    }

    def noParameters = rule {
      capture("(" ~ zeroOrMore(whitespace) ~ ")") ~> ((x: String) => List[Parameter]())
    }

    def parameter = rule {
      normalTypeParam | arrayParam
    }

    def arrayParam = rule { zeroOrMore(whitespace) ~ capture(oneOrMore(!("[") ~ ANY)) ~ "[" ~ zeroOrMore(whitespace) ~ "]" ~ capture(oneOrMore(!(","|")") ~ ANY))  ~> ((a: String, b: String) => GenParameter(b,Some(a)))}
    def genTypeParam = rule { zeroOrMore(whitespace) ~ capture(oneOrMore(!(">") ~ ANY)) ~  capture(">") ~ zeroOrMore(whitespace) ~ capture(oneOrMore(!(","| ")") ~ ANY)) ~> ((a: String, b: String, c: String) => GenParameter(c,Some(a+b)))}
    def normalTypeParam = rule {zeroOrMore(whitespace) ~ capture (oneOrMore(!" " ~ ANY)) ~ zeroOrMore(whitespace) ~ capture(oneOrMore(!(","|")") ~ ANY)) ~> ((a: String, b: String) => GenParameter(b,Some(a)))}

  }

  def parseJavaCode(src: String) =
    new LanguageParser(src).startRule.run() match {
      case Success(e) => Some(e)//Right(e)
//      case Failure(e: ParseError) => sys.error(e.format("", new ErrorFormatter(showTraces = true)))
      case Failure(e) => None//sys.error(e.getMessage); None
    }
}
