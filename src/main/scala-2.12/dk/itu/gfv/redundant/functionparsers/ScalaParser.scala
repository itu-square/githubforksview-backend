package dk.itu.gfv.redundant.functionparsers

import org.parboiled2._

import scala.util.{Failure, Success}
/**
  * Created by scas on 9/6/2016.
  */
object ScalaParser  extends GeneralLanguageAST{

  // TODO
  // Add support for generic type parametrization
  // def func [A,T] (a: A, b: T)
  case class GenParameter(name: String, typeName: Option[String]) extends Parameter
  case class FunctionDefScala(modifier: Option[String], genType: String, returnType: Option[String], name: String, parameters: List[Parameter]) extends FunctionDef
  case class FunctionCallScala(obj: String, functionCall: String)

  class ScalaParser(val input: ParserInput) extends Parser {

    def startRule: Rule1[FunctionDefScala] = rule {
      inputLine ~ EOI
    }

    def inputLine = rule {
      zeroOrMore(whitespace) ~ optional(capture(modifier) ~ oneOrMore(whitespace)) ~ zeroOrMore(whitespace) ~ "def" ~ oneOrMore(whitespace) ~ funName ~ zeroOrMore(whitespace) ~ optional(funParameters) ~ zeroOrMore(whitespace) ~ optional(returnType) ~ zeroOrMore(whitespace) ~ optional( zeroOrMore(ANY))  ~>
        ((a: Option[String], name: String, parameters: Option[List[Parameter]], returnType: Option[String]) => FunctionDefScala(a, "", returnType, name, parameters match {case Some(x) => x; case None => List[GenParameter]()} ))
    }

    def returnType = rule {
      ":" ~ zeroOrMore(whitespace) ~ capture(zeroOrMore(!("=" | "{" | " " | EOI) ~ ANY)) ~> ((a: String) => a)
    }

    def modifier = rule {
      "override" | "protected" | "private" | "protected"
    }

    def whitespace = rule {
      " " | "  "
    }

    def funName = rule {
      capture(zeroOrMore(!("(" | ":" | " ") ~ ANY))
    }

    def funParameters = rule {
      noParameters | parameters
    }

    def parameters = rule {
      "(" ~ zeroOrMore(whitespace) ~ (parameter.+(",")) ~ zeroOrMore(whitespace) ~ ")" ~ zeroOrMore(whitespace) ~> ((a: Seq[Parameter]) => a.toList)
    }

    def noParameters = rule {
      capture(("(" ~ zeroOrMore(whitespace) ~ ")")) ~> ((x: String) => List[Parameter]())
    }

    def parameter = rule {
      optional("implicit" ~ oneOrMore(whitespace)) ~ zeroOrMore(whitespace) ~ capture(oneOrMore(!(":") ~ ANY)) ~ zeroOrMore(whitespace) ~ ":" ~ zeroOrMore(whitespace) ~ capture(oneOrMore(!("," | ")") ~ ANY)) ~> ((a: String, b: String) => GenParameter(a, Some(b)))
    }

    def chars = rule {
      (CharPredicate.Visible)
    }
  }

  def parseScalaCode(src: String) =
    new ScalaParser(src).startRule.run() match {
      case Success(e: FunctionDef) => Some(e)//Right(e)
//      case Failure(e: ParseError) => println(e.format("", new ErrorFormatter(showTraces = true))); None
      case Failure(e) => None//sys.error(e.getMessage)
    }
}