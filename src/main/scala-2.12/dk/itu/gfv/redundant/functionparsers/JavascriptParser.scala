package dk.itu.gfv.redundant.functionparsers

import org.parboiled2._

import scala.util.{Failure, Success}

/**
  * Created by scas on 9/7/2016.
  */
object JavascriptParser  extends GeneralLanguageAST {

    case class GenParameter(name: String, typeName: Option[String]) extends Parameter
    case class FunctionDefJavascript(name: String, parameters: List[Parameter]) extends FunctionDef

    class LanguageParser(val input: ParserInput) extends Parser {

      def startRule: Rule1[FunctionDefJavascript] = rule {
        inputLine ~ EOI
      }

      def whitespace = rule {
        " " | "  "
      }

      def inputLine = rule {
        zeroOrMore(whitespace) ~ "function" ~ zeroOrMore(whitespace) ~ funName ~ zeroOrMore(whitespace) ~ optional(funParameters) ~ zeroOrMore(whitespace) ~ optional("{") ~>
          ((name: String, parameters: Option[List[Parameter]]) => FunctionDefJavascript(name, parameters match {
            case Some(x) => x
            case None => List[Parameter]()
          }))
      }

      def funName = rule {
        capture(zeroOrMore(!("(" | ":") ~ ANY)) ~> ((x: String) => x)
      }

      def funParameters = rule {
        noParameters | parameters
      }

      def parameters = rule {
        "(" ~ zeroOrMore(whitespace) ~ parameter.+(",") ~ zeroOrMore(whitespace) ~ ")" ~ zeroOrMore(whitespace) ~> ((a: Seq[Parameter]) => a.toList)
      }

      def noParameters = rule {
        capture("(" ~ zeroOrMore(whitespace) ~ ")") ~> ((x: String) => List[Parameter]())
      }

      def parameter = rule {
        noTypeParam
      }

      def noTypeParam = rule {
        capture(oneOrMore(!("," | ")") ~ ANY)) ~> ((a: String) => GenParameter(a, None))
      }
    }

    def parseJavascriptCode(src: String) =
      new LanguageParser(src).startRule.run() match {
        case Success(e) => Some(e)//Right(e)
//        case Failure(e: ParseError) => sys.error(e.format("", new ErrorFormatter(showTraces = true)))
        case Failure(e) => None//sys.error(e.getMessage)
      }

}
