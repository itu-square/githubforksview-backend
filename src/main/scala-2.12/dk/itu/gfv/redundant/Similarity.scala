package dk.itu.gfv.redundant

import dk.itu.gfv.redundant.functionparsers.ParseLine
import dk.itu.gfv.redundant.similarity.SimilarityMain
import dk.itu.gfv.repo.RepositoryOperations.AddedLine2
import dk.itu.gfv.util.Util._

/**
  * Created by scas on 09-01-2017.
  */
trait Similarity {

  case class SimResult(score: Double, addedLine: AddedLine2) {
    override def toString = {
      score + "--" + addedLine
    }
  }

  type CommitID = String
  type CommitFile = String
  type Files = Map[CommitFile, Vector[AddedLine2]]

  case class RndCommit(project: String, sha: String, rndAddedLines: Files, forksAddedCommits: Map[CommitID, Files])

  case class SimScore(line: String, file: String, scores: Vector[SimResult])

  case class FunctionDefScore(line: String, file: String, scores: Vector[SimResult])

  case class ProximityScore(line: String, file: String, locations: Vector[AddedLine2])

  case class SimAndProxScore(line: String, file: String, scores: Vector[SimResult])

  case class SimAlgorithm(alg: String, threshold: List[Double])

  case class CommitResult(sha: String, commitAddedLines: Int, simscore: Vector[SimScore], functiondefscore: Vector[FunctionDefScore], proximityscore: Vector[ProximityScore], simandproxscore: Vector[SimAndProxScore])

  case class ProjectResult(projectAddedLines: Int, projectWarnings: Int, results: Vector[CommitResult])

  case class NeighbouringLines(lines: Map[String, Vector[AddedLine2]])

  case class ProjectPatches(rndPatch: Vector[AddedLine2], forksAddedLines: Vector[AddedLine2])

  case class CommitFiles(files: Map[String, Vector[AddedLine2]])

  // filename -> added lines in that file
  case class ProjectCommits(commits: Map[String, Map[String, Vector[AddedLine2]]])

  /**
    * Compute similarity of the replayed lines with the lines added in all the forks before our replayed change
    *
    * @param addedLines - the lines added in the commits in the forks
    * @param lines      - the lines from the patch we're trying to replay
    * @return
    */
  def computeSimilarity(addedLines: Vector[AddedLine2], lines: Vector[AddedLine2], alg: String): Vector[SimScore] = {
    val r = collection.mutable.ListBuffer[SimScore]()
    lines.par.foreach(line => {
      val simscore = addedLines.par.map(x => SimResult(SimilarityMain.checkSimilarityOfStrings(line.normalizedText, x.normalizedText, alg), x)).toVector
      r.+=(SimScore(line.originalText, line.file, simscore))
    })

    r.toVector
  }

  def computeFunctionDef(addedLines: Vector[AddedLine2], lines: Vector[AddedLine2], alg: String) = {
    (for {
      line <- lines
      p = (ParseLine.parseLine(line.originalText, fileExtension(line.file)) match {
        case Some(x) => {
          Some(FunctionDefScore(line.originalText, line.file,
            // compute the similarity of function definitions
            addedLines.map(addedLine => {
              ParseLine.parseLine(addedLine.originalText, fileExtension(addedLine.file)) match {
                case Some(functionDef) => List(SimResult(SimilarityMain.checkSimilarityOfStrings(x.name, functionDef.name, alg), addedLine))
                case None => List()
              }
            }).flatten.toVector))
        }
        case None => None
      })
    } yield p).flatten.map(x => FunctionDefScore(x.line, x.file, x.scores))
  }

  def computeProximity(addedLines: Vector[AddedLine2], lines: Vector[AddedLine2]): Vector[ProximityScore] = {
    val r = collection.mutable.ListBuffer[ProximityScore]()
    lines.par.foreach(line => {
      val sameFileChanges = addedLines.filter(_.file.equals(line.file)) // only take changes from same file
      val sameLocationChanges = sameFileChanges.par.filter(_.position == line.position)
      r.+=(ProximityScore(line.originalText, line.file, sameLocationChanges.toVector))
    })
    r.toVector
  }

  def computeSimilarityAndProximity(addedLines: Vector[AddedLine2], lines: Vector[AddedLine2], alg: String): Vector[SimAndProxScore] = {
    val r = collection.mutable.ListBuffer[SimAndProxScore]()
    lines.par.foreach(line => {
      val sameFileChanges = addedLines.filter(_.file.equals(line.file)) // only take changes from same file
      val sameLocationChanges = sameFileChanges.filter(_.position == line.position)
      val res = sameLocationChanges.par.map(x => SimResult(SimilarityMain.checkSimilarityOfStrings(line.normalizedText, x.normalizedText, alg), x)).toVector
      r.+=(SimAndProxScore(line.originalText, line.file, res))
    })
    r.toVector
  }
}
