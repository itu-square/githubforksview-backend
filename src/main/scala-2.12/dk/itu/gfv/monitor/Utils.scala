package dk.itu.gfv.monitor

import dk.itu.gfv.monitor.db.Models.{CommitParent, Duplicate, Project}
import dk.itu.gfv.monitor.db.NightFetcher.CachedETAG
import dk.itu.gfv.monitor.db.SimilarityDB
import dk.itu.gfv.util.MonitorProperties
import spray.json._
import spray.json.DefaultJsonProtocol._
/**
  * Created by scas on 15-04-2017.
  */
trait Utils extends SimilarityDB with MonitorProperties{

  val duplicatesTableSize = 5

  def commitTimeToInt(commitTime: Long) = (commitTime/1000L).toInt

  def epoch_to_date(commit_time: Int) = new org.joda.time.DateTime(commit_time.toLong * 1000L)

  def language_to_extension_name (language: String) = language match {
    case "JavaScript" => "js"
    case "Java" => "java"
    case "C++" => "cpp"
    case "Python" => "py"
    case "Jupyter Notebook" => "ipynb"
  }


  def transformSimResultstoDuplicates(res: Vector[Result], main_project: Project) = {
    res.flatMap(r => {
      r match {
        case a: SimScore => a.scores.map(b => Duplicate(0, main_project.id, a.addedLineID,b.addedLine, b.score, "line_sim", None, None, None, None, None, ""))
        case a: FunctionDefScore => a.scores.map(b => Duplicate(0, main_project.id, a.addedLineID,b.addedLine, b.score, "func_def_sim", None, None, None, None, None, ""))
        case a: SimAndProxScore => a.scores.map(b => Duplicate(0, main_project.id, a.addedLineID,b.addedLine, b.score, "line_sim_prox", None, None, None, None, None, ""))
      }
    })
  }

  def sameCommitParents(commitDBParent: Seq[CommitParent], gitCommitParents: Map[String,Seq[CommitParent]]): Boolean = {
    val c = commitDBParent.groupBy(_.sha)
    val sameKeySet = gitCommitParents.keySet.intersect(c.keySet).nonEmpty && (gitCommitParents.keySet.diff(c.keySet).isEmpty)
    val sameValues = gitCommitParents.forall(x => {
      c.get(x._1) match {
        case Some(v) => v.toSet == x._2.toSet
        case None => false
      }
    })
    sameKeySet && sameValues
  }

}
