package dk.itu.gfv.monitor

import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.monitor.db.Models.Project
import dk.itu.gfv.monitor.db.NightFetcher._

import scala.concurrent.Await
import scala.concurrent.duration.Duration
/**
  * Created by scas on 20-04-2017.
  */
object FixLanguages extends App with Ssh{


  val main_projects = Await.result(getMainProjects, Duration.Inf)

  main_projects.foreach(project => {
    val forks = Await.result(getForksOfProject(project.id),Duration.Inf)

    forks.foreach(fork => {
      Await.result(updateProject(Project(fork.id,fork.name,fork.owner,project.language, fork.default_branch,fork.fork,fork.fork_of,fork.fork_of_main, fork.forks_count,fork.stargazers_count, fork.subscribers_count, fork.created_at,fork.pushed_at,fork.updated_at)), Duration.Inf)
    })
  })

  ssh match {
    case Some(x) => x.disconnect()
    case None => Unit
  }
}
