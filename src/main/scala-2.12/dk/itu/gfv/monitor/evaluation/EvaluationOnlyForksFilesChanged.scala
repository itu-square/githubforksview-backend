package dk.itu.gfv.monitor.evaluation

import better.files.File
import dk.itu.gfv.monitor.Ssh
import dk.itu.gfv.monitor.db.Models.{FilesModified, FilesModifiedMap}
import dk.itu.gfv.monitor.db.NightFetcher._

import scala.collection.immutable.HashSet
import scala.concurrent.Await
import scala.concurrent.duration.Duration

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by scas on 27-04-2017.
  */
object EvaluationOnlyForksFilesChanged extends Ssh {

  val partition_size = 3000

  def getAddedLines(commits: Seq[String]): Seq[(Int,String,String)] = {

    if(commits.size > partition_size){
      logger.info("partitioning")
      val commits_partitioned = commits.grouped(partition_size)
      commits_partitioned.zipWithIndex.map(x => {logger.info(s"getting added lines for partition ${x._2}" );Await.result(getAddedLinesByCommitsFiltered(x._1),Duration.Inf)}).flatten.toSeq
    }
    else
      Await.result(getAddedLinesByCommitsFiltered(commits),Duration.Inf)
  }

  /*
    This evaluation aims at only searching for commits in forks done by the
    fork's owner. This aims at solving a problem where some merges messed up
    some project's history and same change is now under different commit SHAs
    see the case of https://github.com/petkaantonov/bluebird/commit/b1b1d806333f4148303538258bebeadc928a60c2
    https://github.com/yourcelf/bluebird/commit/5edd9d9e38ec3811e174c98c8bd6660a152f40a8

    Another issue lies in the fact that forks often use different branches to experiment with code or keep
    variants of the project. Therefore, we do keep a list of commits in a fork and we shall not compare
    a commit to commits of that owner in the fork, to avoid getting false positives
   */


  /*
    Find the files that were changed in non-merge commits from a fork.
    The main idea is to see there are indeed many changes to one file, which will lead us to
    believe that there is a potential for duplication, as a file changes often in other forks
   */

  def files_changed(project: String)(implicit commit_non_outliers: HashSet[String]) = {
    logger.info(s"analyzing $project")
    //    val main_project = Await.result(getProjectByName(project), Duration.Inf).get
    //    val main_project_commits = Await.result(getProjectCommits(project), Duration.Inf).distinct
    logger.info("getting forks")
    val forks = getForksOfProjectIdAndNameOnly(project)
    val all_projects = forks.toMap
    logger.info("getting forks' commits") // get project commits and remove the large outliers, commits that have more than 1000 LOC added
    val forks_commits_db = forks.map(x => x._2 -> Await.result(getProjectCommits(x._2), Duration.Inf).filter(x => commit_non_outliers.contains(x.sha))).toMap
    logger.info("getting forks") // remove the large outliers, commits that have more than 1000 LOC added
    val all_commits = forks_commits_db.values.flatten.toSeq.filter(x => commit_non_outliers.contains(x.sha))
    //    logger.info("getting added lines")
    //    val project_all_added_lines_db = Await.result(getAddedLinesByCommits(all_commits.map(_.sha)), Duration.Inf)

    logger.info("getting commit parents")
    val commits_parents = Await.result(getCommitsParents(all_commits.map(_.sha)), Duration.Inf).groupBy(_.sha)
    logger.info("" + commits_parents.size)
    val non_merge_commits = commits_parents.filterNot(x => x._2.size > 1)
    logger.info("non merge commits " + non_merge_commits.size)
    val forks_commits = forks_commits_db.map(x => x._1 -> x._2.filter(x => non_merge_commits.get(x.sha).isDefined))
    logger.info("forks commits: " + forks_commits.values.flatten.size)

    logger.info("there are " + all_commits.size + " commits in this project")
    logger.info("getting all added lines of this project")
    logger.info("getting added lines")
    val project_all_added_lines_db = getAddedLines(forks_commits.map(_._2).flatten.map(_.sha).toSeq)
    logger.info("grouping by commit")
    val project_all_added_lines = project_all_added_lines_db.groupBy(_._2) //group by commit
    logger.info("done")
    /*
        For every commit in every fork, let's try to find if it introduced code that existed in commits from other forks
     */
    forks_commits.keySet.foreach(fork => {

      val commits = forks_commits.get(fork).get

      commits.filter(x => commit_non_outliers.contains(x.sha)).foreach(commit => {
        logger.info("fork: " + fork + " commit: " + commit.sha)
        // we need to get all the commits of forks except this author's commits and the commits that were done previously to this commit
        val other_forks_commits = forks_commits.values.flatten.toSeq.filterNot(x => ((x.author == commit.author) || (x.committer == commit.committer))).filter(_.authorTime < commit.authorTime)
        logger.info("commits in forks " + other_forks_commits.size)
        //        val commits = (Seq(commit) ++ other_forks_commits).map(_.sha)
        logger.info("getting changed files")
        val files_changed_commit = project_all_added_lines.get(commit.sha).getOrElse(Seq()).map(_._3).distinct
        val files_changed_in_other_forks_commits = other_forks_commits.par.map(x => x -> project_all_added_lines.get(x.sha).getOrElse(Seq()).map(_._3).distinct).seq.toMap

        // find which files in this commit have been modified in previous commits as well
        files_changed_commit.foreach(file => {
          val commits_that_modified_this_file = files_changed_in_other_forks_commits.filter(x => x._2.contains(file))
          val modified_file = FilesModified(0,fork, commit.sha,file, commit.authorTime.toInt, commit.author,  s"https://github.com/$fork/commit/${commit.sha}")
          val data_to_insert = insertModifiedFile(modified_file).flatMap(id => {
            val commit_result =  commits_that_modified_this_file.map(x => {
              val project_name = all_projects.get(project_all_added_lines.get(x._1.sha).get.head._1).get
              FilesModifiedMap(0,project_name, x._1.sha, x._2.filter(_.equals(file)).head, x._1.authorTime.toInt, x._1.author, s"https://github.com/${project_name}/commit/${x._1.sha}", id.id)
            }).toSeq
            insertModifiedFileMap(commit_result)
          })
          Await.result(data_to_insert, Duration.Inf)
        })
      })

    })
  }

  def main(args: Array[String]): Unit = {
    try {
      val projects = File("retrospective-analysis.txt").lines.toList
//      implicit val commits_non_outliers = new HashSet[String]().++(Await.result(getNonOutlierCommitsSHA,Duration.Inf))
//      projects.foreach(files_changed(_))
//      val f = getModifiedFilesMap()
      ssh match {
        case Some(x) => x.disconnect()
        case None => Unit
      }
    } catch {
      case ex: java.lang.ExceptionInInitializerError => logger.info("exception")
      case ex: java.net.BindException => logger.info("already binded")
      //      case ex: Exception => logger.info("" + ex.getMessage)
    }
  }
}
