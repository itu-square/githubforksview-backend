package dk.itu.gfv.monitor.evaluation

import java.util.Random

import dk.itu.gfv.monitor.Ssh
import dk.itu.gfv.monitor.db.Models.{Commit, Project}
import dk.itu.gfv.monitor.db.NightFetcher._

import scala.concurrent.Await
import scala.concurrent.duration.Duration
/**
  * Created by scas on 11-05-2017.
  */
object StatisticsFileChanged extends Ssh with App {

  type Author = Int
  type File = Int


  def authorsFile = {
    val authorsAndFiles = Await.result(getAuthorAndFilesMap, Duration.Inf)

    // for each commit and a file modified in that commit, I have the number of forks that modified this file.
    println(s"${authorsAndFiles.groupBy(_._2).size} commits and files modified in the sample")
    val commits_and_files_modified = authorsAndFiles.groupBy(_._2)
    // I can now for example, see how many unique forks have modified the file
    val fileModifiedByDistinctForks = commits_and_files_modified.map(x => x._1 -> x._2.distinct.size)
    val avg = fileModifiedByDistinctForks.values.sum/fileModifiedByDistinctForks.values.size
    println("****")
    println(s"on average $avg forks modify the same file on a commit that is not on the mainline")

    val fileModifiedByDistinctForks1 = Await.result(getRepoAndFilesMap, Duration.Inf).groupBy(_._2).map(x => x._1 -> x._2.distinct.size)
    val avg1 = fileModifiedByDistinctForks1.values.sum/fileModifiedByDistinctForks.values.size
    println("****")
    println(s"on average $avg1 forks modify the same file on a commit that is not on the mainline")
  }

  def eval = {
    val filesModified = Await.result(getFilesModified, Duration.Inf)
    val forksFilesChanged =  Await.result(getFilesModifiedMap, Duration.Inf)

  }

  def eval1 = {
    val projects = Await.result(getMainProjects, Duration.Inf)
    type ProjectID = Int
    type ProjectCommits = Map[ProjectID, Seq[Commit]]
    val projectsMap = Await.result(getAllProjects, Duration.Inf).map(x => x.id -> x)
    val mainCommits: ProjectCommits = projects.map(x => x.id -> Await.result(getMultipleProjectsCommits(Seq(x.id)), Duration.Inf)).toMap

    val forksCommits = {
      projects.map(x => {
        val forks = Await.result(getForksOfProject(x.id), Duration.Inf)
        // get the forks' commits that are only on those forks
        val commits = forks.map(x => x -> Await.result(getMultipleProjectsCommits(Seq(x.id)), Duration.Inf).diff(mainCommits.get(x.fork_of_main).get)).toMap
        commits
      }).flatten.toMap
    }

    val forksAddedLines = forksCommits.map(x => x._1 -> x._2.flatMap(x => Await.result(getAddedLinesBySHA(x.sha), Duration.Inf)).toSeq)

    val modifiedFilesMap = forksAddedLines.values.flatten.groupBy(_.filename)
    val modifiedFilesInForks = modifiedFilesMap.map(x => x._1 -> x._2.toSeq.map(_.project).size)





  }

  def testGral = {
    import java.awt.BasicStroke;
    import java.awt.BorderLayout;
    import java.awt.Color;
    import java.awt.geom.Ellipse2D;
    import java.awt.geom.Rectangle2D;
    import java.io.IOException;

    import javax.swing.JFrame;

    import de.erichseifert.gral.data.DataSeries;
    import de.erichseifert.gral.data.DataTable;
    import de.erichseifert.gral.plots.BarPlot;
    import de.erichseifert.gral.plots.PlotArea;
    import de.erichseifert.gral.plots.XYPlot;
    import de.erichseifert.gral.plots.axes.AxisRenderer;
    import de.erichseifert.gral.plots.points.PointRenderer;
    import de.erichseifert.gral.ui.InteractivePanel;

    val data = new DataTable()
//    val data = new DataTable(Double.getClass, Double.getClass, Double.getClass)

    val POINT_COUNT = 1000
    val rand = new Random()
    var i = 0
    while ( {
      i < POINT_COUNT
    }) {
      val x = rand.nextGaussian
      val y1 = rand.nextGaussian + x
      val y2 = rand.nextGaussian - x
      data.add(x, y1, y2)

      {
        i += 1; i - 1
      }
    }
    import de.erichseifert.gral.data.DataSeries
    import de.erichseifert.gral.plots.XYPlot
    val series1 = new DataSeries("Series 1", data, 0, 1)
    val series2 = new DataSeries("Series 2", data, 0, 2)
    val plot = new XYPlot(series1, series2)
  }


  ssh match {
    case Some(x) => x.disconnect()
    case None => Unit
  }

}
