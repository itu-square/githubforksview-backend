package dk.itu.gfv.monitor.evaluation

import dk.itu.gfv.monitor.db.NightFetcher._

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import better.files.File
import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.monitor.server.SshConnect
import dk.itu.gfv.util.Util

/**
  * Created by scas on 07-04-2017.
  */
object Evaluation extends LazyLogging {

  private val eval_folder_name = "gms-retrospective-evaluation"
  private val eval_file = "analysis.txt"

  def process(project: String) = {
    logger.info("analyzing " + project)
    val result_folder = File(eval_folder_name + "/" + project)
    result_folder.createIfNotExists(true, true)
    val result_file = File(eval_folder_name + "/" + project + "/" + eval_file)
    result_file.createIfNotExists()
    result_file.clear()
    val result_string = new StringBuilder()


    def sb_add(str: String) = result_file.append(str + "\n")


    /*
  1. Get all commits that are on forks but not in the main project.
  2. Each of this commit is a candidate
  3. For each commit, find all the commits to be compared to.
  4.

 */

    val main_project_commits = Await.result(getProjectCommits(project), Duration.Inf).distinct
    val forks = getForksOfProjectOnly(project)

    val forks_commits = Await.result(getMultipleProjectsCommits(forks.map(_.id)), Duration.Inf).distinct
    logger.info("main commits: " + main_project_commits.size + "; forks commits: " + forks_commits.size)

    Util.printToFile(new java.io.File("res-main.txt"))(fp => main_project_commits.foreach(fp.println))
    Util.printToFile(new java.io.File("res-forks.txt"))(fp => forks_commits.foreach(fp.println))

    val all_commits = (main_project_commits ++ forks_commits).distinct
    val all_commits_map = all_commits.map(x => x.sha -> x).toMap
    logger.info("there are " + all_commits.size + " commits in this project")
    logger.info("getting all added lines of this project")
    val project_all_added_lines = Await.result(getAddedLinesByCommits(all_commits.map(_.sha)), Duration.Inf).groupBy(_.commit)
    logger.info("done")

    val commits_in_forks_only = forks_commits.map(_.sha).diff(main_project_commits.map(_.sha)).distinct
    logger.info("there are " + commits_in_forks_only.size + " commits that exist only in forks")
    val commits_to_analyze = all_commits.filter(x => commits_in_forks_only.contains(x.sha))
    logger.info("commits to analyze " + commits_to_analyze.size)
    val commits_parents = Await.result(getCommitsParents(all_commits.map(_.sha)), Duration.Inf).groupBy(_.sha).map(x => x._1 -> x._2.map(_.parent)).toMap
    logger.info("found commits parents")
//    val commits_in_forks = commits_to_analyze.map(commit => commit -> find_commits(commits_parents, all_commits, commit.sha, commit.commit_time)).toMap
    val commits_in_forks = commits_to_analyze.map(commit => commit -> find_commits(commits_parents, all_commits, commit.sha, commit.authorTime.toInt)).toMap
    logger.info("found commits only in forks")
    logger.info("getting added lines for each commit")

    commits_in_forks.keySet.take(4).foreach(commit => {
      logger.info("analyzing commit: " + commit.sha)
      val added_line_in_this_commit = project_all_added_lines.get(commit.sha).get
      logger.info("got added lines in this commit: " + added_line_in_this_commit.size)
      val added_lines_in_forks_commits = project_all_added_lines.filterKeys(x => commits_in_forks.get(commit).get.contains(x)).values.flatten
      logger.info("forks' commits: " + commits_in_forks.get(commit).get.size + "; added_lines: " + added_lines_in_forks_commits.size)

      /*val commits_to_analyze_with_addedLines = commits_to_analyze.map(c => c -> Await.result(getAddedLinesBySHA(c.sha),Duration.Inf)).toMap
      logger.info("done")
      commits_to_analyze_with_addedLines.keySet.foreach(commit => {

        val al = commits_to_analyze_with_addedLines.get(commit).get
        val added_lines_db = Await.result(getAddedLinesByCommits(commits_in_forks.toSeq), Duration.Inf)
        logger.info("got added lines in the forks' commits")
      */
      sb_add("=================")
      sb_add("project: " +forks.find(_.id == project_all_added_lines.get(commit.sha).get.head.project).get.name)
      sb_add("commit: " + commit.sha)
      sb_add("subject: " + commit.message)
      sb_add("timestamp: " + new org.joda.time.DateTime(all_commits_map.get(commit.sha).get.authorTime))

      val res = computeSimilarities(added_line_in_this_commit.toVector, added_lines_in_forks_commits.toVector, "n-levenshtein", 0.8)
//
//      res.foreach(result => {
//        result match {
//
//          case x: SimScore => {
//            sb_add("  @@added line in this commit@@")
//            sb_add("  filename: " + x.addedLineID.filename)
//            sb_add("  " + x.addedLineID.line_number + ":" + x.addedLineID.text)
//            x.scores.foreach(score => {
//              sb_add("    ************SIM-SCORE**************")
//              sb_add("    project: " + forks.filter(_.id == score.addedLine.project).head.name)
//              sb_add("    commit: " + score.addedLine.commit)
//              sb_add("    subject: " + all_commits_map.get(score.addedLine.commit).get.message)
//              sb_add("    timestamp: " + new org.joda.time.DateTime(all_commits.find(_.sha == score.addedLine.commit).get.authorTime))
//              sb_add("    filename: " + score.addedLine.filename)
//              sb_add("    score: " + score.score)
//              sb_add("    " + score.addedLine.line_number + ":" + score.addedLine.text)
//            })
//          }
//          case x: FunctionDefScore => {
//            sb_add("  @@added line in this commit@@")
//            sb_add("  " + x.line.line_number + ":" + x.line.text)
//            x.scores.foreach(score => {
//              sb_add("    ************FUNCTION-DEF-SCORE**************")
//              sb_add("    project: " + forks.filter(_.id == score.addedLine.project).head.name)
//              sb_add("    commit: " + score.addedLine.commit)
//              sb_add("    subject: " + all_commits_map.get(score.addedLine.commit).get.message)
//              sb_add("    timestamp: " + new org.joda.time.DateTime(all_commits.find(_.sha == score.addedLine.commit).get.authorTime))
//              sb_add("    filename: " + score.addedLine.filename)
//              sb_add("    score: " + score.score)
//              sb_add("    " +score.addedLine.line_number + ": " + score.addedLine.text)
//
//            })
//          }
//          case x: SimAndProxScore => {
//            sb_add("  @@added line in this commit@@")
//            sb_add("  " + x.line.line_number + ":" + x.line.text)
//            x.scores.foreach(score => {
//              sb_add("    ************SIM-AND-PROX-SCORE**************")
//              sb_add("    project: " + forks.filter(_.id == score.addedLine.project).head.name)
//              sb_add("    commit: " + score.addedLine.commit)
//              sb_add("    subject: " + all_commits_map.get(score.addedLine.commit).get.message)
//              sb_add("    timestamp: " + new org.joda.time.DateTime(all_commits.find(_.sha == score.addedLine.commit).get.authorTime))
//              sb_add("    filename: " + score.addedLine.filename)
//              sb_add("    score: " + score.score)
//              sb_add("    " +score.addedLine.line_number + ": " + score.addedLine.text)
//            })
//          }
//        }
//      })
    })

//    result_file.write(result_string.toString)
  }


  def main(args: Array[String]): Unit = {
    try {
      val ssh = SshConnect.ssh_connect
    } catch {
      case ex: java.lang.ExceptionInInitializerError => logger.info("exception")
      case ex: java.net.BindException => logger.info("already binded")
      case ex: Exception => logger.info("shit")
    }
    val projects = File("retrospective-analysis.txt").lines.toList
    projects.take(1).foreach(process(_))

  }

}
