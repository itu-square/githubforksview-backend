package dk.itu.gfv.monitor.evaluation

import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.monitor.db.Models.{Commit, CommitOutlier}
import dk.itu.gfv.monitor.db.NightFetcher._
import dk.itu.gfv.monitor.server.SshConnect
import dk.itu.gfv.util.MonitorProperties

import scala.concurrent.Await
import scala.concurrent.duration.Duration
/**
  * Created by scas on 24-04-2017.
  */
object CommitOutliersAnalysis extends LazyLogging with MonitorProperties{

  def insert_data (project: String, commits: Seq[Commit]) = {
    logger.info(commits.size + " commits ")
    logger.info("getting added lines")
    val added_lines = Await.result(getAddedLinesSHAByCommits(commits.map(_.sha)), Duration.Inf)
    logger.info("grouping by commit")
    val commit_to_added_lines = added_lines.groupBy(_._1)
    logger.info("done...mapping to commit outlier objects")
    val insert_data = commit_to_added_lines.par.map(x => CommitOutlier(x._1, project,x._2.head._2, x._2.size)).toSeq.seq
    insertCommitOutliersJDBC(insert_data)
  }

  def analyze_1(project: String) = {

    val project_db = Await.result(getProjectByName(project),Duration.Inf)
    val forks_db = Await.result(getForksOfProject(project_db.get.id),Duration.Inf)

    logger.info("getting main_commits")
    val main_commits = Await.result(getProjectCommits(project_db.get.name),Duration.Inf)
    insert_data(project,main_commits)

    forks_db.foreach(fork => {
      logger.info("getting commits of " + fork.name)
      val fork_commits = Await.result(getProjectCommits(fork.name),Duration.Inf)
      insert_data(project,fork_commits)
    })
  }

  def analyze(project: String) = {
    logger.info(s"commit outliers for $project")
//    val insertData = getProjectAllAddedLines1(project).flatMap(added_lines => {
//      val commit_to_added_lines = added_lines.groupBy(_.commit)
//        insertCommitOutliers(commit_to_added_lines.par.map(x => CommitOutlier(0, project,x._2.head.project,x._1, x._2.size)).toSeq.seq)
//    })
    logger.info("getting added lines")
    val all_added_lines = Await.result(getProjectAndForksAllAddedLines1(project),Duration.Inf)

    logger.info(s"done... ${all_added_lines.size} added_lines. Now grouping by commits")
    val commit_to_added_lines = all_added_lines.groupBy(_.commit)
    val size = commit_to_added_lines.size

    if(size > 5000){
      logger.info("grouping into smaller lists: " + size / 5000)
      val partitions = commit_to_added_lines.grouped(size / 5000)
      logger.info("done...")
      partitions.zipWithIndex.foreach{ case (value,index) => {
        logger.info("mapping into outlier objects partition " + index)
        val insert_data = value.par.map(x => CommitOutlier(x._1, project,x._2.head.project, x._2.size)).toSeq.seq
        logger.info("done...inserting data in db")
        insertCommitOutliersJDBC(insert_data)
      }}
    }
    else {
      logger.info("done...mapping to commit outlier objects")
      val insert_data = commit_to_added_lines.par.map(x => CommitOutlier(x._1, project,x._2.head.project, x._2.size)).toSeq.seq
      logger.info("done...inserting data in db")
      insertCommitOutliersJDBC(insert_data)
    }


//    Await.result(insertData, Duration.Inf)
  }

  def main(args: Array[String]): Unit = {
    try {
      val ssh = local_run match {
        case "false" => Some(SshConnect.ssh_connect)
        case _ => None
      }
      val projects = Await.result(getProjectsThatAreNotForks, Duration.Inf)
      projects.foreach(x => analyze_1(x.name))
//      analyze("maureengarda/Chemotaxis")
//      analyze_1("nltk/nltk")
      ssh match {
        case Some(x) => x.disconnect()
        case None => Unit
      }
    } catch {
      case ex: java.lang.ExceptionInInitializerError => logger.info("exception")
      case ex: java.net.BindException => logger.info("already binded")
      //      case ex: Exception => logger.info("" + ex.getMessage)
    }
  }
}
