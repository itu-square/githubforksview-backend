package dk.itu.gfv.monitor.evaluation

import better.files.File
import dk.itu.gfv.monitor.Ssh
import dk.itu.gfv.monitor.db.Models._
import dk.itu.gfv.monitor.db.NightFetcher._

import scala.collection.immutable.HashSet
import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by scas on 07-04-2017.
  */
object EvaluationOnlyForkDuplicates extends Ssh {


  def process(project: String)(implicit commits_non_outliers: HashSet[String]) = {
    logger.info("analyzing " + project)

    /*
  1. Get all commits that are on forks but not in the main project.
  2. Each of this commit is a candidate
  3. For each commit, find all the commits to be compared to.
  4.

  */
    val main_project = Await.result(getProjectByName(project), Duration.Inf).get
    val forks = getForksOfProjectOnly(project)

    val forks_commits_db = forks.map(x => x.name -> Await.result(getProjectCommits(x.name), Duration.Inf).filter(x => commits_non_outliers.contains(x.sha))).toMap
    val forks_commits1 = forks_commits_db

//    val all_commits = (main_project_commits_map ++ forks_commits1)
//    val all_commits_map = all_commits.values.flatten.toSeq.map(x => x.sha -> x).toMap
    val all_commits_map = forks_commits_db.values.flatten.map(x => x.sha -> x).toMap
    val commits_parents = Await.result(getCommitsParents(forks_commits1.values.flatten.map(_.sha).toSeq), Duration.Inf).groupBy(_.sha)
    val non_merge_commits = commits_parents.filterNot(x => x._2.size > 1)
    val forks_commits = forks_commits1.map(x => x._1 -> x._2.filter(c => non_merge_commits.get(c.sha).isDefined))
    logger.info("there are " + forks_commits.size + " commits in this project")
    logger.info("getting all added lines of this project")

    val project_all_added_lines_db = Await.result(getAddedLinesByCommits(forks_commits.values.flatten.map(_.sha).toSeq), Duration.Inf).filterNot(x => x.text_normalized.equals(""""use strict";"""))
    val project_all_added_lines = project_all_added_lines_db.groupBy(_.commit)
    val project_all_added_lines_map = project_all_added_lines_db.map(x => x.id -> x).toMap
    logger.info("done")

    // [Commit, Seq[AddedLines]]
    val commits_added_lines = project_all_added_lines.map(x => all_commits_map.get(x._1).get -> x._2)

    /*
        For every commit in every fork, let's try to find if it introduced code that existed in commits from other forks
     */

    forks_commits.keySet.foreach(fork => {
      val commits = forks_commits.get(fork).getOrElse(Seq())
      commits.foreach(commit => {
        logger.info("fork: " + fork + " commit: " + commit.sha)
        val added_line_in_this_commit = project_all_added_lines.get(commit.sha).getOrElse(Seq())
        if (added_line_in_this_commit.nonEmpty) {
          // we need to get all the commits of forks except this author's commits
          val other_forks_commits = forks_commits.values.flatten.toSeq.filterNot(x => ((x.author == commit.author) || (x.committer == commit.committer))).filter(_.authorTime < commit.authorTime)

          val added_lines_in_forks_commits = project_all_added_lines.filterKeys(x => other_forks_commits.map(_.sha).contains(x)).values.flatten
          logger.info("added lines: " + added_line_in_this_commit.size + " forks' added lines: " + added_lines_in_forks_commits.size)

          val res = computeSimilarities(added_line_in_this_commit.toVector, added_lines_in_forks_commits.toVector, "n-levenshtein", 0.8)
          val sim_results_for_db = transformSimResultstoDuplicates(res, main_project)

          /*
             Try to remove those duplicates that are on the same commits that have different SHAs.
           */

          val sim_results = sim_results_for_db.filterNot(x => {
            val base_line_added_line = project_all_added_lines_map.get(x.base_line).get
            val base_line_commit = all_commits_map.get(base_line_added_line.commit).get
            val base_line_commit.authorTime = base_line_commit.authorTime

            val duplicate_added_line = project_all_added_lines_map.get(x.duplicate_line).get
            val duplicate_commit = all_commits_map.get(duplicate_added_line.commit).get

            (base_line_added_line.filename == duplicate_added_line.filename && (base_line_commit.author == duplicate_commit.author || base_line_commit.committer == duplicate_commit.committer) && base_line_commit.message == duplicate_commit.message && base_line_commit.authorTime == duplicate_commit.authorTime
              && commits_added_lines.get(base_line_commit).getOrElse(Seq()).size == commits_added_lines.get(duplicate_commit).getOrElse(Seq()).size) || (base_line_added_line.filename == duplicate_added_line.filename && (base_line_commit.author == duplicate_commit.author || base_line_commit.committer == duplicate_commit.committer)
              && base_line_commit.message == duplicate_commit.message && commits_added_lines.get(base_line_commit).getOrElse(Seq()).size == commits_added_lines.get(duplicate_commit).getOrElse(Seq()).size)
          })
          logger.info("inserting similarity results for " + project + ", " + sim_results_for_db.size)
          val duplicates_inserted = insertSimilarityResultsForRetrospectiveAnalysis(sim_results)
          Await.result(duplicates_inserted, Duration.Inf)

        }
      })
    })
  }


  def main(args: Array[String]): Unit = {
    try {
      val projects = File("retrospective-analysis.txt").lines.toList
      implicit val nonOutliersCommits = new HashSet[String]().++(Await.result(getNonOutlierCommitsSHA,Duration.Inf))
      projects.grouped(4).foreach(x => x.par.foreach(process(_)))
      ssh match {
        case Some(x) => x.disconnect()
        case None => Unit
      }
    } catch {
      case ex: java.lang.ExceptionInInitializerError => logger.info("exception")
      case ex: java.net.BindException => logger.info("already binded")
//      case ex: Exception => logger.info("" + ex.getMessage)
    }
  }

}
