<script type = "text/javascript" >
var windowURL = window.location.href


    $("#pages").change(function () {
        $("#pages option:selected").each(updatePageTable($(this).val()));
    });

$("#next").click(function () {
    console.log($(this).val())
    $('#pages option:selected').next().prop('selected', 'selected');
    updatePageTable($(this).val())

});

$("#prev").click(function () {
    $('#pages option:selected').prev().prop('selected', 'selected');
    updatePageTable($(this).val())
});

function updatePageTable(from) {
    var xhr = new XMLHttpRequest();

    var reqURL = windowURL + "&dir=next&from=" + from;
    console.log(reqURL);
    xhr.open("GET", reqURL, true);
    xhr.setRequestHeader("Content-Type", "text/plain");
    xhr.responseType = 'text/plain';
    xhr.onprogress = function () {
        console.log('LOADING', xhr.status);
    };
    xhr.onload = function () {
        console.log('DONE', xhr.status);
    };
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // console.log(xhr.responseText);
            $('#my_table > tbody').replaceWith(xhr.responseText);
            $("#next").prop('value', (from - 5).toString());
            $("#prev").prop('value', (parseInt(from) + 5).toString());
        }
    }
}


$("input[id^='notUseful']:checkbox").change(function () {
    var value = $(this).val();
    var notUseful = $(this).prop('checked');
    console.log("Change: " + value + " to " + notUseful);
    var json_obj = {"id": value, "notUseful": notUseful}
    console.log(JSON.stringify(json_obj))
    sendUsefulData(json_obj)


});

$("input[id^='useful']:checkbox").change(function () {

    var value = $(this).val();
    var useful = $(this).prop('checked');
    console.log("Change: " + value + " to " + useful);
    var json_obj = {"id": value, "useful": useful}
    console.log(JSON.stringify(json_obj))
    sendUsefulData(json_obj)
});

function sendUsefulData(jsonObj) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/duplicates", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.responseType = 'text/html';
    xhr.onprogress = function () {
        console.log('LOADING', xhr.status);
    };

    xhr.onload = function () {
        console.log('DONE', xhr.status);
    };
    xhr.send(JSON.stringify(jsonObj));

}


$(".details_button").click(function () {

    var id = $(this).prop("id");

    var div_id = document.getElementById("div_" + id);

    if (div_id != null) {
        div_id.style.display = div_id.style.display == "none" ? "block" : "none";
    }
    else {
        console.log(div_id);

        var value = $(this).val();
        var value_split = value.split(",");

        console.log(value_split[0]);
        console.log(value_split[1]);
        var parent = $(this).parent();
        var project = value_split[0];
        var commit = value_split[1];
        var filename = value_split[2];
        var line_number = value_split[3];

        var xhr = new XMLHttpRequest();
        xhr.open("GET", "http://localhost:8080/duplicate_context?project=" + project + "&commit=" + commit + "&filename=" + filename + "&line_number=" + line_number, true);
        xhr.setRequestHeader("Content-Type", "text/plain");
        xhr.responseType = 'text/plain';
        xhr.onprogress = function () {
            console.log('LOADING', xhr.status);
        };
        xhr.onload = function () {
            console.log('DONE', xhr.status);
        };
        xhr.send();

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                // $('#successup').html(xhr.responseText);
                parent.append('<div id="div_' + id + '" style="display:yes">' + xhr.responseText + '</div>');
            }
        }
    }


});

$(document).on('mouseenter', ".long_td", function () {
    var $this = $(this);
    if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
        $this.tooltip({
            title: $this.text(),
            placement: "bottom"
        });
        $this.tooltip('show');
    }
});

// $(document).ready(function() {
//     $('#my_table').DataTable({
//         "lengthMenu": [ 5, 7, 10, 25, 50, 75, 100 ]
//     });
// });

</script >