package dk.itu.gfv.monitor.evaluation

import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.monitor.Utils
import dk.itu.gfv.monitor.db.NightFetcher._
import better.files.File
import dk.itu.gfv.monitor.db.Models.Duplicate

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scalaj.http.Http
import spray.json.DefaultJsonProtocol._
import spray.json._

/**
  * Created by scas on 15-04-2017.
  */
object GenerateHTML extends LazyLogging with Utils {


  case class Pagination(currentPage: Int, startIDCurrentPage: Int, endIDCurrentPage: Int, pageNumberInNavigation: Int)


  var c = 1

  val entriesCountMap = collection.mutable.HashMap[String, Int]()

  val header = File("src/main/scala-2.12/dk/itu/gfv/monitor/evaluation/header.html")
  val static_code_begin = "<html>" + "\n" + header.contentAsString + "\n" +
    """<body>
      |
      |<table id="my_table" class="main_table">
      |
      |	<thead>
      |	<th>ID</th>
      |	<th>Score</th>
      |	<th>Type</th>
      |	<th>U</th>
      | <th>NU</th>
      | <th>Link</th>
      |	<th></th>
      |	<th></th>
      |	</thead>
      |	<tbody>""".stripMargin

  val static_table_end =
      "</tbody>\n</table>"
  val static_code_end = "</body>" + File("src/main/scala-2.12/dk/itu/gfv/monitor/evaluation/functions.js").contentAsString + "\n" + "</html>"

  final case class ProjectInfo(project: String, commit: String, file: String, subject: String, timestamp: String, textline: String, line_number: Int)

  final case class DuplicateInfo(id: Int, score: Double, sim_type: String, useful: Option[Boolean], notUseful: Option[Boolean], base_project: ProjectInfo, duplicate_project: ProjectInfo)

  implicit val projectInfo = jsonFormat7(ProjectInfo)
  implicit val duplicateInfo = jsonFormat7(DuplicateInfo)


  val inner_table_begin =
    """<table class="inner_table">
      |					<thead>
      |					<th>Project</th>
      |					<th>Commit</th>
      |         <th>File</th>
      |					<th>Subject</th>
      |					<th>Timestamp</th>
      |
      |					</thead>
      |
      |					<tbody>""".stripMargin


  val inner_table_end =
    """     </tr>
      |       </tbody>
      |				  </table>
      |				  </td>
      |			   </tr>
      |
      |
      |		</tbody>
      |
      |	</table>
      |	</td>""".stripMargin

  def pretty_code(code: String) = {
    val req = Http.apply("http://hilite.me/api?code=" + code).asString
    req.code match {
      case 200 => req.body
      case _ => code
    }
  }

  def make_row(duplicate: DuplicateInfo) = {
    val sb = new StringBuilder()

    def add(str: String) = sb.append(str + "\n")


    def make_inner_table(project_info: ProjectInfo) = {
      val sb = new StringBuilder()
      sb.append(
        """<table class="inner_table">
          |					<thead>
          |					<th>Project</th>
          |					<th>Commit</th>
          |         <th>File</th>
          |					<th>Subject</th>
          |					<th>Timestamp</th>
          |
          |					</thead>
          |
          |					<tbody>""".stripMargin + "\n")

      sb.append("     <tr>\n")
      sb.append("     <td width=100><div  class=\"long_td\">" + project_info.project + "</div></td>\n")
      sb.append("     <td width=20 ><div class=\"long_td\">" + """<a href="https://github.com/""" + project_info.project + "/commit/" + project_info.commit + """" target="_blank" >""" + project_info.commit + """</a><div></td>""" + "\n")
      sb.append("     <td width=50 ><div class=\"long_td\">" + project_info.file + "</div></td>\n")
      sb.append("     <td width=100><div class=\"long_td\">" + project_info.subject.trim + "</div></td>\n")
      sb.append("     <td width=50 ><div class=\"long_td\">" + project_info.timestamp + "</div></td>\n")
      sb.append(
        """</tr>
          |       </tbody>
          |				  </table>""".stripMargin + "\n")

      sb.toString
    }

    def make_td(project_info: ProjectInfo, context: Int) = {
      add("   <td>")
      add("""   <table class="base_line">""")
      add("       <tbody>")
      add("""        <tr><td class="long_td">""")
      add(project_info.line_number + ":" + project_info.textline + """<button class="details_button" id="context_""" + c + "_" + context + """" value="""" + project_info.project + "," + project_info.commit + "," + project_info.file + "," + project_info.line_number + "\">Details</button>")

      add("     </td></tr>")
      add("     <tr>")
      add("      <td>")
      add(make_inner_table(project_info))
      add("   </td>")
      add("   </tr>")
      add("   </tbody>")
      add("   </table>")
      add("   </td>")
    }

    val useful = duplicate.useful match {
      case Some(x) if x == true => {
        """<input type="checkbox" id="useful" checked value="""" + duplicate.id + "\">"
      }
      case _ => {
        """<input type="checkbox" id="useful" value = """" + duplicate.id + "\">"
      }
    }

    val notUseful = duplicate.notUseful match {
      case Some(x) if x == true => {
        """<input type="checkbox" id="notUseful" checked value="""" + duplicate.id + "\">"
      }
      case _ => {
        """<input type="checkbox" id="notUseful" value = """" + duplicate.id + "\">"
      }
    }
    add("   <tr>")
    add("   <td width=10>" + duplicate.id + "</td>")
    add("   <td width=10>" + duplicate.score.toString.take(4) + "</td>")
    add("   <td width=10>" + duplicate.sim_type + "</td>")
    add("   <td width=5>" + useful + "</td>")
    add("   <td width=5>" + notUseful + "</td>")
    add("   <td width=5>" + "" + "</td>")

    make_td(duplicate.base_project, 1)
    make_td(duplicate.duplicate_project, 2)
    add("</tr>")
    c = c + 1
    sb.toString
  }

  def generate_html_local = {
    val result = better.files.File("duplicates_local.txt").contentAsString.parseJson.convertTo[Seq[DuplicateInfo]]
    val sb = new StringBuilder()
    sb.append(static_code_begin + "\n")
    result.foreach(r => sb.append(make_row(r) + "\n"))
    sb.append(static_code_end)
    sb.toString
  }

  def generate_project_html1(top: String, project: String, sha: String = "", dir: Option[String], startFrom: Option[String]) = {
    generate_initial_html(top, project, sha, dir, startFrom)
  }


  def generateButtons(firstID: Int, lastID: Int) = {
    val sb = new StringBuilder()
    sb.append(
      s"""<div id="buttons">
         |<button id="prev" value="$lastID">Prev</button>
         |<select id="pages">""".stripMargin + "\n")
    val nextButton = s"""<button id="next" value="${lastID - 5}">Next</button>"""
    var startingPoint = lastID
    var counter = 1
    sb.append(s"""<option value="$startingPoint" selected="selected">1</option>""" + "\n")
    startingPoint = startingPoint - duplicatesTableSize
    logger.info(s"Starting: $firstID" + s" ending:$lastID")
    while (startingPoint > firstID) {
      logger.info(s"starting point: $startingPoint")
      counter = counter + 1
      sb.append(s"""<option value="$startingPoint">$counter</option>""" + "\n")
      startingPoint = startingPoint - duplicatesTableSize

    }
    if (startingPoint < firstID && startingPoint > 1) {
      sb.append(s"""<option value="$firstID">${counter + 1}</option>""" + "\n")
    }
    sb.append("</select>")
    sb.append(nextButton + "\n </div>")
    sb.toString
  }

  def generateDuplicateReport(start: String, end: Option[String]) = {
    logger.info("fetching duplicates")
    val all_duplicates = Await.result(getDuplicateById(start.toInt), Duration.Inf)

    logger.info("" + all_duplicates)
    logger.info("done")

    val result = generateDuplicatesInfos(all_duplicates, None)
    logger.info("making HTML")
    val sb = new StringBuilder()
    sb.append(static_code_begin + "\n")

    logger.info("making rows")
    result.foreach(r => sb.append(make_row(r) + "\n"))
    logger.info("adding end of html")
    sb.append(static_table_end)
//    sb.append(generateButtons(firstDuplicateID,lastDuplicateID) +"\n")
    sb.append(static_code_end)
    sb.toString
  }

  def generate_initial_html(top: String, project: String, sha: String = "", dir: Option[String], startFrom: Option[String]) = {

    val duplicates = Await.result(getDuplicates(project), Duration.Inf)
    val firstDuplicateID = duplicates.head.id
    val lastDuplicateID = duplicates.last.id

    val projectsLastRowID = lastDuplicateID

    logger.info("" + projectsLastRowID)
    val nrOfRows = top match {
      case "true" => Some(5)
      case "false" => None
    }
    logger.info("fetching duplicates")
    val all_duplicates = duplicates.reverse.take(duplicatesTableSize)

    logger.info("" + all_duplicates)
    logger.info("done")

    val result = generateDuplicatesInfos(all_duplicates, nrOfRows)
    logger.info("making HTML")
    val sb = new StringBuilder()
    sb.append(static_code_begin + "\n")

    logger.info("making rows")
    result.foreach(r => sb.append(make_row(r) + "\n"))
    logger.info("adding end of html")
    sb.append(static_table_end)
    sb.append(generateButtons(firstDuplicateID,lastDuplicateID) +"\n")
    sb.append(static_code_end)
    sb.toString
  }


  def getPage(top: String, project: String, sha: String = "", dir: String, startFrom: String): String = {
    val projectsLastRowID = Integer.parseInt(startFrom)

    logger.info("" + projectsLastRowID)
    val nrOfRows = top match {
      case "true" => Some(5)
      case "false" => None
    }
    logger.info("fetching duplicates")
    val all_duplicates = {
      dir match {
        case "next" => {
          if (projectsLastRowID > 1) {
            Await.result(getNextDuplicatesOfProject(project, projectsLastRowID, duplicatesTableSize), Duration.Inf)
          }
          else {
            Await.result(getNextDuplicatesOfProject(project, 1, duplicatesTableSize), Duration.Inf)
          }
        }
        case "prev" => {
          val greaterThanID = if (projectsLastRowID > 5) projectsLastRowID - duplicatesTableSize else 0
          Await.result(getPrevDuplicatesOfProject(project, greaterThanID, projectsLastRowID, duplicatesTableSize), Duration.Inf)
        }
      }
    }

    logger.info("" + all_duplicates)
    logger.info("done")
    logger.info("making HTML")
    val sb = new StringBuilder()
    logger.info("making rows")
    sb.append("<tbody>\n")
    val result = generateDuplicatesInfos(all_duplicates, nrOfRows)
    result.foreach(r => sb.append(make_row(r) + "\n"))
    sb.append("<\tbody>\n")
    sb.toString
  }

  def generateDuplicatesInfos(all_duplicates: Seq[Duplicate], nrOfRows: Option[Int]) = {
    logger.info("getting ids of added_lines")
    val all_added_lines_ids = all_duplicates.map(_.duplicate_line) ++ all_duplicates.map(_.base_line)
    logger.info("" + all_added_lines_ids.size + " added_lines ")
    logger.info("getting added lines")


    // TODO FILTER BY COMMITS
    val all_added_Lines_db = {
      if (all_added_lines_ids.size > 2000) {
//        sha match {
//          case _ =>
            all_added_lines_ids.grouped(2000).map(x => Await.result(getAddedLineByIds(x), Duration.Inf)).flatten.toSeq
//          case sha => all_added_lines_ids.grouped(2000).map(x => Await.result(getAddedLinesOfCommitByIds(x, sha), Duration.Inf)).flatten.toSeq
//        }

      }
      else {
//        sha match {
//          case _ =>
            Await.result(getAddedLineByIds(all_added_lines_ids), Duration.Inf)
//          case sha => Await.result(getAddedLinesOfCommitByIds(all_added_lines_ids, sha), Duration.Inf)
//        }

      }
    }

    logger.info("mapping added_lines' ids to themselves")
    val all_added_lines_map = all_added_Lines_db.map(x => x.id -> x).toMap
    logger.info("getting all commits")
    val commits = all_added_Lines_db.map(_.commit) //.toSet.filter(x => x.equalsIgnoreCase(sha)).toSeq

    logger.info("" + commits.size + " commits ")
    logger.info("getting all commits from db")
    val all_commits = Await.result(getCommitsBySHA(commits), Duration.Inf)
    logger.info("mapping commits' sha to commits")
    val all_commits_map = all_commits.map(x => x.sha -> x).toMap
    logger.info("getting all project ids")
    val all_projects_ids = all_added_Lines_db.map(_.project)
    logger.info("getting all projects")
    val all_projects_db = Await.result(getProjectByIds(all_projects_ids), Duration.Inf)
    logger.info("mapping all projects' ids to themselves")
    val all_projects_map = all_projects_db.map(x => x.id -> x).toMap

    logger.info("creating project infos")
    val project_infos = all_added_lines_map.par.map(x => {
      x._1 -> {
        val added_line = x._2
        val project = all_projects_map.get(added_line.project).get
        val commit = all_commits_map.get(added_line.commit).get
        ProjectInfo(project.name,
          commit.sha.take(7),
          added_line.filename,
          commit.message,
          epoch_to_date(commit.authorTime.toInt).toString,
          added_line.text_normalized,
          added_line.line_number)
      }
    }).seq
    logger.info("creating duplication info")
    val filtered_duplicates = nrOfRows match {
      case Some(nr) => all_duplicates.groupBy(_.base_line).map(x => x._1 -> x._2.sortWith(_.score > _.score).take(nr)).values.flatten.toSeq
      case None => all_duplicates
    }
    val result = filtered_duplicates.par.map(d => {
      val id = d.id
      val score = d.score
      val sim_type = d.duplicate_type
      val base_project = project_infos.get(d.base_line).get
      val duplicate_project = project_infos.get(d.duplicate_line).get
      DuplicateInfo(id, score, sim_type, d.internalUseful, d.internalNotUseful, base_project, duplicate_project)
    }).seq
    result
  }


  def generate_project_html(top: String, project: String, sha: String = "") = {
    val nrOfRows = top match {
      case "true" => Some(5)
      case "false" => None
    }
    logger.info("fetching duplicates")
    val all_duplicates = project match {
      case "all" => Await.result(getDuplicates, Duration.Inf)
      case _ => Await.result(getDuplicates(project), Duration.Inf)
    }
    logger.info("done")

    logger.info("getting ids of added_lines")
    val all_added_lines_ids = all_duplicates.map(_.duplicate_line) ++ all_duplicates.map(_.base_line)
    logger.info("" + all_added_lines_ids.size + " added_lines ")
    logger.info("getting added lines")


    // TODO FILTER BY COMMITS
    val all_added_Lines_db = {
      if (all_added_lines_ids.size > 2000) {
        sha match {
          case _ => all_added_lines_ids.grouped(2000).map(x => Await.result(getAddedLineByIds(x), Duration.Inf)).flatten.toSeq
          case sha => all_added_lines_ids.grouped(2000).map(x => Await.result(getAddedLinesOfCommitByIds(x, sha), Duration.Inf)).flatten.toSeq
        }

      }
      else {
        sha match {
          case _ => Await.result(getAddedLineByIds(all_added_lines_ids), Duration.Inf)
          case sha => Await.result(getAddedLinesOfCommitByIds(all_added_lines_ids, sha), Duration.Inf)
        }

      }
    }
    logger.info("mapping added_lines' ids to themselves")
    val all_added_lines_map = all_added_Lines_db.map(x => x.id -> x).toMap
    logger.info("getting all commits")
    val commits = all_added_Lines_db.map(_.commit) //.toSet.filter(x => x.equalsIgnoreCase(sha)).toSeq

    logger.info("" + commits.size + " commits ")
    logger.info("getting all commits from db")
    val all_commits = Await.result(getCommitsBySHA(commits), Duration.Inf)
    logger.info("mapping commits' sha to commits")
    val all_commits_map = all_commits.map(x => x.sha -> x).toMap
    logger.info("getting all project ids")
    val all_projects_ids = all_added_Lines_db.map(_.project)
    logger.info("getting all projects")
    val all_projects_db = Await.result(getProjectByIds(all_projects_ids), Duration.Inf)
    logger.info("mapping all projects' ids to themselves")
    val all_projects_map = all_projects_db.map(x => x.id -> x).toMap

    logger.info("creating project infos")
    val project_infos = all_added_lines_map.par.map(x => {
      x._1 -> {
        val added_line = x._2
        val project = all_projects_map.get(added_line.project).get
        val commit = all_commits_map.get(added_line.commit).get
        ProjectInfo(project.name,
          commit.sha.take(7),
          added_line.filename,
          commit.message,
          epoch_to_date(commit.authorTime.toInt).toString,
          added_line.text_normalized,
          added_line.line_number)
      }
    }).seq
    logger.info("creating duplication info")
    val filtered_duplicates = nrOfRows match {
      case Some(nr) => all_duplicates.groupBy(_.base_line).map(x => x._1 -> x._2.sortWith(_.score > _.score).take(nr)).values.flatten.toSeq
      case None => all_duplicates
    }
    val result = filtered_duplicates.par.map(d => {
      val id = d.id
      val score = d.score
      val sim_type = d.duplicate_type
      val base_project = project_infos.get(d.base_line).get
      val duplicate_project = project_infos.get(d.duplicate_line).get
      DuplicateInfo(id, score, sim_type, d.internalUseful, d.internalNotUseful, base_project, duplicate_project)
    }).seq

    logger.info("making HTML")
    val sb = new StringBuilder()
    sb.append(static_code_begin + "\n")
    logger.info("making rows")
    result.foreach(r => sb.append(make_row(r) + "\n"))
    logger.info("adding end of html")
    sb.append(static_code_end)
    better.files.File("results.html").createIfNotExists().write(sb.toString)
    sb.toString
  }

}
