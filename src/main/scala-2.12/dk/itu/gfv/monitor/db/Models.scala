package dk.itu.gfv.monitor.db

import org.joda.time.DateTime

/**
  * Created by scas on 12/5/2016.
  */
object Models {

  case class Project(id: Int, name: String, owner: Int, language: String, default_branch: String, fork: Boolean, fork_of: Int, fork_of_main: Int, forks_count: Int, stargazers_count: Int, subscribers_count: Option[Int], created_at: DateTime, pushed_at: DateTime, updated_at: DateTime )

  // last_commit refers to the HEAD of the branch
  case class Branch(id: Int, name: String, last_commit: String, project: Int)

  case class BranchCommit(id: Int, branch: Int, commit: String) // a table for mapping commits to branches

  case class User(id: Int, login: String, name: String, email: String, created_at: DateTime)

  case class CommitParent(sha: String, parent: String)

  case class Commit(sha: String, author: Int, committer: Int, message: String, authorTime: Int, committerTime: Int)

//  case class AddedLine(id: Long, project: Long, branch: Long, commit: Long, line_number: Int, text: String, text_normalized: String)
  case class AddedLine(id: Int, project: Int, commit: String, filename: String, line_number: Int, text_normalized: String,text: String)
//  case class AddedLine(id: Long, commit: Long, filename: String, line_number: Int, text: String, text_normalized: String)

  // base line is the one that we analyze in our commit. duplicate line is the one that existed previously
  case class Duplicate(id: Int, project: Int, base_line: Int, duplicate_line: Int, score: Double, duplicate_type: String, internalUseful: Option[Boolean], internalNotUseful: Option[Boolean], sentToDev: Option[Boolean], devUseful: Option[Boolean], devUsefulComment: Option[String], devResponseTime: String)

//  case class Duplicate(id: Long, project: String, branch: String, commit_sha: String, commit: Long, added_line: Long, similar_line: Long, sim_type: String, sim_score: Option[Double])

  case class CommitOutlier(sha: String, project_main: String, repo_id: Int, added_lines_size: Int)

  case class FilesModified(id: Int, project: String, commit: String, file: String, commit_time: Int, author_id: Int, url: String)
  case class FilesModifiedMap(id: Int, project: String, commit: String, file: String, commit_time: Int, author_id: Int, url: String, files_modified_id: Int)

  case class Survey(id: Int, link: String, q1: String, q2: String, q3: String, q4: String)
}
