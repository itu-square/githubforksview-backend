package dk.itu.gfv.monitor.db

import java.util.concurrent.ConcurrentHashMap

import dk.itu.gfv.monitor.db.Models.AddedLine
import dk.itu.gfv.redundant.functionparsers.ParseLine
import dk.itu.gfv.redundant.similarity.SimilarityMain
import dk.itu.gfv.util.Util._

/**
  * Created by scas on 09-01-2017.
  */
trait SimilarityDB {



  abstract class Result(line: Int, scores: Vector[SimResult])


  case class SimResult(score: Double, addedLine: Int)

  case class SimScore(addedLineID: Int, scores: Vector[SimResult]) extends Result(addedLineID, scores)

  case class FunctionDefScore(addedLineID: Int,  scores: Vector[SimResult]) extends Result(addedLineID, scores)

  case class ProximityScore(addedLineID: Int, locations: Vector[SimResult]) extends Result(addedLineID, locations)

  case class SimAndProxScore(addedLineID: Int, scores: Vector[SimResult]) extends Result(addedLineID, scores)

  val caching = new ConcurrentHashMap[(String, String), Double]()



  /**
    * Compute similarity of the replayed lines with the lines added in all the forks before our replayed change
    *
    * @param commit_added_lines - the lines added in the commits in the forks
    * @param all_other_existing_lines      - the lines from the patch we're trying to replay
    * @return
    */
  def computeSimilarity1(commit_added_lines: Vector[AddedLine], all_other_existing_lines: Vector[AddedLine], alg: String): Vector[SimScore] = {
    val r = collection.mutable.ListBuffer[SimScore]()
    commit_added_lines.foreach(line => {
      val simscore = all_other_existing_lines.par.map(x => {
        caching.containsKey((line.text_normalized, x.text_normalized)) match {
          case false => {
            val result = SimilarityMain.checkSimilarityOfStrings(line.text_normalized, x.text_normalized, alg)
            caching.put((line.text_normalized, x.text_normalized), result)
            SimResult(result, x.id)
          }
          case true => SimResult(caching.get((line.text_normalized, x.text_normalized)),x.id)
        }
      }).toVector
      r.+=(SimScore(line.id, simscore))
    })

    r.toVector
  }


  /**
    * Compute similarity of the replayed lines with the lines added in all the forks before our replayed change
    *
    * @param commit_added_lines - the lines added in the commits in the forks
    * @param all_other_existing_lines      - the lines from the patch we're trying to replay
    * @return
    */
  def computeSimilarity(commit_added_lines: Vector[AddedLine], all_other_existing_lines: Vector[AddedLine], alg: String): Vector[SimScore] = {
    val r = collection.mutable.ListBuffer[SimScore]()
    commit_added_lines.foreach(line => {
      val simscore = all_other_existing_lines.par.map(x => SimResult(SimilarityMain.checkSimilarityOfStrings(line.text_normalized, x.text_normalized, alg), x.id)).toVector
      r.+=(SimScore(line.id, simscore))
    })

    r.toVector
  }

  def computeFunctionDef(addedLines: Vector[AddedLine], lines: Vector[AddedLine], alg: String) = {
    (for {
      line <- lines
      p = (ParseLine.parseLine(line.text, fileExtension(line.filename)) match {
        case Some(x) => {
          Some(FunctionDefScore(line.id,
            // compute the similarity of function definitions
            addedLines.map(addedLine => {
              ParseLine.parseLine(addedLine.text, fileExtension(addedLine.filename)) match {
                case Some(functionDef) => List(SimResult(SimilarityMain.checkSimilarityOfStrings(x.name, functionDef.name, alg), addedLine.id))
                case None => List()
              }
            }).flatten.toVector))
        }
        case None => None
      })
    } yield p).flatten.map(x => FunctionDefScore(x.addedLineID, x.scores))
  }

  def computeProximity(addedLines: Vector[AddedLine], lines: Vector[AddedLine]): Vector[ProximityScore] = {
    val r = collection.mutable.ListBuffer[ProximityScore]()
    lines.par.foreach(line => {
      val sameFileChanges = addedLines.filter(_.filename.equals(line.filename)) // only take changes from same file
      val sameLocationChanges = sameFileChanges.par.filter(_.line_number == line.line_number)
      r.+=(ProximityScore(line.id, sameLocationChanges.map(x => SimResult(0,x.id)).toVector))
    })
    r.toVector
  }

  def computeSimilarityAndProximity(addedLines: Vector[AddedLine], lines: Vector[AddedLine], alg: String): Vector[SimAndProxScore] = {
    val r = collection.mutable.ListBuffer[SimAndProxScore]()
    lines.par.foreach(line => {
      val sameFileChanges = addedLines.filter(_.filename.equals(line.filename)) // only take changes from same file
      val sameLocationChanges = sameFileChanges.filter(_.line_number == line.line_number)
      val res = sameLocationChanges.par.map(x => SimResult(SimilarityMain.checkSimilarityOfStrings(line.text_normalized, x.text_normalized, alg), x.id)).toVector
      r.+=(SimAndProxScore(line.id, res))
    })
    r.toVector
  }


  def computeSimilarities1(commit_added_lines: Vector[AddedLine], other_lines: Vector[AddedLine], alg: String, threhshold: Double): Vector[Result] = {
    val cs = computeSimilarity(commit_added_lines,other_lines, alg).filter(_.scores.size > 0).map(x => SimScore(x.addedLineID,x.scores.filter(_.score > threhshold))).filter(_.scores.size > 0)
    val cfd = computeFunctionDef(commit_added_lines, other_lines, alg).filter(_.scores.size > 0).map(x => FunctionDefScore(x.addedLineID, x.scores.filter(_.score > threhshold))).filter(_.scores.size > 0)
    val cps =  computeSimilarityAndProximity(commit_added_lines,other_lines, alg).filter(_.scores.size > 0).map(x => SimAndProxScore(x.addedLineID,x.scores.filter(_.score > threhshold))).filter(_.scores.size > 0)
    cs ++ cfd ++ cps
  }


  def computeSimilarities(commit_added_lines: Vector[AddedLine], other_lines: Vector[AddedLine], alg: String, threhshold: Double): Vector[Result] = {
    val cs = computeSimilarity(commit_added_lines,other_lines, alg).filter(_.scores.size > 0).map(x => SimScore(x.addedLineID,x.scores.filter(_.score > threhshold))).filter(_.scores.size > 0)
    val cfd = computeFunctionDef(commit_added_lines, other_lines, alg).filter(_.scores.size > 0).map(x => FunctionDefScore(x.addedLineID, x.scores.filter(_.score > threhshold))).filter(_.scores.size > 0)
    val cps =  computeSimilarityAndProximity(commit_added_lines,other_lines, alg).filter(_.scores.size > 0).map(x => SimAndProxScore(x.addedLineID,x.scores.filter(_.score > threhshold))).filter(_.scores.size > 0)
    cs ++ cfd ++ cps
  }
}
