package dk.itu.gfv.monitor.db

import better.files.{File => ScalaFile, _}
import dk.itu.gfv.monitor.server.SshConnect

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by scas on 05-01-2017.
  */
object InitialData extends DatabaseSchema with DBService {

  def insertInitialData: Unit = {
    // FIXME - make a proper properties file
    val projects = ScalaFile("projects.txt").lines.toList
    logger.info("inserting initial data")
    projects.par.foreach(insertGithubProject(_))
  }

  def main(args: Array[String]): Unit = {
//    logger.info("starting InitialData program")
    resetDb
//
//    logger.info("creating schemas...")
    val schemas = Await.result(createSchemaIfNotExists,Duration.Inf)
//    val session = SshConnect.ssh_connect

    insertInitialData
//    SshConnect.ssh_disconnect(session)
//    println(getForksOfProject("scas-mdd/fork-test"))
//    println(getAllCommitsOfProject(Seq(1)))

//    println(Await.result(getAllCommits,Duration.Inf))
//    println(Await.result(getBranches,Duration.Inf))
//    println(db.run(sql"""SHOW DATABASES"""))


  }
}
