package dk.itu.gfv.monitor.db

import java.io.File
import java.sql.DriverManager
import java.util

import com.typesafe.scalalogging.LazyLogging
import com.zaxxer.hikari.HikariDataSource
import dk.itu.gfv.model.Model.{GithubRepository, Parent}
import dk.itu.gfv.monitor.db.Models._
import dk.itu.gfv.repo.RepositoryOperations.isActiveFork
import dk.itu.gfv.repo.{GithubExtractor, RepositoryOperations}
import dk.itu.gfv.util.MonitorProperties
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.{Ref, Repository, RepositoryCache}
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.util.FS
import org.joda.time.DateTime
import slick.driver.JdbcProfile
import slick.jdbc.MySQLProfile.api._
import slick.jdbc.meta.MTable
import better.files.{File => ScalaFile}
import dk.itu.gfv.monitor.{Ssh, Utils}
import dk.itu.gfv.monitor.db.NightFetcher.CachedETAG
import org.apache.commons.io.FileDeleteStrategy

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}
import concurrent.ExecutionContext.Implicits.global
import collection.JavaConverters._
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.collection.immutable.{HashMap, HashSet}

/**
  * Created by scas on 20-12-2016.
  */
trait DBService extends LazyLogging with Utils with Ssh {

  self: DatabaseSchema =>
  val MYSQL_DUPLICATE_PK = 1062

  case class RepoError(id: Int, full_name: String, fork: Boolean, created_at: String,
                       updated_at: String, pushed_at: String, default_branch: String)

  val repo_error = RepoError(-1, "error", false, "null", "null", "null", "null")


  implicit val dbName = properties.getProperty("db_name")
  val driver = JdbcProfile
  val mysql_port = properties.getProperty("port_mysql_connect")
  val jdbcDriver = properties.getProperty("driver")
  val url = properties.getProperty("url") + ":" + mysql_port + "/" + dbName + "?rewriteBatchedStatements=true&useSSL=false"
  val username = properties.getProperty("user")
  val password = properties.getProperty("password")

  logger.info("connecting to " + url)
  logger.info("connection is local (not via ssh) " + properties.getProperty("local"))
  override val projects_folder = properties.getProperty("data") + "projects"
  //  val db = Database.forURL(url = url, user = username, password = password, driver = jdbcDriver,keepAliveConnection=true)

  val ds = new HikariDataSource()
  //  ds.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource")
  ds.setDriverClassName(properties.getProperty("dataSource.driver"));
  ds.setJdbcUrl(url)
  ds.setUsername(username)
  ds.setPassword(password)
  ds.setMaximumPoolSize(Integer.parseInt(properties.getProperty("dataSource.poolSize")))
  ds.addDataSourceProperty("cachePrepStmts", properties.getProperty("dataSource.cachePrepStmts"))
  ds.addDataSourceProperty("prepStmtCacheSize", properties.getProperty("dataSource.prepStmtCacheSize"))
  ds.addDataSourceProperty("prepStmtCacheSqlLimit", properties.getProperty("dataSource.prepStmtCacheSqlLimit"))
  ds.addDataSourceProperty("autoReconnect", properties.getProperty("dataSource.autoReconnect"))
  //  ds.addDataSourceProperty("useServerPrepStmts", properties.getProperty("dataSource.useServerPrepStmts"))
  ds.setAutoCommit(properties.getProperty("dataSource.autoCommit") match { case "true" => true; case "false" => false })
  ds.setConnectionTimeout(Integer.parseInt(properties.getProperty("dataSource.connectionTimeout")))
  ds.setIdleTimeout(Integer.parseInt(properties.getProperty("dataSource.idleTimeout")))

  val db = Database.forDataSource(ds, None)
  Await.result(db.run(sqlu"""USE #$dbName"""), Duration.Inf)
  Await.result(db.run(sqlu"""SET FOREIGN_KEY_CHECKS=0"""), Duration.Inf)

  //  Await.result(db.run(sqlu"SET foreign_key_checks = 0"), Duration.Inf)


  case class IntentionalRollbackException[R](successResult: R) extends Exception("Rolling back transaction")

  def runWithRollback[R, S <: slick.dbio.NoStream, E <: slick.dbio.Effect](action: DBIOAction[R, S, E]): Future[R] = {

    val block = action.flatMap(r => DBIO.failed(new IntentionalRollbackException(r)))

    val tryResult = db.run(block.transactionally.asTry)

    // not sure how to eliminate these casts from Any
    tryResult.map {
      case Failure(IntentionalRollbackException(successResult)) => logger.info("Successful insertion"); successResult.asInstanceOf[R]
      case Failure(t) => logger.error("Failure. Rolling back"); throw t
      case Success(r) => r.asInstanceOf[R]
    }
  }


  def insertAddedLinesJDBC(addedLinesForInsertion: Seq[AddedLine]) = {
    Class.forName(jdbcDriver)
    val connection = DriverManager.getConnection(url, username, password)
    connection.setAutoCommit(false)

    val sql_insert =
      """INSERT INTO added_lines
        |(id, project, commit, filename,line_number,text_normalized,text) VALUES (?,?,?,?,?,?,?)""".stripMargin
    val st = connection.prepareStatement(sql_insert);
    var b = 0
    for (d <- addedLinesForInsertion) {
      st.setInt(1, d.id)
      st.setInt(2, d.project)
      st.setString(3, d.commit)
      st.setString(4, d.filename)
      st.setInt(5, d.line_number)
      st.setString(6, d.text_normalized)
      st.setString(7, d.text)
      st.addBatch()
      b = b + 1
      if (b == 10000) {
        st.executeBatch()
        st.clearBatch()
        b = 0
      }
    }
    if (b > 0) {
      st.executeBatch
    }
    connection.commit()
    connection.close()
  }

  def folder_path(project: String) = {
    projects_folder + "/" + project + "/"
  }

  def metadata_folder_path(project: String) =
    projects_folder + "/" + project + "/metadata-gfv/"

  type JSON = String

  def hasAtLeastOneReference(repo: Repository) = {
    repo.getAllRefs.values.asScala.toList.exists(ref => ref.getObjectId != null)
  }

  def isGitRepositoryCloned(path: String) =
    RepositoryCache.FileKey.isGitRepository(new File(path), FS.DETECTED)

  /**
    * Clone a repository
    *
    * @param project
    * @param repository
    * @return
    */
  def cloneProject(project: String, repository: GithubRepository): Boolean = {
    var repo_existed_before = false
    val repoFolder = new File(folder_path(project) + repository.full_name)
    logger.info("Repo folder " + repoFolder.getAbsolutePath)

    def clone = {
      val branchesToClone = new util.ArrayList[String]()
      branchesToClone.add("refs/heads/" + repository.default_branch)
      val repo = Git.cloneRepository
        .setNoCheckout(true)
        .setBranchesToClone(branchesToClone) // shuruiz/paper
        .setURI("https://github.com/" + repository.full_name + ".git")
        .setDirectory(new File(folder_path(project) + repository.full_name))
        .call
      repo.fetch.call
      repo.close
    }

    def deleteAndClone = {
      if (repoFolder.exists && repoFolder.isDirectory) {
        for (file <- repoFolder.listFiles()) {
          FileDeleteStrategy.FORCE.delete(file);
        }
        ScalaFile(folder_path(project) + repository.full_name).delete(true)
      }
      clone
      repo_existed_before
    }

    if (RepositoryCache.FileKey.isGitRepository(new File(repoFolder.getAbsolutePath + "/.git"), FS.DETECTED)) {
      // Already cloned. Just need to open a repository here.
      try {
        val git = Git.open(new File(repoFolder.getAbsolutePath + "/.git"))
        if (!hasAtLeastOneReference(git.getRepository)) {
          // the repository is broken and needs to be re-cloned
          deleteAndClone
        }
        else {
          repo_existed_before = true
          repo_existed_before
        }
      }
      catch {
        case ex: Exception => {
          deleteAndClone
        }
      }
    } else {
      // Not present or not a Git repository.
      deleteAndClone
    }
  }

  def printResults[T](f: Future[Iterable[T]]): Iterable[T] = {
    val r = Await.result(f, Duration.Inf)
    r.foreach(println)
    println()
    r
  }

  def resetDb = {
    logger.info("resetting database")
    dropDb
    createDb

  }

  def createDb = {
    try {
      logger.info(s"creating $dbName")
      Await.result(db.run(sqlu"""CREATE DATABASE #$dbName"""), Duration.Inf)
      Await.result(db.run(sqlu"""USE #$dbName"""), Duration.Inf)
    } catch {
      case ex: Exception => println("cannot create database")
    }

  }

  def dropDb = {
    try {
      logger.info(s"dropping $dbName")
      Await.result(db.run(sqlu"""DROP DATABASE IF EXISTS #$dbName"""), Duration.Inf)
    }
    catch {
      // ignore failure due to db not exist
      case _: Throwable => println("weird")
    }
  }


  def createSchemaIfNotExists: Future[Unit] = {
    db.run(MTable.getTables).flatMap(tables => {
      if (tables.isEmpty) {
        Await.result(db.run(sqlu"USE #$dbName"), Duration.Inf)
        db.run(allSchemas.create).andThen {
          case Success(_) => {
            val r = db.run(sqlu"SET foreign_key_checks = 0")
            Await.result(db.run(sqlu"use #$dbName"), Duration.Inf)
            val r1 = db.run(
              sqlu"""ALTER TABLE `added_lines`
CHANGE COLUMN `text` `text` LONGTEXT NOT NULL ,
CHANGE COLUMN `text_normalized` `text_normalized` LONGTEXT NOT NULL ;
""")
            Await.result(r, Duration.Inf)
            Await.result(r1, Duration.Inf)
            println("Schema created ")
          }
          case Failure(x) => x.printStackTrace();
            println("Some error occurred")
        }
      }
      else {
        println("Schema already exists")
        val r = db.run(sqlu"""SET foreign_key_checks = 0""")

        Await.result(db.run(sqlu"""use #$dbName"""), Duration.Inf)
        val r1 = db.run(
          sqlu"""ALTER TABLE `added_lines`
CHANGE COLUMN `text` `text` LONGTEXT NOT NULL ,
CHANGE COLUMN `text_normalized` `text_normalized` LONGTEXT NOT NULL ;
""")

        Await.result(r, Duration.Inf)
        Await.result(r1, Duration.Inf)
        //        db.run(allSchemas.drop).andThen {
        //          case Success(_) => Future.successful()
        //          case Failure(x) => x.printStackTrace
        //        }
        Future.successful(Unit)
      }
    }
    )
  }

  def getMainProjects = {
    db.run(projects.filter(_.fork === false).result)
  }

  def getAddedLineByIds(ids: Seq[Int]) =
    db.run(addedLines.filter(_.id inSetBind ids).result)

  def getAddedLinesOfCommitByIds(ids: Seq[Int], sha: String) =
    db.run(addedLines.filter(_.id inSetBind ids).filter(_.commit === sha).result)

  def getAddedLineById(id: Int) =
    db.run(addedLines.filter(_.id === id).result.map(_.headOption))

  def getProjectByName(reponame: String): Future[Option[Project]] = {
    val q = projects.filter(_.name === reponame)
    db.run(q.result.map(_.headOption))
  }

  def getProjectByIds(ids: Seq[Int]): Future[Seq[Project]] = {
    val q = projects.filter(_.id inSetBind ids)
    db.run(q.result)
  }

  def getProjectById(id: Int): Future[Project] = {
    val q = projects.filter(_.id === id)
    db.run(q.result.map(_.head))
  }

  val insertQuery = projects returning projects.map(_.id) into ((item, id) => item.copy(id = id))

  def create(name: String, owner: Int, language: String, default_branch: String, fork: Boolean, fork_of: Int, fork_of_main: Int, forks_count: Int, stargazers_count: Int, subscribers_count: Option[Int], created_at: DateTime, pushed_at: DateTime, updated_at: DateTime) = {
    val action = insertQuery += Project(0, name, owner, language, default_branch, fork, fork_of, fork_of_main, forks_count, stargazers_count, subscribers_count, created_at, pushed_at, updated_at)
    db.run(action)
    //    action
  }

  def insertProject(main: GithubRepository, main_repo_id: Int, owner: Int) = {

    def getParentID(parent: Option[Parent]): Int = parent match {
      case Some(x) => Await.result(getProjectByName(x.full_name), Duration.Inf) match {
        case Some(x) => x.id
        case None => -2 // this was forked from an inactive fork, which was not inserted!
      }
      case None => -1
    }

    create(main.full_name, owner, main.language.getOrElse(""), main.default_branch, main.fork, getParentID(main.parent), main_repo_id, main.forks_count, main.stargazers_count, main.subscribers_count, main.created_at, main.pushed_at, main.updated_at)
  }

  def insertUser1(user: User): Future[User] = {
    val insertUser = users returning users.map(_.id) into ((item, id) => item.copy(id = id))
    db.run((insertUser += user))
  }

  def insertUser(reponame: String) = {
    GithubExtractor.getUser(reponame) match {
      case Some(user) => insertUserIfNotExists(User(user.id, user.login, user.name.getOrElse(""), user.email.getOrElse(""), user.created_at))
      case None => sys.error("cannot parse user json")
    }
  }

  def insertOrUpdateProject(project: Project) = {
    db.run(projects.insertOrUpdate(project))
  }

  def insertBranches(input_branches: Seq[Branch]): Future[Seq[Branch]] = {
    val insert_branches = branches returning branches.map(_.id) into ((item, id) => item.copy(id = id))
    db.run(insert_branches ++= input_branches)
  }

  def insertCommits(input_commits: Seq[Commit]) = {
    //    val insert_commits = commits returning commits.map(_.sha) into ((item, id) => item.copy(sha = id))
    db.run(commits ++= input_commits)
  }

  def insertCommitsJDBC(input: Seq[Commit]) = {
    Class.forName(jdbcDriver)
    val connection = DriverManager.getConnection(url, username, password)
    connection.setAutoCommit(false)

    val sql_insert =
      """INSERT IGNORE INTO commits (sha, author, committer, message, authorTime, committerTime) VALUES (?,?,?,?,?,?)"""
    val st = connection.prepareStatement(sql_insert)
    var b = 0
    for (d <- input) {
      st.setString(1, d.sha)
      st.setInt(2, d.author)
      st.setInt(3, d.committer)
      st.setString(4, d.message)
      st.setLong(5, d.authorTime)
      st.setLong(6, d.committerTime)
      st.addBatch()
      b = b + 1
      if (b == 10000) {
        st.executeBatch()
        st.clearBatch()
        b = 0
      }
    }
    if (b > 0) {
      st.executeBatch
    }
    connection.commit()
    connection.close()
  }

  def insertCommitOutliersJDBC(input: Seq[CommitOutlier]) = {
    Class.forName(jdbcDriver)
    val connection = DriverManager.getConnection(url, username, password)
    connection.setAutoCommit(false)

    val sql_insert =
      """INSERT IGNORE INTO commit_outliers
        |(sha, project_main, repo_id, added_lines_size) VALUES (?,?,?,?)""".stripMargin
    val st = connection.prepareStatement(sql_insert);
    var b = 0
    for (d <- input) {
      st.setString(1, d.sha)
      st.setString(2, d.project_main)
      st.setInt(3, d.repo_id)
      st.setInt(4, d.added_lines_size)
      st.addBatch()
      b = b + 1
      if (b == 10000) {
        st.executeBatch()
        st.clearBatch()
        b = 0
      }
    }
    if (b > 0) {
      st.executeBatch
    }
    connection.commit()
    connection.close()
  }

  def insertCommitOutliers(input: Seq[CommitOutlier]) = {
    db.run(commit_outliers ++= input)
  }

  def geRepoBranch(branch_name: String, repo_id: Int): Future[Option[Branch]] = {
    val q = branches.filter(_.project === repo_id).filter(_.name === branch_name).result.map(_.headOption)
    db.run(q)
  }

  def insertBranch(branch: Branch): Future[Branch] = {
    val insert_branch = branches returning branches.map(_.id) into ((item, id) => item.copy(id = id))
    db.run(insert_branch += branch)
  }

  def insertModifiedFile(modifiedFile: FilesModified) = {
    val mf = files_modified returning files_modified.map(_.id) into ((item, id) => item.copy(id = id))
    db.run(mf += modifiedFile)
  }

  def insertModifiedFileMap(modifiedFileMap: Seq[FilesModifiedMap]) = {
    db.run(files_modified_map ++= modifiedFileMap)
  }

  def getRepositoryCommits(project: Int): Future[Seq[String]] = {
    val project_commits = for {
      b <- branches if b.project === project
      bc <- branches_commits if bc.branch === b.id
      c <- commits if bc.commit === c.sha
    } yield (c.sha)
    db.run(project_commits.result)
  }

  def getProjectsThatAreNotForks =
    db.run(projects.filter(_.fork === false).result)

  def getAddedLinesSHAByCommits(commits_ids: Seq[String]) = {
    val q = addedLines.filter(_.commit inSetBind commits_ids).map(x => (x.commit, x.project)).result
    db.run(q)
  }

  def getAddedLinesByCommits(commits_ids: Seq[String]) = {
    val q = addedLines.filter(_.commit inSetBind commits_ids).result
    db.run(q)
  }

  def getSizeOfAddedLinesByCommits(commits_ids: Seq[String]) = {
    val q = addedLines.filter(_.commit inSetBind commits_ids).result.map(_.size)
    db.run(q)
  }

  def getAddedLinesByCommitsFiltered(commits_ids: Seq[String]) = {
    val q = addedLines.filter(_.commit inSet commits_ids).map(x => (x.project, x.commit, x.file)).result
    db.run(q)
  }

  def getMultipleProjectsCommits(project_ids: Seq[Int]): Future[Seq[Commit]] = {
    val project_commits = for {
      b <- branches if b.project inSetBind project_ids
      bc <- branches_commits if bc.branch === b.id
      c <- commits if bc.commit === c.sha
    } yield (c)
    db.run(project_commits.result)
  }

  def getCommitParents =
    db.run(commitParents.result)

  def getCommitParents(commit: String) = {
    db.run(commitParents.filter(_.sha === commit).result)
  }

  def getCommitsParents(commits: Seq[String]) = {
    db.run(commitParents.filter(_.sha inSetBind commits).result)
  }

  def getNonOutlierCommitsSHA = {
    db.run(commit_outliers.filter(_.added_lines_size < 1000).map(_.sha).result)
  }

  def getModifiedFilesMap =
    db.run(files_modified_map.result)

  def getDuplicatesLastRow(project: String) = {
    val p = Await.result(getProjectByName(project), Duration.Inf)
    db.run(duplicates.filter(_.project === p.get.id).sortBy(_.id.desc).take(1).result)
  }

  def getDuplicatesOfProjectCount(project: String) = {
    val p = Await.result(getProjectByName(project), Duration.Inf)
    db.run(duplicates.filter(_.project === p.get.id).size.result)
  }

  def getDuplicates =
    db.run(duplicates.result)

  def getDuplicateById(dup_id: Int) =
    db.run(duplicates.filter(_.id === dup_id).result)

  def getDuplicates(project: String) = {
    val p = Await.result(getProjectByName(project), Duration.Inf)
    db.run(duplicates.filter(_.project === p.get.id).result)
  }

  def getNextDuplicates(startFrom: Int, size: Int) = {
    db.run(duplicates.sortBy(_.id.desc).filter(_.id <= startFrom).take(size).result)
  }

  def getNextDuplicatesOfProject(project: String, startFrom: Int, size: Int) = {
    val p = Await.result(getProjectByName(project), Duration.Inf)
    db.run(duplicates.filter(_.project === p.get.id).sortBy(_.id.desc).filter(_.id <= startFrom).take(size).result)
  }

  def getPrevDuplicatesOfProject(project: String, greaterThanID: Int, smallerThanID: Int, size: Int) = {
    val p = Await.result(getProjectByName(project), Duration.Inf)
    db.run(duplicates.filter(_.project === p.get.id).sortBy(_.id.desc).filter(x => x.id > greaterThanID && x.id <= smallerThanID).take(size).result)
  }

  def getPrevDuplicates(greaterThanID: Int, smallerThanID: Int, size: Int) = {
    db.run(duplicates.sortBy(_.id.desc).filter(x => x.id > smallerThanID && x.id <= greaterThanID).take(size).result)
  }


  def getRetrospectiveDuplicates =
    db.run(duplicates_retrospective_analysis.result)

  //  db.run(duplicates_retrospective_analysis.result)

  def getRetrospectiveDuplicates(project: String) = {
    val p = Await.result(getProjectByName(project), Duration.Inf)
    db.run(duplicates_retrospective_analysis.filter(_.project === p.get.id).result)
  }

  def insertSurvey(survey: Survey) = {
    db.run(surveyResponses += survey)
  }


  /**
    * Return a list of project's commits including commits of forks
    *
    * @param project owner/repository
    * @return a collection of commit objects
    */

  def getProjectAllCommits(project: String): Future[scala.Seq[Models.Commit]] = {
    val project_ids = getProjectAndForksIDS(project)
    val project_commits = for {
      b <- branches if (b.project inSetBind project_ids)
      bc <- branches_commits if bc.branch === b.id
      c <- commits if c.sha === bc.commit
    } yield (c)
    db.run(project_commits.distinct.result)
  }

  def getProjectCommitsMap(project: String): HashMap[String, Seq[Commit]] = {
    val projectID = Await.result(getProjectByName(project), Duration.Inf).get
    val project_commits = for {
      b <- branches if (b.project === projectID.id)
      bc <- branches_commits if bc.branch === b.id
      c <- commits if c.sha === bc.commit
    } yield (c)
    HashMap(project -> Await.result(db.run(project_commits.distinct.result), Duration.Inf))
  }

  def getProjectsForksCommits(project: String): Future[scala.Seq[Models.Commit]] = {
    val project_ids = getForksOfProjectOnly(project).map(_.id)
    val project_commits = for {
      b <- branches if (b.project inSetBind project_ids)
      bc <- branches_commits if bc.branch === b.id
      c <- commits if c.sha === bc.commit
    } yield (c)
    db.run(project_commits.distinct.result)
  }

  def getNumberOfAddedLinesInCommit(commitId: String) = {
    db.run(addedLines.filter(_.commit === commitId).length.result)
  }

  def getProjectAddedLines(project: String) = {
    val project_commits_sha = Await.result(getProjectCommitsSHA(project), Duration.Inf)
    db.run(addedLines.filter(_.commit inSetBind project_commits_sha).result)
  }

  def getProjectAddedLinesFromSHA(commit_ids: Seq[String]) = {
    db.run(addedLines.filter(_.commit inSetBind commit_ids).result)
  }

  def getProjectCommitsSHA(project: String) = {
    val project_id = Await.result(getProjectByName(project), Duration.Inf)
    val project_commits = for {
      b <- branches if (b.project === project_id.get.id)
      bc <- branches_commits if bc.branch === b.id
      c <- commits if c.sha === bc.commit
    } yield (c.sha)
    db.run(project_commits.result)
  }

  def getProjectAndForksAllAddedLines1(project: String) = {
    val commits = Await.result(getProjectAllCommits(project), Duration.Inf).map(_.sha)
    db.run(addedLines.filter(_.commit inSet commits).result)
  }


  def getProjectAndForksAllAddedLines(project: String) = {
    val project_ids = getProjectAndForksIDS(project)
    val addedlines = for {
      b <- branches if (b.project inSet project_ids)
      bc <- branches_commits if bc.branch === b.id
      c <- commits if c.sha === bc.commit
      ad <- addedLines if ad.commit === c.sha
    } yield (ad)
    db.run(addedlines.result)
  }

  def getBranchCommitByIds(branchId: Int, sha: String) = {
    db.run(branches_commits.filter(_.branch === branchId).filter(_.commit === sha).result.map(_.headOption))
  }

  def getCommitById(commit_id: String): Future[Option[Commit]] =
    db.run(commits.filter(_.sha === commit_id).result.map(_.headOption))

  def getCommitsBySHA(input_sha: Seq[String]): Future[Seq[Commit]] =
    db.run(commits.filter(_.sha inSet (input_sha.toSet)).result)

  def insertBranchCommit(branchCommit: BranchCommit) =
    db.run(branches_commits.insertOrUpdate(branchCommit))

  def insert_branch_commits(input: Seq[BranchCommit]) =
    db.run(branches_commits ++= input)

  def getUserById(userID: Int): Future[Seq[User]] =
    db.run(users.filter(_.id === userID).result)

  def getUserByName(user_name: String): Future[Option[User]] =
    db.run(users.filter(_.name === user_name).result.map(_.headOption))

  def getUserByEmail(user_email: String): Future[Option[User]] = {
    db.run(users.filter(_.email === user_email).result.map(_.headOption))
  }

  def getAllUsers = db.run(users.result)

  def getProjectCommits(project: String): Future[Seq[Commit]] = {
    val project_id = Await.result(getProjectByName(project), Duration.Inf)

    val project_commits = for {
      b <- branches if (b.project === project_id.get.id)
      bc <- branches_commits if bc.branch === b.id
      c <- commits if c.sha === bc.commit
    } yield (c)
    db.run(project_commits.distinct.result)
  }


  def getAuthorAndFilesMap: Future[Seq[(Int, Int)]] = {
    db.run(files_modified_map.map(x => (x.author, x.files_modified_id)).result)
  }

  def getRepoAndFilesMap: Future[Seq[(String, Int)]] = {
    db.run(files_modified_map.map(x => (x.project, x.files_modified_id)).result)
  }

  def getFilesModified = db.run(files_modified.result)

  def getFilesModifiedMap = db.run(files_modified_map.result)


  def insertCommit(commit: Commit) =
    db.run(commits += commit)

  def insertOrUpdateCommitParent(commitParent: CommitParent) =
    db.run(commitParents.insertOrUpdate(commitParent))


  def insertAddedLines(input: Seq[AddedLine]) = {
    val q = addedLines ++= input
    db.run(q)
  }

  def insertUserIfNotExists(user: User) = {
    val action = (
      users.filter(u => (u.name === user.name && u.email === user.email && user.name.nonEmpty && user.email.nonEmpty)).result.headOption.flatMap {
        case Some(p) => {
          val a = p.copy(p.id, user.login, user.name, user.email, user.created_at)
          Await.result(db.run(users.insertOrUpdate(a)), Duration.Inf)
          DBIO.successful(a)
        }
        case None => users returning users.map(_.id) into ((item, id) => item.copy(id = id)) += user
      }
      )
    //    action
    db.run(action)
  }

  def updateBranch(branch: Branch) = {
    db.run(branches.insertOrUpdate(branch))
  }

  def updateRepository(repo: GithubRepository) = {
    val repository = db.run(projects.filter(_.name === repo.full_name).result.map(_.head))
    repository.andThen {
      case Success(x) => db.run(projects.insertOrUpdate(Project(x.id, repo.full_name, x.owner, x.language, x.default_branch, x.fork, x.fork_of, x.fork_of_main, x.forks_count, x.stargazers_count, x.subscribers_count, repo.created_at, repo.updated_at, repo.pushed_at)))
    }
  }

  def updateRepository(repo_name: String, repo: GithubRepository) = {
    val repository = db.run(projects.filter(_.name === repo_name).result.map(_.head))
    repository.andThen {
      case Success(x) => db.run(projects.insertOrUpdate(Project(x.id, repo.full_name, x.owner, x.language, x.default_branch, x.fork, x.fork_of, x.fork_of_main, x.forks_count, x.stargazers_count, x.subscribers_count, repo.created_at, repo.updated_at, repo.pushed_at)))
    }
  }


  def getBranch(repo_id: Int, branch_name: String) = {
    val q = branches.filter(x => x.project === repo_id && x.name === branch_name).result.map(_.head)
    db.run(q)
  }

  def getAllCommits =
    db.run(commits.result)

  def getAllCommitsOfProject(project_id: Int) = {
    val q = for {
      b <- branches if b.project === project_id
      bc <- branches_commits if bc.branch === b.id
    //c <- commits if c.sha === bc.commit
    } yield (bc.commit)

    db.run(q.result)
  }


  def getProjectAndForksCommits(project: String): Future[Seq[Commit]] = {
    val ids = getProjectAndForksIDS(project)
    val commits_query = for {
      b <- branches if b.project inSetBind ids
      bc <- branches_commits if bc.branch === b.id
      c <- commits if bc.commit === c.sha
    } yield (c)

    db.run(commits_query.result)
  }

  def getAllCommitsOfProject(project_id: Seq[Int]) = {
    val q = for {
      b <- branches if b.project inSetBind project_id
      bc <- branches_commits if bc.branch === b.id
    } yield (bc.commit)

    db.run(q.result)
  }


  /**
    * Given a set of project ids, return the commits in those projects
    *
    * @param project_id
    * @return
    */

  def getProjectsCommitsSHA(project_id: Seq[Int]) = {
    val q = for {
      b <- branches if b.project inSetBind project_id
      bc <- branches_commits if bc.branch === b.id
    } yield (bc.commit)

    db.run(q.distinct.result)
  }

  def getAllAddedLines: Future[Seq[AddedLine]] =
    db.run(addedLines.result)

  def getAddedLinesBySHA(commit_id: String): Future[Seq[AddedLine]] = {
    db.run(addedLines.filter(_.commit === commit_id).result)
  }

  def getBranches =
    db.run(branches.result)

  def getBranchCommits =
    db.run(branches_commits.result)

  def getBranchCommitsOfProject(id: Int) = {
    val q = for {
      b <- branches.filter(_.project === id).map(_.id)
      bc <- branches_commits.filter(_.branch === b)
    } yield (bc)
    db.run(q.result)
  }

  def getBranchCommitsOfProjects(ids: Seq[Int]) = {
    val q = for {
      b <- branches.filter(_.project inSetBind ids).map(_.id)
      bc <- branches_commits.filter(_.branch === b)
    } yield (bc)
    db.run(q.result)
  }

  def getBranchesOfProjects(projects: Seq[Int]) =
    db.run(branches.filter(_.project inSetBind projects).result)

  def getAddedLines1 = {
    val q = addedLines.result
    db.run(q)
    //    db.run(sql"SELECT id, project, branch, commit, filename, line_number, text, text_normalized FROM added_lines;".as[Seq[(Int,Int,Int,Int,String,Int,String,String)]])
  }

  def insertSimilarityResults(results: Seq[Duplicate]): Future[Seq[Duplicate]] = {
    val insert_duplicates = duplicates returning duplicates.map(_.id) into ((item, id) => item.copy(id = id))
    db.run(insert_duplicates ++= results)
  }

  def insertSimilarityResultsForRetrospectiveAnalysis(results: Seq[Duplicate]) = {
    db.run(duplicates_retrospective_analysis ++= results)
  }

  def getSimilarityResults(ids: Seq[Int]) =
    db.run(duplicates.filter(_.id inSetBind (ids)).result)

  def getSimilarityResults(new_duplicate_id: Int) =
    db.run(duplicates.filter(_.baseLine === new_duplicate_id).result)


  def updateSimilarityComments(duplicate_id: Int, useful: Boolean, comment: String) = {
    val timestamp = org.joda.time.DateTime.now()
    val q = sqlu"UPDATE duplicates SET devUseful=#$useful,devUsefulComment='#$comment',devResponseTime='#$timestamp' WHERE id=#$duplicate_id"
    db.run(q)
  }

  def updateUsefulDuplicateRetrospective(duplicate_id: Int, useful: Boolean) = {
    val q = sqlu"UPDATE duplicates_retrospective_analysis SET internalUseful=#$useful WHERE id=#$duplicate_id"
    db.run(q)
  }

  def updateInternalUsefulDuplicate(duplicate_id: Int, useful: Boolean) = {
    val q = sqlu"UPDATE duplicates SET internalUseful=#$useful WHERE id=#$duplicate_id"
    db.run(q)
  }

  def updateInternalNotUsefulDuplicate(duplicate_id: Int, notUseful: Boolean) = {
    val q = sqlu"UPDATE duplicates SET internalNotUseful=#$notUseful WHERE id=#$duplicate_id"
    db.run(q)
  }

  def randomquery(sql: String) = {
    //    db.run(sql"SELECT * FROM added_lines WHERE commit BETWEEN 17 AND 2000 LIMIT 5000000")
    Class.forName(jdbcDriver)
    val connection = DriverManager.getConnection(url, username, password)
    connection.setAutoCommit(false)
    val r = connection.prepareStatement(sql).executeQuery()
    //    connection.close
    r
  }

  /**
    * Get the project and its forks' ids
    *
    * @param project
    * @return
    */

  def getProjectAndForksIDS(project: String): Seq[Int] = {
    val main_repo_id = Await.result(db.run(projects.filter(_.name === project).result.map(_.head.id)), Duration.Inf)
    val forks_ids = Await.result(db.run(projects.filter(_.fork_of_main === main_repo_id).map(_.id).result), Duration.Inf)
    Seq(main_repo_id) ++ forks_ids
  }

  def getAllProjects = db.run(projects.result)

  def insert_added_lines(input: Seq[AddedLine]) =
    db.run(addedLines ++= input)

  def getForksOfProject(project_id: Int): Future[Seq[Project]] =
    db.run(projects.filter(_.fork_of_main === project_id).result)

  def updateProject(project: Project) =
    db.run(projects.insertOrUpdate(project))

  //  def getForksOfProject(project: String): Seq[Project] = {
  //    val main_repo = Await.result(db.run(projects.filter(_.name === project).result), Duration.Inf)
  //    val r = Await.result(db.run(projects.filter(_.fork_of_main === main_repo.head.id).result), Duration.Inf)
  //    main_repo ++ r
  //  }

  def getForksOfProjectOnly(project: String): Seq[Project] = {
    val main_repo = Await.result(db.run(projects.filter(_.name === project).result), Duration.Inf)
    val r = Await.result(db.run(projects.filter(_.fork_of_main === main_repo.head.id).result), Duration.Inf)
    r
  }

  def getForksOfProjectIdAndNameOnly(project: String) = {
    val main_repo = Await.result(db.run(projects.filter(_.name === project).result), Duration.Inf)
    val r = Await.result(db.run(projects.filter(_.fork_of_main === main_repo.head.id).map(x => (x.id, x.name)).result), Duration.Inf)
    r
  }

  def getProjects =
    db.run(projects.result)


  def oneLevelForks(inputProjects: Seq[Project], project: Project) = {
    inputProjects.filter(_.fork_of == project.id)
  }

  def oneLevelForks(inputProjects: Seq[Project], project: String) = {
    //    logger.info(s"$project")

    val p = inputProjects.filter(_.name.equalsIgnoreCase(project))
    //    logger.info("filtered projects" + p)
    p.headOption match {
      case Some(x) => inputProjects.filter(_.fork_of == x.id)
      case None => Seq()
    }
  }


  def oneLevelForks(project: String): Future[Seq[Project]] = {
    logger.info("Projects" + Await.result(db.run(projects.result), Duration.Inf))
    val project_id = Await.result(db.run(projects.filter(_.name === project).result.map(_.head)), Duration.Inf)
    val r = projects.filter(_.fork_of === project_id.id).result
    db.run(r)
  }

  def transformRevCommits(new_commits: Seq[RevCommit]) = {
    new_commits.map(commit => {
      // each commit is done by someone. try to find if that author exists, otherwise add the person indent
      val author_name = Await.result(getUserByName(commit.getAuthorIdent.getName), Duration.Inf)
      val author = author_name match {
        case Some(a) => a.id
        case None => {
          val author_email = Await.result(getUserByEmail(commit.getAuthorIdent.getEmailAddress), Duration.Inf)
          author_email match {
            case Some(e) => e.id
            case None => Await.result(insertUserIfNotExists(User(0, "", commit.getAuthorIdent.getName, commit.getAuthorIdent.getEmailAddress, DateTime.parse("1971-01-01T00:00:00Z"))), Duration.Inf).id
          }
        }
      }
      val committer_name = Await.result(getUserByName(commit.getCommitterIdent.getName), Duration.Inf)
      val committer = committer_name match {
        case Some(a) => a.id
        case None => {
          val committer_email = Await.result(getUserByEmail(commit.getCommitterIdent.getEmailAddress), Duration.Inf)
          committer_email match {
            case Some(e) => e.id
            case None => Await.result(insertUserIfNotExists(User(0, "", commit.getCommitterIdent.getName, commit.getCommitterIdent.getEmailAddress, DateTime.parse("1971-01-01T00:00:00Z"))), Duration.Inf).id
          }
        }
      }
      Commit(commit.getName, author, committer, commit.getFullMessage, commitTimeToInt(commit.getAuthorIdent.getWhen.getTime), commitTimeToInt(commit.getCommitterIdent.getWhen.getTime))
    })
  }

  def deleteBranches(branchIds: Seq[Int]) =
    db.run(branches.filter(_.id inSetBind branchIds).delete)

  def getProjectBranches(project_id: Int): Future[Seq[Branch]] =
    db.run(branches.filter(_.project === project_id).result)

  def deleteProject(project_id: Int) = {
    Await.result(db.run(projects.filter(_.id === project_id).delete), Duration.Inf)
  }

  def insertCommitParents(commit_parents: Seq[CommitParent]) = {
    db.run(commitParents ++= commit_parents)
  }

  def insert_full_branch_and_commits = {
    DBIO.seq(projects.filter(_.name === "").result).withPinnedSession
  }


  // TODO FIXME
  // Branch Commits should be filtered to only contain the commits < $patchSize
  // New commits contains the commits that are
  def insert_new_commits(branch_commits: Seq[RevCommit], new_commits: Seq[RevCommit], new_commits_added_lines: Seq[AddedLine], repo_id: Int, main_git: Git, br: Branch, main_repo: Boolean) = {

    val commits = transformRevCommits(new_commits)
    val new_commits_map = new_commits.map(x => x.getName -> x).toMap
    val insert_commits = insertCommits(commits).flatMap(_ => {
      insert_branch_commits(branch_commits.map(x => BranchCommit(0, br.id, x.getName))).flatMap(_ => {
        val commits_parents = commits.map(x => new_commits_map.get(x.sha).get.getParents.map(_.getName).map(a => CommitParent(x.sha, a))).flatten
        logger.info(s"inserting commit parents of repo_id: $repo_id")
        insertCommitParents(commits_parents).flatMap(_ => {
          logger.info(s"inserting ${new_commits_added_lines.size} added lines of repo_id: $repo_id")
          insertAddedLines(new_commits_added_lines)
        })
      })
    })
    insert_commits
  }

  def insertNonExistingBranchWithRollback(project: String, gitBranch: Ref, repo: Project, main_git: Git, main_repo: Boolean, commitsInBranches: collection.mutable.HashSet[String],
                                          commitsToExclude: collection.mutable.HashSet[String], new_commits_to_be_added_to_commits: collection.mutable.ListBuffer[RevCommit]) = {

    val branchInsertion = Await.result(insertBranch(Branch(0, gitBranch.getName, gitBranch.getObjectId.name, repo.id)), Duration.Inf)

    logger.info(commitsInBranches.size + " commits of " + repo.name + ", repo with id " + repo.id + " ")
    // get this branch's commits excluding first commit
    logger.info(s"getting ${branchInsertion.name}'s commits from ${repo.name}")
    val branch_commits = main_git.log().add(gitBranch.getObjectId).call().asScala.toSeq.init
    // filter out commits that already exist or that should be excluded
    val commitsToAnalyzeAndAddedLines = {
      val commitsToTransform = branch_commits.filterNot(x => commitsToExclude.contains(x.getName) || commitsInBranches.contains(x.getName))
      val c = transformRevCommits(commitsToTransform).par.map(x => RepositoryOperations.patchToAddedLines(main_git, x.sha, true, repo.id)).flatten.flatten.seq
      commitsToExclude.++=(commitsToTransform.map(_.getName).toSet.diff(c.map(_.commit).toSet))
      c
    }
    val commitsToAnalyzeSHA = commitsToAnalyzeAndAddedLines.groupBy(_.commit).keySet
    // common commits that are inserted into branch commits table
    // we filter the ones that are already inserted; these are common commits on multiple branches and we keep track of them
    val branchCommitsToInsert = {
      commitsInBranches.isEmpty match {
        case true => branch_commits.filter(x => commitsToAnalyzeSHA.contains(x.getName))
        case false => branch_commits.filter(x => commitsInBranches.contains(x.getName) || commitsToAnalyzeSHA.contains(x.getName))
      }
    }
    val new_commits = branch_commits.filter(x => commitsToAnalyzeSHA.contains(x.getName))
    new_commits_to_be_added_to_commits.++=(new_commits)
    logger.info(s"${branchInsertion.name} of ${repo.name} has ${new_commits.size} new commits")
    val insertion = insert_new_commits(branchCommitsToInsert, new_commits, commitsToAnalyzeAndAddedLines, repo.id, main_git, branchInsertion, main_repo)

    try {
      val insertNonExistingBranch = DBIO.from(insertion).transactionally
      Await.result(db.run(insertNonExistingBranch), Duration.Inf)
      commitsInBranches.++=(new_commits_to_be_added_to_commits.map(_.getName).toSet)
    }
    catch {
      case ex: java.sql.SQLException if (ex.getErrorCode == 1062) => {
        logger.error(s"duplicate commits for ${branchInsertion.name} of project: $project in repo ${repo.name}")
        println(s"deleting branch ${branchInsertion.id}")
        Await.result(deleteBranches(Seq(branchInsertion.id)), Duration.Inf)
      }
      case ex: java.sql.SQLIntegrityConstraintViolationException if (ex.getMessage.contains("Duplicate entry")) => {
        logger.error(s"duplicate commits for ${branchInsertion.name} of project: $project in repo ${repo.name}")
        println(s"deleting branch ${branchInsertion.id}")
        Await.result(deleteBranches(Seq(branchInsertion.id)), Duration.Inf)
      }
      case ex: java.sql.SQLException => logger.error(s"${ex.getErrorCode} error code with message ${ex.getMessage}")
      case ex: Exception => logger.error(s"${ex.getMessage}")
    }
  }


  def insertProjectBranches(branches: Seq[Ref], project: String, repo: Project, main_git: Git, main_repo: Boolean) = {
    /*
      Commits holds the commits that I have added so far
      CommitsToExclude holds the large commits that are not included.
    */
    val commitsInBranches = new collection.mutable.HashSet[String]()
    val commitsToExclude = new collection.mutable.HashSet[String]()
    val project_forks = getProjectAndForksIDS(project)
    commitsInBranches.++=(Await.result(getProjectsCommitsSHA(project_forks), Duration.Inf).toSet)
    branches.foreach(x => {
      val new_commits_to_be_added_to_commits = new collection.mutable.ListBuffer[RevCommit]()
      // ADD BRANCH TO DB
      logger.info(s"preparing to insert ${x.getName} of ${repo.name} from main project $project")

      val branchExists = Await.result(geRepoBranch(x.getName, repo.id), Duration.Inf)
      branchExists match {
        case Some(branch) => logger.info(s"${branch.name} of ${repo.name} exists with its commits"); Future(branch) // already exists
        case None => insertNonExistingBranchWithRollback(project, x, repo, main_git, main_repo, commitsInBranches, commitsToExclude, new_commits_to_be_added_to_commits)
      }
    })
  }

  def insertGithubProject(project: String) = {
    val cache_map = collection.mutable.HashMap[String, CachedETAG]()
    logger.info("inserting initial main project " + project)
    val repo = GithubExtractor.getRepositoryJsonWithEtag(project, "")
    val initialrepo = repo.get.result.parseJson.convertTo[Option[GithubRepository]].getOrElse(repo_error)
    initialrepo match {
      case x: RepoError => logger.error(s"no $project repository on Github");
        Left(x)
      case x: GithubRepository => {
        /* CLONE MAIN PROJECT AND ADD BRANCHES AND COMMITS AND ADDED LINES */
        val main_repo = cloneProjectIntoDB(project, x, true)

        main_repo match {
          case Some(p) => {
            logger.info("Main repo id is " + p.id)
            /* CLONE FORKED REPOSITORIES AND ADD BRANCHES AND COMMITS */
            val forks_json = GithubExtractor.getProjectForksAndEtags(project) // seq[githubrepos],map[project_name, forks_etag]
            val forks = forks_json._1.filter(x => isActiveFork(x.created_at, x.pushed_at))
            cache_map.put(project, CachedETAG(project, repo.get.etag, forks_json._2.getOrElse(project, "")))
            forks.foreach(fork => {
              val repo_json = GithubExtractor.getRepositoryJsonWithEtag(fork.full_name, "")
              val repo = repo_json.get.result.parseJson.convertTo[Option[GithubRepository]].getOrElse(repo_error)
              repo match {
                case x: RepoError => logger.error("no " + fork.full_name + " repository on Github")
                  Left(x)
                case x: GithubRepository => {
                  cloneProjectIntoDB(project, x, false, p.id)
                  cache_map.put(fork.full_name, CachedETAG(fork.full_name, repo_json.get.etag, forks_json._2.getOrElse(fork.full_name, "")))
                }
              }
            })
          }
          case None => logger.error("could not insert project " + project)
        }
      }
    }
    cache_map.toMap
  }

  def cloneProjectIntoDB(project: String, repository: GithubRepository, main_repo: Boolean, main_repo_id: Int = -1): Option[Project] = {


    def insert_branches(main_git: Git, branches: Seq[Ref], project_db: Project) = {
      try {
        insertProjectBranches(branches, project, project_db, main_git, main_repo)
        main_git.close
        Some(project_db)
      }
      catch {
        case ex: java.sql.BatchUpdateException => {
          logger.error(ex.getMessage)
          deleteProject(project_db.id)
          logger.error("[transaction failed]on project: " + project + " at repo: " + repository.full_name + "; retrying")
          None
          //          cloneProjectIntoDB(project, repository, main_repo, main_repo_id)
        }
        case ex: java.sql.SQLTimeoutException => {
          logger.error(ex.getMessage)
          deleteProject(project_db.id)
          logger.error("[transaction failed]on project: " + project + " at repo: " + repository.full_name + "; retrying")
          None
          //          cloneProjectIntoDB(project, repository, main_repo, main_repo_id)
        }
        case ex: java.sql.SQLException => {
          logger.error(ex.getMessage)
          deleteProject(project_db.id)
          logger.error("[transaction failed]on project: " + project + " at repo: " + repository.full_name + "; retrying")
          None
          //          cloneProjectIntoDB(project, repository, main_repo, main_repo_id)
        }
        case ex: Exception => logger.error("failed on project: " + project + " at repo: " + repository.full_name);
          None
      }
    }

    def insert_repo = {
      val main_git = Git.open(new File(folder_path(project) + repository.full_name + "/.git"))
      main_git.fetch().call()
      val p: Future[Project] = insertUser(repository.full_name).flatMap(u => {
        insertProject(repository, main_repo_id, u.id)
      })
      val s = Await.result(p, Duration.Inf)
      val branches = RepositoryOperations.getBranches(main_git)
      logger.info("inserted " + repository.full_name + " into db")
      logger.info("inserting branches and their commits of " + repository.full_name)
      insert_branches(main_git, branches, s)
    }

    var repo_existed_before = false
    logger.info("inserting repo " + repository.full_name + " that is a fork of main project " + main_repo_id)
    try {
      logger.info("cloning project on hdd and into db " + project + ", repo: " + repository.full_name)
      repo_existed_before = cloneProject(project, repository)
    } catch {
      case ex: Exception => ex.printStackTrace();
        logger.error("cannot clone project to local disk;" + ex.getMessage)
    }

    if (repo_existed_before) {
      // the project existed before. need to check if it was correctly inserted into the databaseGit.open( new File(folder_path(project) + repository.full_name + "/.git")).fetch.call
      val existing_project = Await.result(getProjectByName(repository.full_name), Duration.Inf)
      existing_project match {
        case Some(x) => {
          logger.info(x.name + " exists in the database");
          val main_git = Git.open(new File(folder_path(project) + repository.full_name + "/.git"))
          val branches = RepositoryOperations.getBranches(main_git)
          logger.info("inserted " + repository.full_name + " into db")
          logger.info("inserting branches and their commits of " + repository.full_name)
          insert_branches(main_git, branches, x)
          Some(x)
        }
        case None => insert_repo
      }
    } else {
      logger.info("cloning on hdd done")
      insert_repo
    }
  }

}
