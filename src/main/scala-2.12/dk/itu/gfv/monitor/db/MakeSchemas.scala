package dk.itu.gfv.monitor.db

import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.meta.MTable

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}
import slick.jdbc.MySQLProfile.api._

import concurrent.ExecutionContext.Implicits.global
import NightFetcher.{createSchemaIfNotExists, _}
import dk.itu.gfv.monitor.Ssh
import dk.itu.gfv.monitor.server.SshConnect

/**
  * Created by scas on 26-04-2017.
  */
object MakeSchemas extends Ssh with App {
//  val ssh = SshConnect.ssh_connect

  resetDb
  createSchemaIfNotExists
  //  println(files_modified.schema.createStatements.foreach(println))
  //  println(files_modified_map.schema.createStatements.foreach(println))
//  def createSchemaIfNotExists: Future[Unit] = {
//    db.run(MTable.getTables).flatMap(tables => {
//      if (tables.isEmpty) {
//        Await.result(db.run(sqlu"USE #$dbName"), Duration.Inf)
//        db.run(allSchemas.create).andThen {
//          case Success(_) => {
//            val r = db.run(sqlu"SET foreign_key_checks = 0")
//            Await.result(db.run(sqlu"use #$dbName"), Duration.Inf)
//            val r1 = db.run(
//              sqlu"""ALTER TABLE `added_lines`
//CHANGE COLUMN `text` `text` LONGTEXT NOT NULL ,
//CHANGE COLUMN `text_normalized` `text_normalized` LONGTEXT NOT NULL ;
//""")
//            Await.result(r, Duration.Inf)
//            Await.result(r1, Duration.Inf)
//            println("Schema created ")
//          }
//          case Failure(x) => x.printStackTrace();
//            println("Some error occurred")
//        }
//      }
//      else {
//        println("Schema already exists")
//        val r = db.run(sqlu"""SET foreign_key_checks = 0""")
//
//        Await.result(db.run(sqlu"""use #$dbName"""), Duration.Inf)
//        val r1 = db.run(
//          sqlu"""ALTER TABLE `added_lines`
//CHANGE COLUMN `text` `text` LONGTEXT NOT NULL ,
//CHANGE COLUMN `text_normalized` `text_normalized` LONGTEXT NOT NULL ;
//""")
//
//        Await.result(r, Duration.Inf)
//        Await.result(r1, Duration.Inf)
//        //        db.run(allSchemas.drop).andThen {
//        //          case Success(_) => Future.successful()
//        //          case Failure(x) => x.printStackTrace
//        //        }
//        Future.successful(Unit)
//      }
//    }
//    )
//  }


//  Await.result(createSchemaIfNotExists, Duration.Inf)
  ssh match {
    case Some(x) => x.disconnect()
    case None => Unit
  }
  //  ssh.disconnect();
}
