package dk.itu.gfv.monitor.db

import java.io.File

import dk.itu.gfv.repo.{GithubExtractor, RepositoryOperations}
import dk.itu.gfv.util.{Mail, send}
import better.files.{File => ScalaFile}
import GithubExtractor.{getJSONPagination, getRepositoryJsonWithEtag}
import dk.itu.gfv.model.Model.{GithubRepository, Owner, Parent}
import dk.itu.gfv.monitor.Utils
import dk.itu.gfv.monitor.db.Models._
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Ref

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}
import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import spray.json._
import DefaultJsonProtocol._
import slick.jdbc.MySQLProfile.api._
import scala.collection.JavaConverters._
import scala.collection.immutable.HashMap

/**
  * Created by scas on 20-12-2016.
  */
object NightFetcher extends DatabaseSchema with DBService with Utils {

  //  private val projects_list = ScalaFile("projects.txt").lines.toList

  //
  //  def result[T](f: Future[T]) = {
  //    Await.result(f, Duration.Inf)
  //  }

  /**
    * Find all commtis in the forks that have been created before this commit and have not been integrated back in the mainline.
    * This traverses all the parents starting from the given commit. Any commits that are not traversed
    * are considered to be commits in branches, thus we keep them if they satisfy the time constraint (they need to be before the
    * give commitTime)
    *
    * @param commits    a map of commit to a list of its parents (SHAs)
    * @param cs         a list of Commit objects
    * @param sha        starting point in our traversal
    * @param commitTime commit time to use for discarding newer commits
    * @return
    */
  def find_commits(commits: Map[String, Seq[String]], cs: Seq[Commit], sha: String, commitTime: Int) = {
    val parents = try {
      commits.get(sha).get
    } catch {
      case ex: java.util.NoSuchElementException => println("Cannot find parents of this commit. " + sha + " Likely, the project repository does not contain this fork"); List()
    }

    // should have at least one parent
    // we just mark when we traverse all parents from this commit. Any commits not in this list, are commits from forks, and we finally trim the list by the commit time
    def traverseParentsTailRec(parent: Seq[String]): Map[String, String] = {
      @tailrec
      def innerLoop(parents: Seq[String], a: Map[String, String]): Map[String, String] = {
        if (parents.isEmpty) {
          a
        }
        else {
          val newParents = (for {
            p <- parents
            t = a.get(p) match {
              case None => commits.get(p).getOrElse(List()) // List() as there may be commits that have not added new lines, thus they are not inserted into the DB!
              case Some(x) => Nil
            }
          } yield t).flatten
          innerLoop(newParents, a ++ parents.map(x => x -> "").toMap)
        }
      }

      innerLoop(parent, Map())
    }

    val res = Set(sha) ++ traverseParentsTailRec(parents).toList.map(_._1)
    cs.par.filter(x => x.authorTime.toInt < commitTime).toList.map(_.sha).diff(res.toList).filter(commits.get(_).get.size == 1)
  }


  def sendNotification(msg: String, project: String) = {
    logger.info("sending notification for " + project)
    val from = ("gms.monitor.service@gmail.com", "GMS Monitoring Service")
    val to = Seq("scas@itu.dk")
    val subject = "[GMS][" + project + "]: " + org.joda.time.DateTime.now
    send a new Mail(
      from = from,
      to = to,
      subject = subject,
      message = msg
    )
  }

  /**
    * Returns a list of forks that have new commits since the last time the night fetcher ran
    *
    * @param new_list_of_forks
    * @param old_list_of_forks
    * @return
    */
  def pushed_to_forks(new_list_of_forks: Seq[GithubRepository], old_list_of_forks: Seq[Project]): Seq[GithubRepository] = {
    val t = new_list_of_forks.map(x => x.full_name -> x).toMap
    val z = old_list_of_forks.map(x => x.name -> x).toMap

    val r = t.keySet.intersect(z.keySet).filter(p => {
      val old_pushed_at = z.get(p).get.pushed_at
      val new_pushed_at = t.get(p).get.pushed_at
      new_pushed_at.isAfter(old_pushed_at)
    }).map(x => t.get(x).get).toSeq
    r
  }


  case class CachedETAG(project: String, etag: String, forks_etag: String) {
    //    override def toString = project + "," + etag + "," + forks_etag
  }

  implicit val CachedETAGJson = jsonFormat3(CachedETAG)


  def getUpdatedForksAndUpdateCache(reponame: String) = {
    val (cache, updatedForks) = getUpdatedProjectsWithETAGs(reponame)
    logger.info("Updating caches")
    writeUpdatedCacheETAGs(cache)
    updatedForks
  }

  def writeUpdatedCacheETAGs(cachedETAGs: Map[String, CachedETAG]) = {
    val cached_etags_file = ScalaFile(cache_file).createIfNotExists()
    cached_etags_file.overwrite(cachedETAGs.values.toJson.prettyPrint)
  }

  def getUpdatedProjectsWithETAGs(reponame: String): (Map[String, CachedETAG], Seq[GithubRepository]) = {

    val projects = Await.result(getProjects, Duration.Inf)

    val cached_etags_file = ScalaFile(cache_file).createIfNotExists()
    val cached_etags = cached_etags_file.contentAsString.parseJson.convertTo[Option[List[CachedETAG]]] match {
      case Some(s) => s.map(x => x.project -> x).toMap
      case None => Map()
    }
    //    val cached_etags = ScalaFile(cache_file).lines.toList.map(x => {
    //      val s = x.split(",")
    //      s(0) -> CachedETAG(s(0), s(1), s(2))
    //    }).toMap
    val new_etags = collection.mutable.HashMap[String, CachedETAG]()
    new_etags.++=(cached_etags)

    logger.info("fetching github repo's json of " + reponame)
    val mainRepo = getRepositoryJsonWithEtag(reponame, {
      new_etags.get(reponame) match {
        case Some(x) => x.etag
        case None => ""
      }

    })

    logger.info("fetching forks from github")
    val forksJSON = getJSONPagination(reponame, "forks", {
      new_etags.get(reponame) match {
        case Some(x) => Some(x.forks_etag)
        case None => None
      }
    })

    new_etags.put(reponame, CachedETAG(reponame, mainRepo.get.etag, forksJSON.get.etag))

    def addToList(input: Seq[GithubRepository]): Seq[GithubRepository] = input match {
      case h +: t => {
        new_etags.get(h.full_name) match {
          case Some(x) => {
            val r = getRepositoryJsonWithEtag(h.full_name, x.etag)
            val forks_r = getJSONPagination(h.full_name, "forks", Some(x.forks_etag))
            val forks = forks_r match {
              case Some(x) => {
                x.status_code match {
                  // no new forks; get existing forks of this project from DB
                  case 304 => addToList(oneLevelForks(projects, h.full_name).map(x => GithubRepository(x.id, x.name, Owner(Await.result(getUserById(x.owner), Duration.Inf).head.login), x.fork, x.created_at, x.updated_at, x.pushed_at, x.stargazers_count, Some(x.language), x.forks_count, x.default_branch, Some(Parent(Await.result(getProjectById(x.fork_of), Duration.Inf).name)), x.subscribers_count)))
                  // new forks
                  case _ => addToList(forks_r.get.result.parseJson.convertTo[Seq[GithubRepository]])
                }
              }
              case None => Seq()
            }
            val main_repo = r match {
              case Some(x) => x.status_code match {
                case 304 => {
                  Seq()
                }
                case _ => Seq(r.get.result.parseJson.convertTo[GithubRepository])
              }
              case None => Seq()
            }
            r match {
              case Some(x) => { new_etags.put(h.full_name, CachedETAG(h.full_name, r.get.etag, forks_r match {
                case Some(x) => x.etag
                case None => "" }
              ))}
              case None =>
            }
            main_repo ++ forks ++ addToList(t)
          }
          case None => {
            val r = getRepositoryJsonWithEtag(h.full_name, "")
            val forks_r = getJSONPagination(h.full_name, "forks", None)
            new_etags.put(h.full_name, CachedETAG(h.full_name, r.get.etag, forks_r.get.etag))
            Seq(h) ++ addToList(forks_r.get.result.parseJson.convertTo[Seq[GithubRepository]]) ++ addToList(t)
          }
        }
      }

      case Seq(h)
      => {
        new_etags.get(h.full_name) match {
          case Some(x) => {
            val r = getRepositoryJsonWithEtag(h.full_name, x.etag)
            val forks_r = getJSONPagination(h.full_name, "forks", Some(x.forks_etag))
            val forks = forks_r match {
              case Some(x) => x.status_code match {
                // no new forks; get existing forks of this project from DB
                case 304 => addToList(oneLevelForks(projects, h.full_name).map(x => GithubRepository(x.id, x.name, Owner(Await.result(getUserById(x.owner), Duration.Inf).head.login), x.fork, x.created_at, x.updated_at, x.pushed_at, x.stargazers_count, Some(x.language), x.forks_count, x.default_branch, Some(Parent(Await.result(getProjectById(x.fork_of), Duration.Inf).name)), x.subscribers_count)))
                // new forks
                case _ => addToList(forks_r.get.result.parseJson.convertTo[Seq[GithubRepository]])
              }
              case None => Seq()
            }
            val main_repo = r.get.status_code match {
              case 304 => Seq()
              case _ => Seq(r.get.result.parseJson.convertTo[GithubRepository])
            }
            new_etags.put(h.full_name, CachedETAG(h.full_name, r.get.etag, forks_r.get.etag))
            main_repo ++ forks
          }
          case None => {
            val r = getRepositoryJsonWithEtag(h.full_name, "")
            val forks_r = getJSONPagination(h.full_name, "forks", None)
            new_etags.put(h.full_name, CachedETAG(h.full_name, r.get.etag, forks_r.get.etag))
            Seq(h) ++ addToList(forks_r.get.result.parseJson.convertTo[Seq[GithubRepository]])
          }
        }
      }
      case Seq()
      => Seq()
    }

    val forks_result = forksJSON.get.status_code match {
      case 304 => logger.info(s"$reponame 's forks were not updated"); addToList(oneLevelForks(projects, reponame).map(x => GithubRepository(x.id, x.name, Owner(Await.result(getUserById(x.owner), Duration.Inf).head.login), x.fork, x.created_at, x.updated_at, x.pushed_at, x.stargazers_count, Some(x.language), x.forks_count, x.default_branch, Some(Parent(Await.result(getProjectById(x.fork_of), Duration.Inf).name)), x.subscribers_count)))
      case _ => logger.info(s"$reponame forks were updated"); addToList(forksJSON.get.result.parseJson.convertTo[Seq[GithubRepository]])
    }
    //cached_etags_file.overwrite(new_etags.values.toJson.prettyPrint)
    //    printToFile(new File(cache_file))(fp => new_etags.values.foreach(fp.println))
    //    ScalaFile(cache_file).write(new_etags.values.mkString("\n"))
    mainRepo.get.status_code match {
      case 304 => logger.info(s"$reponame was not updated"); (new_etags.toMap, forks_result)
      case _ => logger.info(s"$reponame was updated"); (new_etags.toMap, Seq(mainRepo.get.result.parseJson.convertTo[GithubRepository]) ++ forks_result)
    }
  }

  type ProjectCommit = Map[String, Commit]


  def monitor_project(project: String)(implicit analyze: Boolean) = {

    val projectNewCommits = new collection.mutable.HashMap[String, Seq[Commit]]()

    logger.info("running monitoring on " + project)
    // allow to add new projects to the list of project without stopping the GMS, if the project does not exist in the database
    Await.result(getProjectByName(project), Duration.Inf) match {
      case Some(p) => Nil
      case None => insertGithubProject(project)
    }
    val p = Await.result(getProjectByName(project), Duration.Inf)
    //    val new_commits_to_analyze_with_addedLines = collection.mutable.HashMap[Commit, Vector[AddedLine]]()
    val (cachedETAGs, projects_github) = getUpdatedProjectsWithETAGs(project)
    //    logger.info(projects_github.toString)
    logger.info("getting project")
    val projects_db = {
      val a = Seq(Await.result(getProjectByName(project), Duration.Inf).get)
      val b = getForksOfProjectOnly(project)
      a ++ b
    }
    lazy val projects_github_map = projects_github.map(x => x.full_name -> x).toMap
    lazy val branches_db = Await.result(getBranchesOfProjects(projects_db.map(_.id)), Duration.Inf)
    logger.info("fetching all commits (including forks' commit) from DB of " + project)

    //    val commitsBeforeUpdate = Await.result(getProjectAllCommits(project), Duration.Inf)

    val forks = getForksOfProjectOnly(project)
    val oldMainProjectCommits = getProjectCommitsMap(project)
    val oldForksCommits = forks.flatMap(x => getProjectCommitsMap(x.name)).toMap

    val oldCommitMerges = Await.result(getCommitParents, Duration.Inf).groupBy(_.sha).filter(x => x._2.size > 1)
    val oldProjectCommits = (oldMainProjectCommits ++ oldForksCommits) map (x => x._1 -> x._2.filterNot(z => oldCommitMerges.contains(z.sha)))
    // TODO
    // CHECK IF AFTER A PROJECT IS UPDATED, WE INSERT NEW COMMITS THAT WERE INTRODUCED IN THE PREVIOUS UPDATE, THUS DUPLICATION
    //

    /* updated projects - insert new data into db and fetch the changes */
    val updated_projects = pushed_to_forks(projects_github, projects_db)
    logger.info("the following projects have been updated " + updated_projects)
    updated_projects.foreach(fork => {
      // new branches
      val fork_id = Await.result(getProjectByName(fork.full_name), Duration.Inf).get
      val old_branches = branches_db.filter(_.project == fork_id.id)
      val old_branches_map = old_branches.map(x => x.name -> x).toMap
      val git = Git.open(new File(folder_path(project) + fork.full_name + "/.git"))
      git.fetch.call

      val branches = RepositoryOperations.getBranches(git).map(x => x.getName -> x).toMap

      // updated branches
      val updated_branches = {
        // same branches
        val same_branches = branches.keys.toSeq.intersect(old_branches.map(_.name))
        logger.info("same branches " + same_branches)

        def up_br(input: Seq[String]): Seq[Ref] = input match {
          case h +: t => {
            val last_commit_old = old_branches_map.get(h).get.last_commit
            //            println("last_commit_old " + last_commit_old)
            val last_commit_after_fetch = branches.get(h).get.getObjectId.getName
            //            println("last_commit_after_fetch " + last_commit_after_fetch)
            // check if the last_commit from DB is the same with last commit from git repo after fetch
            // if no, this branch has new commits
            if (!last_commit_old.equals(last_commit_after_fetch)) Seq(branches.get(h).get) ++ up_br(t) else up_br(t)
          }
          case Seq(h) => {
            val last_commit_old = old_branches_map.get(h).get.last_commit
            //            println("last_commit_old " + last_commit_old)
            val last_commit_after_fetch = branches.get(h).get.getObjectId.getName
            //            println("last_commit_after_fetch " + last_commit_after_fetch)
            // check if the last_commit from DB is the same with last commit from git repo after fetch
            if (!last_commit_old.equalsIgnoreCase(last_commit_after_fetch)) Seq(branches.get(h).get) else Nil
          }
          case Seq() => Seq()
        }

        up_br(same_branches)
      }
      logger.info("there were " + updated_branches.size + " branches updated in total ")
      updated_branches.foreach(br => {
        val br_db = Await.result(getBranch(fork_id.id, br.getName), Duration.Inf)

        // FIXME This might be dangerous and break if a branch is deleted.
        val last_commit_objId = git.getRepository.resolve(old_branches_map.get(br.getName).get.last_commit)
        val first_commit_objId = git.getRepository.resolve(br.getObjectId.getName)

        //        val branchCommits = git.log().add(br.getObjectId).call().asScala.toSeq.init//.addRange(last_commit_objId, first_commit_objId).call.asScala.toSeq
        val newCommits = git.log().add(br.getObjectId).addRange(last_commit_objId, first_commit_objId).call.asScala.toSeq
        //
        val newCommitsAddedLines = newCommits.flatMap(x => RepositoryOperations.patchToAddedLines(git, x.getName, true, fork_id.id).getOrElse(Vector()))
        val dataInsertion = insert_new_commits(newCommits, newCommits, newCommitsAddedLines, fork_id.id, git, br_db, fork_id.name.equalsIgnoreCase(project))

        try {
          val insertion = DBIO.from(dataInsertion).transactionally
          Await.result(db.run(insertion), Duration.Inf)
          val old_br = old_branches_map.get(br.getName).get
          Await.result(updateBranch(Branch(old_br.id, old_br.name, br.getObjectId.getName, fork_id.id)), Duration.Inf)
        }
        catch {
          case ex: java.sql.SQLException if (ex.getErrorCode == 1062) => {
            logger.error(s"duplicate commits for ${
              br_db.name
            } of project: $project in repo ${
              fork_id.name
            }")
          }
          case ex: java.sql.SQLIntegrityConstraintViolationException if (ex.getMessage.contains("Duplicate entry")) => {
            logger.error(s"duplicate commits for ${
              br_db.name
            } of project: $project in repo ${
              fork_id.name
            }")
          }
          case ex: java.sql.SQLException => logger.error(s"${
            ex.getErrorCode
          } error code with message ${
            ex.getMessage
          }")
          case ex: Exception => logger.error(s"${
            ex.getMessage
          }")
        }
      })

      val new_branches = branches.keys.toSeq.diff(old_branches.map(_.name)).map(branches.get(_).get)
      if (new_branches.nonEmpty) {
        logger.info("there are " + new_branches.size + " new branches in " + fork.full_name + " of project " + project)
        insertProjectBranches(new_branches, project, fork_id, git, fork_id.name.equalsIgnoreCase(project))
      }
      projectNewCommits.put(fork.full_name, Await.result(getProjectCommits(fork.full_name), Duration.Inf))
      // Update fork's metadata
      Await.result(insertOrUpdateProject(Project(fork_id.id, fork_id.name, fork_id.owner, fork_id.language, fork.default_branch, fork_id.fork, fork_id.fork_of, fork_id.fork_of_main, fork.forks_count, fork.stargazers_count, fork.subscribers_count, fork.created_at, fork.pushed_at, fork.updated_at)), Duration.Inf)
    })

    /* check for new forks */
    val new_forks = projects_github.map(_.full_name).diff(projects_db.map(_.name)).map(projects_github_map.get(_)).flatten
    if (new_forks.nonEmpty) {
      logger.info("there are " + new_forks.size + " new forks of " + project)
      /* add new forks to the db. we do this after the analysis so that its easier to query the db for the previous state */
      // filter new forks to only clone if they have done commits
      new_forks.filter(x => RepositoryOperations.isActiveFork(x.created_at, x.pushed_at)).foreach(fork => {
        logger.info("New fork to be inserted " + fork)
        GithubExtractor.getRepository(fork.full_name) match {
          case Some(repo) => cloneProjectIntoDB(project, repo, false)
          case None => logger.info("cannot decode json from new fork repository")
        }
      })
    }

    /* if we get so far, we need to save the new cached etags*/
    writeUpdatedCacheETAGs(cachedETAGs)

    if (analyze) {

      val duplicates = new collection.mutable.ListBuffer[Duplicate]()

      // all the projects that were updated
      // merge commits


      val mergeCommits = Await.result(getCommitParents, Duration.Inf).groupBy(_.sha).filter(_._2.size > 1)
      //      projectNewCommits.foreach { case (fork, commits) => {
      projectNewCommits.map(x => x._1 -> x._2.filterNot(z => mergeCommits.contains(z.sha))).foreach {
        case (fork, commits) => {
          val newCommits = commits.diff(oldProjectCommits.get(fork).get)
          val addedLinesInNewCommits = Await.result(getAddedLinesByCommits(newCommits.map(_.sha)), Duration.Inf)
          val addedLinesInNewCommitsMap = Await.result(getAddedLinesByCommits(newCommits.map(_.sha)), Duration.Inf).groupBy(_.commit)
          if (addedLinesInNewCommits.nonEmpty) {
            // these added lines need to be compared to the added lines from forks' commits that are not in the main, or the commits in the main that are not existing in this fork before each commit was done.
            val forksCommits = {
              val c = oldProjectCommits.toMap.-(fork).flatMap(x => x._2.diff(oldProjectCommits.get(project).get)) // forks commits that are not in main
              val m = oldProjectCommits.get(project).get.diff(oldProjectCommits.get(fork).get) // main commits that do not exist in fork
              c ++ m
            }.toSeq

            val addedLinesInForksCommits = Await.result(getAddedLinesByCommits(forksCommits.map(_.sha)), Duration.Inf).groupBy(_.commit)


            val r = newCommits.flatMap(c => {
              // filter out commits that are done after this commit
              val filteredCommits = forksCommits.filter(x => x.authorTime < c.authorTime).map(_.sha).toSet

              val addedLinesInThisCommit = addedLinesInNewCommitsMap.get(c.sha).getOrElse(Vector())
              val addedLinesInOtherForks = addedLinesInForksCommits.filter(x => filteredCommits.contains(x._1)).values.flatten // get the added lines of only the commits that existed before this commit

              val res =
              //            logger.info("computing similarities on the following commits: " + x.toSeq)
              //            val added_lines_db = Await.result(getAddedLinesByCommits(x.map(_.sha)), Duration.Inf)
              //            logger.info(added_lines_db.size + " existing added lines to be compared to")
                computeSimilarities(addedLinesInThisCommit.toVector, addedLinesInOtherForks.toVector, "n-levenshtein", 0.8)
              //          }).toVector
              //
              //          val sim_results_for_db = transformSimResultstoDuplicates(res, p.get)
              //
              val sim_results_for_db = transformSimResultstoDuplicates(res, p.get)

              sim_results_for_db
            })
            duplicates.++=(r)
          }
        }
      }
      val msg = "New commits to analyze in " + project + "\n"
      val duplicates_inserted = insertSimilarityResults(duplicates.toSeq)
      val duplicates_db = duplicates_inserted.andThen {
        case Success(r) => {
          val m = msg + "\nDuplicates\n" + r.mkString("\n")
          sendNotification(m, project)
        }
        case Failure(f) => {
          logger.error("Failed to insert duplicates in " + project)
          sendNotification("Could not insert duplicates", project)
        }
      }
      Await.result(duplicates_db, Duration.Inf)

      //        val allCommitsUpdated = Await.result(getProjectAllCommits(project), Duration.Inf)
      //        logger.info("existing commits:" + commitsBeforeUpdate.size)
      //        logger.info("all commits after new forks:" + allCommitsUpdated.size)
      //
      //        // seq(1,2,3).diff(seq(1,2,4) => seq(3)
      //        val newCommits = allCommitsUpdated.diff(commitsBeforeUpdate)
      //        logger.info("new commits" + newCommits.size)
      //        val allAddedLinesUpdated = Await.result(getAddedLinesByCommits(newCommits.map(_.sha)), Duration.Inf)
      //        val allAddedLinesUpdatedToAnalyze = allAddedLinesUpdated.groupBy(_.commit)
      //        //    logger.info("" + allAddedLinesUpdated.size)
      //        //    logger.info("" + allAddedLinesUpdatedToAnalyze)
      //        val new_commits_to_analyze_with_addedLines = newCommits.map(x => x -> {
      //          allAddedLinesUpdatedToAnalyze.get(x.sha) match {
      //            case Some(a) => a.toVector
      //            case None => Vector()
      //          }
      //        }).toMap
      //        if (new_commits_to_analyze_with_addedLines.nonEmpty) {
      //          /* run the duplicate analysis on the added lines from new commits */
      //          //    val new_commits_db = Await.result(getProjectAllCommits(project), Duration.Inf)
      //          //    val new_commits_to_analyze = new_commits_db.diff(commits_db)
      //          logger.info(new_commits_to_analyze_with_addedLines.size + " new commits in " + project)
      //
      //          val msg = new_commits_to_analyze_with_addedLines.size + " new commits to analyze in " + project + "\n" + new_commits_to_analyze_with_addedLines.mkString("\n")
      //  //        lazy val commits_parents = Await.result(getCommitsParents(allCommitsUpdated.map(_.sha)), Duration.Inf).groupBy(_.sha).map(x => x._1 -> x._2.map(_.parent)).toMap
      //
      //          logger.info("analyzing commits now")
      //          //FIXME the find_commits should be replaced by taking all the commits of forks
      //          new_commits_to_analyze_with_addedLines.foreach { case (commit, al) if al.nonEmpty => {
      //            logger.info(s"analyzing $commit")
      //            // find the commits in the forks that are before this commit and not in the main project.
      //
      //            val commits_in_forks = {
      //              val forksCommits = Await.result(getProjectsForksCommits(project), Duration.Inf).filter(_.authorTime < commit.authorTime)
      //              val mainProjectCommits = Await.result(getProjectCommitsSHA(project),Duration.Inf)
      //              forksCommits.filterNot( x => mainProjectCommits.contains(x.sha))
      //            }
      //
      //
      //  //          val commits_in_forks = forks_commits// find_commits(commits_parents, allCommitsUpdated, commit.sha, commit.authorTime.toInt)
      //            logger.info("new commits will be compared against " + commits_in_forks.size + " commits")
      //            //          val added_lines_db = Await.result(getAddedLinesByCommits(commits_in_forks.toSeq), Duration.Inf)
      //            logger.info(al.size + s" new added lines in ${commit.sha} commit")
      //            //          logger.info(added_lines_db.size + " existing added lines to be compared to")
      //
      //            // analyze 5 commits at a time, which is max 25k added lines at a time
      //            val commitsInForksGroupedInBuckets = commits_in_forks.grouped(5)
      //
      //            logger.info("computing similarities for " + project + " on " + al.size + " new added lines")
      //            //val res = computeSimilarities(al, added_lines_db.toVector, "n-levenshtein", 0.8)
      //            val res = commitsInForksGroupedInBuckets.flatMap(x => {
      //              logger.info("computing similarities on the following commits: " + x.toSeq)
      //              val added_lines_db = Await.result(getAddedLinesByCommits(x.map(_.sha)), Duration.Inf)
      //              logger.info(added_lines_db.size + " existing added lines to be compared to")
      //              computeSimilarities(al, added_lines_db.toVector, "n-levenshtein", 0.8)
      //            }).toVector
      //
      //            val sim_results_for_db = transformSimResultstoDuplicates(res, p.get)
      //
      //            logger.info("inserting similarity results for " + project + ", " + sim_results_for_db.size)
      //            val duplicates_inserted = insertSimilarityResults(sim_results_for_db)
      //            val duplicates_db = duplicates_inserted.andThen {
      //              case Success(r) => {
      //                val m = msg + "\n" + "commit: " + commit + "\nDuplicates\n" + r.mkString("\n")
      //                sendNotification(m, project)
      //              }
      //              case Failure(f) => {
      //                logger.error("Failed to insert duplicates in " + project + " at commit " + commit + "; " + sim_results_for_db)
      //                sendNotification("Could not insert duplicates", project)
      //              }
      //            }
      //            Await.result(duplicates_db, Duration.Inf)
      //          }
      //          }
      //        }
    }

  }

  //  def start_fetcher = {
  //    logger.info("starting the monitor service")
  //    while (true) {
  //      projects_list foreach monitor_project
  //      Thread.sleep(10000)
  //    }
  //  }
  //
  //  def main(args: Array[String]): Unit = {
  //
  //    logger.info("starting night fetcher")
  //    if (args.size == 1) {
  //      if (args(0).equalsIgnoreCase("true"))
  //        analyze = true
  //    }
  //    start_fetcher
  //
  //    //    time("test", Await.result(getAddedLines1,Duration.Inf))
  //  }
}
