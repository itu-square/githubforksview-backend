package dk.itu.gfv.monitor.db

import dk.itu.gfv.monitor.db.Models._
import org.joda.time.DateTime
import slick.jdbc.MySQLProfile.api._
import slick.lifted.{TableQuery, Tag}
import com.github.tototoshi.slick.MySQLJodaSupport._
import slick.model.ForeignKeyAction

/**
  * Created by scas on 12/5/2016.
  */
trait DatabaseSchema {

  val sha_length = "varchar(40)"
  val default_time = org.joda.time.DateTime.parse("1971-01-01T10:00:00Z")
  class AddedLines(tag: Tag) extends Table[AddedLine](tag, "added_lines") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def project = column[Int]("project")

    //    def branch = column[Int]("branch")

    //    def commit_sha = column[String]("commit_sha")
    def commit = column[String]("commit",O.SqlType(sha_length))

    def file = column[String]("filename")

    def lineNumber = column[Int]("line_number")

    def text_normalized = column[String]("text_normalized")

    def text = column[String]("text")



    //    def projectFK = foreignKey("FK_ADDEDLINE_PROJECT", project, projects)(_.id,onDelete=ForeignKeyAction.Cascade)
    //    def branchFK = foreignKey("FK_ADDEDLINE_BRANCHES", branch, branches)(_.id,onDelete=ForeignKeyAction.Cascade)
    def commitFK = foreignKey("FK_ADDEDLINE_COMMITS", commit, commits)(_.sha, onDelete = ForeignKeyAction.Cascade)

    def idx = index("id", id)
    def idx1 = index("commit", commit)
    def idx2 = index("id_commit", (id,commit))
    def idx3 = index("commit_id", (commit,id))
    //    def idx4 = index("project", (project))
    //    def idx5 = index("project_commit", (project, commit))
    //    def idx6 = index("project_id_commit", (id, project, commit))

    //    def * = (id, project, branch, commit, file, lineNumber, text, text_normalized) <> (AddedLine.tupled, AddedLine.unapply)
    def * = (id, project, commit, file, lineNumber, text_normalized,text) <> (AddedLine.tupled, AddedLine.unapply)

  }

  val addedLines = TableQuery[AddedLines]

  class BranchCommits(tag: Tag) extends Table[BranchCommit](tag, "branch_commits") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def branch = column[Int]("branch")

    def commit = column[String]("commit",O.SqlType(sha_length))

    def branchFK = foreignKey("FK_BRANCH_COMMIT", branch, branches)(_.id, onDelete = ForeignKeyAction.Cascade)
    def commitFK = foreignKey("FK_BRANCH_COMMIT_COMMITS", commit, commits)(_.sha, onDelete = ForeignKeyAction.Cascade)

    def idx = index("id", id)

    def idx2 = index("commit", commit)

    //    def idx3 = index("id_commit", (id, commit))

    //    def idx4 = index("commit_id", (commit, id))
    //
    def idx5 = index("branch", branch)
    //
    def idx6 = index("branch_commit", (branch,commit))
    //
    def idx7 = index("commit_branch", (commit, branch))

    def * = (id, branch, commit) <> (BranchCommit.tupled, BranchCommit.unapply)
  }

  val branches_commits = TableQuery[BranchCommits]

  class Branches(tag: Tag) extends Table[Branch](tag, "branches") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def lastCommit = column[String]("last_commit")

    def project = column[Int]("project")

    def projectFK = foreignKey("FK_BRANCH_PROJECT", project, projects)(_.id, onDelete = ForeignKeyAction.Cascade)


    def idx = index("id", id)
    //
    def idx2 = index("project", project)
    //
    def idx3 = index("id_project", (id, project))
    //
    def idx4 = index("project_id", (project, id))

    def * = (id, name, lastCommit, project) <> (Branch.tupled, Branch.unapply)
  }

  val branches = TableQuery[Branches]


  class CommitParents(tag: Tag) extends Table[CommitParent](tag, "commit_parents") {
    //    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def sha = column[String]("sha", O.SqlType(sha_length))

    def parent = column[String]("parent",O.SqlType(sha_length))

    def commitFK = foreignKey("FK_COMMIT_PARENT_COMMITS", sha, commits)(_.sha, onDelete = ForeignKeyAction.Cascade)

    //    def idx = index("id_index", id)
    def idx1 = index("sha", sha)
    //    def idx2 = index("parent", parent)
    //    def idx3 = index("sha_parent", (sha,parent))
    //    def idx4 = index("parent_sha", (parent, sha))


    def * = (sha, parent) <> (CommitParent.tupled, CommitParent.unapply)
  }

  val commitParents = TableQuery[CommitParents]

  class Commits(tag: Tag) extends Table[Commit](tag, "commits") {
    def sha = column[String]("sha", O.PrimaryKey, O.SqlType(sha_length))

    //    def parent = column[String]("parent_sha")

    def author = column[Int]("author")
    def committer = column[Int]("committer")

    def message = column[String]("message")

    def authorTime = column[Int]("authorTime")
    def committerTime = column[Int]("committerTime")

    def idx = index("sha", sha)

    //    def branchFK = foreignKey("FK_BRANCH_COMMIT_SHA", sha, branches_commits)(_.commit, onDelete=ForeignKeyAction.Cascade)

    //    def authorFK = foreignKey("FK_AUTHOR_USER", commitAuthor, users)(_.id, onDelete=ForeignKeyAction.Cascade)

    def * = (sha, author,committer,  message, authorTime, committerTime) <> (Commit.tupled, Commit.unapply)
  }

  val commits = TableQuery[Commits]

  class CommitOutliers(tag: Tag) extends Table[CommitOutlier](tag, "commit_outliers"){

    def sha = column[String]("sha", O.PrimaryKey, O.SqlType(sha_length))
    def project_main = column[String]("project_main")
    def repo_id = column[Int]("repo_id")
    def added_lines_size = column[Int]("added_lines_size")
    def * = (sha,project_main,repo_id,added_lines_size) <> (CommitOutlier.tupled, CommitOutlier.unapply)
  }

  val commit_outliers = TableQuery[CommitOutliers]

  class Duplicates(tag: Tag) extends Table[Duplicate](tag, "duplicates"){
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def project = column[Int]("project")
    def baseLine = column[Int]("base_line")
    def duplicateLine = column[Int]("duplicate_line")
    def score = column[Double]("score")
    def duplicate_type = column[String]("duplicate_type")
    def internalUseful = column[Option[Boolean]]("internalUseful", O.Default(None))
    def internalNotUseful = column[Option[Boolean]]("internalNotUseful", O.Default(None))
    def sentToDev = column[Option[Boolean]]("sentToDev", O.Default(None))
    def devUseful = column[Option[Boolean]]("devUseful", O.Default(None))
    def devUsefulComment = column[Option[String]]("devUsefulComment")
    def devResponseTime = column[String]("devResponseTime")

    def idx = index("id", id)
    def idx1 = index("baseLine", baseLine)
    //    def idx2 = index("baseLine_id",(baseLine,id))
    //    def idx3 = index("id_baseLine",(id,baseLine))
    //    def idx4 = index("id_baseLine",(baseLine, duplicateLine))
    //    def idx5 = index("id_baseLine",(id,baseLine))

    def * = (id, project, baseLine, duplicateLine, score, duplicate_type, internalUseful, internalNotUseful, sentToDev, devUseful, devUsefulComment, devResponseTime) <> (Duplicate.tupled, Duplicate.unapply)

  }

  val duplicates = TableQuery[Duplicates]

  class DuplicatesRetrospectiveAnalysis(tag: Tag) extends Table[Duplicate](tag, "duplicates_retrospective_analysis"){
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def project = column[Int]("project")
    def baseLine = column[Int]("base_line")
    def duplicateLine = column[Int]("duplicate_line")
    def score = column[Double]("score")
    def duplicate_type = column[String]("duplicate_type")
    def internalUseful = column[Option[Boolean]]("internalUseful", O.Default(None))
    def internalNotUseful = column[Option[Boolean]]("internalNotUseful", O.Default(None))
    def sentToDev = column[Option[Boolean]]("sentToDev", O.Default(None))
    def devUseful = column[Option[Boolean]]("devUseful", O.Default(None))
    def devUsefulComment = column[Option[String]]("devUsefulComment")
    def devResponseTime = column[String]("devResponseTime")

    def idx = index("id", id)
    def idx1 = index("baseLine", baseLine)
    //    def idx2 = index("baseLine_id",(baseLine,id))
    //    def idx3 = index("id_baseLine",(id,baseLine))
    //    def idx4 = index("id_baseLine",(baseLine, duplicateLine))
    //    def idx5 = index("id_baseLine",(id,baseLine))

    def * = (id, project, baseLine, duplicateLine, score, duplicate_type, internalUseful, internalNotUseful, sentToDev, devUseful, devUsefulComment, devResponseTime) <> (Duplicate.tupled, Duplicate.unapply)

  }

  val duplicates_retrospective_analysis = TableQuery[DuplicatesRetrospectiveAnalysis]

  class Projects(tag: Tag) extends Table[Project](tag, "projects") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name",O.SqlType("varchar(180)"))

    def owner = column[Int]("owner")

    def language = column[String]("language")

    def default_branch = column[String]("default_branch")

    def fork = column[Boolean]("fork")

    def fork_of = column[Int]("fork_of")

    def fork_of_main = column[Int]("fork_of_main")

    def forks_count = column[Int]("forks_count")

    def stargazers_count = column[Int]("stargazers_count")

    def subscribers_count = column[Int]("subscribers_count", O.Default(0))

    def created_at = column[DateTime]("created_at", O.Default(default_time))

    def pushed_at = column[DateTime]("pushed_at",O.Default(default_time))

    def updated_at = column[DateTime]("updated_at",O.Default(default_time))

    def ownerFK = foreignKey("FK_PROJECT_OWNER", owner, users)(_.id, onDelete = ForeignKeyAction.Cascade, onUpdate = ForeignKeyAction.Cascade)


    def idx = index("id", id)

    def idx4 = index("name", name)
//
    def idx2 = index("fork_of_main", fork_of_main)
//
    def idx3 = index("id_fork_of", (id, fork_of_main))
//
//    def idx4 = index("owner", owner)


    def * = (id, name, owner, language, default_branch, fork, fork_of, fork_of_main, forks_count, stargazers_count, subscribers_count.?, created_at, pushed_at, updated_at) <> (Project.tupled, Project.unapply)
  }

  val projects = TableQuery[Projects]

  class Users(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def login = column[String]("login")

    def name = column[String]("name")

    def email = column[String]("email")

    def created_at = column[DateTime]("created_at",O.Default(default_time))

    def idx = index("id", id)

    def * = (id, login, name, email, created_at) <> (User.tupled, User.unapply)
  }

  val users = TableQuery[Users]

  class FilesModifiedAnalysis(tag: Tag) extends Table[FilesModified](tag, "files_modified_analysis"){

    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def project = column[String]("project", O.SqlType("varchar(180)"))

    def commit = column[String]("commit", O.SqlType(sha_length))

    def file = column [String]("file")

    def commit_time = column[Int]("commit_time")

    def author = column[Int]("author_id")

    def url = column[String]("url")

    def idx = index("project_index",project)

    def * = (id,project,commit,file,commit_time,author,url ) <> (FilesModified.tupled, FilesModified.unapply)
  }

  val files_modified = TableQuery[FilesModifiedAnalysis]

  class FilesModifiedMapAnalysis(tag: Tag) extends Table[FilesModifiedMap](tag, "files_modified_map_analysis"){

    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def project = column[String]("project", O.SqlType("varchar(180)"))

    def commit = column[String]("commit", O.SqlType(sha_length))

    def file = column [String]("file")

    def commit_time = column[Int]("commit_time")

    def author = column[Int]("author_id")

    def url = column[String]("url")

    def files_modified_id = column[Int]("files_modified_id")

    def fk = foreignKey("files_to_results", files_modified_id, files_modified)(_.id)

    def idx = index("project_index",project)
    def idx1 = index("files_modified_index",files_modified_id)

    def * = (id,project,commit,file,commit_time,author,url, files_modified_id) <> (FilesModifiedMap.tupled, FilesModifiedMap.unapply)
  }

  val files_modified_map = TableQuery[FilesModifiedMapAnalysis]


  class SurveyResponse(tag: Tag) extends Table[Survey](tag, "survey"){
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def link = column[String]("link")
    def q1 = column[String]("q1")
    def q2 = column[String]("q2")
    def q3 = column[String]("q3")
    def q4 = column[String]("q4")

    def * = (id,link,q1,q2,q3,q4) <> (Survey.tupled, Survey.unapply)
  }

  val surveyResponses = TableQuery[SurveyResponse]
  val allSchemas = users.schema ++ projects.schema ++ branches.schema ++ branches_commits.schema ++ commits.schema ++ commitParents.schema ++ addedLines.schema ++ duplicates.schema ++ files_modified.schema ++files_modified_map.schema ++ commit_outliers.schema ++ duplicates_retrospective_analysis.schema ++ surveyResponses.schema

}
