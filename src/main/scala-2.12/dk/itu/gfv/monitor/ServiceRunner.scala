package dk.itu.gfv.monitor

import dk.itu.gfv.monitor.db.{NightFetcher}
import better.files.{File => ScalaFile}

/**
  * Created by scas on 03-03-2017.
  */
object ServiceRunner extends Ssh {

  def main(args: Array[String]): Unit = {
    logger.info("gms started")
    implicit val analyze = args.length match {
      case 1 => args(0).toBoolean
      case _ => logger.error("provide a argument that describes if the service should analyze duplicates or not (true false)"); sys.exit(1);
    }
//    try {
      val projects_to_be_monitored_file = ScalaFile(projects_to_be_monitored)
      logger.info("starting monitoring projects")
      projects_to_be_monitored_file.lines.toList.foreach(p => {
        val project = p.trim.replaceAll("\r?\n", "\n").trim
        NightFetcher.monitor_project(project)
      })
      ssh match {
        case Some(x) => x.disconnect()
        case None => Unit
      }
    }
//    catch {
//      case ex: Exception => ex.printStackTrace; logger.error(ex.getMessage);
//    }
//  }

}
