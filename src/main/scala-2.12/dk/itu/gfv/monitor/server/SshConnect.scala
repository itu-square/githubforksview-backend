package dk.itu.gfv.monitor.server

import com.jcraft.jsch.Channel
import com.jcraft.jsch.ChannelExec
import com.jcraft.jsch.JSch
import com.jcraft.jsch.JSchException
import com.jcraft.jsch.Session
import java.io.{FileInputStream, IOException, InputStream}
import java.sql.SQLException
import java.util.{ArrayList, Arrays, Properties}
import java.util.logging.Level
import java.util.logging.Logger

import com.typesafe.scalalogging.LazyLogging;

/**
  * Created by scas on 10-03-2017.
  */
object SshConnect extends LazyLogging{

  private val properties = new Properties()
  properties.load(new FileInputStream("config.properties"))
  private val user = properties.getProperty("user_server")
  private val password = properties.getProperty("password_server")
  private val host = properties.getProperty("host_server")
  private val ssh_port = properties.getProperty("ssh_port_server").toInt
  private val port_local_ssh = properties.getProperty("port_local_ssh").toInt
  private val port_mysql_server = properties.getProperty("port_mysql_server").toInt

  def ssh_connect: Session = {
    val jsch = new JSch()
    val session = jsch.getSession(user,host, ssh_port)
    val lport = port_local_ssh
    val rhost = "localhost"
    val rport = port_mysql_server
    session.setPassword(password)
    session.setConfig("StrictHostKeyChecking", "no")
    println("Establishing Connection...")
    session.connect
    session.setServerAliveInterval(60 * 1000) // send an alive message every 60 s
    try {
      val assigned_port = session.setPortForwardingL(lport, rhost, rport)
      println("localhost:" + assigned_port + " -> " + rhost + ":" + rport)
      session
    }
    catch {
      case ex: java.net.UnknownHostException => logger.error("no internet connection"); session
      case ex: Exception => logger.info("already binded"); session
    }
  }

  def ssh_disconnect(session: Session) =
    session.disconnect

  def main(args: Array[String]): Unit = {
    ssh_connect
  }
}
