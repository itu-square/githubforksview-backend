package dk.itu.gfv.monitor

import dk.itu.gfv.model.Model.GithubRepository
import dk.itu.gfv.monitor.db.{DBService, DatabaseSchema}
import dk.itu.gfv.monitor.db.Models._
import dk.itu.gfv.monitor.db.NightFetcher.{CachedETAG, cache_file}
import dk.itu.gfv.repo.{GithubExtractor, RepositoryOperations}
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.revwalk.RevCommit
import better.files.{File => ScalaFile}
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.collection.JavaConverters._
import spray.json._
import spray.json.DefaultJsonProtocol._
/**
  * Created by scas on 29-04-2017.
  */
object InsertOrUpdateData extends Ssh with DatabaseSchema with DBService with Utils {

  /*

  1. Insert or update user, return id.
  2. If project is cloned, then do a git fetch
  3. Insert or update project (with new updated, forks count etc)
  4. Get all branches of repository
   - get all branches of this project from DB
   - get all branches from the git repository
  5. Find which branches are on DB but not on git repository
  6. Delete these branches from DB
  7. For the rest of the branches that are in git repository and on db
   - insert branch if not exists
   - insert commits if not exists
     - for each commit, check if it exists
     - if it does, then
        * check if a branch commit exists, and insert it if it does not
        * check if a commit parent exist, and insert it if it does not
        * check if there are added lines of this commit and get the COUNT(*) of them
        * if there are not any added lines, add them
        * if there are added lines, then verify if it is the same number
        * if it is not, then simply insert each added line if it does not exist

   */

  def insertUpdateProject(mainProject: String, repo: GithubRepository, mainRepoId: Int = -1): Project = {
    logger.info(s"inserting user of ${repo.full_name} in the main $mainProject project")
    val user = Await.result(insertUser(repo.full_name), Duration.Inf)

    // 2. If project is cloned, then do a git fetch. If it is not, then now it is cloned on HDD and return the git object
    val git = cloneProject(mainProject, repo) match {
      case true => {
        val git = Git.open(new java.io.File(folder_path(mainProject) + repo.full_name + "/.git"))
        git.fetch.call
        git
      }
      case false => Git.open(new java.io.File(folder_path(mainProject) + repo.full_name + "/.git"))
    }

    // 3. Insert or update project (all metadata)
    logger.info(s"finding if this is a fork and its parent")
    val parentID = repo.parent match {
      case Some(parent_name) => {
        Await.result(getProjectByName(parent_name.full_name), Duration.Inf) match {
          case Some(parent) => parent.id
          case None => -2
        }
      }
      case None => mainRepoId
    }

    logger.info(s"inserting project ${repo.full_name} from main $mainProject")
    val projectDB = Await.result(getProjectByName(repo.full_name), Duration.Inf) match { //  case class Project(id: Int, name: String, owner: Int, language: String, fork: Boolean, fork_of: Int, fork_of_main: Int, forks_count: Int, created_at: DateTime, pushed_at: DateTime, updated_at: DateTime)
      case Some(p) => Await.result(insertOrUpdateProject(Project(p.id, repo.full_name, user.id, repo.language.getOrElse(""), repo.default_branch, repo.fork, parentID, p.fork_of_main, repo.forks_count, repo.stargazers_count, repo.subscribers_count, repo.created_at, repo.pushed_at, repo.updated_at)), Duration.Inf); p
      case None => Await.result(insertProject(repo, mainRepoId, user.id), Duration.Inf)
    }

    //4. Get all branches of repository; get all branches of this project from DB
    val branchesDB = Await.result(getProjectBranches(projectDB.id), Duration.Inf)
    // get all branches of this project from git
    val repoGitBranches = RepositoryOperations.getBranches(git)

    //5. Find which branches are on DB but not on git repository
    //val branchesDeletedFromGit = branchesDB.map(_.name).diff(repoGitBranches.map(_.getName))
    //6.
    //Await.result(deleteBranches(branchesDB.filter(x => branchesDeletedFromGit.contains(x.name)).map(_.id)), Duration.Inf)

    //7. For the rest of the branches that are in git repository and on db
    //- insert branch if not exists
    insertUpdateBranches(git,repoGitBranches, projectDB.id)
    projectDB
  }

  def insertUpdateBranches(git: Git, gitBranches: Seq[Ref], repoId: Int) = {
    gitBranches.foreach(insertUpdateBranch(git, _, repoId))
  }

  def insertUpdateBranch(git: Git, branch: Ref, repoId: Int) = {
    logger.info(s"updating branches of $repoId")
    val br = Await.result(geRepoBranch(branch.getName, repoId), Duration.Inf) match {
      case Some(x) => {
        val updatedBranch = Branch(x.id, x.name, branch.getObjectId.getName, x.project)
        Await.result(updateBranch(updatedBranch), Duration.Inf)
        updatedBranch
      }
      case None => Await.result(insertBranch(Branch(0, branch.getName, branch.getObjectId.getName, repoId)), Duration.Inf)
    }

    val branchCommits = git.log().add(branch.getObjectId).call().asScala.toSeq.init
    insertUpdateBranchCommits(branchCommits, br, git, repoId)
  }

  def insertUpdateBranchCommits(branchCommits: Seq[RevCommit], branch: Branch, git: Git, repoId: Int) = {
    branchCommits.foreach(insertUpdateCommit(_, branch, git, repoId))
  }

  def insertUpdateCommit(revCommit: RevCommit, branch: Branch, git: Git, repoId: Int) = {
    logger.info("updating commits")
    val commit = Await.result(getCommitById(revCommit.getName), Duration.Inf) match {
      case Some(x) => x // commit already exists
      case None => {
        // commit does not exist so insert it
        val commitToInsert = transformRevCommits(Seq(revCommit)).head
        Await.result(insertCommit(commitToInsert), Duration.Inf)
        commitToInsert
      }
    }
    //check if a branch commit exists, and insert it if it does not
    logger.info("updating branch commit")
    insertOrUpdateBranchCommit(branch, commit)

    // insert commit parents if they do not exist
    logger.info("updating commit parents")
    insertOrUpdateCommitParents(revCommit)

    // check if commit has added lines
    logger.info(s"checking if there are added lines for this commit ${revCommit.getName}")
    val numberOfAddedLines = Await.result(getNumberOfAddedLinesInCommit(commit.sha), Duration.Inf)
    // if there are no added lines, then simply insert each added line if it does not exist
    logger.info(s"$numberOfAddedLines for this commit ${revCommit.getName}")
    if(numberOfAddedLines == 0) insertAddedLinesJDBC(RepositoryOperations.patchToAddedLines(git,commit.sha,true,repoId).getOrElse(Seq()))

  }

  def insertOrUpdateCommitParents(revCommit: RevCommit) = {
    val gitCommitParents = revCommit.getParents.map(x => CommitParent(revCommit.getName, x.getName)).toSeq
    gitCommitParents.foreach(x => Await.result(insertOrUpdateCommitParent(x), Duration.Inf))
  }

  def insertOrUpdateBranchCommit(branch: Branch, commit: Commit) = {
    Await.result(getBranchCommitByIds(branch.id, commit.sha), Duration.Inf) match {
      case None => Await.result(insertBranchCommit(BranchCommit(0, branch.id, commit.sha)), Duration.Inf)
      case _ => Nil
    }
  }

  def insertUpdateData(project: String) = {
    logger.info(s"working on $project")
    val mainProject = GithubExtractor.getRepositoryJsonWithEtag(project,"")
    mainProject.get.result.parseJson.convertTo[Option[GithubRepository]] match {
      case Some(githubRepository) => {
        logger.info(s"inserting $project")
        val mainProjectDB = insertUpdateProject(project, githubRepository)
        logger.info(s"getting forks of $project")
        val forksAndEtags = GithubExtractor.getProjectForksAndEtags(project)
        val forks = forksAndEtags._1

        forks.foreach(fork => {
          insertUpdateProject(project, fork, mainProjectDB.id)

        })

        val cached_etags_file = ScalaFile(cache_file)
        val cached_etags = cached_etags_file.contentAsString.parseJson.convertTo[Option[List[CachedETAG]]] match {
          case Some(s) => s.map(x => x.project -> x).toMap
          case None => Map()
        }
        val new_etags = collection.mutable.HashMap[String, CachedETAG]()
        new_etags.++=(cached_etags)
        forksAndEtags._2.foreach(x => new_etags.put(x._1,CachedETAG(x._1,"",x._2)))
        cached_etags_file.overwrite(new_etags.values.toJson.prettyPrint)
      }
      case None => logger.info(s"cannot find repository $project on Github")
    }
  }

  def main(args: Array[String]): Unit = {
    val file = ScalaFile(projects_to_be_monitored)
    args.size match {
      case 1 => better.files.File(args(0)).lines.toSeq.foreach(x => {
        insertUpdateData(x)
        file.appendLine(x+"\n")
      })
      case _ => sys.error("Please provide the file with the projects that should be updated")
    }

    ssh match {
      case Some(x) => x.disconnect()
      case None => Unit
    }
  }

}
