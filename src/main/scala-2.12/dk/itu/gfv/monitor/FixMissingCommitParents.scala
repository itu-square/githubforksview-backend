package dk.itu.gfv.monitor

/**
  * Created by scas on 03-04-2017.
  */

import java.io.File

import scala.collection.JavaConverters._
import better.files.{File => ScalaFile}
import dk.itu.gfv.monitor.db.Models.CommitParent
import dk.itu.gfv.monitor.db.NightFetcher._
import org.eclipse.jgit.api.Git

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object FixMissingCommitParents extends Ssh {

  override val projects_folder = properties.getProperty("data") + "projects"
  def main(args: Array[String]): Unit = {
    val projects = ScalaFile(args(0))
    projects.lines.toList.foreach(project => {
      val commits_parents = collection.mutable.HashMap[String, Seq[String]]()
      val folder = new File(projects_folder + "/" + project)
      if (folder.exists() && folder.isDirectory) {

        folder.listFiles.toList.foreach(p => {
          val inner_folders = p.listFiles().toList
          inner_folders.foreach(repo_folder => {
            if (repo_folder.isDirectory) {
              try {
                val git = Git.open(new File(repo_folder.getAbsolutePath + "/.git"))
                val all_commits = git.log.all.call.asScala.toSeq
                commits_parents.++=(all_commits.init.map(x => x.getName -> x.getParents.map(_.getName).toSeq).toMap)
              }
              catch {
                case ex: org.eclipse.jgit.api.errors.NoHeadException => {
                  val names = repo_folder.getAbsolutePath.split("/").toList
                  val name = names.isEmpty match {
                    case true => {
                      val t = repo_folder.getAbsolutePath.split("""\\""").toList
                      t.init.last + "/" + t.last
                    }
                    case false if names.size == 1 => {
                      val t = repo_folder.getAbsolutePath.split("""\\""").toList
                      t.init.last + "/" + t.last
                    }
                    case false => names.init.last + "/" + names.last
                  }
                  logger.error("git project is invalid, " + project + " repo: " + name)
                }
              }
            }
          })
        })

        val commits = commits_parents.keySet
        val commit_parents_to_insert = collection.mutable.MutableList[CommitParent]()
        commits.foreach(c => {
          val p = commits_parents.get(c).get
          p.foreach(parent => commit_parents_to_insert += (CommitParent(c, parent)))
        })
        try {
          Await.result(insertCommitParents(commit_parents_to_insert.toSeq), Duration.Inf)
        } catch {

          case ex: Exception => logger.error("error inserting commit parents of " + project)
        }
      }
      else {
        logger.error(projects_folder + "/" + project + " does not exist")
      }
    }
    )
    ssh match {
      case Some(x) => x.disconnect()
      case None => Unit
    }
  }

}
