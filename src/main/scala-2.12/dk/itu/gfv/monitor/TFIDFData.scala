package dk.itu.gfv.monitor

import better.files.File
import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.tfidf.TFIDF
import dk.itu.gfv.util.Util
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Ref

import collection.JavaConverters._
/**
  * Created by scas on 19-04-2017.
  */
object TFIDFData extends App with TFIDF with LazyLogging with Utils{
  val languages = List[String]()
  case class Project(name: String, language: String) {
    override def toString: String = name + "," + language
  }

  val projects = File(tfidf_projects_folder + "projects.txt").lines.toList.map(x => Project(x.split(",")(0),x.split(",")(1)))
  val file_extensions = projects.map(x => language_to_extension_name(x.language))
  def clone_projects(projects: Seq[String]) = {
    projects.foreach(project => {
      val repo = Git
        .cloneRepository
        .setNoCheckout(false)
        .setCloneAllBranches(true)
        .setURI("https://github.com/" + project + ".git")
        .setDirectory(new java.io.File(tfidf_projects_folder + project))
        .call
      repo.fetch
      repo.close
    })
  }

  case class ProjectFile(name: String, content: String)
  case class ProjectFiles(project: String, files: List[ProjectFile])

  /**
    * Read all the files of a project recursively from all the branches
    * @param project
    * @return
    */

  def files_in_project_all_branches(project: String): ProjectFiles = {
    val git = Git.open(new java.io.File(tfidf_projects_folder + project + "/.git"))
    val branches = git.branchList().call().asScala.toList

    def branch_files(branches: List[Ref]): List[ProjectFile] = branches match {
      case branch :: t => {
        git.checkout().setStartPoint(branch.getName).call()
        val files = Util.files(tfidf_projects_folder + project, true)
        files.map(x => ProjectFile(x,File(x).contentAsString)) ++ branch_files(t)
      }
      case List(branch) => {
        git.checkout().setStartPoint(branch.getName).call()
        Util.files(tfidf_projects_folder + project, true)
        val files = Util.files(tfidf_projects_folder + project, true)
        files.map(x => ProjectFile(x,File(x).contentAsString))
      }
      case List() => List()
    }
    ProjectFiles(project,branch_files(branches))
  }

  /**
    * Read all the files of a project recursively from all the branches
    * @param project
    * @return
    */

  def files_in_project_main_branch(project: Project): ProjectFiles = {
    val files = Util.files(tfidf_projects_folder + project.name, true).filter(x => file_extensions.contains(Util.fileExtension(x)))
    logger.info(project + " has " + files.size + " files")
    ProjectFiles(project.name,files.map(x => ProjectFile(x,File(x).contentAsString)))
  }

  case class Result(word: String, file: String, score: Double) {
    override def toString: String = word + "  @@score@@   " + score
  }

  def make_tfidf = {
    val project_files = projects.map(files_in_project_main_branch(_)).groupBy(_.project)
    val result_data = File(tfidf_projects_folder + "tf_idf_data").createIfNotExists(true,true)

    project_files.keySet.foreach(project => {
      val files = project_files.get(project).get.map(_.files).flatten
      val results = collection.mutable.MutableList[Result]()

      files.foreach(file => {
//        add(" ** " + file.name + " ** " )
        val words = file.content.split(separator).toSeq
        words.foreach(word => {
          val score = compute_tfidf(word,file.content,files.map(_.content))
          results.+=(Result(word.trim, file.name, score))
        })

      })
      File(result_data + "/" + project + "/tf_idf.txt").createIfNotExists(false,true).write(results.distinct.sortWith(_.score > _.score).mkString("\n"))
    })
  }

  make_tfidf
}
