package dk.itu.gfv.monitor

import java.io.FileNotFoundException

import better.files.{File => ScalaFile}
import dk.itu.gfv.monitor.db.NightFetcher._
import spray.json._
import spray.json.DefaultJsonProtocol._
import collection.JavaConverters._
/**
  * Created by scas on 29-03-2017.
  */
object InsertData extends Ssh {

  val cache_map =  new java.util.concurrent.ConcurrentHashMap[String,CachedETAG]()

  def main(args: Array[String]): Unit = {
    logger.info("gms initial data started")
    val projects = try {
      args.toList
      //ScalaFile("projects.txt").lines.toList
    } catch {
      case ex: FileNotFoundException => sys.error("projects.txt not found")
      case ex: Exception => sys.error(ex.getMessage)
    }
    // create cache file with args as projects
    val cacheFile = ScalaFile(cache_file)
    val cache = cacheFile.exists match {

      case true if cacheFile.isEmpty => {
//        cacheFile.createIfNotExists()
//        cacheFile.overwrite(projects.map(x => CachedETAG(x,"","")).toJson.prettyPrint)
        None
      }
      case true => cacheFile.contentAsString.parseJson.convertTo[Option[List[CachedETAG]]] match {
        case Some(caches) =>  {
          logger.info("Found some existing caches")
//          cache.overwrite((caches ++ projects.map(x => CachedETAG(x,"",""))).toJson.prettyPrint);
          Some(caches.map(x => x.project -> x).toMap)
        }
        case None => logger.error("cannot decode the cache list"); None
      }
      case false => {
        logger.info("Cache file does not exist. Creating it")
        cacheFile.createIfNotExists()
        cacheFile.overwrite(projects.map(x => CachedETAG(x,"","")).toJson.prettyPrint)
        None
      }
    }

    cache match {
      case Some(x) => cache_map.putAll(x.asJava)
      case None => None
    }

    try {
      //      resetDb
      //      val schemas = Await.result(createSchemaIfNotExists,Duration.Inf)
      val projects_to_be_monitored_file = ScalaFile(projects_to_be_monitored)
      projects_to_be_monitored_file.createIfNotExists()
      logger.info("starting inserting projects")
      projects.foreach(project => {
          val cached_ETAGs = insertGithubProject(project)
          cache_map.putAll(cached_ETAGs.asJava)
          while (projects_to_be_monitored_file.isLocked(ScalaFile.RandomAccessMode.readWrite)) {
          }
          if (!projects_to_be_monitored_file.isLocked(ScalaFile.RandomAccessMode.readWrite)) {
            projects_to_be_monitored_file.append(project + "\n")
          }
        cacheFile.overwrite(cache_map.values.asScala.toSeq.toJson.prettyPrint)
        })

      ssh match {
        case Some(x) => x.disconnect()
        case None => Unit
      }
    }
    catch {
      case ex: Throwable => ex.printStackTrace; logger.error(ex.getMessage);
    }
  }
}
