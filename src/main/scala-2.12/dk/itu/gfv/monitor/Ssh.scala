package dk.itu.gfv.monitor

import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.util.MonitorProperties

/**
  * Created by scas on 27-04-2017.
  */
trait Ssh extends MonitorProperties with LazyLogging{

  val ssh = try {
    local_run match {
      case "false" => Some(dk.itu.gfv.monitor.server.SshConnect.ssh_connect)
      case _ => None
    }
    }catch {
    case ex: java.lang.ExceptionInInitializerError => logger.info("exception"); None
    case ex: java.net.BindException => logger.info("already binded"); None
    }
}
