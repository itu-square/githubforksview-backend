package dk.itu.gfv.monitor.frontend

import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.monitor.db.NightFetcher._
import dk.itu.gfv.monitor.db.Models.{Duplicate, Project}
import dk.itu.gfv.repo.PatchParser.DiffFile
import dk.itu.gfv.repo.{GithubExtractor, PatchParser, RepositoryOperations}
import dk.itu.gfv.util.MonitorProperties
import org.eclipse.jgit.api.Git
import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.Elements

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by scas on 15-02-2017.
  */
object GithubScrapper extends MonitorProperties with LazyLogging {

  val link = "https://github.com/"
  val attr_data_line = "data-line-number"

  def rowToString(row: Int, filename: String) =
    (""""row_""" + filename + "_" + String.valueOf(row) + """"""")

  def sourceAtCommit(project: String, sha: String) =
    Jsoup.connect(link + project + "/commit/" + sha).get

  def rowsToExtract(row_nr: Int) = {
      if (row_nr > 3) {
        Seq(row_nr - 3, row_nr - 2, row_nr - 1, row_nr, row_nr + 1, row_nr + 2, row_nr + 3)
      }
      else if (row_nr == 3) {
        Seq(row_nr - 2, row_nr - 1, row_nr, row_nr + 1, row_nr + 2, row_nr + 3)
      }
      else if (row_nr == 2) {
        Seq(row_nr - 1, row_nr, row_nr + 1, row_nr + 2, row_nr + 3)
      }
      else {
        //      else if (row_nr == 1) {
        Seq(row_nr, row_nr + 1, row_nr + 2, row_nr + 3)
      }
  }

  def row(doc: Document, rowIndex: String) = {
    val rows = doc.getElementsByTag("tr").iterator.asScala.toList
    rows.filter(_.getElementsByAttributeValue(attr_data_line, rowIndex).asScala.nonEmpty)
  }

  case class RowData(row: Int, text: String)

  def transformFilePatchIntoTableRows(file: String, row: Int) = {
    val lines = file.split("\n")
    val startHunk = lines.filter(x => x.startsWith("@@ ")).head
    val indexOfStartHunk = lines.indexOf(startHunk)
    val noMetadataLines = lines.drop(indexOfStartHunk)
    var addedIndex = 0
    var rowIndex = 0

    noMetadataLines.zipWithIndex.foreach{ case (value, index) => {
        if (value.startsWith("+")) {
          addedIndex = addedIndex + 1
          if (addedIndex == row) {
            rowIndex = index
          }
        }
      }
    }


    val rowsNeeded = rowsToExtract(row).map(r => RowData(r,noMetadataLines(r)))
    rowsNeeded.map(x => {
      if (x.text.startsWith("-")){
        val el = new Element("tr")
        el.append(s"""<td id="diff-468e74fef61041fcf36a65fa634afe63L224" data-line-number="${x.row}" class="blob-num blob-num-deletion js-linkable-line-number"></td>
                    |<td class="blob-num blob-num-deletion empty-cell"></td>
                    |<td class="blob-code blob-code-deletion">
                    |    <span class="blob-code-inner">${x.text}</span>
                    |</td>""".stripMargin)
        el
      }
      else if(x.text.startsWith("+")){
        val el = new Element("tr")
        el.append(s"""
                   |<td class="blob-num blob-num-addition empty-cell selected-line"></td>
                   |<td id="diff-468e74fef61041fcf36a65fa634afe63R224" data-line-number="${x.row}" class="blob-num blob-num-addition js-linkable-line-number"></td>
                   |<td class="blob-code blob-code-addition">
                   | <span class="blob-code-inner">${x.text}</span>
                   |</td>""".stripMargin)
        el
      }
      else {
        val el = new Element("tr")
        el.append(s"""<td id="diff-468e74fef61041fcf36a65fa634afe63L225" data-line-number="${x.row}" class="blob-num blob-num-context js-linkable-line-number"></td>
                   |    <td id="diff-468e74fef61041fcf36a65fa634afe63R225" data-line-number="${x.row}" class="blob-num blob-num-context js-linkable-line-number"></td>
                   |  <td class="blob-code blob-code-context">
                   |    <span class="blob-code-inner">${x.text}</span>
                   |  </td>
                   |</tr>""".stripMargin)
        el
      }
    })
  }

  case class RowsContextFileHeader(file_header: List[Element], table_rows: List[Element])

  def getRowsContextAndFileHeader(project: String, sha: String, filename: String, row_nr: Int): RowsContextFileHeader = {
    val doc = sourceAtCommit(project, sha)
    val doc_diffs = doc.getElementsByClass("js-diff-progressive-container").asScala.toList // this div contains the actual diffs of files
    logger.info("elements of the doc_diffs " + doc_diffs.size)
    val doc_file_diffs = collection.mutable.ListBuffer[Element]()
    val rows = rowsToExtract(row_nr)
    logger.info(s"rows detected $rows" )

    try {
      doc_diffs.foreach(x => {
        val el = x.children.asScala.toList.grouped(2).map(x => x.last) // this gives only the diff-x divs
        val r = el.filter(x => x.getElementsByAttributeValue("data-path", filename).asScala.nonEmpty) // get that
        doc_file_diffs.++=(r)
      })
  }
    catch {
      case ex: Exception => logger.error("cannot do a diff and get rows and context")
    }
    doc_file_diffs.size match {
      case 0 => {
//        doc.getElementsByClass("load-diff-button").asScala.foreach(x => x.)
        val divToLoadSeparately = doc.getElementsByClass("file-header").asScala.filter(_.getElementsByAttributeValue("data-path", filename).asScala.nonEmpty)
        val urlToLoad = divToLoadSeparately.filter(_.getElementsByAttribute("data-fragment-url").asScala.nonEmpty)
        urlToLoad.headOption match {
          case Some(x) => x.`val`()
          case None => logger.error("some error happened")
        }
        val mainProject = {
          logger.info(s"getting project id from db $project")
          val p = Await.result(getProjectByName(project), Duration.Inf)
          p match {
            case Some(x) if x.fork == false => x
            case Some(x) => Await.result(getProjectById(p.get.fork_of_main), Duration.Inf)
            case None => logger.error("Cannot find this project in the database"); sys.exit(0)
          }
        }
        val git = Git.open(new java.io.File(folder_path(mainProject.name) + "/" + project + "/.git"))
        val patch = RepositoryOperations.getPatchJGIT(git, sha, true)
        val filesInPatch = PatchParser.splitPatchByFiles(patch.getOrElse(""))
        filesInPatch match {
          case Some(x) => {
            val file = x.filter(a => a.asInstanceOf[DiffFile].filename.equalsIgnoreCase(filename)).headOption
            val tableRows = file match {
              case Some(fileToTransform) => doc_file_diffs.++=(transformFilePatchIntoTableRows(fileToTransform.asInstanceOf[DiffFile].patch, row_nr))
              case None => logger.error("cannot find the file in the patch"); sys.exit(1)
            }
          }
        }
      }
      case _ => Nil
    }

//    logger.info(s"doc_file_diffs $doc_file_diffs")


    val file_rows = doc_file_diffs.toList.flatMap(_.getElementsByTag("tr").asScala.toList)
//    logger.info(s"file rows $file_rows")
    val table_rows = collection.mutable.ListBuffer[Element]()
    rows.foreach(row => {
      table_rows ++= (file_rows.filter(_.getElementsByAttributeValue(attr_data_line, String.valueOf(row)).asScala.nonEmpty))
    })
//    logger.info(s"table rows $table_rows")
      //      println(row)
      //      println(file_rows.filter(_.getElementsByAttributeValue(attr_data_line, String.valueOf(row)).nonEmpty))
      val filtered_rows = table_rows.filter(_.getElementsByAttributeValue(attr_data_line, String.valueOf(row_nr)).asScala.nonEmpty)
//      val checkbox_row = table_rows.filter(_.getElementsByClass("blob-num blob-num-addition empty-cell").asScala.nonEmpty)

//    logger.info(s"$filtered_rows")
//    logger.info(s"$checkbox_row")
      //checkbox_row.head.getElementsByClass("blob-num blob-num-addition empty-cell").head.insertChildren(0,List(new Element("input").attr("type", "checkbox")))
      //      val td = filtered_rows.last.children.toList.last.getElementsByClass("blob-code blob-code-addition").attr("class","new_class").attr("title", "This is similar (0.92) to line #50 -- if (!IS_SD_PRINTING) { -- from Marlin_main.cpp of fork TEST at commit x").attr("bgcolor", "#00FF7F")

      val td = filtered_rows.filter(_.getElementsByClass("blob-code blob-code-addition").asScala.nonEmpty).last.getElementsByClass("blob-code blob-code-addition")

      td.attr("class", "new_class").attr("bgcolor", "#A6F3A6")

    RowsContextFileHeader(doc_file_diffs.toList,table_rows.toList)
  }

  def get_github_commit_context(project: String, commit: String, filename: String, line_number: Int) = {
    logger.info(s"getting commit context for $project at $commit and file:$filename at line $line_number")
    val fork_rows = getRowsContextAndFileHeader(project, commit, filename, line_number)
    val file_headers = fork_rows.file_header.head.getElementsByClass("file-header").asScala.toSeq
    file_headers.size match {
      case 0 => Nil
      case _ => file_headers.head.getElementsByClass("file-actions").remove

    }

    val start_of_fork_table =
    //            """<table class="form-control input-contrast comment-form-textarea js-comment-field js-improved-comment-field js-task-list-field js-quick-submit js-suggester-field js-quote-selection-target js-session-resumable" data-tab-size="7" >
      """<table class="diff-table tab-size " data-tab-size="8" >
        |              <colgroup>
        |                <col width="40">
        |                <col width="40">
        |                <col>
        |              </colgroup>""".stripMargin
    val end_of_table = "</table></p>"
    file_headers.isEmpty match {
      case true => "" + "\n" + start_of_fork_table + "\n"  + fork_rows.table_rows.mkString("\n") + end_of_table + "\n"
      case false => file_headers.mkString("\n") + "\n" + start_of_fork_table + "\n"  + fork_rows.table_rows.mkString("\n") + end_of_table + "\n"
    }
  }

  type DuplicateInfo = List[(Duplicate, Project)]

  def removeHeaders(project: String, sha: String) = {
    val doc = sourceAtCommit(project, sha)
    /* remove all the headers and buttons that we do not need */
    doc.getElementsByClass("position-relative js-header-wrapper ").remove
    doc.getElementsByClass("container repohead-details-container").remove
    doc.getElementsByClass("reponav js-repo-nav js-sidenav-container-pjax").remove
    doc.getElementsByClass("btn btn-outline float-right").remove
    doc.getElementsByClass("BtnGroup float-right").remove
    doc.getElementsByClass("BtnGroup js-prose-diff-toggles").remove
    doc.getElementsByClass("js-quote-selection-container ").remove
    doc.getElementsByClass("container site-footer-container").remove
    doc.getElementsByClass("file-actions").remove
    doc.body().append(
      """<script>
        |function showTableRow(id) {
        | if(document.getElementById(id).style.display === 'none'){
        |      console.log("none");
        |      document.getElementById(id).style.display='';
        |      $('.nav-tabs a[href="#' + id + '_tab0"]').tab('show');
        |    }
        |    else {
        |      console.log("display");
        |      document.getElementById(id).style.display='none';
        |    }
        |}
        |</script>""".stripMargin)

    doc.body().append( """<script type="text/javascript">
                                   |    function updateDB(id, test_area_id, useful) {
                                   |        console.log(id);
                                   |        var comment = document.getElementById(test_area_id).value
                                   |        console.log(comment);
                                   |        console.log(useful);
                                   |        var json_obj = {"id": id, "comment": comment, "useful": useful}
                                   |        console.log(JSON.stringify(json_obj))
                                   |
                                   |        var xhr = new XMLHttpRequest();
                                   |
                                   |        xhr.open("POST", "/gms", true);
                                   |        xhr.setRequestHeader("Content-Type", "application/json");
                                   |        xhr.responseType = 'text/html';
                                   |
                                   |        xhr.onload = function () {
                                   |            alert("Thank your for input!");
                                   |        }
                                   |        xhr.send(JSON.stringify(json_obj));
                                   |
                                   |    }
                                   |
                                   |   function submitSurvey(){
                                   |    var link = window.location.href
                                   |    var q1=document.getElementById("q1");
                                   |    var q2=document.getElementById("q2");
                                   |    var q3=document.getElementById("q3");
                                   |    var q4=document.getElementById("q4");
                                   |    var json_data = {"link": link, "q1": q1.value, "q2" :q2.value, "q3": q3.value, "q4": q4.value};
                                   |    var xhr = new XMLHttpRequest();
                                   |    xhr.open("POST", "/survey", true);
                                   |    xhr.setRequestHeader("Content-Type", "application/json");
                                   |    xhr.responseType = 'text/html';
                                   |    xhr.onload = function () {
                                   |      alert("Thank your for input!");
                                   |    }
                                   |    xhr.send(JSON.stringify(json_obj));
                                   |  }
                                   |</script>""".stripMargin)
    try {
      doc.getElementsByClass("js-diff-progressive-retry d-none").remove
    } catch {
      case ex: Exception => Nil
    }
    doc
  }

  def createRowID(row: Int, filename: String) = {
    "row_" + filename.replaceAllLiterally("/","_").replaceAllLiterally(".","_") + "_" + row
  }

  def addInfo(doc: Document) = {
    val info = """<div class="container">
                 |        <p style="color:blue;">README: Thank you for helping us!</p>
                 |      <p>
                 |      You received this e-mail and URL because you have done a commit recently that we have detected to have similarities with commits of other forks.
                 |      In the following commit diff you will find the rows where we indicate possible similarities (top 5) highlighted with strong green.
                 |      If you think this information could be useful or not for preventing developers to duplicate work, you can use either the Useful button or the Not Useful button, and you can include some comments too if you'd like :-)
                 |      All the information is private and will not be disclosed to any parties, except us researchers. For any information or questions,
                 |      please write an e-mail to scas at itu dot dk.
                 |      We thank you in advance for your time!
                 |     </p>
                 |      </div> """.stripMargin
    doc.getElementsByClass("pagehead repohead instapaper_ignore readability-menu experiment-repo-nav").get(0).html(info)
  }


  def updateHTML(project: String, sha: String, filesAndRows: Map[String, List[Int]]) = {
    val files = filesAndRows.keySet

    val doc = removeHeaders(project, sha)
    addInfo(doc)
    doc.head().append(
      """<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        |<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        |
        |<!-- Optional theme -->
        |<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        |
        |<!-- Latest compiled and minified JavaScript -->
        |<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>""".stripMargin)
    val doc_diffs = doc.getElementsByClass("js-diff-progressive-container").asScala.toList // this div contains the actual diffs of files
    val added_lines = Await.result(getAddedLinesBySHA(sha), Duration.Inf)
    files.foreach(filename => {
      val rows = filesAndRows.get(filename).get
      val doc_file_diffs = collection.mutable.ListBuffer[Element]()
      doc_diffs.foreach(x => {

        val el = x.children.asScala.toList.grouped(2).map(x => x.last).toList // this gives only the diff-x divs
        val r = el.filter(x => x.getElementsByAttributeValue("data-path", filename).asScala.nonEmpty)
        doc_file_diffs.++=(r)
      })

      val file_rows = doc_file_diffs.toList.flatMap(_.getElementsByTag("tr").asScala.toList)
      rows.foreach(row => {
        //      println(row)
        //      println(file_rows.filter(_.getElementsByAttributeValue(attr_data_line, String.valueOf(row)).nonEmpty))
        val filtered_rows = file_rows.filter(_.getElementsByAttributeValue(attr_data_line, String.valueOf(row)).asScala.nonEmpty)
        val checkbox_row = filtered_rows.filter(_.getElementsByClass("blob-num blob-num-addition empty-cell").asScala.nonEmpty)

        //      println(filtered_rows)
        //      println("*****CHECKBOX_ROW")
        //      println(checkbox_row)
        //checkbox_row.head.getElementsByClass("blob-num blob-num-addition empty-cell").head.insertChildren(0,List(new Element("input").attr("type", "checkbox")))
        //      val td = filtered_rows.last.children.toList.last.getElementsByClass("blob-code blob-code-addition").attr("class","new_class").attr("title", "This is similar (0.92) to line #50 -- if (!IS_SD_PRINTING) { -- from Marlin_main.cpp of fork TEST at commit x").attr("bgcolor", "#00FF7F")
        val td = filtered_rows.filter(_.getElementsByClass("blob-code blob-code-addition").asScala.nonEmpty).last.getElementsByClass("blob-code blob-code-addition")

        td.attr("class", "new_class").attr("bgcolor", "#A6F3A6")
        //      println(td)
        //      println("""**** TD HEAD""")
        //      println(td.head)
        //      println(td.last)
        //      td.l
        //        val new_output = td.html.replaceFirst("""<span class="blob-code-inner">""","""<span class="blob-code-inner"><a href=""""+rows.get(row).get+"""" target="_blank">""")
        //        td.html(new_output)
        td.append("""<button id="gms_btn1"  type="button" onclick="showTableRow('""" + createRowID(row,filename) + """')">Details</button>""")

        val row_added_line = added_lines.filter(x => x.filename == filename && x.line_number == row).head
        logger.info(s"row added line $row_added_line")
        val duplicated_lines = Await.result(getSimilarityResults(row_added_line.id), Duration.Inf)
        logger.info(s"duplicated-lines $duplicated_lines")
        val filtered_duplicated_lines = duplicated_lines.sortWith(_.score > _.score).take(5)
        // we need the duplicated_line, the added line of this duplicate, and the project info
        val duplicated_info = filtered_duplicated_lines.map(x => x -> {
          val duplicated_added_line = Await.result(getAddedLineById(x.duplicate_line), Duration.Inf).get
          (duplicated_added_line, Await.result(getProjectById(duplicated_added_line.project), Duration.Inf))
        })
        logger.info("duplicated info: " + duplicated_info)
        val sb = new StringBuilder()

        sb.append("<tr id=" +  createRowID(row,filename) +
          """ class="inline-comments js-inline-comments-container" style="display:none">
            |    <td></td>
            |    <td></td>
            |    <td>
            |     <div>
            |       <nav class="nav nav-tabs" role="tablist">
            |         <ul id="myTab" class="nav nav-tabs">""".stripMargin)
//        println(duplicated_info)
        duplicated_info.zipWithIndex.foreach { case (duplicated_line_info, index) => {
          val s = """<li class=""><a href="#""" + createRowID(row,filename) + "_tab" + index + """" data-toggle="tab">""" + duplicated_line_info._2._2.name + "</a></li>\n"
          sb.append(s)
        }
        }
        sb.append(
          """ </ul>
            |</nav>
            |</div>""".stripMargin)
        sb.append("<div id=\"myTabContent\" class=\"tab-content\">\n")
        duplicated_info.zipWithIndex.foreach { case (duplicated_line_info, index) => {
          val duplicated_line = duplicated_line_info._1
          val duplicated_added_line = duplicated_line_info._2._1
          val commit = Await.result(getCommitById(duplicated_added_line.commit), Duration.Inf).head
          val project_db = duplicated_line_info._2._2
          val div = """<div class="tab-pane fade" """ + """id="""" +  createRowID(row,filename) + "_tab" + index + """">"""
          sb.append(div + "\n")
          val form = """<p class="form-control input-contrast comment-form-textarea js-comment-field js-improved-comment-field js-task-list-field js-quick-submit js-suggester-field js-quote-selection-target js-session-resumable" style="height=400px;resize:both;overflow:auto;"> """
          sb.append(form + "\n")
          val info = """<a href="https://github.com/"""+project_db.name+"/commit/"+duplicated_added_line.commit +"""" target=_"blank">""" + project_db.name + "/" + duplicated_added_line.commit.take(8) + "</a>"
          sb.append(info + "\n")
//          sb.append("<br>Filename: " + duplicated_added_line.filename + "\n")
          if(duplicated_line.score >= 0.9){
            sb.append("<br>Highly similar " + duplicated_line.duplicate_type + "\n")
          }
          else {
            sb.append("<br>Relatively similar " + duplicated_line.duplicate_type + "\n")
          }

          sb.append("<br>Subject: " + commit.message + "\n")
          sb.append("<br>Date: " + new org.joda.time.DateTime(commit.authorTime * 1000L) + "\n")
          sb.append("<br><p><br><b>Diff containing a similar line to the selected line</b></p>" + "\n")
          // commit context
          sb.append(get_github_commit_context(project_db.name, duplicated_added_line.commit, duplicated_added_line.filename, duplicated_added_line.line_number))
          val comment_area =
            """<textarea name="comment" id="text_area_gms_""".stripMargin + duplicated_line.id + """"
              |              placeholder="[Optional] Leave a comment if this would have been useful or not"
              |              aria-label="Comment body"
              |              class="form-control input-contrast comment-form-textarea js-comment-field js-improved-comment-field js-task-list-field js-quick-submit js-suggester-field js-quote-selection-target js-session-resumable"
              |              ></textarea> <div class="position-relative float-right ml-1">""".stripMargin
          sb.append(comment_area + "\n")
          val button_useful = """<button class="btn js-inline-comment-form" onclick="updateDB(""" + duplicated_line.id + """,'text_area_gms_""" + duplicated_line.id + """',true)">Useful</button>"""
          val button_not_useful = """<button class="btn js-hide-inline-comment-form" onclick="updateDB(""" + duplicated_line.id + """,'text_area_gms_""" + duplicated_line.id + """',false)">Not Useful</button>"""
          sb.append(button_not_useful + "\n" + "</div>\n")
          sb.append(button_useful + "\n")
          sb.append("</div>" + "\n")
        }
        }
        val end_tags = """</div></td></tr>"""
        sb.append(end_tags + "\n")
        if(checkbox_row.nonEmpty && checkbox_row.size > 1){
          checkbox_row.last.after(sb.toString)
        }
        else
          checkbox_row.head.after(sb.toString)

      })
        
    })
    val surveyQuestions = better.files.File("src/main/scala-2.12/dk/itu/gfv/monitor/frontend/survey.txt").lines.toSeq
    val surveyDiv = {
      val sb = new StringBuilder()
      sb.append("""<style type="text/css">
                    .survey{
                      border: 1px solid #ddd;
                    }
                   </style>
                   <div class="file diff-view ">""".stripMargin + "\n")
      surveyQuestions.zipWithIndex.foreach{case (value, index) => {
        sb.append(s"""<p class="file-header">Q: $value </p>
                    |<textarea class="comment-form-textarea survey" id="q$index"></textarea>""".stripMargin + "\n")
      }}
      sb.append("""<button id="surveyButton" onclick="submitSurvey();" >Submit Survey</button>
                   |</div>""".stripMargin)
      sb.toString
    }

    doc.body.append(surveyDiv)
    doc
  }
}
