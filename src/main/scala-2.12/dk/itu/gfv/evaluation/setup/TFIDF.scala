package dk.itu.gfv.evaluation.setup

import dk.itu.gfv.util.Random._
import dk.itu.gfv.util.{MonitorProperties, Util}
import dk.itu.gfv.util.Util._

import scala.collection.immutable.HashMap
;

object TFIDF extends App with MonitorProperties{

    def compute_tfidf(word: String, document: String, documentList: Seq[String])(implicit separator: String) = {
        val word_trimmed = word.trim.toLowerCase
        val tf_score = tf(word_trimmed, document, separator)
        val idf_score = idf(word_trimmed, documentList, separator)
        tf_score * idf_score
    }

    def tf(word: String, document: String, delimiter: String) = {
        val documentTokens = document.split(delimiter).map(_.trim.toLowerCase).toList
        val wordFrequency = (word: String) => {
            val documentWordFrequency = documentTokens.groupBy(identity).map(e => e._1 -> e._2.length)
            documentWordFrequency.getOrElse(word, 0)
        }
        wordFrequency(word).toDouble / documentTokens.size
    }

    def idf(word: String, documentList: Seq[String], delimiter: String) = {
        def numberOfDocsContainingWord(documentList: Seq[String]) = {
            val documentTokens = documentList.map(document => document.split(delimiter).map(_.trim.toLowerCase).toList)
            documentTokens.foldLeft(0) {(acc, e) => if (e.contains(word)) acc + 1 else acc }
        }
        Math.log(documentList.size.toDouble / numberOfDocsContainingWord(documentList))
    }

}
