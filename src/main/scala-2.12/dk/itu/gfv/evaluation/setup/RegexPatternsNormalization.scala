package dk.itu.gfv.evaluation.setup

import java.util.regex.Pattern

/**
  * Created by scas on 01-11-2016.
  */
trait RegexPatternsNormalization {

  val pythonKeywordsRegex = """\+\s*\"\"\"\s*"""
  val pythonKeywordsPattern = Pattern.compile(pythonKeywordsRegex)
  val breakKeywordRegex = """\+\s*break.*(\r\n|\n)?"""
  val breakKeywordPattern = Pattern.compile(breakKeywordRegex)
  val elseKeywordRegex = """\+\s*\}?\s*else\s*\{?.*"""
  val elseKeywordPattern = Pattern.compile(elseKeywordRegex)
  //  val bracketsKeyword = Pattern.compile("""\+\s*(\{|\})\s*""")
  val returnKeywordRegex = """\+\s*return\s*([\d]*|null|[\w]*)?(;|}|\(|\{)?\s*"""
  val returnKeywordPattern = Pattern.compile(returnKeywordRegex)
  //  val semicolon = Pattern.compile("""\+\s*(\}\s*;|;|\}\s*,)\s*""")
  val bracketsSemicolonRegex = """\+\s*(\{|\}|,|\(|\)|;|\]|\[|\s)+\s*"""
  val bracketsSemicolonPattern = Pattern.compile(bracketsSemicolonRegex)
  // // ActionScript, AutoHotkey, C, C++, C#, D,[13] Go, Java, JavaScript, Objective-C, PHP, PL/I, Rust (can be nested), Scala (can be nested), SASS, SQL, Swift, Visual Prolog, CSS
  // # Bourne shell and other UNIX shells, Cobra, Perl, Python, Ruby, Seed7, Windows PowerShell, PHP, R, Make, Maple, Nimrod
  // -- Euphoria, Haskell, SQL, Ada, AppleScript, Eiffel, Lua, VHDL, SGML
  // % TeX, Prolog, MATLAB,[10] Erlang, S-Lang, Visual Prolog
  // ; AutoHotkey, AutoIt, Lisp, Common Lisp, Clojure, Rebol, Scheme, many assemblers
  // * COBOL (if fixed-form and * in column 7), PAW, many assemblers, Fortran (if fixed-form and * in column 1)
  val commentStartRegex = """(\+\s*(\/\/|\/\*\*?|--|%|(#!/)).*)"""
  //    """(\+\s*(\/\/|\/\*\*?|--|%|#[^(#\s*define|#\s*undef|#\s*else|#\s*ifdef|#\s*if|#\s*elif)]).*)"""

  val multiLineCommentRegex = """(\+\s*\*.*)"""
  val commentEndRegex = """(\+.*?(?=\*\/)\*\/\s*)"""
  val commentsRegex = commentStartRegex + "|" + multiLineCommentRegex + "|" + commentEndRegex
  val commentsRegexPattern = Pattern.compile(commentsRegex)
  //  val emptyline = Pattern.compile("""^\+\s*((\r\n)|\n)?$""")
  val emptylineRegex = """\+\s*"""
  val emptylinePattern = Pattern.compile(emptylineRegex,Pattern.MULTILINE)
  val emptylineNoPlusRegex = """^\s*$"""
  val emptylineNoPlusPattern = Pattern.compile(emptylineNoPlusRegex,Pattern.MULTILINE)
  val javaScalaImportRegex = """(\+\s*import .*)"""
  val csharpImportRegex = """(\+\s*using .*)"""
  val objCImportRegex = """(\+\s*(#|@)import .*)"""
  val cImportRegex = """(\+\s*#include .*)"""
  val pythonImportRegex = """(\+\s*from .*)"""
  val otherImportRegex = """(\+\s*(use|require) .*)"""
  val importRegex = javaScalaImportRegex + "|" + csharpImportRegex + "|" + cImportRegex + "|" + objCImportRegex + "|" + otherImportRegex + "|" + pythonImportRegex
  val importRegexPattern = Pattern.compile(importRegex)

  val annotationRegex = """(\+\s*@.*)"""
//  val annotationRegex = Pattern.compile(javaAnnotations)

  val modifiersRegex = """(\+\s*(public|private|protected)\s*(\{|\()?\s*)"""
//  val modifiersRegex = Pattern.compile(modifiers)

  /**
    * These are to be used only after the patch was parsed and an Vector[AddedLine] object was created.
    */
  val pythonKeywordsWithoutAddition = Pattern.compile("""\s*\"\"\"\s*""")
  val breakKeywordWithoutAddition = Pattern.compile("""\s*break\s*;?.*""")
  val elseKeywordWithoutAddition = Pattern.compile("""\s*\}?\s*else\s*\{?.*""")
  //  val bracketsKeyword = Pattern.compile("""\+\s*(\{|\})\s*""")
  val returnKeywordWithoutAddition = Pattern.compile("""\s*return\s*([\d]*|null|[\w]*)?(;|})?\s*""")
  //  val semicolon = Pattern.compile("""\+\s*(\}\s*;|;|\}\s*,)\s*""")
  val bracketsSemicolonWithoutAddition = Pattern.compile("""\s*(\{|\}|\(|\)|;|,|:|\]|\[|\s)+\s*""")
  // // ActionScript, AutoHotkey, C, C++, C#, D,[13] Go, Java, JavaScript, Objective-C, PHP, PL/I, Rust (can be nested), Scala (can be nested), SASS, SQL, Swift, Visual Prolog, CSS
  // # Bourne shell and other UNIX shells, Cobra, Perl, Python, Ruby, Seed7, Windows PowerShell, PHP, R, Make, Maple, Nimrod
  // -- Euphoria, Haskell, SQL, Ada, AppleScript, Eiffel, Lua, VHDL, SGML
  // % TeX, Prolog, MATLAB,[10] Erlang, S-Lang, Visual Prolog
  // ; AutoHotkey, AutoIt, Lisp, Common Lisp, Clojure, Rebol, Scheme, many assemblers
  // * COBOL (if fixed-form and * in column 7), PAW, many assemblers, Fortran (if fixed-form and * in column 1)
  val commentStartWithoutAddition =
  //    """(\+\s*(\/\/|\/\*\*?|--|%|#[^(#\s*define|#\s*undef|#\s*else|#\s*ifdef|#\s*if|#\s*elif)]).*)"""
  """(\s*(\/\/|\/\*\*?|--|%|#!/).*)"""
  val multiLineCommentWithoutAddition = """(\s*\*.*)"""
  val commentEndWithoutAddition = """(.*?(?=\*\/)\*\/\s*)"""
  val commentsRegexWithoutAddition = Pattern.compile(commentStartWithoutAddition + "|" + multiLineCommentWithoutAddition + "|" + commentEndWithoutAddition)
  //  val emptyline = Pattern.compile("""^\+\s*((\r\n)|\n)?$""")
  val emptylineWithoutAddition = Pattern.compile("""^\s*$""",Pattern.MULTILINE)
  val javaScalaImportWithoutAddition = """(\s*import .*)"""
  val csharpImportWithoutAddition = """(\s*using .*)"""
  val objCImportWithoutAddition = """(\s*(#|@)import .*)"""
  val cImportWithoutAddition = """(\s*#include .*)"""
  val pythonImportWithoutAddition = """(\s*from .*)"""
  val otherImportWithoutAddition = """(\s*(use|require) .*)"""
  val importRegexWithoutAddition = Pattern.compile(javaScalaImportWithoutAddition + "|" + csharpImportWithoutAddition + "|" + cImportWithoutAddition + "|" + objCImportWithoutAddition + "|" + otherImportWithoutAddition + "|" + pythonImportWithoutAddition)


}
