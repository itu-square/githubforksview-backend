package dk.itu.gfv.evaluation.setup

import java.io.File
import java.text.SimpleDateFormat

import dk.itu.gfv.redundant.Similarity
import dk.itu.gfv.redundant.TokenFrequency.LineFrequency
import dk.itu.gfv.redundant.functionparsers.ParseLine
import dk.itu.gfv.redundant.model.LineChangeModel
import dk.itu.gfv.redundant.similarity.SimilarityMain
import dk.itu.gfv.repo.PatchParser.{BinaryFile, DiffFile}
import dk.itu.gfv.repo.{GithubExtractor, PatchParser, RepositoryOperations}
import dk.itu.gfv.repo.RepositoryOperations._
import dk.itu.gfv.util.{MonitorProperties, ProjectsPaths}
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.{RevCommit, RevWalk}
import dk.itu.gfv.util.Util._

import scala.collection.JavaConverters._
import scala.collection.immutable.HashMap

/**
  * Created by scas on 01-11-2016.
  */
object FixedRandomCommits extends App with RegexPatternsNormalization with ProjectsPaths with MonitorProperties with Similarity {



  class Project(project: String, outputFolder: String, randomCommits: List[String], simAlgs: List[SimAlgorithm], nrofcommits: Int) {
    private val git = Git.open(new File(repodir + project + "/.git"))
    //    private lazy val fileExtensions = readFromFileLines("file-extensions-corrected.txt")
    private lazy val cs = git.log.all.call.asScala.toList
    private lazy val commits = cs.map(x => x.getName.take(7) -> x.getParents.map(_.getName.take(7)).toList).toMap
    private lazy val sdf = new SimpleDateFormat("""yyyy-MM-dd'T'HH:mm:ss""")

    private lazy val resultFolder = new File(repodir + project + "/" + outputFolder + "/" + sdf.format(new java.util.GregorianCalendar().getTime).replaceAll(":", "-"))
    private lazy val commonLinesThreshold = 30
    private lazy val pairSize = 4
    def processProject = process(project, resultFolder.getAbsolutePath + "/", simAlgs, git, randomCommits, cs, commits, nrofcommits, true, true, pairSize, commonLinesThreshold)

  }




  /**
    * Remove common lines from the input. While doing so, decrement the position of the remaining lines by 1
    * @param input
    * @param commonLines
    * @return
    */
  def removeCommonLines(input: Vector[AddedLine2], commonLines: Vector[String]): (Vector[AddedLine2], Int) = {
    var c = 0;
    def decrementPosition(input: Vector[AddedLine2]) = input map (x => x.copy(x.sha, x.file, x.position - 1, x.normalizedText, x.originalText))
    def innerProcess(addedLines: Vector[AddedLine2]): Vector[AddedLine2] = addedLines match {
      case h +: t => {
        if (commonLines.intersect(Vector(h.normalizedText)).nonEmpty) {
          c = c + 1
          innerProcess(decrementPosition(t))
        }
        else {
          h +: innerProcess(t)
        }
      }
      case Vector(h) => {
        if (commonLines.intersect(Vector(h.normalizedText)).nonEmpty) {
          c = c + 1
          Vector()
        }
        else
          Vector(h)
      }
      case Vector() => Vector()
    }
    (innerProcess(input), c)
  }

  /**
    *
    * @param patchFiles
    * @param sha
    * @return
    */
  def patchToAddedLines(patchFiles: Seq[DiffFile], sha: String) = {
    val res = collection.mutable.ListBuffer[AddedLine2]()
    patchFiles.foreach(file => {
      val hunks = PatchParser.splitPatchByHunks(file.patch)
      hunks.getOrElse(Nil).foreach(hunk => {
        val hunkResult = collection.mutable.ListBuffer[AddedLine2]()
        val hunkLines = hunk.patch.split("\r?\n").toVector.drop(2)
        val startHunk = {
          try {
            LineChangeModel.stringToRangeHunk(hunkLines.head).start
          } catch {
            case ex: Exception => println("commit " + sha + " some exception when finding the index of the hunk start \n"); 0
          }
        }
        var c = {
          if (startHunk == 0)
            0
          else
            startHunk - 1
        }
        hunkLines.drop(1).foreach(value => {
          if (value.startsWith("+")) {
            if (!((value.isEmpty || emptylinePattern.matcher(value).matches || commentsRegexPattern.matcher(value).matches || pythonKeywordsPattern.matcher(value).matches || elseKeywordPattern.matcher(value).matches || importRegexPattern.matcher(value).matches || breakKeywordPattern.matcher(value).matches || returnKeywordPattern.matcher(value).matches || bracketsSemicolonPattern.matcher(value).matches))) {
              c = c + 1
              hunkResult.+=(AddedLine2(sha, file.filename, c, value.drop(1).trim, value.drop(1).trim))
            }
          }
          else if (value.startsWith("-")) {
            // do not do anything if a line has removed
          }
          else {
            // unchanged line, increment line counter
            c = c + 1
          }
        })
        if (!hunkResult.isEmpty) {
          // changes could be only added new lines, or whitespace changes to empty lines, thus if the hunk is empty we do not consider it
          res.++=(hunkResult)
        }
      })
    })
    Some(res.toVector)
  }

  def patchToAddedLinesRemoveStopwords(patchFiles: Seq[DiffFile], sha: String) = {
    val res = collection.mutable.ListBuffer[AddedLine2]()
    patchFiles.foreach(file => {
      val hunks = PatchParser.splitPatchByHunks(file.patch)
      hunks.getOrElse(Nil).foreach(hunk => {
        val hunkResult = collection.mutable.ListBuffer[AddedLine2]()
        val hunkLines = hunk.patch.split("\r?\n").toVector.drop(2)
        val startHunk = {
          try {
            LineChangeModel.stringToRangeHunk(hunkLines.head).start
          } catch {
            case ex: Exception => println("commit " + sha + " some exception when finding the index of the hunk start \n"); 0
          }
        }
        var c = {
          if (startHunk == 0)
            0
          else
            startHunk - 1
        }
        hunkLines.drop(1).foreach(value => {
          if (value.startsWith("+")) {
            if (!(value.isEmpty || emptylinePattern.matcher(value).matches || commentsRegexPattern.matcher(value).matches || pythonKeywordsPattern.matcher(value).matches || elseKeywordPattern.matcher(value).matches || importRegexPattern.matcher(value).matches || breakKeywordPattern.matcher(value).matches || returnKeywordPattern.matcher(value).matches || bracketsSemicolonPattern.matcher(value).matches)) {
              val v = RemoveStopWords.removeStopWords(value)
              if (!((v.isEmpty || emptylinePattern.matcher(v).matches || commentsRegexPattern.matcher(v).matches || pythonKeywordsPattern.matcher(v).matches || elseKeywordPattern.matcher(v).matches || importRegexPattern.matcher(v).matches || breakKeywordPattern.matcher(v).matches || returnKeywordPattern.matcher(v).matches || bracketsSemicolonPattern.matcher(v).matches))) {
                c = c + 1
                hunkResult.+=(AddedLine2(sha, file.filename, c, v.drop(1).trim, value.drop(1).trim))
              }
            }
          }
          else if (value.startsWith("-")) {
            // do not do anything if a line has removed
          }
          else {
            // unchanged line, increment line counter
            c = c + 1
          }
        })
        if (!hunkResult.isEmpty) {
          // changes could be only added new lines, or whitespace changes to empty lines, thus if the hunk is empty we do not consider it
          res.++=(hunkResult)
        }
      })
    })
    Some(res.toVector)
  }


  /**
    * Given an SHA and a repository, creates a list of added lines objects from files to the list of added lines in the patch, containing the line number where the line was added in that file
    * The algorithm removes all the white lines, empty lines, new lines, and removes or not stopwords.
    *
    * @param git
    * @param sha
    * @return
    */

  def patchToAddedLines(git: Git, sha: String, removeStopwords: Boolean): Option[Vector[AddedLine2]] = {
    RepositoryOperations.getPatchJGIT(git, sha) match {
      case Some(patch) => {
        val patchFiles = PatchParser.splitPatchByFiles(patch).getOrElse(List()).flatMap {
          case _: BinaryFile => None
          case diff: DiffFile => Some(diff)
        }
        if (removeStopwords) {
          patchToAddedLinesRemoveStopwords(patchFiles, sha)
        }
        else {
          patchToAddedLines(patchFiles, sha)
        }

      }
      case None => None
    }
  }

  def writeSimAlgResults(randomCommits: List[String], simalgs: List[SimAlgorithm], projectResults: Vector[ProjectResult], outputPath: String) = {
    randomCommits.foreach(commit => {
      val commitFolder = new File(outputPath + "/" + commit)
      commitFolder.mkdirs
      var index = 0
      simalgs.zipWithIndex.foreach {
        case (algorithmName, i) => {
          index = index + i
          algorithmName.threshold.zipWithIndex.foreach {
            case (threshold, j) => { // this is because I changed from a list of 4 algorithms , each having its similarity threshold, to only 2 algorithms each with 2 threshold.
              index = index + j
              val outputFile = new File(commitFolder.getAbsolutePath + "/" + algorithmName.alg + "-" + threshold + ".txt")
              val algResult = projectResults(index).results.filter(x => x.sha.equals(commit)).head
              printToFile(outputFile)(fp => {
                fp.println("***SIMSCORE***")
                algResult.simscore.sortBy(_.file).zipWithIndex.foreach {
                  case (value, index) => {
                    fp.println(index + "," + value.file + "," + value.line + "," + value.scores.size + "," + value.scores.sortWith(_.score > _.score))
                  }
                }
                fp.println("***FUNCTION DEF***")
                algResult.functiondefscore.sortBy(_.file).zipWithIndex.foreach {
                  case (value, index) => {
                    fp.println(index + "," + value.file + "," + value.line + "," + value.scores.size + "," + value.scores.sortWith(_.score > _.score))
                  }
                }
                fp.println("***PROXIMITY***")
                algResult.proximityscore.sortBy(_.file).zipWithIndex.foreach {
                  case (value, index) => {
                    fp.println(index + "," + value.file + "," + value.line + "," + value.locations.size + "," + value.locations)
                  }
                }
                fp.println("***SIM & PROXIMITY***")
                algResult.simandproxscore.sortBy(_.file).zipWithIndex.foreach {
                  case (value, index) => {
                    fp.println(index + "," + value.file + "," + value.line + "," + value.scores.size + "," + value.scores.sortWith(_.score > _.score))
                  }
                }
              })
            }
          }
        }
      }
    })
  }

  /**
    *
    * @param input
    * @param pairSize
    * @return
    */

  def makePairsCommit(input: Map[String, Map[String, Vector[AddedLine2]]], pairSize: Int) = {
    input.map(x => x._1 -> makePairs(x._2, pairSize))
  }

  def makePairs(input: Map[String, Vector[AddedLine2]], pairSize: Int) = {
    def keepConsecutiveLines(i: Vector[AddedLine2]) = {
      if (i.size >= pairSize) {
        var pointers = (if (pairSize > 2) List.range(0, pairSize) else List(0, 1))
        val result = collection.mutable.ListBuffer[AddedLine2]()
        var lastPointer = pointers.head
        val normalizedText = new StringBuilder()
        val originalText = new StringBuilder()
        while (lastPointer < i.size) {
          var r = true
          val a = i(pointers.head)
          for (c <- 0 until pointers.size) {
            //          println(c)
            //          println("Pointers values " + pointers)
            if (c == pointers.size - 1) {
              normalizedText.append(i(pointers(c)).normalizedText)
              originalText.append(i(pointers(c)).originalText)
            }
            else {
              //            println(i(pointers(c)))
              if (i(pointers(c + 1)).position != i(pointers(c)).position + 1) {
                r = false
              }
              else {

                normalizedText.append(i(pointers(c)).normalizedText + "\n")
                originalText.append(i(pointers(c)).originalText + "\n")
              }
            }
            //          println(r)
          }
          if (r) {
            result.+=(AddedLine2(a.sha, a.file, a.position, normalizedText.toString, originalText.toString))
          }
          normalizedText.clear
          originalText.clear
          pointers = pointers.map(x => x + 1)
          lastPointer = pointers.last
        }
        result.toVector
      }
      else {
        Vector()
      }
    }
    input.map(x => x._1 -> keepConsecutiveLines(x._2))
  }

  def removeCommonLinesTFIDF(input: Vector[AddedLine2], tfidf: Map[String, String], allProjects: List[String], tfidf_values: Seq[String], cache: collection.mutable.Map[String, Boolean]): (Vector[AddedLine2], Int) = {
    var c = 0;
    def isCommonLine(input: String) =  cache.get(input).getOrElse(allProjects.forall(project => TFIDF.compute_tfidf(input,tfidf.get(project).getOrElse("_____ERROR_____"), tfidf_values) == 0.0))
    def decrementPosition(input: Vector[AddedLine2]) = input map (x => x.copy(x.sha, x.file, x.position - 1, x.normalizedText, x.originalText))
    def innerProcess(addedLines: Vector[AddedLine2]): Vector[AddedLine2] = addedLines match {
      case h +: t => {
        if (isCommonLine(h.normalizedText)) {
          c = c + 1
          innerProcess(decrementPosition(t))
        }
        else {
          h +: innerProcess(t)
        }
      }
      case Vector(h) => {
        if (isCommonLine(h.normalizedText)) {
          c = c + 1
          Vector()
        }
        else
          Vector(h)
      }
      case Vector() => Vector()
    }
    (innerProcess(input), c)
  }



  def evaluateConfigurations(project: String, withFreqLines: Boolean, withStopWords: Boolean, useTFIDF: Boolean = false, outputFolderPath: String, randomCommits: List[String], pairSize: Int,
                             rndCommitsAndAddedLinesWithStopwords: List[RndCommit], rndCommitsAndAddedLinesNoStopwords: List[RndCommit], simalg: List[SimAlgorithm], commonLinesThreshold: Int, tfidf: Map[String, String]) = {
    val tfidf_values = tfidf.values.toSeq
    val allProjects = tfidf.keys.toList
    val cache = collection.mutable.Map[String, Boolean]()
    if (withFreqLines) {
      val freqLinesFolder = outputFolderPath + "with-freq-lines/"
      if (withStopWords) { // with freq lines, with stop words
      val stopWordsFolder = freqLinesFolder + "with-stop-words/"
        writeResults("wfl-wsw", project, randomCommits, stopWordsFolder, pairSize, rndCommitsAndAddedLinesWithStopwords, simalg)
      }
      if ((!withStopWords) == false) { // with freq lines, no stop words
      val noStopWordsFolder = freqLinesFolder + "no-stop-words/"
        writeResults("wfl-nosw", project, randomCommits, noStopWordsFolder, pairSize, rndCommitsAndAddedLinesNoStopwords, simalg)
      }
    }

    if ((!withFreqLines) == false) {


      def removeCommonLinesByTFIDF: (List[RndCommit], List[RndCommit]) = {

        val addedLinesWithStopwordsNoFreqLines = rndCommitsAndAddedLinesWithStopwords.map(x => {
          RndCommit(project, x.sha,
            x.rndAddedLines.map(a => a._1 -> removeCommonLinesTFIDF(a._2,tfidf,allProjects,tfidf_values,cache)._1),
            x.forksAddedCommits.map(b => b._1 -> b._2.map(c => c._1 -> removeCommonLinesTFIDF(c._2, tfidf, allProjects, tfidf_values, cache)._1))
          )
        })

        val addedLinesNoStopwordsNoFreqLines = rndCommitsAndAddedLinesNoStopwords.map(x => {
          RndCommit(project, x.sha,
            x.rndAddedLines.map(a => a._1 -> removeCommonLinesTFIDF(a._2,tfidf,allProjects,tfidf_values,cache)._1),
            x.forksAddedCommits.map(b => b._1 -> b._2.map(c => c._1 -> removeCommonLinesTFIDF(c._2, tfidf, allProjects, tfidf_values, cache)._1))
          )
        })
        (addedLinesWithStopwordsNoFreqLines,addedLinesNoStopwordsNoFreqLines)
      }

      def removeFrequentLines: (List[RndCommit], List[RndCommit]) = {
        val frequentLinesInBaseCode = readFromFileLines(repodir + project + "/" + linefrequency).toVector.map(x => {
          val lastComma = x.lastIndexOf(',')
          val str = x.substring(0, lastComma)
          val nrOfAppearances = Integer.parseInt(x.substring(lastComma + 1, x.size))
          LineFrequency(str, nrOfAppearances)
        })
        val frequentLines = frequentLinesInBaseCode.filter(_.numberOfAppearances >= commonLinesThreshold).map(_.line)

        val addedLinesWithStopwordsNoFreqLines = rndCommitsAndAddedLinesWithStopwords.map(x => {
          RndCommit(project, x.sha,
            x.rndAddedLines.map(a => a._1 -> removeCommonLines(a._2, frequentLines)._1),
            x.forksAddedCommits.map(b => b._1 -> b._2.map(c => c._1 -> removeCommonLines(c._2, frequentLines)._1))
          )
        })

        val addedLinesNoStopwordsNoFreqLines = rndCommitsAndAddedLinesNoStopwords.map(x => {
          RndCommit(project, x.sha,
            x.rndAddedLines.map(a => a._1 -> removeCommonLines(a._2, frequentLines)._1),
            x.forksAddedCommits.map(b => b._1 -> b._2.map(c => c._1 -> removeCommonLines(c._2, frequentLines)._1))
          )
        })
        (addedLinesWithStopwordsNoFreqLines, addedLinesNoStopwordsNoFreqLines)
      }
      val (addedLinesWithStopwordsNoFreqLines: List[RndCommit], addedLinesNoStopwordsNoFreqLines: List[RndCommit]) = if (useTFIDF) removeCommonLinesByTFIDF  else removeFrequentLines
      val noFreqLinesFolder = outputFolderPath + "no-freq-lines/"
      new File(noFreqLinesFolder).mkdirs()
      if (withStopWords) { // no freq lines, with stop words
      val stopWordsFolder = noFreqLinesFolder + "with-stop-words/"
        writeResults("nofl-wsw", project, randomCommits, stopWordsFolder, pairSize, addedLinesWithStopwordsNoFreqLines, simalg)
      }
      if ((!withStopWords) == false) { // no freq lines, no stop words
      val noStopWordsFolder = noFreqLinesFolder + "no-stop-words/"
        writeResults("nofl-nosw", project, randomCommits, noStopWordsFolder, pairSize, addedLinesNoStopwordsNoFreqLines, simalg)
      }
    }
  }

  def writeResults(resultType: String, project: String, randomCommits: List[String], outputFolder: String, pairSize: Int, rndCommits: List[RndCommit], simalg: List[SimAlgorithm]) = {
    for (size <- 1 to pairSize) {
      val folder = outputFolder + size + "-pairs/"
      new File(folder).mkdirs()
      if (size == 1) {
        val r = time("Computing one pair", compute(rndCommits, simalg))
        val csv = makeCSV(simalg, r, randomCommits.size)
        writeStringToXLSFile(csv, folder + resultType + "-" + size + "-pairs.xlsx", "\t")
        writeSimAlgResults(randomCommits, simalg, r, folder)
      }
      else {
        println("Computing " + size + " consecutive lines pairs")
        val processedCommitsForPairs = time("Make Pairs " + size, rndCommits.map(x => RndCommit(project, x.sha, makePairs(x.rndAddedLines, size), makePairsCommit(x.forksAddedCommits, size))))
        val r = time("Computing score of " + size + " consecutive lines for " + resultType, compute(processedCommitsForPairs, simalg))
        val csv = makeCSV(simalg, r, randomCommits.size)
        writeStringToXLSFile(csv, folder + resultType + "-" + size + "-pairs.xlsx", "\t")
        writeSimAlgResults(randomCommits, simalg, r, folder)
      }
    }
  }

  def compute(rndCommitsAndAddedLines: List[RndCommit], simalg: List[SimAlgorithm]) = {
    val commitResults = collection.mutable.ListBuffer[CommitResult]()
    rndCommitsAndAddedLines.foreach(commit => {
      val tempAddedLines = commit.forksAddedCommits.values.flatten.map(x => x._2).flatten.toVector
      val patchAddedLines = commit.rndAddedLines.values.flatten.toVector
      println("Temp added lines " + tempAddedLines.size)
      println("patchAddedLines " + patchAddedLines.size)
      simalg.foreach(alg => {
        //          val simscore = time("Computing similarity " + alg, computeSimilarity(tempAddedLines, patchAddedLines, alg.alg, alg.threshold))
        val simscore = computeSimilarity(tempAddedLines, patchAddedLines, alg.alg)
        //          val functiondefscore =  time("Computing function def "+ alg, computeFunctionDef(tempAddedLines, patchAddedLines, alg.alg, alg.threshold))
        val functiondefscore = computeFunctionDef(tempAddedLines, patchAddedLines, alg.alg)
        //          val proximityscore = time("Computing proximity "+ alg, computeProximity(tempAddedLines, patchAddedLines))
        val proximityscore = computeProximity(tempAddedLines, patchAddedLines)
        //          val simandproxscore = time("Computing sim and prox "+ alg, computeSimilarityAndProximity(tempAddedLines, patchAddedLines, alg.alg, alg.threshold - 0.2))
        val simandproxscore = computeSimilarityAndProximity(tempAddedLines, patchAddedLines, alg.alg)
        alg.threshold.foreach(threshold => {
          commitResults.+=(CommitResult(commit.sha, patchAddedLines.size,
            simscore.map(x => SimScore(x.line, x.file, x.scores.filter(_.score >= threshold))),
            functiondefscore.map(x => FunctionDefScore(x.line, x.file, x.scores.filter(_.score >= threshold))).filterNot(_.scores.isEmpty),
            proximityscore,
            simandproxscore.map(x => SimAndProxScore(x.line, x.file, x.scores.filter(_.score >= threshold)))
          ))
        })
      })
    })
    def makeResult = {
      val t = collection.mutable.ListBuffer[CommitResult]()
      val r = collection.mutable.ListBuffer[ProjectResult]()
      for (a <- 0 until simalg.size + 2) {
        // each sim algorithm has a result at a multiple of 4; e.g., n-levenshtein is at indexes 0,4,8,etc.
        // then this way we just reconstruct the project results which expects for each algorithm the result of the random commits
        t.+=(commitResults(a))
        var index = a
        for (i <- 0 until rndCommitsAndAddedLines.size - 1) {
          index = index + simalg.size + 2
          t.+=(commitResults(index))
        }
        r.+=(ProjectResult(0, 0, t.toVector))
        t.clear
      }
      r.toVector
    }
    makeResult
  }

  def process(project: String, outputFolderPath: String, simalg: List[SimAlgorithm], git: Git, randomCommits: List[String], cs: List[RevCommit], commits: Map[String, List[String]], nrofcommits: Int, withFreqLines: Boolean, withStopWords: Boolean, pairSize: Int, commonLinesThreshold: Int) = {
    val walk = new RevWalk(git.getRepository)
    def processCommit(rndCommits: List[String]): Map[String, List[String]] = {
      rndCommits.map(commit => {
        println("processing commit: " + commit)
        walk.reset
        val rndCommitTime = try {
          walk.parseCommit(git.getRepository.resolve(commit)).getCommitTime
        } catch {
          case ex: Exception => {
            val patchTime = GithubExtractor.getPatchAtCommit(project, commit).split("\r?\n").toVector.take(3).last.replace("Date: ", "")
            java.lang.Math.toIntExact(GithubExtractor.getCommitTimeFromGithubPatch(patchTime))
          }
        }
        commit -> RepositoryOperations.findCommits(commits, cs, commit, rndCommitTime).filter(commits.get(_).get.size == 1)
      }).toMap
    }

    println("Starting to process commits")
    val processedCommits = time("Processing commits", processCommit(randomCommits))

    val cachedCommits = collection.mutable.Map[String, Map[String, Vector[AddedLine2]]]() // sha -> Map(file -> List[AddedLines])
    //FIXME
    time("creating added lines", processedCommits.keySet.foreach(rndSHA => {
      cachedCommits.get(rndSHA) match {
        case Some(x) => Nil
        case None => {
          // add random patch to the list of known patches and their added lines
          patchToAddedLines(git, rndSHA, false) match {
            case Some(x) => cachedCommits.+=(rndSHA -> x.groupBy(_.file).map(a => (a._1, a._2.sortWith(_.position < _.position)))) // TODO should be already sorted from the parser, but I am not 100% sure
            case None => Nil
          }
        }
      }
    }
    ))
    processedCommits.keySet.foreach(rndSHA => {
      val commitsToProcess = processedCommits.get(rndSHA).get
      commitsToProcess.foreach(commit => {
        cachedCommits.get(commit) match {
          case Some(x) => Nil
          case None => {
            patchToAddedLines(git, commit, false) match {
              case Some(x) => cachedCommits.put(commit, x.groupBy(_.file).map(a => (a._1, a._2.sortWith(_.position < _.position)))) // TODO should be already sorted from the parser, but I am not 100% sure
              case None => Nil
            }
          }
        }
      })
    })
    val addedLinesWithStopwords = cachedCommits.toMap

    val rndCommitsAndAddedLinesWithStopwords: List[RndCommit] = processedCommits.map(x => {
      val patchAddedLines = addedLinesWithStopwords.get(x._1).get
      val forksAddedLines = addedLinesWithStopwords.filterKeys(a => x._2.contains(a))
      RndCommit(project, x._1, patchAddedLines, forksAddedLines)
    }).toList // for each randomCommit, we will have a list of sha -> files -> addedlines

    val rndCommitsAndAddedLinesNoStopwords: List[RndCommit] = {
      cachedCommits.clear
      time("No stop words creating", processedCommits.keySet.foreach(rndSHA => {
        cachedCommits.get(rndSHA) match {
          case Some(x) => Nil
          case None => {
            // add random patch to the list of known patches and their added lines
            patchToAddedLines(git, rndSHA, true) match {
              case Some(x) => cachedCommits.+=(rndSHA -> x.groupBy(_.file).map(a => (a._1, a._2.sortWith(_.position < _.position)))) // TODO should be already sorted from the parser, but I am not 100% sure
              case None => Nil
            }
          }
        }
      }
      ))
      processedCommits.keySet.foreach(rndSHA => {
        val commitsToProcess = processedCommits.get(rndSHA).get
        commitsToProcess.foreach(commit => {
          cachedCommits.get(commit) match {
            case Some(x) => Nil
            case None => {
              patchToAddedLines(git, commit, true) match {
                case Some(x) => cachedCommits.put(commit, x.groupBy(_.file).map(a => (a._1, a._2.sortWith(_.position < _.position)))) // TODO should be already sorted from the parser, but I am not 100% sure
                case None => Nil
              }
            }
          }
        })
      })
      val addedLinesNoStopwords = cachedCommits.toMap
      processedCommits.map(x => {
        val patchAddedLines = addedLinesNoStopwords.get(x._1).get
        val forksAddedLines = addedLinesNoStopwords.filterKeys(a => x._2.contains(a))
        RndCommit(project, x._1, patchAddedLines, forksAddedLines)
      }).toList // for each randomCommit, we will have a list of sha -> files -> addedlines
    }
    val tfidf = HashMap[String, String]()
    evaluateConfigurations(project, true, true, false, outputFolderPath, randomCommits, pairSize, rndCommitsAndAddedLinesWithStopwords, rndCommitsAndAddedLinesNoStopwords, simalg, commonLinesThreshold, tfidf)
  }

  def writeXLS(input: String, output: String, delimiter: String) = {
    dk.itu.gfv.util.Util.writeStringToXLSFile(input, output, delimiter)
  }

  def makeCSV(simAlgorithms: List[SimAlgorithm], projectResult: Vector[ProjectResult], nrOfCommits: Int): String = {
    println("MakeCSV")
    def makeHeader = {
      val sb = new StringBuilder()
      sb.append("Project\tCommit\t#Added lines\t\t\t\t\t\t\tSIM-ALGORITHMS AND SIM-TREHSHOLD\n")
      val algsString = {
        val sb1 = new StringBuilder()
        simAlgorithms.foreach(x => {
          x.threshold.foreach(threshold => {
            sb.append(x.alg + "\t\t" + threshold + "\t\t")
          })
        })
        sb1.toString
      }
      sb.append("\t\t\t\t")
      sb.append(algsString + "\n")
      sb.append("\t\t")
      simAlgorithms.foreach(x => {
        x.threshold.foreach{threshold => {
          sb.append("\t#simwarnings\t#fundefwarnings\t#proxwarnings\t#simproxwarnings")
        }}
      })
      sb.append("\n")
      sb.toString
    }

    val header = makeHeader

    val sb = new StringBuilder()
    var commit = 0
    while (commit < nrOfCommits) {
      sb.append("\t")
      for (alg <- 0 until simAlgorithms.size + 2) {
        val pres = projectResult(alg)
        val commitResult = pres.results(commit)
        val commitSimscoreWarnings = commitResult.simscore.foldLeft(0)(_ + _.scores.size)
        val commitFunctionDefWarnings = commitResult.functiondefscore.foldLeft(0)(_ + _.scores.size)
        val commitProximityScoreWarnings = commitResult.proximityscore.foldLeft(0)(_ + _.locations.size)
        val commitSimAndProxScoreWarnings = commitResult.simandproxscore.foldLeft(0)(_ + _.scores.size)
        //        println("Commit " + commitResult.sha + " with " + commitResult.commitAddedLines + " added lines had ")
        //        println("    similarity: " + commitSimscoreWarnings + " warnings ")
        //        println("    function-def: " + commitFunctionDefWarnings + " warnings ")
        //        println("    proximity: " + commitProximityScoreWarnings + " warnings ")
        //        println("    simandprox: " + commitSimAndProxScoreWarnings + " warnings ")
        if (alg == 0) {
          // first time add the data about sha and added lines in that commit
          sb.append(commitResult.sha + "\t" + commitResult.commitAddedLines + "\t" + commitSimscoreWarnings + "\t" + commitFunctionDefWarnings + "\t" + commitProximityScoreWarnings + "\t" + commitSimAndProxScoreWarnings)
        }
        else {
          sb.append("\t" + commitSimscoreWarnings + "\t" + commitFunctionDefWarnings + "\t" + commitProximityScoreWarnings + "\t" + commitSimAndProxScoreWarnings)
        }
      }
      sb.append("\n")
      commit += 1
    }
    val res = header + sb.toString
    res
  }


  /**
    * FIXED COMMITS RESULTS
    * COMMITS THAT WERE RANDOMLY CHOSEN AND WE REUSED THEM TO TEST OUR ALGORITHMS AND RESULTS
    */

  val simAlgs = List(SimAlgorithm("n-levenshtein", List(0.8, 0.9)), SimAlgorithm("jaro-winkler", List(0.8, 0.9)))
  assert(simAlgs.forall(_.threshold.size == 2))
  val nrofcommits = 10
  val fixed_commits_folder = "fixed-commits-results"
  //    val ktap = new Project("ktap/ktap", fixed_commits_folder, List("7ee59b1", "06375ec", "2dadf98", "0ed0fa3", "15f385d", "349920d", "a224bf6", "ab8ebd4", "92ad667", "9dc3c55"), simAlgs, nrofcommits)
  //    time("ktap", ktap.processProject)


  //      val biolab = new Project("biolab/orange3", fixed_commits_folder, List("4087906", "5be5889", "0f90e47", "5131424", "39f6100", "83d91ce", "d71ec42", "1d7f935", "65fc439", "3838069"), simAlgs.take(1), nrofcommits)
  //      time("biolab", biolab.processProject)


  //  val ember = new Project("emberjs/ember.js", List("488e9a4", "9702180", "3a3aa05", "8764e14", "4c7da5d", "e03fcf8", "b7afd25", "07d1ec3", "50dd9cf", "f7ac13b"),simAlgs, nrofcommits)
  //  ember.writeXLS

  //      println(RepositoryOperations.randomCommitsBySHA(Git.open(new File(repodir + "ckaestne/typechef/.git")),nrofcommits).map(x => '"' + x + '"').toVector)
  //  val typechef = new Project("ckaestne/typechef", List("38273c2", "b743a32", "f58c127", "3f3aa5b", "8bdb013", "3aa0c74", "57b6609", "5a38dd2", "0154be6", "ecda9bc"), simAlgs.take(1), nrofcommits)
  //  typechef.writeXLS

  //    println(RepositoryOperations.randomCommitsBySHA(Git.open(new File(repodir + "emberjs/ember.js/.git")),10).map(x => '"' + x + '"').toVector)
  //  val ember = new Project("emberjs/ember.js", List("488e9a4", "9702180", "3a3aa05", "8764e14", "4c7da5d", "e03fcf8", "b7afd25", "07d1ec3", "50dd9cf", "f7ac13b"),simAlgs, nrofcommits)
  //  time("ember",ember.writeXLS)

  //  println(RepositoryOperations.randomCommitsBySHA(Git.open(new File(repodir + "contao/core-bundle/.git")),10).map(x => '"' + x + '"').toVector)
  //   val contao = new Project("contao/core-bundle",List("d5b0e81", "465950b", "891e1d3", "a2d34fd", "fe316f5", "2dc450c", "357d722", "455d4fe", "92cb467", "07020e0"), simAlgs, nrofcommits)
  //  contao.writeXLS

  //  println(RepositoryOperations.randomCommitsBySHA(Git.open(new File(repodir + "AskUbuntu/ProFormaComments/.git")),nrofcommits).map(x => '"' + x + '"').toVector)
  //  val askubuntu= new Project("AskUbuntu/ProFormaComments", List("5eab8ae", "69470af", "2c14268", "6241f8d", "7495650", "bd01ff4", "ff85643", "5a37161", "1f71de3", "67089d1"), simAlgs, nrofcommits)
  //  askubuntu.writeResults
  //  askubuntu.writeXLS

  //  println(RepositoryOperations.randomCommitsBySHA(Git.open(new File(repodir + "adobe/brackets/.git")),nrofcommits).map(x => '"' + x + '"').toVector)
  val adobe = new Project("adobe/brackets", fixed_commits_folder, List("8338376", "b3a576c", "d89738d", "35b958d", "5601610", "171888e", "bea90d7", "c8e59a8", "bbac945", "6baa5b4"), simAlgs.take(1), nrofcommits)
  time("adobe", adobe.processProject)


  /**
    * KNOWN DUPLICATES FROM PULL REQUESTS
    */
  val known_duplicates_folder = "known-duplicates-results"
  //  val adobe_known_duplicates = new Project("adobe/brackets", known_duplicates_folder, List("1f4049d"), simAlgs, 1)
  //  adobe_known_duplicates.processProject
  //  val apache_known_duplicates = new Project("apache/spark", known_duplicates_folder, List("3513fdb"), simAlgs, 1)
  //  apache_known_duplicates.processProject
  //    val askubuntu_known_duplicates = new Project("AskUbuntu/ProFormaComments", known_duplicates_folder, List("51451fe"), simAlgs, 1)
  //    askubuntu_known_duplicates.processProject
  //  val biolab_known_duplicates = new Project("biolab/orange3", known_duplicates_folder, List("14bf932"), simAlgs, 1)
  //  biolab_known_duplicates.processProject
  //  val contao_known_duplicates = new Project("contao/core-bundle", known_duplicates_folder, List("ace4aa3"), simAlgs, 1)
  //  contao_known_duplicates.processProject
  //  val emberjs_known_duplicates = new Project("emberjs/ember.js", known_duplicates_folder, List("6328005"), simAlgs, 1)
  //  emberjs_known_duplicates.processProject
  //  val ktap_known_duplicates = new Project("ktap/ktap", known_duplicates_folder, List("4f70810"), simAlgs, 1)
  //  ktap_known_duplicates.processProject
  //  val ktap1_known_duplicates = new Project("ktap/ktap", known_duplicates_folder, List("d2bb7de"), simAlgs, 1)
  //  ktap1_known_duplicates.processProject

  //  val marlin = Evaluation.getAllAddedLines("MarlinFirmware/Marlin", "arnoudja/Marlin", "7f9dacf")
  //  val marlin_known_duplicates = new Project ("MarlinFirmware/Marlin", known_duplicates_folder, List ("7f9dacf"), simAlgs, 1)
  //  marlin_known_duplicates.processProject
  //  val laravel_known_duplicates = new Project("laravel/framework", known_duplicates_folder, List("74ec836"), simAlgs, 1)
  //  laravel_known_duplicates.processProject
  //  val zend_known_duplicates = new Project("zendframework/zendframework", known_duplicates_folder, List("8b052e8"), simAlgs, 1)
  //  zend_known_duplicates.processProject
}
