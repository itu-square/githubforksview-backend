package dk.itu.gfv.evaluation.setup

import java.io.File
import java.text.SimpleDateFormat

import dk.itu.gfv.evaluation.setup.FixedRandomCommits.{RndCommit, SimAlgorithm}
import dk.itu.gfv.monitor.db.Models.AddedLine
import dk.itu.gfv.redundant.TokenFrequency
import dk.itu.gfv.repo.RepositoryOperations.AddedLine2
import dk.itu.gfv.util.{ProjectsPaths, MonitorProperties, Util}
import dk.itu.gfv.util.Util.{printToFile,readFromFileLines}
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.RevCommit

import scala.collection.JavaConverters._
import scala.collection.immutable.HashMap

/**
  * Created by scas on 21-11-2016.
  */
object ChatEvaluation extends ProjectsPaths with App with MonitorProperties{

  val chatProjects = List("chatprojects/17708-chupanw", "chatprojects/17708-danielzsandberg", "chatprojects/17708-gabrielcsf", "chatprojects/17708-shuiblue", "chatprojects/17708-waqarcmu")
  val simalg = List(SimAlgorithm("n-levenshtein", List(0.8, 0.9)), SimAlgorithm("jaro-winkler", List(0.8, 0.9)))

  def makeLineFrequency = {
    chatProjects.foreach(folder => {
      val outputFolder = repodir + folder + "/"
      val lines = TokenFrequency.linesFrequency(repodir + folder)
      printToFile(new File(outputFolder + linefrequency))(fp => lines.foreach(fp.println))
    })
  }

  case class ProjectCommits(project: String, cs: List[RevCommit])

  case class CommitAddedLine(project: String, sha: String, commitTime: Int, addedLines: Vector[AddedLine2])

  def getProjectCommits(input: List[String]): List[ProjectCommits] = input match {
    case h :: t => {
      println("Processing " + h)
      val git = Git.open(new File(repodir + h + "/.git"))
      val cs = git.log.all.call.asScala.toList.init // discard first commit (usually the one that adds a lot of files)
      ProjectCommits(h, cs) :: getProjectCommits(t)
    }
    case List(h) => {
      val git = Git.open(new File(repodir + h + "/.git"))
      val cs = git.log.all.call.asScala.toList.init
      List(ProjectCommits(h, cs))
    }
    case List() => Nil
  }

  val projectCommits = getProjectCommits(chatProjects)
//  val addedLinesWithStopwords = {
//    val result = collection.mutable.ListBuffer[CommitAddedLine]()
//    projectCommits.foreach(project => {
//      // create an object commitaddedline
//      val git = Git.open(new File(repodir + project.project + "/.git"))
//      project.cs.foreach(commit => {
//        if (!(commit.getName.equals("9b721f42c0a43c063b185a3b64df5897586e2f16") || commit.getName.equals("7af9d6b567547f7a6bead31057c98ae388a182af"))) // initial commit from danielzsandberg and chupanw
//          result.+=(CommitAddedLine(project.project, commit.getName.take(7), commit.getCommitTime, FixedRandomCommits.patchToAddedLines(git, commit.getName, false).getOrElse(Vector())))
//      })
//    })
//    result.toList
//  }
//  val addedLinesNoStopwords = {
//    val result = collection.mutable.ListBuffer[CommitAddedLine]()
//    projectCommits.foreach(project => {
//      // create an object commitaddedline
//      val git = Git.open(new File(repodir + project.project + "/.git"))
//      project.cs.foreach(commit => {
//        if (!(commit.getName.equals("9b721f42c0a43c063b185a3b64df5897586e2f16") || commit.getName.equals("7af9d6b567547f7a6bead31057c98ae388a182af"))) // initial commit from danielzsandberg and chupanw
//          result.+=(CommitAddedLine(project.project, commit.getName.take(7), commit.getCommitTime, FixedRandomCommits.patchToAddedLines(git, commit.getName, true).getOrElse(Vector())))
//      })
//    })
//    result.toList
//  }

  /**
    * Return addedlines in commits including stopwords. If filterbyCommitTime is set to true, then for each random commit there will be considered only commits in forks before this commit.
    *
    * @param filterByCommitTime
    * @return
    */

//  def getRndCommitsAndAddedLinesWithStopwords(filterByCommitTime: Boolean) = {
//    addedLinesWithStopwords.map(commit => {
//      // get the project for this commit
//      val sha = commit.sha
//      val commitTime = commit.commitTime
//      val project = commit.project
//      // get all the added lines in this random commit
//      val patchAddedLines = commit.addedLines.groupBy(_.file)
//      // get all the commits that are in the other projects before this commit
//      val forksAddedLines = if (filterByCommitTime) {
//        addedLinesWithStopwords.
//          filterNot(_.project.equals(project)).
//          filter(_.commitTime < commitTime).
//          map(_.addedLines).
//          toVector.flatten.
//          groupBy(_.sha).
//          map(x => x._1 -> x._2.sortWith(_.position < _.position).groupBy(_.file))
//      } else {
//        addedLinesWithStopwords.
//          filterNot(_.project.equals(project)).
//          map(_.addedLines).
//          toVector.flatten.
//          groupBy(_.sha).
//          map(x => x._1 -> x._2.sortWith(_.position < _.position).groupBy(_.file))
//      }
//      RndCommit(project, sha, patchAddedLines, forksAddedLines)
//    })
//  }

//  def getRndCommitsAndAddedLinesNoStopwords(filterByCommitTime: Boolean) = {
//    addedLinesWithStopwords.map(commit => {
//      // get the project for this commit
//      val sha = commit.sha
//      val commitTime = commit.commitTime
//      val project = commit.project
//      // get all the added lines in this random commit
//      val patchAddedLines = commit.addedLines.groupBy(_.file)
//      // get all the commits that are in the other projects before this commit
//      val forksAddedLines = if (filterByCommitTime) {
//        addedLinesNoStopwords.
//          filterNot(_.project.equals(project))
//          .filter(_.commitTime < commitTime)
//          .map(_.addedLines)
//          .toVector.flatten
//          .groupBy(_.sha)
//          .map(x => x._1 -> x._2.sortWith(_.position < _.position).groupBy(_.file))
//      } else {
//        addedLinesNoStopwords
//          .filterNot(_.project.equals(project))
//          .map(_.addedLines)
//          .toVector.flatten
//          .groupBy(_.sha)
//          .map(x => x._1 -> x._2.sortWith(_.position < _.position).groupBy(_.file))
//      }
//      RndCommit(project, sha, patchAddedLines, forksAddedLines)
//    })
//  }

  class EvaluateFeatures(projects: Map[String, String], feature: String) {

    val tfidf = tf_idf_projects(projects.keys.toList)
    val tfidf_values = tfidf.values.toSeq
    def compute_feature(project: String, commitID: String) = {

//      println("Evaluating encryption of " + project + " from commit " + commitID)
//      val rndCommitsAndAddedLinesWithStopwords = getRndCommitsAndAddedLinesWithStopwords(false).filter(x => x.sha.equals(commitID))
//      val rndCommitsAndAddedLinesNoStopwords = getRndCommitsAndAddedLinesNoStopwords(false).filter(x => x.sha.equals(commitID))
//      val sdf = new SimpleDateFormat("""yyyy-MM-dd'T'HH:mm:ss""")
//      val outputFolderPath = new File(repodir + project + "/gfv-results/" + feature + "-feature/" + sdf.format(new java.util.GregorianCalendar().getTime).replaceAll(":", "-"))
//
//      FixedRandomCommits.evaluateConfigurations(project, true, true, true, outputFolderPath.getAbsolutePath + "/", List(commitID), 1, rndCommitsAndAddedLinesWithStopwords, rndCommitsAndAddedLinesNoStopwords, simalg, 15,tfidf)
    }


    feature match {
      case "encryption" => {
        projects.keySet.foreach(key => {
          compute_feature(key, projects.get(key).get)
        })
      }
      case "history" => {
        projects.keySet.foreach(key => {
          compute_feature(key, projects.get(key).get)
        })
      }
      case "authPlugin" => {
        projects.keySet.foreach(key => {
          compute_feature(key, projects.get(key).get)
        })
      }
    }

  }
  def tf_idf_projects (projects: List[String])(implicit separator: String): Map[String, String]  = {
    def computeProject(p: String) = {
      val projectFiles = Util.files(repodir + p, true).filter(x => (Util.fileExtension(x)).equals("java"))
      val projectDocument =  projectFiles.map(readFromFileLines(_)).flatten
      HashMap(p -> (projectDocument.mkString(separator)))
    }

    def compute(input: List[String]): Map[String, String] = input match {
      case h :: t => computeProject(h) ++ compute(t)
      case List(h) => computeProject(h)
      case List() => HashMap()
    }
    compute(projects)
  }



  val encryptionFeature = Map(
    "chatprojects/17708-chupanw" -> "547151f",
    "chatprojects/17708-danielzsandberg" -> "4aed384",
    "chatprojects/17708-gabrielcsf" -> "3e758a0",
    "chatprojects/17708-shuiblue" -> "1f64087", // but it is not clear which commit added the feature
    "chatprojects/17708-waqarcmu" -> "1f84cb5" // but it is not clear which commit added the feature
  )

    new EvaluateFeatures(encryptionFeature, "encryption")

  val historyFeature = Map(
    "chatprojects/17708-chupanw" -> "567a3c9",
    "chatprojects/17708-danielzsandberg" -> "4cf8dd4",
    "chatprojects/17708-gabrielcsf" -> "3da9c80",
    "chatprojects/17708-shuiblue" -> "93d1c4f",
    "chatprojects/17708-waqarcmu" -> "c3f1c90" // but it is not clear which commit added the feature
  )

    new EvaluateFeatures(historyFeature, "history")

  val authPluginFeature = Map(
    "chatprojects/17708-chupanw" -> "0b425ad",
    "chatprojects/17708-danielzsandberg" -> "27687be",
    "chatprojects/17708-gabrielcsf" -> "f4a6588",
    "chatprojects/17708-shuiblue" -> "23ff821",
    "chatprojects/17708-waqarcmu" -> "c3f1c90"
  )

//  new EvaluateFeatures(authPluginFeature, "authPlugin")

  val tfidf = tf_idf_projects(chatProjects)

  val s = "socket.close();".trim.toLowerCase
  val t = "} catch (IOException ex) {".trim.toLowerCase
  val z = "e.printStackTrace();".trim.toLowerCase
  val x = "public void stop() {".trim.toLowerCase
  val y = "public void send(TextMessage msg) {".trim.toLowerCase
  val u = "try {".trim.toLowerCase
  val w = "String oldMsg = msg.getContent();".trim.toLowerCase
  val a = "public static String encrypt(Message msg) {".trim.toLowerCase


  def tryTFIDF(s: String) = {
    println("***" + s + "***")
    chatProjects.foreach(p => {
      println(p + " -> " + TFIDF.compute_tfidf(s, tfidf.get(p).get, tfidf.values.toSeq))
    })
  }

  tryTFIDF(s)
  tryTFIDF(t)
  tryTFIDF(z)
  tryTFIDF(x)
  tryTFIDF(y)
  tryTFIDF(u)
  tryTFIDF(w)
  tryTFIDF(a)


}
