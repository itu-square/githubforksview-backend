package dk.itu.gfv.evaluation.setup

import java.io.File

import dk.itu.gfv.repo.{GithubExtractor, RepositoryOperations}
import dk.itu.gfv.model.Model._
import dk.itu.gfv.redundant.model.LineChangeModel
import dk.itu.gfv.redundant.model.LineChangeModel.{Commit, LineChange}
import dk.itu.gfv.util.ProjectsPaths
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.internal.storage.file.FileRepository
import spray.json._
import spray.json.DefaultJsonProtocol._
/**
  * Created by scas on 06-09-2016.
  */
object CloneProjects extends ProjectsPaths {

  val inputProjects = "evaluation/projects.txt"

  def cloneProject(repository: GithubRepository) = {
    RepositoryOperations.cloneRepository(repository.full_name,repository.default_branch)
    val git = new Git(new FileRepository(repodir + repository.full_name + "/.git"))
    val forks = GithubExtractor.getJSONPagination(repository.full_name, "forks", None).get.result.parseJson.convertTo[Option[List[GithubRepository]]]
    val activeForks = forks match {
      case Some(f) => f.filter(x => RepositoryOperations.isActiveFork(x.created_at, x.pushed_at)).toSeq
      case None => Seq()
    }
    println("Hold on tight... adding and fetching " + activeForks.size + " forks of this project " + repository.full_name)
    RepositoryOperations.addForksAsRemotes(git,activeForks)
    RepositoryOperations.fetchForksRemotes(git,activeForks)
  }

  def cloneProject(repository: String, default_branch: String) = {
    RepositoryOperations.cloneRepository(repository,default_branch)
    val git = new Git(new FileRepository(repodir + repository + "/.git"))
    val forks = GithubExtractor.getJSONPagination(repository, "forks", None).get.result.parseJson.convertTo[Option[List[GithubRepository]]]
    val activeForks = forks match {
      case Some(f) => f.filter(x => RepositoryOperations.isActiveFork(x.created_at, x.pushed_at)).toSeq
      case None => Seq()
    }
    println("Hold on tight... adding and fetching " + activeForks.size + " forks of this project " + repository)
    RepositoryOperations.addForksAsRemotes(git,activeForks)
    RepositoryOperations.fetchForksRemotes(git,activeForks)
  }

  def computeAddedLines(project: String, git: Git) = {
    val remotes = RepositoryOperations.getRemotes(git.getRepository).keySet
    val commits = for {
      b <- remotes
      // for each branch get a log of commits from this branch
    // TODO fix me, the forkname must be corrected in the case of origin
      lastSlash = b.lastIndexOf('/')
      forkname = if(b.startsWith("origin/")){
        project
      }else {
        b.substring(0, lastSlash)
      }
      c = RepositoryOperations.getCommitsFromRemotesBranch(git, b).map(x => Commit(forkname,b.substring(lastSlash+1,b.size), x.getName, x.getCommitTime))
    } yield c
    // this should only keep unique commits in the project
    val c = commits.flatten.toList.par.groupBy(_.sha).map(_._2.head).toList.sortWith(_.commitTime < _.commitTime)
    val addedLines = c.drop(1).map(x => {
        RepositoryOperations.getPatchJGIT(git, x.sha) match {
          case Some(patch) => LineChangeModel.patchToLineChange(patch, Commit(x.fork,x.branch,x.sha,x.commitTime))
          case None => Nil
        }
      }).flatten

    println(addedLines.toJson.prettyPrint)
  }



  def main(args: Array[String]): Unit = {
    cloneProject("MarlinFirmware/Marlin", "RC")
//  RepositoryOperations.cloneRepository()
//    computeAddedLines("scas-mdd/redundant-test",Git.open(new File("C:/ITU/MyRepositories/GithubForkViews/githubforkviews-backend/evaluation/repositories/scas-mdd/redundant-test/.git")))
  }

}
