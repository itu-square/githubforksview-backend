package dk.itu.gfv.evaluation.setup

import java.io.File
import java.text.SimpleDateFormat

import dk.itu.gfv.redundant.model.LineChangeModel
import dk.itu.gfv.redundant.model.LineChangeModel.Commit
import dk.itu.gfv.repo.{ RepositoryOperations}
import dk.itu.gfv.util.Util._
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.RevWalk
import dk.itu.gfv.redundant.similarity.SimilarityMain

import scala.annotation.tailrec
import scala.util.Random
import dk.itu.gfv.repo.RepositoryOperations._

import collection.JavaConverters._

/**
  * Created by scas on 10/12/2016.
  */
object DuplicatedCommits extends App with RegexPatternsNormalization {


  val sdf = new SimpleDateFormat("""yyyy-MM-dd'T'HH:mm:ss""")

  /* CONFIGURATION PARAMETERS */
  private val NR_RANDOM_COMMITS = 10
  private val NR_OF_ADDED_LINES_IN_PATCH = 20


  case class ProjectRandomResult(commit: String, file: String, scores: (Vector[(String, Seq[SimilarityMain.Sim])], Vector[Evaluation.FunctionDefResult],  Vector[(String, LineChangeModel.LineChange, String)], Vector[(String, Double, LineChangeModel.LineChange)]))

  /**
    * Given a list of commits and a list of its parents, (Map(sha -> List(parentsSHA,....,...,)), it returns a number of random
    * commits (sha) that are not merge commits
    *
    * @param input
    * @param size
    * @return
    */
  def randomCommitsBySHA(input: Map[String,List[String]], size: Int): List[String] = {
    // remove the merged commits (those which have 2 or more parents)
    val noMergesInput = input.filter(x => x._2.size < 2).map(_._1).toVector // use vector for fast accessing at random indexes
    val result = collection.mutable.HashMap[String, String]()
    var i = 0
    while (i < size) {
      val c = noMergesInput(Random.nextInt(noMergesInput.size))
      result.get(c) match {
        case None => {
          result.+=(c -> "")
        }
        case _ => Nil
      }
      i = i + 1
    }
    result.keySet.toList
  }



  /**
    * This computes the random evaluation for 10 random commits of a project and excludes the commits that are common.
    * It only takes into consideration the commits that are in the forks but not common to all forks.
    *
    * @param project
    */

  def random_commits1(project: String) = {
    val sb = new StringBuilder()
    val p = project
    val resultFolder = new File(repodir + p + "/random-commits-results/rnd-" + sdf.format(new java.util.GregorianCalendar().getTime).replaceAll(":","-"))
    resultFolder.mkdirs
    val git = Git.open(new File(repodir + p + "/.git"))
    val walk = new RevWalk(git.getRepository)
    val cs = git.log.all.call.asScala.toList
    val commits = cs.map(x => x.getName.take(7) -> x.getParents.toList.map(_.getName.take(7))).toMap
    println(commits.size + " commits in " +project)
    val rndCommits = randomCommitsBySHA(commits, NR_RANDOM_COMMITS)
    println(rndCommits.size + " random commits selected in this project" + rndCommits)

    val allRandomResults = collection.mutable.MutableList[ProjectRandomResult]()
    rndCommits.foreach(rndCommit => {
      walk.reset()
      val rndCommitTime = walk.parseCommit(git.getRepository.resolve(rndCommit)).getCommitTime
      println("Analyzing random commit " + rndCommit)
      val commitsToAnalyze = time("findCommits", RepositoryOperations.findCommits(commits, cs, rndCommit, rndCommitTime))
      val tempAddedLines = commitsToAnalyze.par.map(x => {
        RepositoryOperations.getPatchJGIT(git, x) match {
          case Some(patch) => LineChangeModel.patchToLineChange(patch, Commit("", "", x, 0), commentsRegexPattern, importRegexPattern, returnKeywordPattern, bracketsSemicolonPattern)
          case None => Nil
        }
      }).toList.flatten
    })
      // a map of files and lines added in that file, including the line number
//      val rndCommitsPatchLines = patchToAddedLines(git, rndCommit)
//      // filter only to the number of lines allowed by the predefined value NR_OFADDED_LINES_IN_PATCH
//      val rndCommitPatchAddedLines = rndCommitsPatchLines match {
//        case Some(f) => {
//          f.map(x => {
//            val file = x._1
//            val tempLines = if (x._2.size > NR_OF_ADDED_LINES_IN_PATCH) x._2.take(NR_OF_ADDED_LINES_IN_PATCH) else x._2
//            file -> tempLines
//          })
//        }
//        case None => Map("error_" -> List())
//      }
//
//      val commitFolder = new File(resultFolder.getAbsolutePath + "/" + rndCommit)
//      commitFolder.mkdirs
//
//
//      // files in this patch
//      val files = rndCommitPatchAddedLines.keySet.toList
//
//      files.foreach(file => {
//        val simscore = time("computeSimilarity", Evaluation.computeSimilarity(tempAddedLines, rndCommitPatchAddedLines.get(file).getOrElse(List()))).toVector
//        val functiondefscore = time("computeFunctionDef", Evaluation.computeFunctionDef(tempAddedLines, file, rndCommitPatchAddedLines.get(file).getOrElse(List()))).toVector
//        val proximity = time("proximity", Evaluation.proximityCheck(tempAddedLines, file, rndCommitPatchAddedLines.get(file).getOrElse(List()))).toVector
//        val proximityAndsimilarity = time("proximityAndSimilarity", Evaluation.similarityAndProximity(tempAddedLines, file, rndCommitPatchAddedLines.get(file).getOrElse(List()), 0.6)).toVector
//
//        allRandomResults.+=(ProjectRandomResult(rndCommit, file, (simscore, functiondefscore, proximity, proximityAndsimilarity)))
//        /* FILE OUTPUT */
//
//        sb.append("******SimScore*****\n")
//        simscore.foreach(x => sb.append(x + "\n"))
//        sb.append("******FunctionDef*****\n")
//        functiondefscore.foreach(x => sb.append(x + "\n"))
//        sb.append("******Proximity*****\n")
//        proximity.foreach(x => sb.append(x + "\n"))
//        sb.append("******ProximityAndSImilarity*****\n")
//        proximityAndsimilarity.foreach(x => sb.append(x + "\n"))
//        printToFile(new File(commitFolder.getAbsolutePath + "/" + file.replaceAllLiterally("/", "-") + "-result.csv"))(fp => fp.println(sb.toString))
//
//        /* VISUAL OUTPUT WITH COLORS */
//        sb.clear
//        sb.append("AddedLines/Result\n")
//        sb.append("******SimScore*****")
//        val fileStats = {
//          for (a <- 1 to NR_OF_ADDED_LINES_IN_PATCH) { // add the header which is from 1 to 20
//            sb.append("," + a)
//          }
//          sb.append("\n")
//          if(simscore.isEmpty){
//            sb.append("-\n")
//          }
//          else {
//            val highestNrOfResults = simscore.sortWith(_._2.size > _._2.size).head._2.size
//            for (a <- 1 to highestNrOfResults) {
//              sb.append(a)
//              for (b <- 0 to simscore.size - 1) {
//                sb.append("," + {
//                  try {
//                    simscore(b)._2(a - 1).simscore
//                  } catch {
//                    case ex: IndexOutOfBoundsException => "-"
//                  }
//                })
//              }
//              sb.append("\n")
//            }
//          }
//          sb.append("******FunctionDef*****")
//          for (a <- 1 to NR_OF_ADDED_LINES_IN_PATCH) { // add the header which is from 1 to 20
//            sb.append("," + a)
//          }
//          if(functiondefscore.isEmpty){
//            sb.append("-\n")
//          }else {
//            for (a <- 1 to functiondefscore.size) {
//              for (b <- 0 to functiondefscore.size - 1) {
//                sb.append("," + {
//                  try {
//                    functiondefscore(b).forkParsedFunc.simscore
//                  }
//                  catch {
//                    case ex: Exception => "-"
//                  }
//                })
//              }
//            }
//            sb.append("\n")
//          }
////          sb.append("******Proximity*****")
////          for (a <- 1 to NR_OF_ADDED_LINES_IN_PATCH) { // add the header which is from 1 to 20
////            sb.append("," + a)
////          }
////          if(proximity.isEmpty){
////            sb.append("-\n")
////          }
////          else {
////            for (a <- 1 to proximity.size) {
////              for (b <- 0 to proximity.size - 1) {
////                sb.append(",y" )
////            }
////            sb.append("\n")
////          }
////          sb.append("******ProximityAndSImilarity*****")
////          for (a <- 1 to NR_OF_ADDED_LINES_IN_PATCH) { // add the header which is from 1 to 20
////            sb.append("," + a)
////          }
//          sb.toString
//        }
//        printToFile(new File(commitFolder.getAbsolutePath + "/" + file.replaceAll("/","-") + "-visual-stats.csv"))(fp => fp.println(fileStats))
//        sb.clear
//      })


//

      /* OUTPUT */

//      sb.append("******SimScore*****\n")
//      simscore.foreach(x => sb.appen  val simscore = {
      //        for {
      //          file <- files
      //          res = time("computeSimilarity", Evaluation.computeSimilarity(tempAddedLines, rndCommitPatchAddedLines.get(file).getOrElse(List())))
      ////          highestNrOfResults = res.sortWith(_._2.size > _._2.size).head._2.size
      ////          fileStats = {
      ////            val sb1 = new StringBuilder()
      ////            sb1.append("AddedLines/Result")
      ////            for (a <- 1 to res.size ){
      ////              sb1.append("," + a)
      ////            }
      ////            sb1.append("\n")
      ////            val highestNrOfResults = res.sortWith(_._2.size > _._2.size).head._2.size
      ////            for (a <- 1 to highestNrOfResults){
      ////              sb1.append(a)
      ////              for(b <- 0 to res.size-1){
      ////                sb1.append(","+ {
      ////                  try {
      ////                    res(b)._2(a-1)
      ////                  }catch {
      ////                    case ex: IndexOutOfBoundsException => "-"
      ////                  }
      ////                } )
      ////              }
      ////              sb1.append("\n")
      ////
      ////            }
      ////            sb1.toString
      ////          }
      ////          printToFile(new File(commitFolder.getAbsolutePath + "/" + file + "-simscore.csv"))(fp => fp.println(fileStats))
      //
      //        } yield (res)
      //      }.flatten
      //
      //      val functiodefscore = {
      //        for {
      //          file <- files
      //          res = time("computeFunctionDef", Evaluation.computeFunctionDef(tempAddedLines, file, rndCommitPatchAddedLines.get(file).getOrElse(List())))
      //        } yield (res)
      //      }.flatten
      //
      //      val proxmity = {
      //        for {
      //          file <- files
      //          res = time("proximity", Evaluation.proximityCheck(tempAddedLines, file, rndCommitPatchAddedLines.get(file).getOrElse(List())))
      //        } yield (res)
      //      }.flatten
      //
      //      val proxmityAndSimilarity = {
      //        for {
      //          file <- files
      //          res = time("proximityAndSimilarity", Evaluation.similarityAndProximity(tempAddedLines, file, rndCommitPatchAddedLines.get(file).getOrElse(List()), 0.6))
      //        } yield (res)
      //      }.flattend(x + "\n"))
//      sb.append("******FunctionDef*****\n")
//      functiodefscore.foreach(x => sb.append(x + "\n"))
//      sb.append("******Proximity*****\n")
//      proxmity.foreach(x => sb.append(x + "\n"))
//      sb.append("******ProximityAndSImilarity*****\n")
//      proxmityAndSimilarity.foreach(x => sb.append(x + "\n"))
//      printToFile(new File(commitFolder.getAbsolutePath + "/result.csv" ))(fp => fp.println(sb.toString))

      /* STATS */
//      val rndCommitSHA = rndCommit
//      printToFile(new File(commitFolder.getAbsolutePath + "/stats.txt"))(fp => {
//        fp.println("****General project stats****")
//        fp.println(commits.size + " commits in this project")
//        fp.println(rndCommits.size + " random commits selected in this project")
//        fp.println(rndCommits.mkString(","))
//        fp.println("****Random commits stats****")
//        fp.println(tempAddedLines.groupBy(_.commit.sha).size + " commits before commit: " + rndCommitSHA)
//        fp.println(tempAddedLines.size + " added lines in this project before commit: " + rndCommitSHA)
//        rndCommitPatchAddedLines.foreach(x => fp.println("Patch of file " + x._1 + " had " + x._2.size + " changes "))
//      })
//      sb.clear
//    })
//    val s = allRandomResults.toList
//    try{
//      val t = s.filter(x => x.scores._1.head._2.isEmpty)
//      val r = s.map(x => x.scores._1).flatten.take(5).map(x => x._2).filter(x => x.isEmpty).size
//      printToFile(new File(resultFolder.getAbsolutePath + "/allresults.txt"))(fp => {
//        fp.println("The project had " + t.size + " cases with first line added having no returned value from the tool out of " + s.map(x => x.scores._1).size)
//        fp.println("The project had " + r + " cases with the 5 lines added having no returned value from the tool out of " + s.map(x => x.scores._1).size)
//      })
//
//    }
//    catch {
//      case ex: Exception => Nil
//    }
//

  }

  val projects = readFromFileLines(evaluationDir + "projects.txt")
  projects.foreach(random_commits1(_))

//  random_commits1("testrepo")
//  random_commits1("adobe/brackets")
}
