package dk.itu.gfv.evaluation.setup

import dk.itu.gfv.util.Util._
import org.apache.commons.lang3.StringUtils._
/**
  * Created by scas on 07-11-2016.
  */
object RemoveStopWords extends App{

  private lazy val stopwords = readFromFileLines("stopwords.txt")

  def removeStopWords(input: String) = {
    var result = input
    for (str <- stopwords)
//      result = result.replaceAllLiterally(char, " ")
      result = replace(result,str, " ") // apparently this is faster
    result
  }

  val a = """From 6baa5b4998e2ceac46b967b414c7bceec8558c40 Mon Sep 17 00:00:00 2001
            |From: Martin Zagora <zaggino@gmail.com>
            |Date: Mon, 22 Aug 2016 12:10:49 +1000
            |Subject: [PATCH] sync package.json to electron-app folder
            |
            |---
            | gulpfile.js | 26 +++++++++++++++++++++++++-
            | 1 file changed, 25 insertions(+), 1 deletion(-)
            |
            |diff --git a/gulpfile.js b/gulpfile.js
            |index af52243..5bc70b2 100644
            |--- a/gulpfile.js
            |+++ b/gulpfile.js
            |@@ -1,3 +1,5 @@
            |+const _ = require('lodash');
            |+const fs = require('fs');
            | const gulp = require('gulp');
            | const path = require('path');
            | const watch = require('gulp-watch');
            |@@ -6,6 +8,25 @@ const BASE_DIR = 'electron-app';
            | const DIST_DIR = 'dist';
            | const JS_GLOB = `${BASE_DIR}/**/*.{js,json}`;
            |
            |+ <p>
            |+function syncPackageJson() {
            |+    const packageJson = require(path.resolve(__dirname, 'package.json'));
            |+    const appJson = _.pick(packageJson, [
            |+        'name',
            |+        'productName',
            |+        'description',
            |+        'author',
            |+        'license',
            |+        'homepage',
            |+        'version',
            |+        'apiVersion',
            |+        'issues',
            |+        'repository',
            |+        'dependencies',
            |+        'optionalDependencies'
            |+    ]);
            |+    fs.writeFileSync(path.resolve(__dirname, BASE_DIR, 'package.json'), JSON.stringify(appJson, null, 2));
            |+}
            |+
            | function copyJs(filePath) {
            |     let from;
            |     let to;
            |@@ -21,7 +42,10 @@ function copyJs(filePath) {
            |         .pipe(gulp.dest(to));
            | }
            |
            |-gulp.task('build', () => copyJs());
            |+gulp.task('build', () => {
            |+    syncPackageJson();
            |+    copyJs();
            |+});
            |
            | gulp.task('watch', ['build'], () => {
            |     watch(JS_GLOB, file => copyJs(file.path));
            |""".stripMargin
  time("a",println(removeStopWords(a)))
}
