package dk.itu.gfv.evaluation.setup

import dk.itu.gfv.redundant.similarity.SimilarityMain
import dk.itu.gfv.util.Util

/**
  * Created by scas on 11/4/2016.
  */
object LevenshteinDistance extends App{

  // http://michalis.site/2013/12/levenshtein/
  def levenshteinDistance(s: String, t: String): Int = {
//    println("s is: " + s )
//    println("t is: " + t )
    if (s.equals(t)) {
      0
    }
    else if (s.isEmpty) {
      t.size
    }

    else if (t.isEmpty) {
      s.size
    }
    else {
      val tLength = t.size
      val columns = tLength + 1
      val v0 = new Array[Int](columns)
      val v1 = new Array[Int](columns)
      for (i <- 0 until columns) {
        v0.update(i, i)
      }
      for (i <- 0 until s.size) {
        v1.update(0, i + 1)
        for (j <- 0 until tLength) {
          val cost = if (s(i).equals(t(j))) 0 else 1
          v1.update(j + 1, Math.min(Math.min(v1(+j) + 1, v0(j + 1) + 1), v0(j) + cost))
          v0.update(j, v1(j))
        }
        v0.update(tLength, v1(tLength))
      }
      v1(tLength)
    }
  }

  // From info.debatty normalized levenshtein distance
  /**
    * Compute distance as Levenshtein(s1, s2) / max(|s1|, |s2|).
    *
    * @param s1
    * @param s2
    * @return
    */
  def distance(s1: String, s2: String): Double = {
    val mLen = Math.max(s1.size, s2.size)
    if (mLen == 0){
      0
    }else {
      levenshteinDistance(s1, s2) / mLen.toDouble
    }
  }

  /**
    * Return 1 - distance.
    *
    * @param s1
    * @param s2
    * @return
    */
//  def similarityNormalized(s1: String, s2: String): Double = Util.time("similarity " + s1.size + ";" + s2.size, 1.0 - distance(s1, s2))
  def similarityNormalized(s1: String, s2: String): Double = 1.0 - distance(s1, s2)

//  println(distance("jack", "jake"))
  println(similarityNormalized("jack", "jake"))
  println(SimilarityMain.checkSimilarityOfStrings("jack", "jake", "n-levenshtein"))



}
