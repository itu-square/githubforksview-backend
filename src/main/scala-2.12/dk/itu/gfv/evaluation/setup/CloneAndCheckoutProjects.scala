package dk.itu.gfv.evaluation.setup

import java.io.File
import java.util

import dk.itu.gfv.repo.GithubExtractor
import dk.itu.gfv.util.ProjectsPaths
import dk.itu.gfv.util.Util._
import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.model.Model.GithubRepository
import org.eclipse.jgit.api.Git
import dk.itu.gfv.redundant.TokenFrequency.linesFrequency
import spray.json._
import spray.json.DefaultJsonProtocol._
/**
  * Created by scas on 18-11-2016.
  */

object CloneAndCheckoutProjects extends ProjectsPaths with App with LazyLogging{

  val prjs = readFromFileLines(evaluationDir + "projects.txt")
  val outputFolder = evaluationDir + "repos-checkout/"

  def cloneAndCheckoutProject(project: String) = {
    val json = GithubExtractor.getRepositoryJson(project)
    val repository = json match {
      case Some(x) => x.parseJson.convertTo[Option[GithubRepository]]
      case None => None
    }
    
    val branch = repository match {
      case Some(x) => x.default_branch
      case None => logger.error("Could not decode json")
    }
    val branchesToClone = new util.ArrayList[String]()
    branchesToClone.add("refs/heads/" + branch)
    val repo = Git.cloneRepository
      .setNoCheckout(false)
      .setBranchesToClone(branchesToClone)
      .setURI("https://github.com/" + project + ".git")
      .setDirectory(new File(outputFolder + project))
      .call
    //    repo.fetch
    println("done")
  }

  def cloneAndCheckoutProjects = prjs.foreach(project => cloneAndCheckoutProject(project))

  prjs.foreach(x => {
    val folder = outputFolder + x + "/"
    println(folder)
    val lf = linesFrequency(folder)
    printToFile(new File(repodir + x + "/" + linefrequency))(fp => lf.foreach(fp.println))
  })

}
