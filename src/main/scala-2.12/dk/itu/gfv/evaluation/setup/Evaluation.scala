package dk.itu.gfv.evaluation.setup

import java.io.{File}

import dk.itu.gfv.model.Model.GithubRepository
import dk.itu.gfv.redundant.Backend
import dk.itu.gfv.redundant.functionparsers.ParseLine
import dk.itu.gfv.redundant.similarity.SimilarityMain
import dk.itu.gfv.redundant.similarity.SimilarityMain.{ParsedFunctionSim, Sim}
import dk.itu.gfv.util.ProjectsPaths
import dk.itu.gfv.util.Util._
import dk.itu.gfv.redundant.model.LineChangeModel
import dk.itu.gfv.redundant.model.LineChangeModel.Commit
import dk.itu.gfv.repo.{GithubExtractor, RepositoryOperations}
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.{RevWalk}
import dk.itu.gfv.redundant.ProximityDetection.proximityChange
import spray.json._
import spray.json.DefaultJsonProtocol._
/**
  * Created by scas on 9/21/2016.
  */
object Evaluation extends ProjectsPaths {


  // Start with a project

  //
  // Get the added lines in the patch that was rejected
  // Load the added lines that were precomputed
  // Filter out the fork + starting sha, then filter out all the commits and added lines before this commitTime from all the other forks.

  val simTreshold = 0.8
  case class InputLine(line: String, lineNumber: Int, fork: String, sha: String, file: String)


  // we need to store all the repositories while we clone
  def fixMissingJson(project: String) = {
    println(project)
    val p = GithubExtractor.getRepositoryJson(project).get.parseJson.convertTo[GithubRepository]
    val forks = readFromFileToString(repodir + project + "/forks.json").parseJson.convertTo[List[GithubRepository]]
    printToFile(new File(repodir + project + "/allrepos.json"))(fp => fp.println((List(p) ++ forks).toJson.prettyPrint))
  }

  def computeAllAddedLines(project: String) = {
    val git = Git.open(new File(repodir + project + "/.git"))
    val remotes = RepositoryOperations.getRemotes(git.getRepository).keySet

//    printToFile(new File(repodir + project + "/alladdedlines.json"))(fp => fp.println(addedLines.asJson.spaces2))
  }

  /**
    * Computes similarity score and proximity on same file. If sim score is higher than the threshold and is in proximity, then we report this line
    * @param addedLines
    * @param file
    * @param lines
    * @param simTreshold
    * @return
    */
  def similarityAndProximity(addedLines: List[LineChangeModel.LineChange], file: String, lines: List[String], simTreshold: Double) = {
    val l = lines.map(x => x.split(",",2).toList)
    val r = for {
      line <- l
      c = for {
        lineChange <- addedLines
        if(lineChange.file.equals(file)) // only do proximity on same files
          simscore = SimilarityMain.checkSimilarityOfStrings(line(1), lineChange.line, "n-levenshtein")
          if(simscore >= simTreshold && proximityChange(Integer.parseInt(line(0)), lineChange.rangeHunk))
            r = (line(1), simscore, lineChange)

    } yield r
    } yield c

    r.flatten
  }

  /**
    * Check proximity change on the same file
    * @param addedLines
    * @param file
    * @param lines
    * @return
    */
  def proximityCheck(addedLines: List[LineChangeModel.LineChange], file: String, lines: List[String]) = {
    val l = lines.map(x => x.split(",",2).toList) // Array(lineNr, lineText)
    val sameFileChanges = addedLines.par.filter(x => x.file.equals(file)).toList
    val proximity = {
      for {
        a <- l
        r = for {
          lineChange <- sameFileChanges
          if(proximityChange(Integer.parseInt(a(0)), lineChange.rangeHunk))
            c = (a(1),lineChange, a(0))
      } yield c
      }yield r
    }
    proximity.flatten
  }


  /**
    * Compute similarity of the replayed lines with the lines added in all the forks before our replayed change
    * @param addedLines - the lines added in the commits in the forks
    * @param lines - the lines from the patch we're trying to replay
    * @return
    */
  def computeSimilarity(addedLines: List[LineChangeModel.LineChange], lines: List[String]) = {
    val textLines = lines.map(x => x.split(",",2)(1))
    print("computing similarity...")
    val r = for {
      line <- textLines
      simscore = addedLines.par.map(x => Sim(SimilarityMain.checkSimilarityOfStrings(line, x.line, "n-levenshtein"), x)).filter(x => x.simscore > simTreshold).seq.sortWith(_.simscore > _.simscore)
    } yield (line, simscore)
    println("done")
    r
  }

  case class FunctionDefResult(inputLine: String, forkParsedFunc: ParsedFunctionSim)

  def computeFunctionDef(addedLines: List[LineChangeModel.LineChange], file: String, lines: List[String]) = {
    print("computing function defs similarity...")
    val textLines = lines.map(x => x.split(",",2)(1))
    val fileExt = fileExtension(file)
    val r = for {
      line <- textLines
      p = ParseLine.parseLine(line, fileExt) match {
        case Some(x) => {
          // compute the similarity of function definitions
          addedLines.map(addedLine => {
            ParseLine.parseLine(addedLine.line, fileExtension(addedLine.file)) match {
              case Some(functionDef) => FunctionDefResult(line,ParsedFunctionSim(Backend.checkSimilarityOfStrings(x.name, functionDef.name, "n-levenshtein"), addedLine))
              case None => FunctionDefResult(line,ParsedFunctionSim(0, addedLine))
             }
          })
        }
        case None => Nil
      }
    } yield p
    println("done")
    r.flatten.par.filter(x => x.forkParsedFunc.simscore > simTreshold).toList
  }




  /**
    * All added lines before the commit given by the SHA and excluding changes in the fork
    *
    * @param project
    * @param fork
    * @param sha
    */
  def getAllAddedLines(project: String, fork: String, sha: String) = {
    print("Getting all added lines in forks...")
    val forks = scala.io.Source.fromFile(repodir + project + "/allrepos.json").getLines.mkString("\n").parseJson.convertTo[List[GithubRepository]]
    val git = Git.open(new File(repodir + project + "/.git"))
    val walk = new RevWalk(git.getRepository)
    val commitTime = try{
      val commit = walk.parseCommit(git.getRepository.resolve(sha))
      commit.getCommitTime
    } catch {
      case ex: Exception => {
        val patchTime = GithubExtractor.getPatchAtCommit(project, sha).split("\r?\n").toList.take(3).last.replace("Date: ", "")
        GithubExtractor.getCommitTimeFromGithubPatch(patchTime)
      }
    }
    // filter the forks to be at this commit time
    val existingForksAtThatCommit = forks.filter(x => {
      val s = x.created_at.getMillis / 1000
      s < (commitTime)
    })
    // for each fork get the commits from the branches before the commitTime
    val reposWithoutFork = existingForksAtThatCommit.filter(x => !x.full_name.equals(fork))
    val commits = for {
      fork <- reposWithoutFork
      commits = getReposCommits(project, fork.full_name, commitTime)
    } yield commits

    // FIXME I think here we should filter the commits that existed in the fork that made the change
    val forksCommits = getReposCommits(project, fork, commitTime)
    //
    val commonCommits = forksCommits.map(x => x.sha).intersect(commits.flatten.map(x => x.sha))

    val noduplicatesCommits= commits.flatten.par.groupBy(_.sha).map(_._2.head).toList // filter duplicated commits by sha
    val allCommits = noduplicatesCommits.par.filterNot(x => commonCommits.contains(x.sha)).toList // this should filter our common commits from all repositories
//    println(allCommits)
    // filter any overlapping commits.
    val allAddedLines = allCommits.map(commit => {
      val addedLinesInPatch = RepositoryOperations.getPatchJGIT(git, commit.sha) match {
        case Some(p) => LineChangeModel.patchToLineChange(p, commit)
        case None => Nil
      }
      addedLinesInPatch
    }).flatten
//    println( " found " + allCommits.size + " unique commits and " + allAddedLines.size + " added lines; done")
    allAddedLines
  }

  /**
    * Return a forks' all commits from all branches, that existed before the given SHA. The comparison
    * is done via commit time, and is not 100% reliable, but it is sufficient for this case
    *
    * @param project
    * @param fork
    * @param commitTime
    * @return
    */
  def getReposCommits(project: String, fork: String, commitTime: Long) = {

    val git = Git.open(new File(repodir + project + "/.git"))
    val forksBranches =
      if (project.equals(fork)) {
        RepositoryOperations.getForksBranches(git, "origin").keySet.map(x => x.substring(5))
      }
      else {
        RepositoryOperations.getForksBranches(git, fork).keySet.map(x => x.substring(5)) // this will be: remotes/user/reponame/branch
      }

    val commits = (for {
      b <- forksBranches
      lastSlash = b.lastIndexOf('/')
      forkname = {
        if (b.startsWith("remotes/origin/")) project else b.substring(8, lastSlash)
      }
      // for each branch get a log of commits from this branch
      c = RepositoryOperations.getCommitsFromRemotesBranch(git, b).map(x => Commit(forkname, b.substring(b.lastIndexOf('/') + 1, b.size), x.getName, x.getCommitTime))
    } yield c).flatten
    // filter out all common commits and keep only those before the commit time of the specified SHA commit
    commits.par.groupBy(_.sha).map(_._2.head).filter(x => commitTime > x.commitTime).toList
  }

  def computeProject(project: String, fork: String, sha: String, file: String, addedLines: List[LineChangeModel.LineChange], inputLines: List[String], output: String): Unit = {
    val resultSimScore = computeSimilarity(addedLines, inputLines)
    val resultFunctionDefScore = computeFunctionDef(addedLines, file, inputLines)
    println(resultFunctionDefScore)
    val header = "Line,Fork-line,SimScore,Fork"
    val sb = new StringBuilder()
    sb.append(header + "\n")
    resultSimScore.foreach(x => {
      x._2.foreach(y => {
        sb.append(x._1 + "," + y.linechange.line + "," + y.simscore + ",https://github.com/" + y.linechange.commit.fork +"/commit/" + y.linechange.commit.sha + " on file " + y.linechange.file + "\n")
      })
    })
    sb.append("******FunctionDef*****\n")
    resultFunctionDefScore.foreach(x => {
      sb.append(x.inputLine + "," + x.forkParsedFunc.linechange.line + "," + x.forkParsedFunc.simscore + ",https://github.com/" + x.forkParsedFunc.linechange.commit.fork +"/commit/" + x.forkParsedFunc.linechange.commit.sha + " on file " + x.forkParsedFunc.linechange.file + "\n")
    })

    sb.append("*****Similarity && Proximity*****\n")
    val simprox = similarityAndProximity(addedLines,file,inputLines, 0.6)
    simprox.foreach(x => {
      sb.append(x._1, "," + x._3.line + "," + x._2 +  ",https://github.com/" + x._3.commit.fork +"/commit/" + x._3.commit.sha + " on file " + x._3.file + "\n")
    })

    sb.append("*****Proximity*****\n")
    val proximity = proximityCheck(addedLines,file,inputLines)
    proximity.foreach(x => {
      sb.append(x._1, "," + x._3 + "," + x._2.rangeHunk + ",https://github.com/" + x._2.commit.fork +"/commit/" + x._2.commit.sha + " on file " + x._2.file + "\n")
    })


    printToFile(new File(repodir + project + "/" + output))(fp => fp.println(sb.toString))
    //    CsvToXls.convertCSVToXLS(repodir + project + "/eval.csv", repodir + project + "/eval.xlsx")
  }


  def computeProject(project: String, fork: String, sha: String, file: String, inputLines: List[String], output: String): Unit = {
    val addedLines = getAllAddedLines(project, fork, sha)
    val par = addedLines.par
    val nrOfCommits = par.groupBy(x => x.commit.sha).size
    val nrOfForks = par.groupBy(x => x.commit.fork).size
    val totalNrOfBranches = par.groupBy(x => x.commit.branch).size
    val nrOfAddedLines = addedLines.size
    val sizeOfReplayedChange = inputLines.size
    computeProject(project, fork, sha, file, addedLines, inputLines, output)
    printToFile(new File(repodir + project + "/stats.txt"))(fp => {
      fp.println("Commits: " + nrOfCommits)
      fp.println("Forks: " + nrOfForks)
      fp.println("Branches: " + totalNrOfBranches)
      fp.println("Added lines: " + nrOfAddedLines)
      fp.println("Duplicated change size : " + sizeOfReplayedChange)
    })
  }


  def evaluateProject(input: String, output: String) = {
    val proj = readFromFileLines(evaluationDir + "input/" + input)
    val projInput = proj.drop(4)//.map(x => x.split(",",2)(1))
    computeProject(proj(0),proj(1),proj(2),proj(3),projInput, output)
  }


  def zendFrameworkDoc3 = {
    val input = "zendframework3.txt"
    val missingSHA = "ac3692028a012860899e1b9ee0c9f67a0e07f0fc"
    val proj = readFromFileLines(evaluationDir + "input/" + input)
    val projInput = proj.drop(4)//.map(x => x.split(",",2)(1))
    val addedLines = getAllAddedLines(proj(0),proj(1),proj(2))
    val git = Git.open(new File(repodir + proj(0) + "/.git"))

      val l = RepositoryOperations.getPatchJGIT(git, missingSHA) match {
      case Some(p) => LineChangeModel.patchToLineChange(p, Commit("GeeH/zf2-documentation", "master", missingSHA, 1363290529))
      case None => Nil
    }
    val allAddedLines = addedLines ++ l
    computeProject(proj(0),proj(1),proj(2),proj(3),allAddedLines,projInput, "zf2-doc-eval11.csv")
  }

  def marlinEvaluation = {
    val marlinFiles = List("marlin.txt", "marlin1.txt","marlin2.txt","marlin3.txt","marlin4.txt","marlin5.txt","marlin6.txt")
    marlinFiles.foreach(x => {
      val m = readFromFileLines(evaluationDir + "input/" + x)
      val input = m.drop(4)
      val addedLines = getAllAddedLines(m(0),m(1),m(2))
      computeProject(m(0),m(1),m(2),m(3),addedLines,input, x + "-result.csv")
    })
    //    val ktap1 = readFromFileLines(evaluation + "input/ktap1.txt")
    //    val ktapInput1 = ktap1.drop(4)
    //    val ktapAddedLines = getAllAddedLines(ktap1(0),ktap1(1),ktap1(2))
    //    computeProject(ktap1(0),ktap1(1),ktap1(2),ktap1(3),ktapAddedLines, ktapInput1, "1-kp_events.csv")
  }


  def main(args: Array[String]) {
    fixMissingJson("MarlinFirmware/Marlin")
    marlinEvaluation

//    zendFrameworkDoc3 OK NOW zf2-doc-eval11.csv
    // One file only
//    evaluateProject("adobe.txt", "adobe-eval.csv")
//    evaluateProject("ansible.txt", "ansible-eval.csv")
//    evaluateProject("apache.txt", "apache-eval.csv") // Apache
//    evaluateProject("askubuntu.txt", "askubuntu-eval.csv") // Askubuntu
//    evaluateProject("biolab.txt", "biolab-eval.csv") // Biolab // OK
//    evaluateProject("contao.txt", "contao-eval.csv") // Biolab // OK
//    evaluateProject("emberjs.txt", "emberjs-eval.csv") // EmberJS
//    evaluateProject("ktap.txt", "ktap-eval.csv") // KTAP .. All KTAP ok
//    evaluateProject("laravel.txt", "laravel-eval.csv") // Laravel
//    zendFrameworkDoc3
//    evaluateProject("zendframework2.txt", "zf2doc-eval.csv") // ZEND ZF2 DOC
//    evaluateProject("zendframework3.txt", "zf2doc-eval1.csv") // ZEND ZF2 DOC


//   Multi files on same commit; reuse the added lines
//    /* different files but at the same commit*/
//    val ktap1 = readFromFileLines(evaluation + "input/ktap1.txt")
//    val ktapInput1 = ktap1.drop(4)
//    val ktapAddedLines = getAllAddedLines(ktap1(0),ktap1(1),ktap1(2))
//    computeProject(ktap1(0),ktap1(1),ktap1(2),ktap1(3),ktapAddedLines, ktapInput1, "1-kp_events.csv")
//
//    val ktap2= readFromFileLines(evaluation + "input/ktap2.txt")
//    val ktapInput2 = ktap2.drop(4)
//    computeProject(ktap2(0),ktap2(1),ktap2(2),ktap2(3),ktapAddedLines, ktapInput2, "2-kp_transport.csv")
//
//    val ktap3= readFromFileLines(evaluation + "input/ktap3.txt")
//    val ktapInput3 = ktap3.drop(4)
//    computeProject(ktap3(0),ktap3(1),ktap3(2),ktap3(3),ktapAddedLines, ktapInput3, "3-ktap_h.csv")
//
//    val ktap4= readFromFileLines(evaluation + "input/ktap4.txt")
//    val ktapInput4 = ktap4.drop(4)
//    computeProject(ktap4(0),ktap4(1),ktap4(2),ktap4(3),ktapAddedLines, ktapInput4, "4-lib_base.csv")

      // NOT OK -- no branch
//    val zend = readFromFileLines(evaluation + "input/zendframework.txt")
//    val zendInput = zend.drop(4)
//    val zend1 = readFromFileLines(evaluation + "input/zendframework1.txt")
//    val zendInput1 = zend1.drop(4)
//    val zendAddedLines = getAllAddedLines(zend(0),zend(1),zend(2))
//    computeProject(zend(0),zend(1),zend(2),zend(3),zendAddedLines, zendInput, "1-SharedEventManager.csv")
//    computeProject(zend1(0),zend1(1),zend1(2),zend1(3),zendAddedLines, zendInput1, "2-ViewModel.csv")


  }

}
