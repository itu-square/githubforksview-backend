package dk.itu.gfv.util

/* Copyright (c) 2016-2017 Stefan Stanciulescu
   IT University of Copenhagen

    version 0.1

    This file is part of GithubForksView.
    GithubForksView is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GithubForksView is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GithubForksView.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.util.concurrent.TimeUnit

import scala.io.Source
import java.io._

import scala.io.Codec
import java.nio.charset.CodingErrorAction
import java.nio.file.Path

import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.io.IOUtils
import org.apache.poi.hssf.record.CFRuleBase.ComparisonOperator
import org.apache.poi.hssf.record.cf.PatternFormatting
import org.apache.poi.ss.usermodel.{BorderStyle, IndexedColors}
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xssf.usermodel.{XSSFColor, XSSFWorkbook}

import scala.collection.JavaConverters._
import scala.sys.process.{Process, ProcessLogger}

object Util extends LazyLogging{

  implicit val codec = Codec("UTF-8")
  codec.onMalformedInput(CodingErrorAction.REPLACE)
  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

  /**
    * Read from file and return a string
    *
    * @param path
    * @return
    */
  def readFromFileToString(path: String): String = {
    try {
      val file = Source.fromFile(new File(path))(codec).getLines.mkString("\n")
      return file
    } catch {
      case f: FileNotFoundException => {
        println("File not found...")
        return ""
      }
      case ex: IOException => {
        println("IOException...")
        return ""
      }
    }
  }

  /**
    * Return a list of the files in the directory and all its subfolders <=> recursive is set
    * Filter the type of the files desired.
    *
    * @param dir
    * @param recursive
    * @return
    */
  def files(dir: String, recursive: Boolean): List[String] = {
    if(new File(dir).exists && new File(dir).isDirectory &&  !(new File(dir).isHidden)){
      val dir_files = new File(dir).listFiles
      val result = collection.mutable.MutableList[String]()

      def search(files: Array[File]): Unit = {
        for (f <- files) {

          if (f.isFile && !java.nio.file.Files.isSymbolicLink(f.toPath)) {
            result += (f.getAbsolutePath)
          }
          else if (f.isDirectory && recursive && !f.isHidden && !java.nio.file.Files.isSymbolicLink(f.toPath)) {
            (search(f.listFiles()))
          }
          else if (!recursive && !java.nio.file.Files.isSymbolicLink(f.toPath))
            result += (f.getAbsolutePath)
        }
      }

      search(dir_files)
      result.toList
    }
    else {
      logger.error("Directory does not exist."); List()
    }
  }


  def fileExtension(file: String) = {
    val lastDot = file.lastIndexOf('.')
    lastDot match {
      case -1 => file
      case _ => file.substring(lastDot + 1, file.length)
    }
  }

  /**
    * Read a file and return a list of lines
    *
    * @param path
    * @return List of strings
    */
  def readFromFileLines(path: String): List[String] = {
    try {
      Source.fromFile(new File(path))(codec).getLines.toList
    } catch {
      case f: FileNotFoundException => {
        sys.error("File not found...")
      }
      case ex: IOException => {
        sys.error("IOException...")
      }
    }
  }

  /**
    * Run a process given by the command in the given folder
    *
    * @param in
    * @return A 3-tuple containing the result, the error, and exit code
    */

  def run(in: String, folder: String): (List[String], List[String], Int) = {
    var out = List[String]()
    var err = List[String]()
    val process = Process(in, new File(folder))
    val exit = process ! ProcessLogger((s) => out ::= s, (s) => err ::= s)
    (out.reverse, err.reverse, exit)
  }

  /**
    * Print to file
    *
    * @param f
    * @param op
    */
  // copyright http://stackoverflow.com/questions/4604237/how-to-write-to-a-file-in-scala
  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) = {
    val p = new java.io.PrintWriter(f)
    try {
      op(p)
    } finally {
      p.close
    }
  }

  // copyright http://stackoverflow.com/questions/9160001/how-to-profile-methods-in-scala
  def time[T](methodName: String, block: => T): T = {
    val t0 = System.nanoTime
    val result = block // call-by-name
    val t1 = System.nanoTime
    println("Elapsed time: " + TimeUnit.MILLISECONDS.convert(t1 - t0, TimeUnit.NANOSECONDS) + " ms for method " + methodName)
//    println("Elapsed time: " + TimeUnit.NANOSECONDS.convert(t1 - t0, TimeUnit.NANOSECONDS) + " nanoseconds for method " + methodName)
    result
  }

  /**
    * Transform an input string (e.g., a CSV as comma or tab-delimited string) into an XLS file and output it to a file on the disk
    *
    * @param inputString
    * @param outputPath
    * @param delimiter
    */


  def writeStringToXLSFile(inputString: String, outputPath: String, delimiter: String) = {
    val wb = new XSSFWorkbook
    val helper = wb.getCreationHelper
    val sheet = wb.createSheet("new sheet")
    val lines = IOUtils.readLines(new StringReader(inputString))
    def isAllDigits(x: String) = x.toCharArray.forall(a => a.isDigit)
    for (i <- 0 to lines.size - 1) {
      val str = lines.get(i).split(delimiter)
      val row = sheet.createRow(i)
      for (j <- 0 to str.length - 1) {
        val s = str(j)
        if (s.length > 1 && !s.charAt(0).equals('0') && isAllDigits(s)) {
          row.createCell(j).setCellValue(Integer.valueOf(s).doubleValue)
        }
        else if(s.length == 1 && isAllDigits(s)){
          row.createCell(j).setCellValue(Integer.valueOf(s).doubleValue)
        }
        else {
          row.createCell(j).setCellValue(helper.createRichTextString(s))
        }
      }
    }

    // Add colors to the sheet. If the value of the result is 0, then bckg color is green, otherwise red.
    val sheetCF = sheet.getSheetConditionalFormatting

    val rule1 = sheetCF.createConditionalFormattingRule(ComparisonOperator.EQUAL, "0");
    val rule2 = sheetCF.createConditionalFormattingRule(ComparisonOperator.GT, "0");
    val fill1 = rule1.createPatternFormatting
//    fill1.setFillBackgroundColor(IndexedColors.CORAL.index);
    fill1.setFillBackgroundColor(new XSSFColor(new java.awt.Color(198,239,206))) // Green
    fill1.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

    val fill2 = rule2.createPatternFormatting
    fill2.setFillBackgroundColor(new XSSFColor(new java.awt.Color(255,199,206))) // Fade Red
    fill2.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

    val cfRules = List(rule1, rule2)

    //4 to lines.size - 1
    // D4 to lines D13; and then until S4 to S13

    val startRow = if(sheet.getLastRowNum == 3) 3 else 4
    val endRow = sheet.getLastRowNum + 1
    val startCell = 'D'
//    println("nr of rows" + sheet.getPhysicalNumberOfRows)
//    println(startRow)
//    println(endRow)
    val endCell = sheet.getRow(startRow).getPhysicalNumberOfCells

    val ranges = {
      val sb = new StringBuilder()
      for (a <- 0 until endCell - startRow) {
        val columnLetter = startCell + a
        sb.append(columnLetter.toChar + startRow.toString + ":" + columnLetter.toChar + endRow.toString + ";")
      }
      sb.append((startCell + (endCell - startRow)).toChar + startRow.toString + ":" + (startCell + (endCell - startRow)).toChar + endRow.toString)
      sb.toString
    }

    // Draw the borders between the sim algorithms
    val borderStyle = wb.createCellStyle
    borderStyle.setBorderRight(BorderStyle.MEDIUM)
    borderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex)

    var keepIndex = 0
    for (r <- sheet.rowIterator.asScala.toList.drop(1)){
      val cells = r.cellIterator().asScala.toList.drop(2)
      cells.head.setCellStyle(borderStyle)
      cells.zipWithIndex.foreach{ case (value, index) => {
        if(index - 4 == keepIndex){
          keepIndex = index
          value.setCellStyle(borderStyle)
        }
      }}
      keepIndex = 0
    }

    val regions = ranges.split(";").toList.map(x => CellRangeAddress.valueOf(x)).toArray
    sheetCF.addConditionalFormatting(regions, rule1, rule2)
    val fileOut = new FileOutputStream(outputPath)
    wb.write(fileOut)
    fileOut.close
  }
}