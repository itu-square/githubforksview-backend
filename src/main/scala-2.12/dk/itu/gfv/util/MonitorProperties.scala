package dk.itu.gfv.util

import java.io.FileInputStream
import java.util.Properties


/**
  * Created by scas on 08-12-2016.
  */
trait MonitorProperties {

  implicit val separator = "\n"
  final val commits_window = 4 // weeks
//  implicit val xls_separator = "\t"

  val projects_folder = "projects"

  final val old_main_project_json = "old_main.json"
  final val new_main_project_json = "new_main.json"
  final val old_forks_project_json = "old_forks.json"
  final val new_forks_project_json = "new_forks.json"
  final val old_branch_heads = "old_branch_heads.json"
  final val new_branch_heads = "new_branch_heads.json"

  val properties = new Properties()
  properties.load(new FileInputStream("config.properties"))
  final val local_run = properties.getProperty("local")
//  final val cache_file = properties.getProperty("data") + "cache.txt"
  final val cache_file = properties.getProperty("local_data") + "cache.txt"
  final val projects_to_be_monitored = properties.getProperty("local_data") + "projects-to-be-monitored.txt"

  final val tfidf_projects_folder = properties.getProperty("local_data") + "tfidf_projects/"
  // git_project -> branch -> List[Commits]
  type NewCommits = Map[String, Map[String, List[String]]]

}
