package dk.itu.gfv.util

import java.io.File

import dk.itu.gfv.repo.{GithubExtractor, RepositoryOperations}
import org.joda.time.DateTime

/**
  * Created by scas on 27-03-2017.
  */
object Stats extends App{

  val projects_txt = scala.io.Source.fromFile("projects-all.txt").getLines().toList
  val july = org.joda.time.DateTime.parse("2016-07-01T00:00:00Z")
  def afterJuly2016(date: DateTime) = {
    val july = org.joda.time.DateTime.parse("2016-07-01T00:00:00Z")
    date.isAfter(july)
  }
  var initial_all_forks = 0
  var initial_first_level_forks = 0
  var first_level_forks_pushed = 0
  var first_level_forks_after_january = 0
  var first_level_forks_after_july = 0
  var all_level_forks_pushed = 0
  var all_level_forks_after_january = 0
  var all_level_forks_after_july = 0
  case class Data (project: String, all_forks: Int, all_level_forks_active: Int, all_forks_active_after_jan: Int, all_forks_active_after_july: Int ,
                   first_level_forks: Int, first_level_forks_active: Int, first_level_forks_active_after_jan: Int, first_level_forks_active_after_july: Int) {
    override def toString = {
      project +  "," + all_forks + "," + all_level_forks_active + "," + all_forks_active_after_jan +  "," + all_forks_active_after_july + "," + first_level_forks + "," + first_level_forks_active + "," + first_level_forks_active_after_jan + "," + first_level_forks_active_after_july
    }
  }
  val results_file = better.files.File("results_data.csv")
  results_file.createIfNotExists()

  val results = collection.mutable.MutableList[Data]()
  projects_txt.foreach(project => {
    val main_repo = GithubExtractor.getRepository(project).get
    println("====================================================")
    val forks = GithubExtractor.getFirstLevelForks(project)
    val all_forks = GithubExtractor.getProjectForks(project)

    println(main_repo.full_name + " has " + main_repo.forks_count + " forks")
    println("******ALL*****")

    val all_forks_active = all_forks.filter(x => RepositoryOperations.isActiveFork(x.created_at,x.pushed_at)).size
    val all_forks_active_after_july_2016 = all_forks.filter(x => RepositoryOperations.isActiveFork(x.created_at,x.pushed_at) && afterJuly2016(x.pushed_at)).size
    val all_forks_active_after_january_2016 = all_forks.filter(x => RepositoryOperations.isActiveFork(x.created_at,x.pushed_at) && x.pushed_at.isAfter(org.joda.time.DateTime.parse("2016-01-01T00:00:00Z"))).size

    println(" #forks with nested forks " + all_forks.size )
    println("   and " + all_forks_active + " forks that have been pushed to")
    println("   and " + all_forks_active_after_january_2016 + " forks that have been pushed to after 1st January 2016")
    println("   and " + all_forks_active_after_july_2016 + " forks that have been pushed to after 1st July 2016")

    println("******FIRST LEVEL *****")
    val first_level_forks_active = forks.filter(x => RepositoryOperations.isActiveFork(x.created_at,x.pushed_at)).size
    val first_level_forks_active_after_july_2016 = forks.filter(x => RepositoryOperations.isActiveFork(x.created_at,x.pushed_at) && afterJuly2016(x.pushed_at)).size
    val first_level_forks_active_after_january_2016 = forks.filter(x => RepositoryOperations.isActiveFork(x.created_at,x.pushed_at) && x.pushed_at.isAfter(org.joda.time.DateTime.parse("2016-01-01T00:00:00Z"))).size
    println("   and " + first_level_forks_active + " forks that have been pushed to")
    println("   and " + first_level_forks_active_after_january_2016 + " forks that have been pushed to after 1st January 2016")
    println("   and " + first_level_forks_active_after_july_2016 + " forks that have been pushed to after 1st July 2016")


    println("====================================================")
    val d = (Data(project,all_forks.size, all_forks_active,all_forks_active_after_january_2016,all_forks_active_after_july_2016,forks.size, first_level_forks_active,first_level_forks_active_after_january_2016,first_level_forks_active_after_july_2016))
    results_file.append(d.toString + "\n")
    initial_all_forks = initial_all_forks + all_forks.size
    initial_first_level_forks = initial_first_level_forks + forks.size
    first_level_forks_pushed = first_level_forks_pushed + first_level_forks_active
    first_level_forks_after_january = first_level_forks_after_january + first_level_forks_active_after_january_2016
    first_level_forks_after_july = first_level_forks_after_july + first_level_forks_active_after_july_2016

    all_level_forks_pushed = all_level_forks_pushed + all_forks_active
    all_level_forks_after_january = all_level_forks_after_january + all_forks_active_after_january_2016
    all_level_forks_after_july = all_level_forks_after_july + all_forks_active_after_july_2016

  })

  println("======ALL FORKS=====")
  println("initial_all_forks: " + initial_all_forks)
  println("pushed to " + all_level_forks_pushed)
  println("after january'16 " + all_level_forks_after_january)
  println("after july'16 " + all_level_forks_after_july)

  println("======FIRST LEVEL FORKS======")
  println("initial_all_forks: " + initial_first_level_forks)
  println("pushed to " + first_level_forks_pushed)
  println("after january'16 " + first_level_forks_after_january)
  println("after july'16 " + first_level_forks_after_july)

}
