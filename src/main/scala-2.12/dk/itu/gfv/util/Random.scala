package dk.itu.gfv.util

import java.io.{ByteArrayOutputStream, File, FileWriter}

import dk.itu.gfv.monitor.{Ssh, Utils}
import dk.itu.gfv.repo.{GithubExtractor, RepositoryOperations}
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.{DiffFormatter, RawTextComparator}
import org.eclipse.jgit.treewalk.CanonicalTreeParser
import collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by scas on 14-09-2016.
  */
object Random extends Ssh with App with Utils {


  val git = Git.open(new File(properties.getProperty("local_data") + "projects/scas-mdd/fork-test/scas-mdd/fork-test/.git"))

  val c1 = git.getRepository.resolve("0ac949f94457ea8d22ac341a45030c2fdd8661bb")
//  val c2 = git.getRepository.resolve("abffbed9fb3add0bef08ec0a0879c65bf84da134")
  val c2 = "abffbed9fb3add0bef08ec0a0879c65bf84da134"

//  val t = RepositoryOperations.getPatchJGIT(git,"abffbed9fb3add0bef08ec0a0879c65bf84da134")
//  println(t)

//  val revWalk = new RevWalk(git.getRepository)
//  val commit = revWalk.parseCommit(c1.toObjectId)
//  val commit2 = revWalk.parseCommit(c2.toObjectId)

  val repository = git.getRepository
  //a new reader to read objects from getObjectDatabase()
  val reader = repository.newObjectReader
  //Create a new parser.
  val oldTreeIter = new CanonicalTreeParser
  val newTreeIter = new CanonicalTreeParser
  //parse a git repository string and return an ObjectId
  try {
    val old = repository.resolve(c2 + "~1^{tree}") // previous ommit
    val head = repository.resolve(c2 + "^{tree}") // head commit
    //Reset this parser to walk through the given tree
    oldTreeIter.reset(reader, old)
    newTreeIter.reset(reader, head)
    val diffs = git.diff() //Returns a command object to execute a diff command
      .setNewTree(newTreeIter)
      .setOldTree(oldTreeIter)
      .call //returns a DiffEntry for each path which is different

    val out = new ByteArrayOutputStream
    //Create a new formatter with a default level of context.
    val df = new DiffFormatter(out)
    df.setRepository(repository)
    //Set the repository the formatter can load object contents from.
    df.setDiffComparator(RawTextComparator.WS_IGNORE_ALL)
    df.scan(oldTreeIter,newTreeIter)

    val t = df.format(diffs)

    println(out.toString)
    for (diff <- diffs.asScala.toList){
      df.format(diff)
      val fileHeader = df.toFileHeader(diff)
      println(fileHeader.getPatchType)
      println(fileHeader.getHunks)
    }
  }
  catch {
    case ex: Exception => ex.printStackTrace()
  }

}
