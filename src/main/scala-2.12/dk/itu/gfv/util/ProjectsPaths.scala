package dk.itu.gfv.util

/* Copyright (c) 2016-2017 Stefan Stanciulescu
   IT University of Copenhagen

    version 0.1

    This file is part of GithubForksView.
    GithubForksView is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GithubForksView is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GithubForksView.  If not, see <http://www.gnu.org/licenses/>.

 */
trait ProjectsPaths {
  val githubURL = "https://github.com/"
//  val externalStorageRepositories = "E:/integrated-views/githubforkviews/repositories/"
//  val repodir = externalStorageRepositories
  val repodir = "evaluation/repositories/"
  val evaluationDir = "evaluation/"
  val projectsPath = "projects.txt"
  val forkscache = "forks.cache"
  val forksjson = "forks.json"
  val linefrequency = "line-frequency.txt"
}
