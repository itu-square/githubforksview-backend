package dk.itu.gfv.repo

/* Copyright (c) 2016-2017 Stefan Stanciulescu
   IT University of Copenhagen

    version 0.1

    This file is part of GithubForksView.
    GithubForksView is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GithubForksView is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GithubForksView.  If not, see <http://www.gnu.org/licenses/>.
    
 */


import java.io.{File, FileWriter}
import java.nio.charset.CodingErrorAction

import com.google.gson.{GsonBuilder, JsonParser => GJsonParser}
import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.model.Model._
import org.joda.time.DateTimeZone
import org.joda.time.format.{DateTimeFormat, ISODateTimeFormat}

import scala.collection.mutable.StringBuilder
import scala.io.{Codec, Source}
import scalaj.http.{Http, _}
import spray.json._
import DefaultJsonProtocol._
import org.eclipse.jgit.api.Git

import scala.util.{Failure, Success, Try}
import scala.util.parsing.json.JSONObject

object GithubExtractor extends LazyLogging {

  case class Response(result: String, status_code: Int, etag: String)

  // **** CODEC RELATED FIXES ****//
  implicit val codec = Codec("UTF-8")
  codec.onMalformedInput(CodingErrorAction.REPLACE)
  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
  val token_limit = 300
  val forks = "forks"
  val commits = "commits"
  val branches = "branches"
  val threads = 4
  val ETagHeader = "If-None-Match"
  val accessToken = "access_token"
  val api = "https://api.github.com/"
  val api_repo = "https://api.github.com/repos/"
  val api_users = "https://api.github.com/users/"
  val rate_limit = "rate_limit"
  val TIMEOUT = 60000
  val auth = Source.fromFile(new File("github.txt")).getLines.toList

  var token1 = auth(0).split(";")(0)
  var token2 = auth(1).split(";")(0)
  var token3 = auth(2).split(";")(0)
  var token4 = auth(3).split(";")(0)
  var token5 = auth(4).split(";")(0)

  var authentication = token1
  var currentToken = token1
  var totalNumberOfForks = 0
  var requests = 0
  val gson = new GsonBuilder().setPrettyPrinting().create

  tokens

  def getCurrentDirectory = System.getProperty("user.dir")

  /**
    * Returns a triple with the errorcode, the headers as a map, and the body
    *
    * @param url
    * @return
    */
  //  def request(url: String): (Int, Map[String, IndexedSeq[String]], String) = {
  //
  //    def call: HttpResponse[String] = Http(url).headers(("Content-Type","application/json")).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).asString
  //
  //    val requestCall: HttpResponse[String] = try {
  //      call
  //    } catch {
  //      case ex: java.net.UnknownHostException => println("Unknown host. Check either the internet connection or the domain"); sys.exit(1)
  //      case ex: java.net.SocketException => logger.error("socket exception " + ex.getMessage + "\n trying again"); call
  //    }
  //    requestCall.code match {
  //      case 200 => (requestCall.code, requestCall.headers, requestCall.body)
  //      case 301 => {
  //        val secondRequestCall: HttpResponse[String] = Http(url).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT), HttpOptions.followRedirects(true)).asString
  //        (secondRequestCall.code, secondRequestCall.headers, secondRequestCall.body)
  //      }
  //      case 400 => {
  //        logger.error("400 returned; bad request, existing");  sys.exit(0)
  //      }
  //      case 403 => {
  //        sleep
  //        println("Forbidden request")
  //        request(url)
  //        (requestCall.code, requestCall.headers, requestCall.body)
  //      }
  //      case 401 => {
  //        sleep
  //        request(url)
  //      }
  //      case 404 => logger.error("Requested blob file does not exist"); (128, Map(), "")
  //      case 408 => request(url)
  //      case 429 => println(url); request(url)
  //      case 500 => request(url)
  //      case 503 => request(url)
  //    }
  //  }

  def request(url: String, etag: Option[String]) = {
    requestWithEtag(url, etag.getOrElse(""))
  }

  def requestWithEtag(url: String, etag: String): Option[(Int, Map[String, IndexedSeq[String]], String)] = {

    val requestCall: HttpResponse[String] = try {
      Http(url).header("If-None-Match", etag).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).asString
    } catch {
      case ex: java.net.UnknownHostException => logger.error("Unknown host. Check either the internet connection or the domain"); sys.error("internet might be down?!?!?; unknown host exception")
      case ex: java.net.SocketException => Http(url).header("If-None-Match", etag).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).asString
    }
    requestCall.code match {
      case 200 => Some((requestCall.code, requestCall.headers, requestCall.body))
      case 301 => {
        logger.info("timeout on github request")
        val secondRequestCall: HttpResponse[String] = Http(url).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT), HttpOptions.followRedirects(true)).asString
        Some((secondRequestCall.code, secondRequestCall.headers, secondRequestCall.body))
      }
      case 304 => {
        Some((requestCall.code, requestCall.headers, requestCall.body))
      }
      case 403 => {
        sleep
        logger.error(s"Forbidden request for $url; retrying")
        requestWithEtag(url, etag)
        Some((requestCall.code, requestCall.headers, requestCall.body))
      }
      case 401 => {
        sleep
        requestWithEtag(url, etag)
      }
      case 404 => logger.error(s"Requested blob file does not exist; Tried $url with etag: $etag"); None
      case 408 => requestWithEtag(url, etag)
      case 429 => println(url); requestWithEtag(url, etag)
      case 500 => requestWithEtag(url, etag)
      case 503 => requestWithEtag(url, etag)
    }
  }

  def checkValidURL(url: String): Boolean = {
    url.contains("https://github.com/") && url.contains("blob/")
  }

  def transformURL(url: String): Option[GithubURL] = {
    checkValidURL(url) match {
      case true => {
        val s = url.split("/", 8)
        Some(GithubURL(s(3), s(4), s(6), s(7)))
      }
      case false => None
    }
  }

  def getRepositoryJson(repoName: String): Option[String] = {
    sleep
    val url = api + "repos/" + repoName + "?access_token=" + authentication
    //    println(reponame)
    request(url, None) match {
      case Some((statusCode, headers, body)) => Some(body)
      case None => None
    }
  }

  def getRepositoryJsonWithEtag(reponame: String, etag: String): Option[Response] = {
    sleep
    val url = api + "repos/" + reponame + "?access_token=" + authentication
    //    println(reponame)
    val response = request(url, Some(etag))
    response match {
      case Some((statusCode, headers, body)) => {
        val new_etag = headers.get("ETag").get.headOption.getOrElse("")
        Some(Response(body, statusCode, new_etag))
      }
      case None => None
    }

  }

  def getRepository(reponame: String): Option[GithubRepository] = {
    getRepositoryJson(reponame) match {
      case Some(x) => x.parseJson.convertTo[Option[GithubRepository]]
      case None => None
    }
  }

  //  def getAllLevelForks(reponame: String): Seq[GithubRepository] = {
  //    sleep
  //    println("Getting all forks of " + reponame)
  //    // checkBySameHash if the repository is a fork or not
  //    def getParentOfFork(repo: String): GithubRepository = {
  //      val jsonResponse = getRepositoryJson(repo)
  //      val requestedRepository = jsonResponse.parseJson.convertTo[Option[GithubRepository]] match {
  //        case Some(x) => getParentOfFork(x.parent.get.full_name)
  //        case None => jsonResponse.parseJson.convertTo[GithubRepository]
  //      }
  //      requestedRepository
  //    }
  //
  //    val parent = getParentOfFork(reponame)
  //    //    println("Parent is " + parent)
  //    val forksJSON = getJSONPagination(parent.full_name, forks).parseJson.convertTo[Option[Seq[GithubRepository]]]
  //    forksJSON match {
  //      case Some(f) => {
  //        var all = Seq[GithubRepository]()
  //        all = parent +: f
  //        val forksList = scala.collection.mutable.ListBuffer[String]()
  //        f.foreach(x => forksList += x.full_name)
  //        while (forksList.size != 0) {
  //          var forks = forksList
  //          forksList.clear
  //          forks.foreach(x => {
  //            val f = getJSONPagination(x, "forks").parseJson.convertTo[Option[Seq[GithubRepository]]].get
  //            all = all ++ f
  //            forksList ++= (f.map(x => x.full_name))
  //          })
  //        }
  //        all
  //      }
  //      case None => List()
  //    }
  //  }

  def getFirstLevelForks(reponame: String): Seq[GithubRepository] = {
    sleep
    getJSONPagination(reponame, forks, None).get.result.parseJson.convertTo[Seq[GithubRepository]]
  }

  def getProjectForks(reponame: String): Seq[GithubRepository] = {
    sleep
    logger.info("getting all forks of " + reponame)
    val forks_decoded = getJSONPagination(reponame, "forks", None).get.result.parseJson.convertTo[Option[Seq[GithubRepository]]].getOrElse(List())

    def get_all_forks(input_forks: Seq[GithubRepository], parent: String): Seq[GithubRepository] = {
      // keep all forks of first level
      val first_level_forks = input_forks
      // then filter only for forks that have nested forks
      val nested_first_level_forks = first_level_forks.filter(_.forks_count > 0)
      // we need to make for the first level forks, a parent and transform these into GithubRepository
      val first_level_forks_parent = first_level_forks.map(x => GithubRepository(x.id, x.full_name, x.owner, x.fork, x.created_at, x.updated_at, x.pushed_at, x.stargazers_count, x.language, x.forks_count, x.default_branch, Some(Parent(parent)), x.subscribers_count))

      // this will get the forks of nested forks
      def get_nested_forks(input: Seq[GithubRepository]): Seq[GithubRepository] = input match {
        case h :: t => {
          val forks = getJSONPagination(h.full_name, "forks", None).get.result.parseJson.convertTo[Option[Seq[GithubRepository]]].getOrElse(List())
          get_all_forks(forks, h.full_name) ++ get_nested_forks(t)
        }
        case List(h) => {
          val forks = getJSONPagination(h.full_name, "forks", None).get.result.parseJson.convertTo[Option[Seq[GithubRepository]]].getOrElse(List())
          get_all_forks(forks, h.full_name)
        }
        case List() => Nil
      }

      first_level_forks_parent ++ get_nested_forks(nested_first_level_forks)
    }

    get_all_forks(forks_decoded, reponame)
  }

  def getProjectForksAndEtags(reponame: String): (Seq[GithubRepository], Map[String, String]) = {
    sleep
    logger.info("getting all forks of " + reponame)
    val forks_json = getJSONPagination(reponame, "forks", None)
    val forks_decoded = forks_json.get.result.parseJson.convertTo[Option[Seq[GithubRepository]]].getOrElse(List())
    val repoForksEtags = collection.mutable.Map[String, String]()
    repoForksEtags.put(reponame, forks_json.get.etag)

    def get_all_forks(input_forks: Seq[GithubRepository], parent: String): Seq[GithubRepository] = {
      // keep all forks of first level
      val first_level_forks = input_forks
      // then filter only for forks that have nested forks
      val nested_first_level_forks = first_level_forks.filter(_.forks_count > 0)
      // we need to make for the first level forks, a parent and transform these into GithubRepository
      val first_level_forks_parent = first_level_forks.map(x => GithubRepository(x.id, x.full_name, x.owner, x.fork, x.created_at, x.updated_at, x.pushed_at, x.stargazers_count, x.language, x.forks_count, x.default_branch, Some(Parent(parent)), x.subscribers_count))

      // this will get the forks of nested forks
      def get_nested_forks(input: Seq[GithubRepository]): Seq[GithubRepository] = input match {
        case h :: t => {
          val forks_json = getJSONPagination(h.full_name, "forks", None)
          val forks = forks_json.get.result.parseJson.convertTo[Option[Seq[GithubRepository]]].getOrElse(List())
          repoForksEtags.put(h.full_name, forks_json.get.etag)
          get_all_forks(forks, h.full_name) ++ get_nested_forks(t)
        }
        case List(h) => {
          val forks_json = getJSONPagination(h.full_name, "forks", None)
          val forks = forks_json.get.result.parseJson.convertTo[Option[Seq[GithubRepository]]].getOrElse(List())
          repoForksEtags.put(h.full_name, forks_json.get.etag)
          get_all_forks(forks, h.full_name)
        }
        case List() => Nil
      }

      first_level_forks_parent ++ get_nested_forks(nested_first_level_forks)
    }

    (get_all_forks(forks_decoded, reponame), repoForksEtags.toMap)
  }


  //  def getProjectForks(reponame: String): Seq[GithubRepository] = {
  //    sleep
  //    println("Getting all forks of " + reponame)
  //    // checkBySameHash if the repository is a fork or not
  //    val forksJSON = getJSONPagination(reponame, forks).decodeOption[Seq[GithubRepository]].getOrElse(List())
  //    var all = Seq[GithubRepository]()
  //    all = forksJSON
  //    val forksList = scala.collection.mutable.ListBuffer[String]()
  //    forksJSON.foreach(x => forksList += x.full_name)
  //    while (forksList.nonEmpty) {
  //      val forks = forksList.clone
  //      forksList.clear
  //      forks.foreach(x => {
  //        val f = getJSONPagination(x, "forks").decodeOption[Seq[GithubRepository]].get
  //        all = all ++ f
  //        forksList ++= (f.map(x => x.full_name))
  //      })
  //    }
  //
  //    val allforks = all.map(_.full_name)
  //    allforks.map(x => getRepositoryJson(x).decodeOption[GithubRepository]).flatten
  //  }

  def pagination(url: String, etagParam: Option[String]): Option[Response] = {
    request(url, etagParam) match {
      case Some((statusCode, headersMap, resultString)) => {
        val etag = headersMap.get("ETag").get.head
        val link = headersMap.get("Link")
        link match {
          case None => Some(Response(resultString, statusCode, etag))
          case Some(x) => {
            sleep
            val lastPage = (link.get.last.split(",")(1).split("&page=")(1).split(">")(0)).toInt
            val json = prettyPrintJson(resultString)
            val stringBuilder = new StringBuilder
            stringBuilder.append(json)
            for (i <- 2 to lastPage) {
              sleep
              val x = url + "&page=" + i
              request(x, None) match {
                case Some((statusCode, headersMap, result)) => {
                  if (!result.contentEquals("[]")) {
                    val resultString1 = prettyPrintJson(result)
                    val lastBracketIndex = stringBuilder.lastIndexOf("]")
                    stringBuilder.deleteCharAt(lastBracketIndex)
                    stringBuilder.append(",")
                    val j = resultString1.drop(2)
                    stringBuilder.append(j)
                  }
                }
                case None => None
              }
            }
            // (stringBuilder.toString, etag)
            Some(Response(stringBuilder.toString, statusCode, etag))
          }
        }
      }
      case None => None
    }
  }


  //    def paginationWithEtag(url: String, etag1: String): Response = {
  //      sleep
  //      val (statusCode, headersMap, resultString) = requestWithEtag(url, etag1)
  //      val etag = headersMap.get("ETag").get.head
  //      val link = headersMap.get("Link")
  //      link match {
  //        case None => Response(resultString, statusCode, etag)
  //        case x => {
  //          sleep
  //          val lastPage = (link.get.last.split(",")(1).split("&page=")(1).split(">")(0)).toInt
  //          val stringBuilder = new StringBuilder
  //          stringBuilder.append(prettyPrintJson(resultString))
  //          for (i <- 2 to lastPage) {
  //            sleep
  //            val x = url + "&page=" + i
  //            val (statusCode, headersMap, result) = request(x)
  //            if (!result.contentEquals("[]")) {
  //              val resultString1 = prettyPrintJson(result)
  //              val lastBracketIndex = stringBuilder.lastIndexOf("]")
  //              stringBuilder.deleteCharAt(lastBracketIndex)
  //              stringBuilder.append(",")
  //              val j = resultString1.drop(2)
  //              stringBuilder.append(j)
  //            }
  //          }
  //          Response(stringBuilder.toString, statusCode, etag)
  //        }
  //      }
  //    }


  def getJSONPagination(reponame: String, cmd: String, etag: Option[String]) = {
    sleep
    val url = cmd match {
      case "forks" => api_repo + reponame + "/" + forks + "?access_token=" + authentication + "&per_page=100"
      case "branches" => api_repo + reponame + "/" + branches + "?access_token=" + authentication + "&per_page=100"
    }
    pagination(url, etag)
  }


  //    def getJSONPaginationWithEtag(reponame: String, cmd: String, etag: String) = {
  //      sleep
  //      val url = cmd match {
  //        case "forks" => api_repo + reponame + "/" + forks + "?access_token=" + authentication + "&per_page=100"
  //        case "branches" => api_repo + reponame + "/" + branches + "?access_token=" + authentication + "&per_page=100"
  //      }
  //      pagination(url, Some(etag))
  //    }

  def getCommits(reponame: String, branch: String): Option[Seq[CommitGithub]] = {
    getCommitsJSON(reponame, branch) match {
      case Some(x) => {
        try {
          x.result.parseJson.convertTo[Option[Seq[CommitGithub]]]
        }
        catch {
          case ex: Exception => x.result.parseJson.convertTo[Option[CommitGithub]] match {
            case Some(x) => Some(Seq(x))
            case None => None
          }
        }
      }
      case None => None
    }
  }

  def getCommitsJSON(reponame: String, branch: String): Option[Response] = {
    sleep
    val url = s"$api_repo$reponame/commits?sha=$branch&access_token=$authentication&per_page=100"
    pagination(url, None)
  }


  //  def getCommitsJSON(reponame: String, branch: String, commit_window: Int) = {
  //    sleep
  //    val dt = new org.joda.time.DateTime()
  //    val fmt = ISODateTimeFormat.dateTime();
  //    val window = fmt.print(dt.minusWeeks(commit_window))
  //    val url = api_repo + reponame + "/" + commits + "?since=" + window + "&access_token=" + authentication + "&per_page=100"
  //    pagination(url, None)
  //  }

  //
  private def getRepositoryBranchesJSON(reponame: String): Option[Response] = {
    sleep
    pagination(api_repo + reponame + "/" + branches + "?access_token=" + authentication + "&per_page=100", None)
  }

  def getRepositoryBranches(reponame: String): Option[Seq[Branch]] = {
    getRepositoryBranchesJSON(reponame) match {
      case Some(x) => x.result.parseJson.convertTo[Option[Seq[Branch]]]
      case None => None
    }
  }


  //  def mostRecentUpdatedRepositories(repositories: Seq[GithubRepository]): Seq[GithubRepository] = {
  //    repositories.sortBy(x => x.pushed_at)
  //  }

  def getPatchAtCommit(reponame: String, commit: String) = {
    val req = request("https://github.com/" + reponame + "/commit/" + commit + ".patch", None)
    req.get._3
  }

  //
  def getCommitTimeFromGithubPatch(date: String) = org.joda.time.DateTime.parse(date, DateTimeFormat.forPattern("EEE, d MMM yyyy HH:mm:ss Z")).getMillis() / 1000

  def getDiffAtCommit(reponame: String, commit: String) = {
    val link = "https://github.com/" + reponame + "/commit/" + commit + ".diff"
    val req = request(link, None)
    req.get._3
  }

  def getBlobFile(reponame: String, branch: String, file: String) = {
    val url1 = "https://raw.githubusercontent.com/" + reponame + "/" + branch + "/" + file
    val r = request(url1, None)
    r.get._3
  }

  def prettyPrintJson(json: String): String = {
    try {
      val jsonParser = new GJsonParser
      gson.toJson(jsonParser.parse(json))
      //      json
    }
    catch {
      // some retrieval of patches give a normal string, which cannot be parsed...
      case e: Exception => return json
    }

  }

  def getCache(cmd: String, dir: String): String = {
    return Source.fromFile(new File(dir + "/cache/" + cmd + ".cache")).getLines.mkString
  }

  def getBranchCache(cmd: String, dir: String, branch: String): String = {
    var branchFile = branch
    if (branch.contains("/")) {
      branchFile = branch.replaceAll("/", "-")
    }
    return Source.fromFile(new File(dir + "/cache/" + branchFile + "-commits.cache")).getLines.mkString
  }

  def getUserJSON(reponame: String): Option[String] = {
    sleep
    val url = api_users + reponame.split("/")(0) + "?access_token=" + authentication
    val response = request(url, None)
    response match {
      case Some((statusCode, headers, body)) => Some(body)
      case None => None
    }
  }

  def getUserFromJSON(json: Option[String]): Option[User] = {
    sleep
    json match {
      case Some(x) => x.parseJson.convertTo[Option[User]]
      case None => None
    }
  }

  def getUser(reponame: String): Option[User] = {
    sleep
    getUserFromJSON(getUserJSON(reponame))
  }

  private[this] val headers: Map[String, String] =
    Map("User-Agent" -> "GitHubScala/1.0.0", "Accept" -> "application/vnd.github.v3.full+json")

  private[this] def generateCredentials(token: String): String = {
    if (token != null && token.length > 0) "token " + token else null
  }

  private[this] def generateHeaders(token: String): Map[String, String] = {
    headers ++ Map("Authorization" -> generateCredentials(token))
  }

  private[this] def getData(path: String, headers: Map[String, String]) = {
    Http(api + path).method("GET").headers(headers).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).asString
  }


  private[this] def postData(path: String, headers: Map[String, String], data: Map[String, Any]) = {
//    logger.info(s"requesting post to " + api + path)
    Http(api + path).method("POST").headers(headers).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).postData(JSONObject(data).toString()).asString
  }

  private[this] def deleteData(path: String, headers: Map[String, String]) = {
    Http(api_repo + path).method("DELETE").headers(headers).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).asString
  }

  /**
    * Create a repository on the token's respective owner
    *
    * @param authToken   personal github token for that user
    * @param name        repository name
    * @param description repository description
    * @param isPrivate   set if the repository is public or not
    * @param hasIssues   allow for issues
    * @return a GithubRepository object if it is successful or None otherwise
    */

  def createGithubRepository(authToken: String, name: String, description: String, isPrivate: Boolean, hasIssues: Boolean): Option[GithubRepository] = {
    val headers = generateHeaders(authToken) ++ Map("Content-Type" -> "application/json")
    val response = postData("user/repos", headers,
      Map(
        "name" -> name,
        "description" -> description,
        "private" -> isPrivate,
        "has_issues" -> hasIssues,
        "has_wiki" -> true,
        "auto_init" -> true
      )
    )
    response.code match {
      //      case 200 => response.body.parseJson.convertTo[Option[GithubRepository]]
      case 201 => response.body.parseJson.convertTo[Option[GithubRepository]]
      case 422 => None
      case _ => sys.error(response.body)
    }
  }

  case class RepositoryDoesNotExistException(msg: String) extends Throwable

  /**
    * Delete a github repository by using the auth token, the owner of that repository and the repository's name.
    * Returns Success is the repository is sucessfully deleted, and a failure otherwise.
    *
    * @param authToken
    * @param owner
    * @param reponame
    * @return
    */

  //TODO this should be refactored to give more meaningful error messages. For example, if a repository does not exist, or simply there is no token access...
  def deleteGithubRepository(authToken: String, owner: String, reponame: String): Try[String] = {
    val response = deleteData(s"$owner/$reponame", generateHeaders(authToken))
    response.code match {
      case 204 => Success(s"Repository $owner/$reponame deleted successfully")
      case 404 => Failure(RepositoryDoesNotExistException("Invalid link. Repository probably does not exist"))
      case _ => Failure(new RuntimeException(response.throwError.body))
    }
  }

  /**
    * Fork a repository to the current authenticated user (defined by AuthToken). It requires that the token allows
    * for creating new repositories on Github.
    *
    * @param authToken
    * @param owner
    * @param repositoryToFork
    * @return Success if 202 is returned from Github's API server or a Failure if it already exists or some other error occurred
    */

  def forkGithubRepository(authToken: String, owner: String, repositoryToFork: String) = {
    val response = postData(s"repos/$owner/$repositoryToFork/forks", generateHeaders(authToken), Map())
    response.code match {
      case 202 => Success(response.body.parseJson.convertTo[Option[GithubRepository]])
      case 404 => Failure(RepositoryDoesNotExistException("Invalid link. Repository probably does not exist"))
      case _ => Failure(new RuntimeException(response.throwError.body))
    }
  }

  def createCommit(git: Git, auth: String) = {

  }


  //  def wasModified(reponame: String, cmd: String, cache: String, branch: String): (Boolean) = {
  //    var reponame = api_repo + reponame
  //    cmd match {
  //      case "branches" => reponame = api_repo + reponame + "/branches?access_token=" + authentication + "&per_page=100"
  //      case "commits"  => reponame = api_repo + reponame + "/commits?access_token=" + authentication + "&per_page=100&sha=" + branch
  //      case "user"     => reponame = "https://api.github.com/users/" + reponame.split("/")(0) + "?access_token=" + authentication
  //      case "repo"     => reponame = api_repo + reponame + "?access_token=" + authentication
  //      case "issues"   => reponame = api_repo + reponame + "/issues?access_token=" + authentication + "&per_page=100&state=all"
  //      case "pulls" =>
  //        reponame = api_repo + reponame + "/pulls?access_token=" + authentication + "&per_page=100&state=all"; println()
  //      case "comments" => reponame = api_repo + reponame + "/issues/comments?access_token=" + authentication + "&per_page=100"
  //      case "forks"    => reponame = api_repo + reponame + "/forks?access_token=" + authentication + "&per_page=100"
  //      case _          => reponame = api_repo + reponame + "/" + cmd
  //    }
  //    sleep
  //
  //    val request = Http(reponame).header(ETagHeader, cache.trim).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).asString
  //    //val request = Http(reponame).header(ETagHeader, cache.trim).options(HttpOptions.connTimeout(TIMEOUT),
  //    //  HttpOptions.readTimeout(TIMEOUT)).asCodeHeaders
  //    //val etag = request._2.get("ETag").get(0)
  //    val modified = request.headers.get("Status").get.split(" ")(1)
  //    if (Integer.parseInt(modified) == 304)
  //      return (false)
  //    else return (true)
  //
  //  }


  def tokens = {
    if (token2.equals("token"))
      token2 = token1
    if (token3.equals("token"))
      token3 = token1
    if (token4.equals("token"))
      token4 = token1
    if (token5.equals("token"))
      token5 = token1
  }

  def getLimitRate(token: String): Option[String] = {

    val url = "https://api.github.com/rate_limit?access_token=" + token

    request(url, None) match {
      case Some((statusCode, headers, response)) => Some(prettyPrintJson(response))
      case None => None
    }
  }

  // sleep if reached limit for both credentials. otherwise, do not sleep and switch credentials

  def sleep: Any = {

    currentToken = authentication
    val currentLimitRate = getLimitRate(currentToken)
    currentLimitRate match {
      case Some(limit) => {
        if (limit.parseJson.convertTo[Resources].resources.core.remaining < token_limit) {

          val auth1LimitJson = getLimitRate(token1).get.parseJson.convertTo[Resources]
          val auth2LimitJson = getLimitRate(token2).get.parseJson.convertTo[Resources]
          val auth3LimitJson = getLimitRate(token3).get.parseJson.convertTo[Resources]
          val auth4LimitJson = getLimitRate(token4).get.parseJson.convertTo[Resources]
          val auth5LimitJson = getLimitRate(token5).get.parseJson.convertTo[Resources]

          val auth1LimitRemaining = auth1LimitJson.resources.core.remaining
          val auth2LimitRemaining = auth2LimitJson.resources.core.remaining
          val auth3LimitRemaining = auth3LimitJson.resources.core.remaining
          val auth4LimitRemaining = auth4LimitJson.resources.core.remaining
          val auth5LimitRemaining = auth5LimitJson.resources.core.remaining

          val auth1Reset = auth1LimitJson.resources.core.reset
          val auth2Reset = auth2LimitJson.resources.core.reset
          val auth3Reset = auth3LimitJson.resources.core.reset
          val auth4Reset = auth4LimitJson.resources.core.reset
          val auth5Reset = auth5LimitJson.resources.core.reset

          //val mapReset = Map[String, Long](token1 -> auth1Reset, token2 -> auth2Reset, token3 -> auth3Reset, token4 -> auth4Reset)
          val mapReset = Map[Long, String](auth1Reset -> token1, auth2Reset -> token2, auth3Reset -> token3, auth4Reset -> token4, auth5Reset -> token5)
          //val mapLimit = Map[String, Int](token1 -> auth1LimitRemaining, token2 -> auth2LimitRemaining, token3 -> auth3LimitRemaining, token4 -> auth4LimitRemaining)
          val mapLimit = Map[Int, String](auth1LimitRemaining -> token1, auth2LimitRemaining -> token2, auth3LimitRemaining -> token3, auth4LimitRemaining -> token4, auth5LimitRemaining -> token5)

          //val maxRemainingQueries = max(mapLimit.keys.toList).get
          val maxRemainingQueries = mapLimit.keys.toList.max
          println(maxRemainingQueries + "--" + mapLimit.get(maxRemainingQueries).get)
          if (maxRemainingQueries > token_limit) {
            authentication = mapLimit.get(maxRemainingQueries).get
            logger.info("Switching authentication to " + authentication)

          } else {
            // find the minimal time to wait for reset of queries
            val minRemainingWaitTime = mapReset.keys.toList.min
            authentication = mapReset.get(minRemainingWaitTime).get
            logger.info("Switching authentication to " + authentication)

            // sleep
            val sleepTime = getRemainingSeconds(minRemainingWaitTime) + 60
            logger.info("Reached limit. Sleeping for " + sleepTime + " seconds")
            if (sleepTime < 0) {
              logger.error("Something is off, probably the systems time clock. Waiting for 10 min")
              Thread.sleep(600000L)
            }
            else {
              Thread.sleep((sleepTime) * 1000)
            }
            sleep // make sure everything is fine
          }
        }
      }
      case None => None
    }
  }

  def getRemainingSeconds(value: Long): Long = {
    val dt = org.joda.time.DateTime.now(DateTimeZone.UTC)
    val to_wait = value - (dt.getMillis / 1000L)
    to_wait
  }

  def minValue(l: Long, r: Long): (Long) = {
    if (r >= l) l else r
  }
}