package dk.itu.gfv.repo

/* Copyright (c) 2016-2017 Stefan Stanciulescu
   IT University of Copenhagen

    version 0.1

    This file is part of GithubForksView.
    GithubForksView is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GithubForksView is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GithubForksView.  If not, see <http://www.gnu.org/licenses/>.

 */

import net.fosd.vgit.LineTable
import org.eclipse.jgit.api._
import org.eclipse.jgit.diff.DiffEntry.ChangeType
import org.eclipse.jgit.diff.DiffEntry.Side._
import org.eclipse.jgit.diff._
import org.eclipse.jgit.lib._
import org.eclipse.jgit.revwalk.{RevCommit, RevWalk}
import org.eclipse.jgit.treewalk.filter.PathFilter
import org.eclipse.jgit.treewalk.{TreeWalk, CanonicalTreeParser, AbstractTreeIterator}

import collection.JavaConverters._

/**
  * Created by scas on 08-07-2016.
  */
object AnalyzeFile extends App {

  /**
    * Return all ref objects of remotes
    *
    * @param repository
    * @return
    */
  def getRemotes(repository: Repository): Map[String, Ref] = {
    repository.getRefDatabase.getRefs(Constants.R_REMOTES).asScala.toMap
  }

  /**
    * Get HEAD commit of given branch
    *
    * @param repository
    * @param branch
    * @param ref
    * @return RevCommit object
    */
  def getBranchCommit(repository: Repository, branch: String, ref: Map[String, Ref]): RevCommit = {
    ref.get(branch) match {
      case Some(x) => new RevWalk(repository).parseCommit(x.getObjectId)
      case None => sys.error("Cannot find head of branch " + branch)
    }
  }

  /**
    * Get file content from a commit
    *
    * @param repository
    * @param commit
    * @param file
    * @return RawText object
    */
  def getFileContent(repository: Repository, commit: RevCommit, file: String): RawText = {
    val tree = commit.getTree()
    val treeWalk = new TreeWalk(repository)
    treeWalk.addTree(tree)
    treeWalk.setRecursive(true)
    treeWalk.setFilter(PathFilter.create(file))
    if (!treeWalk.next()) {
      return new RawText(new Array[Byte](0))
    }
    val objectId = treeWalk.getObjectId(0)
    val loader = repository.open(objectId)
    new RawText(loader.getBytes)
  }

  /**
    * Get commits that modified a file
    *
    * @param repository
    * @param branch
    * @param file
    * @return
    */
  def getCommitsThatModifiedFile(repository: Repository, branch: String, file: String): List[RevCommit] = {
    val git = new Git(repository)
      git.log.add(repository.resolve(branch)) // add branch
        .addPath(file).call().asScala.toList   // file to look for
  }

  /**
    * Find the object id of a file
    *
    * @param git
    * @param branch
    * @param file
    * @return
    */

  def fileObjectID(git: Git, branch: String, file: String): ObjectId = {
    val repository = git.getRepository
    val lastCommit = repository.exactRef("refs/remotes/" + branch).getObjectId
    val revWalk = new RevWalk(repository)
    val commit = revWalk.parseCommit(lastCommit)
    val tree = commit.getTree()
    val treeWalk = new TreeWalk(repository)
    treeWalk.addTree(tree)
    treeWalk.setRecursive(true)
    treeWalk.setFilter(PathFilter.create(file))
    if (!treeWalk.next()) {
      //      return new RawText(new Array[Byte](0))
    }
    val objectId = treeWalk.getObjectId(0)
    objectId
  }

  def getForksBranches(git: Git, forkname: String) = {
    git.getRepository.getAllRefs.asScala.toList.filter(x => x._1.startsWith("refs/remotes/"+forkname+"/")).toMap
  }

  def getBranchThatModifiedFileMostRecently(git: Git, branches: Map[String, Ref], filename: String) = {
    case class R(commitTime: Int, commit: RevCommit, branch: String)
    val commits =
    for {
      b <- branches.keySet
      c = getCommitsThatModifiedFile(git.getRepository,b,filename).head
    } yield R(c.getCommitTime, c, b)
    commits.toSeq.sortBy(_.commitTime).reverse.head.branch
  }

  /**
    * Generate a table representing the line and at which revision it appeared
    *
    * @param gitProject
    * @param file
    * @return
    */

  // Copied and adapted from https://github.com/ckaestne/vgit/commit/f927fe45ea7020ff1dd82d73073175ed8ac0e000 on July 8th 2016
  def genLineTable(gitProject: Git, file: String, branch: String, origRepo: String): LineTable = {

    val repository = gitProject.getRepository
    val walk = new RevWalk(repository)
    val diffAlg = DiffAlgorithm.getAlgorithm(DiffAlgorithm.SupportedAlgorithm.HISTOGRAM)
    var comparator: RawTextComparator = RawTextComparator.WS_IGNORE_ALL
    val EMPTY = Array[Byte]()
    val reader = repository.newObjectReader
    val cs: ContentSource = ContentSource.create(reader)

    var lineTable = new LineTable()
    var counter = 0
    processCommits
    repository.close();

    def processCommits: Unit = {
      val remotes = getRemotes(repository)

      val parentCommit = getBranchCommit(repository, "origin/" + branch, remotes)
      val commits = collection.mutable.MutableList[(String, RevCommit)]()
      val commitsInOrigRepoModifiedFile = getCommitsThatModifiedFile(repository, "origin/" + branch, file)

//      remotes.keySet.filterNot(x => x.trim.startsWith("origin/")).foreach(x => {
//        println("Filtering the repository for diff commits of this file from forks")
//        val repoCommitsThatModifiedFile = getCommitsThatModifiedFile(repository, x,file)
//        val newCommits = repoCommitsThatModifiedFile.diff(commitsInOrigRepoModifiedFile)
//        if(newCommits.size > 0){
//          commits += ((x, getBranchCommit(repository, x, remotes)))
//        }
//      })
      remotes.keySet.filterNot(x => x.trim.startsWith("origin/")).foreach(x => {
        commits += ((x, getBranchCommit(repository, x, remotes))) // user/reponame/branch, commit
      })

      val initialFileContent = getFileContent(repository, parentCommit, file)
      lineTable.init(origRepo, initialFileContent)
      def analyzecommits(commitsInput: collection.mutable.MutableList[(String, RevCommit)]) = {
        var input = commitsInput
        while (input.size > 0) {
          val commit = input.head
          val commitSplit = commit._1.split("/")
          val current = prepareTreeParser(commit._2)
          val parent = prepareTreeParser(parentCommit)
          val diff = new Git(repository).diff().setOldTree(parent).setNewTree(current).setPathFilter(PathFilter.create(file)).call().asScala.toList
          assert(diff.size <= 1)
          for (entry <- diff) {
            counter += 1
            println("processing %d %s".format(counter, commit._1 + " at " + commit._2.getName.substring(0, 7)))
            if (entry.getChangeType == ChangeType.DELETE) {
              // the file is deleted, so we cannot do a comparison
              val aRaw: Array[Byte] = open(OLD, entry)
              val bRaw: Array[Byte] = open(OLD, entry)
              val a = new RawText(aRaw)
              val b = new RawText(bRaw)

              val (diffEntry, newText) = (diffAlg.diff(comparator, a, b), b)
              //lineTable.update(commitSplit(0) + "/" + commitSplit(1) + "/commit/" + commitSplit(2) + "?sha=" + commit._2.getName.substring(0,7), origRepo, new EditList(), new RawText(open(OLD, entry)))
              lineTable.update(commitSplit(0) + "/" + commitSplit(1) + "/commit/" + commitSplit(2) + "?sha=" + commit._2.getName.substring(0, 7), origRepo, diffEntry, newText)
            } else {
              val (diffEntry, newText) = diffText(entry)
              lineTable.update(commitSplit(0) + "/" + commitSplit(1) + "/commit/" + commitSplit(2) + "?sha=" + commit._2.getName.substring(0, 7), origRepo, diffEntry, newText)
            }
          }
          if (diff.isEmpty) {
            lineTable.update(commitSplit(0) + "/" + commitSplit(1) + "/commit/" + commitSplit(2) + "?sha=" + commit._2.getName.substring(0, 7), origRepo, new EditList(), new RawText(new Array[Byte](0)))
          }
          input = input.tail
        }

      }
      analyzecommits(commits)
    }


    def diffText(entry: DiffEntry): (EditList, RawText) = {
      val aRaw: Array[Byte] = open(OLD, entry)
      val bRaw: Array[Byte] = open(NEW, entry)
      val a = new RawText(aRaw)
      val b = new RawText(bRaw)
      (diffAlg.diff(comparator, a, b), b)
    }

    def open(side: DiffEntry.Side, entry: DiffEntry): Array[Byte] = {
      val id = entry.getId(side)
      assert(id.isComplete)
      open_(entry.getPath(side), id.toObjectId)
    }

    def open_(path: String, id: ObjectId): Array[Byte] = {
//      printlnprintln(path)
      val loader = cs.open(path, id)
      loader.getBytes
    }

    // from the commit we can build the tree which allows us to construct the TreeParser
    def prepareTreeParser(commit: RevCommit): AbstractTreeIterator = {

      val tree = walk.parseTree(commit.getTree().getId())
      val oldTreeParser = new CanonicalTreeParser()
      val oldReader = repository.newObjectReader()
      try {
        oldTreeParser.reset(oldReader, tree.getId())
      } finally {
        oldReader.close()
      }
      return oldTreeParser;
    }
//    println(lineTable)
    lineTable
  }

}
