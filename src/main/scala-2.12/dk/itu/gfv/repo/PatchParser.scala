package dk.itu.gfv.repo

/**
  * Created by scas on 17-08-2016.
  */

import java.util.regex.Pattern

import org.parboiled2.Parser.DeliveryScheme.Try
import org.parboiled2._

import scala.util.{Failure, Success}

/**
  * Created by scas on 20-10-2015.
  * Converts a patch into multi patches or into a
  */
object PatchParser {

  sealed abstract class DiffType(filename: String)
  case class DiffFile(filename: String, patch: String) extends DiffType(filename)
  case class BinaryFile(filename: String) extends DiffType(filename)
  case class Hunk(patch: String)

  class PatchParser(val input: ParserInput) extends Parser {

    def sep = rule { "\n" | "\r\n" }

    def startRule = rule { inputLine ~ EOI }
    def inputLine = rule { fileDiff }
    def fileDiff: Rule1[Seq[DiffType]] = rule {zeroOrMore( binaryFile| line | gitDiff | noPatch )}
    def line: Rule1[DiffType] = rule {diff ~ sep ~ optional("new file mode " ~ chars ~ sep) ~  index ~ sep ~ optional(patchFile) ~> ((name: String, x: Option[String]) => { DiffFile(name,x.getOrElse(""))}) }
    def patchFile = rule {capture(zeroOrMore(!("diff --git") ~ ANY))}
    def index = rule {chars}
    def diff = rule {("diff --git a/" ~ capture(zeroOrMore(CharPredicate.Visible | generalPunctuation)) ~ chars)}
    def gitDiff: Rule1[DiffType] = rule {capture(("diff --cc") ~ zeroOrMore(!("diff --cc" | "diff --git a/" ) ~ ANY)) ~> ((x: String) => BinaryFile("git-diff from merge; ignore"))}
    def commit = rule {chars}
    def chars = rule {zeroOrMore(CharPredicate.Visible | " " | generalPunctuation)}
    def noPatch = rule {diff ~ sep ~ optional("new file mode " ~ chars ~ sep) ~ zeroOrMore(!("diff --git") ~ ANY) ~> ((x: String) => BinaryFile("")) }
    def binaryFile = rule {diff ~ sep ~ optional("new file mode " ~ chars ~ sep) ~ index ~ sep ~ "Binary files" ~ zeroOrMore(!("diff --git") ~ ANY) ~> ((name: String) => {BinaryFile(name)})}
    def generalPunctuation = CharPredicate('\u2010' to '\u205F')
  }

  class HunksParser(val input: ParserInput) extends Parser{
    var headerText = ""
    def sep = rule {"\n" | "\r\n"}
    def startRule = rule { inputLine ~ EOI }
    def inputLine: Rule1[Seq[Hunk]] = rule { header ~ sep ~ fileDiff }
    def fileDiff: Rule1[Seq[Hunk]] = rule {zeroOrMore(line)}
    def line = rule {diff ~ sep  ~ patchFile ~> ((x,y) => Hunk(headerText + "\n" + x.trim + "\n" + y)) }
    def patchFile: Rule1[String] = rule {capture(zeroOrMore(!("@@" ~ optional("@") ~ " -") ~ ANY))}
    def diff: Rule1[String] = rule {capture("@@" ~ optional("@") ~ " -" ~ chars)}
    def header = rule {optional("index " ~ chars ~ sep) ~ capture( chars ~ sep  ~ chars) ~> (x => headerText = x)}
    def chars = rule {zeroOrMore(CharPredicate.Visible | " ")}

//    def isStartOfLine = lastChar.equals('\n') && cursorChar.equals('@') && charAt(cursor + 1).equals('@')

    }

  /**
    * Splits a patch into several patches, one for each file that is in the original patch
    *
    * @param src
    */
  def splitPatchByFiles(src: String): Option[Seq[DiffType]] = {
    new PatchParser(src).startRule.run() match {
      case Success(e) => Some(e)
//      case Failure(e: ParseError) => sys.error(e.format("", new ErrorFormatter(showTraces = true)))
      case Failure(e) => None //sys.error(e.getMessage)
    }
  }

  /**
    * Split one patch into its hunks. It requires that the patch is applied to a single file.
    *
    * @param src
    */
  def splitPatchByHunks(src: String): Option[Seq[Hunk]] = {
    new HunksParser(src).startRule.run() match {
      case Success(e) => Some(e)
//      case Failure(e: ParseError) =>  Right(Seq(Hunk("--- /dev/null\n+++ /dev/null\n@@ -0,0 +0,0 @@ \nmalformed hunk no lines\nmalformedhunk\n"))) // sys.error(e.format("", new ErrorFormatter(showTraces = true)))
      case Failure(e: ParseError) => None//{println(e.format("", new ErrorFormatter(showTraces = true))); None} //sys.error(e.getMessage)
      case Failure(e: Exception) => None
    }
  }

//  import com.roundeights.hasher.Implicits._
//  import scala.language.postfixOps
//
//  case class Hash(lineNumber: Int, hash: String)
//  val whitespaces = Pattern.compile("""^\s*|\r?\n$""").matcher("")
//  def hashLine(line: Int, input: String): Hash = Hash(line, input.sha1)
//
//  def hashesFromPatch(inputPatch: DiffFile): List[Hash] = {
//    val splittedPatch = inputPatch.patch.split("\n").toList
//    for (x <- splittedPatch.drop(7) if (x.startsWith("+") && !whitespaces.reset(x).matches)) yield {hashLine(0,x.substring(1).replace("\r?\n", ""))}
//  }

}
