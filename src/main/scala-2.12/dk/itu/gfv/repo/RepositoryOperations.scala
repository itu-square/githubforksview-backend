package dk.itu.gfv.repo

/* Copyright (c) 2016-2017 Stefan Stanciulescu
   IT University of Copenhagen

    version 0.1

    This file is part of GithubForksView.
    GithubForksView is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GithubForksView is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GithubForksView.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.{ByteArrayOutputStream, File, FileInputStream, FileWriter}
import java.util
import java.util.Properties
import java.util.regex.{Matcher, Pattern}

import dk.itu.gfv.evaluation.setup.{RegexPatternsNormalization, RemoveStopWords}
import dk.itu.gfv.model.Model.GithubRepository
import dk.itu.gfv.redundant.model.LineChangeModel
import dk.itu.gfv.repo.PatchParser.{BinaryFile, DiffFile}
import dk.itu.gfv.util.{MonitorProperties, ProjectsPaths}
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.{DiffFormatter, RawText, RawTextComparator}
import org.eclipse.jgit.lib._
import org.eclipse.jgit.revwalk.filter.RevFilter
import org.eclipse.jgit.revwalk.{RevCommit, RevWalk}
import org.eclipse.jgit.transport.{URIish, UsernamePasswordCredentialsProvider}
import dk.itu.gfv.util.Util._
import com.typesafe.scalalogging.LazyLogging
import dk.itu.gfv.monitor.db.Models.AddedLine
import org.eclipse.jgit.api.ListBranchCommand.ListMode
import org.eclipse.jgit.treewalk.{CanonicalTreeParser, TreeWalk}
import org.eclipse.jgit.treewalk.filter.PathFilter
import org.joda.time.DateTime

import scala.collection.JavaConverters._
import scala.annotation.tailrec
import scala.util.Random
import spray.json.DefaultJsonProtocol._
import spray.json._

/**
  * Created by scas on 09-07-2016.
  */
object RepositoryOperations extends ProjectsPaths with RegexPatternsNormalization with LazyLogging with MonitorProperties {

  private final val patchSize = properties.getProperty("patchSize").toInt

  def changeHEAD(repository: Git, sha: String) = {
    repository.checkout.setStartPoint(sha).call
  }

  /**
    * Return all ref objects of remotes
    *
    * @param repository
    * @return
    */
  def getRemotes(repository: Repository): Map[String, Ref] = {
    repository.getRefDatabase.getRefs(Constants.R_REMOTES).asScala.toMap
  }

  def getBranches(git: Git): Seq[Ref] = git.branchList().setListMode(ListMode.ALL).call.asScala.toSeq

  /**
    * Returns a set of strings that contains the names of the forks of the project and includes the branches. This also contains the origin
    * E.g., user/repository/branch
    *
    * @param git
    * @return
    */
  def getForksAndBranches(git: Git): Set[String] = getRemotes(git.getRepository).keySet


  /**
    * Returns a set of strings containing the names of the forks. This also contains the origin
    * E.g., user/repository
    *
    * @param git
    * @return
    */
  def getProjectForks(git: Git): Set[String] = getRemotes(git.getRepository).keySet.map(_.replaceFirst("refs/", "")).map(x => {
    val temp = x.split("/")
    temp(0) + "/" + temp(1)
  })

  /**
    * Get file content from a commit
    *
    * @param repository
    * @param commit
    * @param file
    * @return RawText object
    */
  def getFileContent(repository: Repository, commit: RevCommit, file: String): RawText = {
    val tree = commit.getTree
    val treeWalk = new TreeWalk(repository)
    treeWalk.addTree(tree)
    treeWalk.setRecursive(true)
    treeWalk.setFilter(PathFilter.create(file))
    if (!treeWalk.next) {
      return new RawText(new Array[Byte](0))
    }
    val objectId = treeWalk.getObjectId(0)
    val loader = repository.open(objectId)
    new RawText(loader.getBytes)
  }

  /**
    * The two parameters represent the two dates for creating the fork and respectively, when was the
    * last commit pushed. It returns true if the fork has pushed changes after it was forked.
    *
    * @param createdAt
    * @param pushedAt
    */

  def isActiveFork(createdAt: String, pushedAt: String): Boolean = {
    val createdAtDateTime = org.joda.time.DateTime.parse(createdAt)
    val pushedAtDateTime = org.joda.time.DateTime.parse(pushedAt)
    pushedAtDateTime.isAfter(createdAtDateTime)
  }

  /**
    * The two parameters represent the two dates for creating the fork and respectively, when was the
    * last commit pushed. It returns true if the fork has pushed changes after it was forked.
    *
    * @param createdAt
    * @param pushedAt
    */

  def isActiveFork(createdAt: DateTime, pushedAt: DateTime): Boolean =
    pushedAt.isAfter(createdAt)

  /**
    * Return all the commits existing in a repository
    *
    * @param gitrepo
    */
  def getAllCommits(gitrepo: Git): Seq[RevCommit] = {
    gitrepo.log.all.call.asScala.toSeq
  }

  /**
    * Get HEAD commit of given branch
    *
    * @param repository
    * @param branch
    * @param ref
    * @return RevCommit object
    */
  def getBranchCommit(repository: Repository, branch: String, ref: Map[String, Ref]): RevCommit = {
    ref.get(branch) match {
      case Some(x) => new RevWalk(repository).parseCommit(x.getObjectId)
      case None => sys.error("Cannot find head of branch " + branch)
    }
  }

  /**
    *
    * @param gitrepo
    * @param remoteName
    * @param untilSHA
    * @return
    */
  def getCommitsFromRemoteBeforeCommit(gitrepo: Git, remoteName: String, untilSHA: String): List[RevCommit] = {
    val firstCommit = new RevWalk(gitrepo.getRepository).asScala.toList.last
    val until = gitrepo.getRepository.resolve(untilSHA)
    val remote = getRemotes(gitrepo.getRepository).get(remoteName).getOrElse(sys.error("The requested remote does not exist -> " + remoteName))
    gitrepo.log.add(remote.getObjectId).addRange(firstCommit, until).call.asScala.toList
  }

  def getCommitsFromRemote(gitrepo: Git, remoteName: String): List[RevCommit] = {
    val remote = getRemotes(gitrepo.getRepository).get(remoteName).getOrElse(sys.error("The requested remote does not exist -> " + remoteName))
    gitrepo.log.add(remote.getObjectId).call.asScala.toList
  }

  def getCommitsFromRemotesBranch(gitrepo: Git, branch: String): List[RevCommit] = {
    try {
      val b = gitrepo.getRepository.resolve(branch)
      val walk = new RevWalk(gitrepo.getRepository)
      val c = walk.parseCommit(b)
      walk.setRevFilter(RevFilter.NO_MERGES)
      walk.markStart(c)
      walk.iterator.asScala.toList
    }
    catch {
      case ex: Exception => System.out.println("Cannot get non-merge commits from branches"); List()
    }
  }

  /**
    * Get commits that modified a file
    *
    * @param repository
    * @param branch
    * @param file
    * @return
    */
  def getCommitsThatModifiedFile(repository: Repository, branch: String, file: String): List[RevCommit] = {
    val git = new Git(repository)
    git.log.add(repository.resolve(branch)) // add branch
      .addPath(file).call.asScala.toList // file to look for
  }

  def getPatch(git: Git, sha: String): Option[String] = {
    val gitfolder = git.getRepository.getDirectory.getPath
    val result = run("""git show --format="" --ignore-all-space """ + sha, gitfolder)
    result._3 match {
      case 0 => Some(result._1.mkString("\n"))
      case _ => None
    }
  }

  /**
    * Given a repository, it returns a number of random commits (sha) that are not merge commits
    *
    * @param git
    * @param size
    * @return list of commits identified by the first 7 characters in the SHA identifier
    */

  def randomCommitsBySHA(git: Git, size: Int) = {
    val commits = git.log.all.call.asScala.toList
    val commitsAndParentsMap = commits.map(x => x.getName.take(7) -> x.getParents.toList.map(_.getName.take(7))).toMap
    val noMergeCommits = commitsAndParentsMap.filter(x => x._2.size < 2).map(_._1).toVector
    val result = collection.mutable.HashMap[String, String]()
    var i = 0
    while (i < size) {
      val c = noMergeCommits(Random.nextInt(noMergeCommits.size))
      result.get(c) match {
        case None => {
          // did not find this commit, then add to the list if we can parse the patch
          RepositoryOperations.getPatchJGIT(git, c) match {
            case Some(x) => {
              // we want to the patch to have added something
              if (x.split("\n").toList.filter(x => x.startsWith("+") && !x.startsWith("++")).size > 0) {
                result.+=(c -> "")
                i = i + 1
              }
            }
            case None => Nil // we cannot parse the patch, thus we skip it.
          }
        }
        case _ => Nil // found the commit, so we need to look for another
      }
    }
    result.keySet.toVector
  }

  /**
    * Find all commtis in the forks that have been created before this commit and have not been integrated back in the mainline.
    * This traverses all the parents starting from the given commit. Any commits that are not traversed
    * are considered to be commits in branches, thus we keep them if they satisfy the time constraint (they need to be before the
    * give commitTime)
    *
    * @param commits    - a map of commit to a list of its parents (SHAs)
    * @param cs         a list of RevCommit objects
    * @param sha        starting point in our traversal
    * @param commitTime commit time to use for discarding newer commits
    * @return
    */
  def findCommits(commits: Map[String, List[String]], cs: List[RevCommit], sha: String, commitTime: Int) = {
    //    val parents = new RevWalk(git.getRepository).parseCommit(git.getRepository.resolve(sha)).getParents.toList.map(_.getName)
    val parents = try {
      commits.get(sha).get
    }catch {
      case ex: java.util.NoSuchElementException => println("Cannot find parents of this commit. Likely, the project repository does not contain this fork"); List()
    }
      // should have at least one parent
      // we just mark when we traverse all parents from this commit. Any commits not in this list, are commits from forks, and we finally trim the list by the commit time
      def traverseParentsTailRec(parent: List[String]): Map[String, String] = {
        @tailrec
        def innerLoop(parents: List[String], a: Map[String, String]): Map[String, String] = {
          if (parents.isEmpty) {
            a
          }
          else {
            val newParents = (for {
              p <- parents
              t = a.get(p) match {
                case None => commits.get(p).get
                case Some(x) => Nil
              }
            } yield t).flatten
            innerLoop(newParents, a ++ parents.map(x => x -> "").toMap)
          }
        }
        innerLoop(parent, Map())
      }

      val res = Set(sha) ++ traverseParentsTailRec(parents).toList.map(_._1)
      cs.par.filter(_.getCommitTime < commitTime).toList.map(_.getName.take(7)).diff(res.toList)
  }

  case class AddedLine2(sha: String, file: String, position: Int, normalizedText: String, originalText: String) {
//    override def toString = {
//      "id: " + sha + "," + file + "," + "p: " + position + "," + normalizedText
//    }
  }


  def emptylineCheck(x: String): Boolean = {
    val p = Pattern.compile(emptylineRegex,Pattern.MULTILINE)
    val m = p.matcher(x)
    m.matches()
  }


  def patchToAddedLinesInternal(patch: String,sha: String,removeStopWords: Boolean, project: Int): Vector[AddedLine] = {
    val patchFiles = PatchParser.splitPatchByFiles(patch).getOrElse(List()).flatMap {
      case _: BinaryFile => None
      case diff: DiffFile => Some(diff)
    }
    computePatchToAddedLines(patchFiles,sha,removeStopWords, project)
  }

  def computePatchToAddedLines(diffs: Seq[PatchParser.DiffFile], sha:String,removeStopWords: Boolean, project: Int): Vector[AddedLine] = {
    //        val res = collection.mutable.Map[String, List[String]]()
//    logger.info("removing stop words set to " + removeStopWords)
    val res = collection.mutable.ArrayBuffer[AddedLine]()

    def computeHunks(file: DiffFile, hunks: Option[Seq[PatchParser.Hunk]]) = {
      hunks.getOrElse(Nil).foreach(hunk => {
        val hunkResult = collection.mutable.ListBuffer[AddedLine]()
        val hunkLines = hunk.patch.split("\r?\n").toList.drop(2)
        val startHunk = {
          try {
            LineChangeModel.stringToRangeHunk(hunkLines.head).end
          } catch {
            case ex: Exception => println("commit " + sha + " some exception when finding the index of the hunk start \n"); 0
          }
        }
        var c = {
          if (startHunk == 0)
            0
          else
            startHunk - 1
        }
//        println(startHunk)
//        println(c)

        hunkLines.drop(1).foreach(value => {
          if (value.startsWith("+")) {
            c = c + 1
            val originalText = value
            // remove html comments, if any
            //                if (!commentsRegex.matcher(originalText).matches) {
            if (!Pattern.matches(commentsRegex, originalText) || !Pattern.matches(annotationRegex, originalText) || !Pattern.matches(modifiersRegex, originalText)) {
              if (removeStopWords) {
                val x = RemoveStopWords.removeStopWords(originalText)
                //                    if (!(emptyline.matcher(x).matches || pythonKeywords.matcher(x).matches || elseKeyword.matcher(x).matches || importRegex.matcher(x).matches || breakKeyword.matcher(x).matches || returnKeyword.matcher(x).matches || bracketsSemicolon.matcher(x).matches)) {
                if (!(x.trim.isEmpty || Pattern.matches(pythonKeywordsRegex, originalText) || Pattern.matches(elseKeywordRegex, originalText) || Pattern.matches(importRegex, originalText) || Pattern.matches(breakKeywordRegex, originalText) || Pattern.matches(returnKeywordRegex, originalText) || Pattern.matches(bracketsSemicolonRegex, originalText))) {
                  if (!x.trim.isEmpty)
                    hunkResult.+=(AddedLine(0, project, sha, file.filename, c, x.trim, originalText.drop(1).trim))
                }
              }
              else {
                val x = originalText
                if (!(x.trim.isEmpty || Pattern.matches(pythonKeywordsRegex, x) || Pattern.matches(elseKeywordRegex, x) || Pattern.matches(importRegex, x) || Pattern.matches(breakKeywordRegex, x) || Pattern.matches(returnKeywordRegex, x) || Pattern.matches(bracketsSemicolonRegex, x))) {
                  hunkResult.+=(AddedLine(0, project, sha, file.filename, c, originalText.drop(1).trim, originalText.drop(1).trim))
                }
              }
            }
          }
          else if (value.startsWith("-")) {
            // do not do anything if a line has removed

          }
          else {
            c = c + 1
          }
        })
        if (!hunkResult.isEmpty) {
          // changes could be only added new lines, or whitespace changes to empty lines, thus if the hunk is empty we do not consider it
          res.++=(hunkResult)
        }
      })
    }

    diffs.foreach(file => {
      val hunks = PatchParser.splitPatchByHunks(file.patch)
      val currentPatchSize = hunks.getOrElse(Nil).map(x => x.patch.split("\r?\n").toList.drop(2)).flatten.count(_.startsWith("+"))
      if(currentPatchSize <= patchSize) { // eliminate commits that have more than $patchSize added lines
        computeHunks(file, hunks)
      }
      else if(patchSize == 0){ // no limit of commit size
        computeHunks(file, hunks)
      }
    })
    res.toVector
  }

  /**
    * Given an SHA and a repository, creates a list of added lines objects from files to the list of added lines in the patch, containing the line number where the line was added in that file
    *
    * @param git
    * @param sha
    * @return
    */

  def patchToAddedLines(git: Git, sha: String, removeStopWords: Boolean = false, project: Int = 0): Option[Vector[AddedLine]] = {
    RepositoryOperations.getPatchJGIT(git, sha) match {
      case Some(patch) => {
        val res = patchToAddedLinesInternal(patch,sha,removeStopWords, project)
        Some(res)
      }
      case None => None
    }
  }

  /**
    * Return a diff unified patch from a commit SHA
    *
    * @param git
    * @param sha
    * @return
    */

  def getPatchJGIT(git: Git, sha: String, fullUnifiedContext: Boolean = false): Option[String] = {
    //    println(sha)
    val repository = git.getRepository
    //a new reader to read objects from getObjectDatabase()
    val reader = repository.newObjectReader
    //Create a new parser.
    val oldTreeIter = new CanonicalTreeParser
    val newTreeIter = new CanonicalTreeParser
    //parse a git repository string and return an ObjectId
    try {
      val old = repository.resolve(sha + "~1^{tree}") // previous ommit
      val head = repository.resolve(sha + "^{tree}") // head commit
      //Reset this parser to walk through the given tree
      oldTreeIter.reset(reader, old)
      newTreeIter.reset(reader, head)
      val diffs = git.diff() //Returns a command object to execute a diff command
        .setNewTree(newTreeIter)
        .setOldTree(oldTreeIter)
        .call //returns a DiffEntry for each path which is different

      val out = new ByteArrayOutputStream
      //Create a new formatter with a default level of context.
      val df = new DiffFormatter(out)
      //Set the repository the formatter can load object contents from.
      df.setDiffComparator(RawTextComparator.WS_IGNORE_ALL)
      fullUnifiedContext match {
        case true => df.setContext(999999999)
        case false => Nil
      }
      df.setRepository(git.getRepository)
      val diffTextLines = collection.mutable.ListBuffer[String]()
      //A DiffEntry is 'A value class representing a change to a file' therefore for each file you have a diff entry

      for (diff <- diffs.asScala.toList) {
        //      df.toFileHeader(diff).getScriptText
        //Format a patch script for one file entry.
        df.format(diff)
        val r = new RawText(out.toByteArray)
        r.getLineDelimiter
        diffTextLines.+=(out.toString)
        out.reset
      }
      Some(diffTextLines.mkString("\n"))
    }
    catch {
      case ex: Exception => None // basically, this will exclude the first commit which should exist in any fork
    }
  }



  /**
    * Find the object id of a file
    *
    * @param git
    * @param branch
    * @param file
    * @return
    */

  def fileObjectID(git: Git, branch: String, file: String): ObjectId = {
    val repository = git.getRepository
    val lastCommit = repository.exactRef("refs/remotes/" + branch).getObjectId
    val revWalk = new RevWalk(repository)
    val commit = revWalk.parseCommit(lastCommit)
    val tree = commit.getTree()
    val treeWalk = new TreeWalk(repository)
    treeWalk.addTree(tree)
    treeWalk.setRecursive(true)
    treeWalk.setFilter(PathFilter.create(file))
    if (!treeWalk.next()) {
      //      return new RawText(new Array[Byte](0))
    }
    val objectId = treeWalk.getObjectId(0)
    objectId
  }

  def getForksBranches(git: Git, forkname: String): Map[String, Ref] = {
    git.getRepository.getAllRefs.asScala.toList.filter(x => x._1.startsWith("refs/remotes/" + forkname + "/")).toMap
  }

  def getBranchThatModifiedFileMostRecently(git: Git, branches: Map[String, Ref], filename: String) = {
    case class R(commitTime: Int, commit: RevCommit, branch: String)
    val commits =
      for {
        b <- branches.keySet
        c = getCommitsThatModifiedFile(git.getRepository, b, filename).head
      } yield R(c.getCommitTime, c, b)
    commits.toSeq.sortBy(_.commitTime).reverse.head.branch
  }

  def pushAllChanges(git: Git, authToken: String) = {
    git.push.setPushAll().setCredentialsProvider(new UsernamePasswordCredentialsProvider("${token}", authToken)).call()
    git.getRepository.close
    git.close
  }

  def createBranchAndPush(git: Git, branchName:String, authToken: String) = {
    try {
      git.checkout().setCreateBranch(true).setName(branchName).call()
    }catch {
      case ex: Exception => sys.error("some error occurred when creating a branch. perhaps creating a branch with an existing name")
    }
    git.push.setCredentialsProvider(new UsernamePasswordCredentialsProvider("${token}", authToken)).call()
    git.getRepository.close()
    git.close()
  }


  /**
    * Clone the repository locally to the machine but do not checkout a working directory
    *
    * @param reponame
    */
  def cloneRepository(reponame: String, branch: String, outputFolder: String, dontCheckout: Boolean =  false) = {
    val localdir = new File(outputFolder + "/" + reponame)
    if (localdir.exists && localdir.isDirectory) {
      println("Repository already exists.")
//      Git.open(new File(localdir + "/.git"))
    }
    else {
      print("Project does not exist locally. Creating a clone...")
      new java.io.File(outputFolder).mkdirs()
      val branchesToClone = new util.ArrayList[String]()
      branchesToClone.add("refs/heads/" + branch)
      val repo = Git.cloneRepository
        .setNoCheckout(dontCheckout)
        .setBranchesToClone(branchesToClone)
        .setURI("https://github.com/" + reponame + ".git")
        .setDirectory(localdir)
        .call
      repo.fetch.call()
      println("done")
      repo.getRepository.close
    }
  }


  /**
    * Clone the repository locally to the machine but do not checkout a working directory
    *
    * @param reponame
    */
  def cloneRepository(reponame: String, branch: String): Git = {
    val localdir = new File(repodir + reponame)
    if (localdir.exists && localdir.isDirectory) {
      println("Repository already exists.")
      Git.open(localdir)
    }
    else {
      print("Project does not exist locally. Creating a clone...")
      val branchesToClone = new util.ArrayList[String]()
      branchesToClone.add("refs/heads/" + branch)
      val repo = Git.cloneRepository
        .setNoCheckout(true)
//        .setBranchesToClone(branchesToClone)
        .setURI("https://github.com/" + reponame + ".git")
        .setDirectory(localdir)
        .call
      repo.fetch
      println("done")
      repo
    }
  }

  def deleteLocalGitRepository(path: String) =
    better.files.File(path).delete()


  /**
    * Add forks as remotes to the git repository
    *
    * @param gitrepo
    * @param forks
    */
  def addForksAsRemotes(gitrepo: Git, forks: Seq[GithubRepository]): Unit = {
    print("Adding forks as remotes...")
    forks.foreach(fork => {
      val uri = new URIish(githubURL + fork.full_name + ".git")
      val remote = gitrepo.remoteAdd()
      remote.setName(fork.full_name)
      remote.setUri(uri)
      remote.call
    })
    val fw = new FileWriter(gitrepo.getRepository.getDirectory.getAbsoluteFile.getParentFile.getAbsolutePath + "/" + forksjson)
    fw.write(forks.toJson.prettyPrint)
    fw.close
    println("done")
  }

  // TODO this needs to keep track of which forks have been updated or not and maybe update those?
  def addNewForksAsRemotes(gitrepo: Git, forks: List[GithubRepository]): List[GithubRepository] = {
    // decode existing forks
    val existingForks = readFromFileToString(gitrepo.getRepository.getDirectory.getAbsoluteFile.getParentFile.getAbsolutePath + "/" + forksjson).parseJson.convertTo[List[GithubRepository]]
    // see if there have been added new forks
    val newForks = forks.diff(existingForks)
    // add those as remotes
    newForks.foreach(x => {
      gitrepo.fetch.setRemote(x.full_name).call
    })
    val fw = new FileWriter(gitrepo.getRepository.getDirectory.getAbsoluteFile.getParentFile.getAbsolutePath + "/" + forksjson)
    fw.write(forks.toJson.prettyPrint)
    fw.close
    newForks
  }

  def updateForksRemotes(gitrepo: Git, forks: List[GithubRepository]) = {
    print("Updating existing forks' changes...")
    var i = 1
    val s = forks.size
    forks.foreach(x => {
      println("Fetching fork #" + i + " out of " + s)
      gitrepo.fetch.setRemote(x.full_name).call
      i = i + 1
    })
    println("done")
  }

  /**
    * Fetch the forks' changes via remotes
    *
    * @param gitrepo
    * @param remoteNames
    */
  def fetchForksRemotes(gitrepo: Git, remoteNames: Seq[GithubRepository]) = {
    print("Fetching forks' changes...")
    var i = 1
    val s = remoteNames.size
    remoteNames.foreach(x => {
      println("Fetching fork #" + i + " out of " + s)
      try {
        gitrepo.fetch.setRemote(x.full_name).call
        i = i + 1
      } catch {
        case ex: Exception => println(ex.getMessage)
      }

    }) // fetch will get all remote branches; did not figure out how to fetch a single branch
    // hack to remove non-default branches because I can't figure out how to fetch a single branch (the default one)
    //    val allRemoteBranches = gitrepo.branchList().setListMode(ListMode.REMOTE).call.asScala.toList.map(x => x.getName) // get all remote branches
    //    val onlyDefaultBranches = remoteNames.map(x => "refs/remotes/"+x._2 + "/" + x._1)
    //    val origBranch = allRemoteBranches.find(x => x.contains("refs/remotes/origin/")).get
    //    val deleteBranches = allRemoteBranches.diff(List(origBranch) ++ onlyDefaultBranches)
    //    deleteBranches.foreach(x => gitrepo.branchDelete.setBranchNames(x).setForce(true).call)
    println("done")
  }

}
