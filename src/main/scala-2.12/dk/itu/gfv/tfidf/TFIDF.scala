package dk.itu.gfv.tfidf

import better.files.File
import dk.itu.gfv.util.MonitorProperties
import dk.itu.gfv.util.Util._

import scala.collection.immutable.HashMap

/**
  * Created by scas on 19-04-2017.
  */
trait TFIDF extends MonitorProperties {

  def compute_tfidf(word: String, document: String, documentList: Seq[String])(implicit separator: String) = {
    val word_trimmed = word.trim.toLowerCase
    val tf_score = tf(word_trimmed, document, separator)
    val idf_score = idf(word_trimmed, documentList, separator)
    tf_score * idf_score
  }

  def tf(word: String, document: String, delimiter: String) = {
    val documentTokens = document.split(delimiter).map(_.trim.toLowerCase).toList
    val wordFrequency = (word: String) => {
      val documentWordFrequency = documentTokens.groupBy(identity).map(e => e._1 -> e._2.length)
      documentWordFrequency.getOrElse(word, 0)
    }
    wordFrequency(word).toDouble / documentTokens.size
  }

  def idf(word: String, documentList: Seq[String], delimiter: String) = {
    def numberOfDocsContainingWord(documentList: Seq[String]) = {
      val documentTokens = documentList.map(document => document.split(delimiter).map(_.trim.toLowerCase).toList)
      documentTokens.foldLeft(0) { (acc, e) => if (e.contains(word)) acc + 1 else acc }
    }
    Math.log(documentList.size.toDouble / numberOfDocsContainingWord(documentList))
  }

  def tf_idf_language(language: String, files: List[String]) = {
      val projectDocument = files.map(readFromFileLines(_)).flatten
      HashMap(language -> (projectDocument.mkString(separator)))
  }

  def tf_idf_projects(projects: List[String])(implicit separator: String): Map[String, String] = {
    val valid_extensions = File("file-extensions-corrected.txt").lines.toList
    def computeProject(p: String) = {
      val projectFiles = files(tfidf_projects_folder + p, true).filter(x => valid_extensions.contains(fileExtension(x)))
      val projectDocument = projectFiles.map(readFromFileLines(_)).flatten
      HashMap(p -> (projectDocument.mkString(separator)))
    }

    def compute(input: List[String]): Map[String, String] = input match {
      case h :: t => computeProject(h) ++ compute(t)
      case List(h) => computeProject(h)
      case List() => HashMap()
    }
    compute(projects)
  }

}
