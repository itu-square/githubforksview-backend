
package dk.itu.gfv.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.headers.RawHeader
import dk.itu.gfv.monitor.Ssh
import dk.itu.gfv.monitor.db.Models.Survey
import spray.json.DefaultJsonProtocol._
import dk.itu.gfv.monitor.db.NightFetcher._
import dk.itu.gfv.monitor.evaluation.GenerateHTML
import dk.itu.gfv.monitor.frontend.GithubScrapper

import scala.concurrent.duration.Duration
import scala.io.StdIn
import scala.concurrent.{Await}

object BootAkka extends Ssh {

  final case class PostGMS(id: Int, comment: String, useful: Boolean)

  final case class PostDuplicatesRetrospective(id: String, useful: Boolean)
  final case class PostDuplicateInternalUseful(id: String, useful: Boolean)
  final case class PostDuplicateInternalNotUseful(id: String, notUseful: Boolean)

  final case class SurveyResponse(link: String, q1: String, q2: String, q3: String, q4: String)

  // formats for unmarshalling and marshalling
  implicit val postGMS = jsonFormat3(PostGMS)
  implicit val postDuplicatesRetrospective = jsonFormat2(PostDuplicatesRetrospective)
  implicit val internalUsefulDuplicate = jsonFormat2(PostDuplicateInternalUseful)
  implicit val internalNotUsefulDuplicate = jsonFormat2(PostDuplicateInternalNotUseful)
  implicit val surveyResponseCodec = jsonFormat5(SurveyResponse)


  def main(args: Array[String]) {

    // needed to run the route
    implicit val system = ActorSystem("gms")
    implicit val materializer = ActorMaterializer()
    // needed for the future map/flatmap in the end
    implicit val executionContext = system.dispatcher

    val route: Route =
      path("gms") {
        get {
          // there might be no item for a given id
          parameters('repo, 'sha, 'files, 'rows.as[String]) {
            (repo, sha, files, rows) => {
              respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {
                //              respondWithMediaType(MediaTypes.`text/html`) {
                val result = {
                  val f = files.split(",").toList
                  val r = rows.split(";").toList
                  if (r.size != f.size) {
                    "WRONG URL; NR OF FILES DO NOT MATCHES THE NR OF ROWS"
                  }
                  else {
                    val filesAndRows = f.zip(r).map(x => x._1 -> x._2.split(",").toList.map(x => Integer.valueOf(x).toInt)).toMap
                    try {
                      val result = dk.itu.gfv.monitor.frontend.GithubScrapper.updateHTML(repo, sha, filesAndRows)
                      result.html
                    }
                    catch {
                      case ex: java.net.UnknownHostException => "Please make sure you are connected to internet"
                      //case ex: Exception => logger.error("Unknown error" + ex.getMessage); "Unknown error"
                    }

                  }
                }
                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, result))
              }
            }
          }

        }
      } ~
        path("gms") {
          post {
            entity(as[PostGMS]) { c => {
              respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {
                Await.result(updateSimilarityComments(c.id, c.useful, c.comment), Duration.Inf)
                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say hello to akka-http</h1>"))
              }
            }
            }
          }
        } ~
        path("test") {
          get {
            respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {
              complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, "it's working"))
            }
          }
        } ~
        path("test6") {
          get {
            respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {
              val test = scala.io.Source.fromFile("test6.html").getLines.toList.mkString("\n")
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, test))
            }
          }
        } ~
        path("duplicates") {
          get {
            parameters('top, 'repo, 'sha.?) {
              (top, repo, sha) => {
                respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {
                  val html = GenerateHTML.generate_project_html(top, repo, sha.getOrElse(""))
                  complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))

                }
              }
            }
          }
        } ~
        path("duplicates1") {
          get {
          parameters('top, 'repo, 'sha.?,'dir.?,'from.?) {
            (top, repo, sha,dir,from) => {
              respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {

                dir match {
                  case Some(x) =>  {
                    val html = GenerateHTML.getPage(top,repo,sha.getOrElse(""), x, from.get)
                    complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, html))
                  }
                  case None => {
                    val html = GenerateHTML.generate_project_html1(top,repo,sha.getOrElse(""), dir, from)
                    complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))
                  }
                }

  //              if (dir.isDefined && from.isDefined){
  //                val html = GenerateHTML.generate_project_html_initial(top,repo,sha.getOrElse(""), dir.get, Integer.parseInt(from.get))
  //                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))
  //              }
  //              else {
  //                val html = GenerateHTML.generate_project_html(top, repo, sha.getOrElse(""))
  //                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))
  //              }
              }
            }
          }
      }
    } ~
        path("duplicate") {
          get {
            parameters('start, 'end.?) {
              (start, end) => {
                respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {

//                  end match {
//                    case Some(x) =>  {
//                      val html = GenerateHTML.getPage(start,end)
//                      complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, html))
//                    }
//                    case None => {
                      val html = GenerateHTML.generateDuplicateReport(start, None)
                      complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))
//                    }
                  }

                  //              if (dir.isDefined && from.isDefined){
                  //                val html = GenerateHTML.generate_project_html_initial(top,repo,sha.getOrElse(""), dir.get, Integer.parseInt(from.get))
                  //                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))
                  //              }
                  //              else {
                  //                val html = GenerateHTML.generate_project_html(top, repo, sha.getOrElse(""))
                  //                complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, html))
                  //              }
                }
              }
            }
        } ~
      path("survey") {
        post {
          entity(as[SurveyResponse]) { s => {
            respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*")))
            Await.result(insertSurvey(Survey(0, s.link, s.q1, s.q2, s.q3, s.q4)), Duration.Inf)
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, ""))
          }
          }
        }
      } ~
      path("duplicates") {
        post {
          entity(as[PostDuplicateInternalUseful]) { c => {
            respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {
              println(c)
              Await.result(updateInternalUsefulDuplicate(Integer.parseInt(c.id), c.useful), Duration.Inf)
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "OK"))
            }
          }
          }
        }
      } ~
      path("duplicates") {
        post {
          entity(as[PostDuplicateInternalNotUseful]) { c => {
            respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {
              println(c)
              Await.result(updateInternalNotUsefulDuplicate(Integer.parseInt(c.id), c.notUseful), Duration.Inf)
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "OK"))
            }
          }
          }
        }
      } ~
      path("duplicate_context") {
        get {
          parameters('project, 'commit, 'filename, 'line_number.as[Int]) {
            (project, commit, filename, line_number) => {
              respondWithHeaders(List(RawHeader("Access-Control-Allow-Origin", "*"))) {
                val result = GithubScrapper.get_github_commit_context(project, commit, filename, line_number)
                complete(HttpEntity(ContentTypes.`text/plain(UTF-8)`, result))
              }
            }
          }
        }
      }
    val port = args.size match {
      case 1 => args(0).toInt
      case _ => sys.error("please provide a port number")
    }
    val bindingFuture = Http().bindAndHandle(route, "localhost", port)
    println(s"Server online at http://localhost:$port/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ ⇒ {
      ssh match {
        case Some(x) => x.disconnect(); system.terminate()
        case None => system.terminate()
      }
    }) // and shutdown when done

  }

}