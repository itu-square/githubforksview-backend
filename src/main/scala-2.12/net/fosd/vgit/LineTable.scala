package net.fosd.vgit

/* Copyright (c)
    Author:
    Christian Kaestner, CMU

    version 0.1

    This file is part of GithubForksView.
    GithubForksView is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GithubForksView is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GithubForksView.  If not, see <http://www.gnu.org/licenses/>.

 */
// copied from https://github.com/ckaestne/vgit on July 8th,2016 at commit f927fe45ea7020ff1dd82d73073175ed8ac0e000
import org.eclipse.jgit.diff.{RawText, Edit, EditList}
import scala.annotation.tailrec
import scala.collection.JavaConverters._

/**
  * line table indicates which revision contains which line in which place
  */
class LineTable {

  val COMMON_CODE = "white"
  val DELETED_CODE_FROM_UPSTREAM = "#c5ff54"
  val CODE = "98FB98" //"#00ffff"
  val DELETED_SPLIT =  COMMON_CODE //"#FFB6C1" // Light Pink
  val ADDED_SPLIT = CODE // PaleGreen

  type GitCommitId = String
  type LineNr = Int

  var commitIds: List[GitCommitId] = List()
  var lines: List[Line] = List()

  case class Line(id: String, text: String, revisionPositions: List[LineNr]) {
    def incHeadPos(by: Int) = {
      var h = revisionPositions.head
      if (h != -1)
        h += by
      Line(id, text, h :: revisionPositions.tail) // why doing taking the tail here?
    }

    def setHeadPos(to: Int) = Line(id, text, to :: revisionPositions.tail) // what is this method actually for?
  }

  def init(id: GitCommitId, initialContent: RawText): Unit = {
    commitIds ::= id
    lines = List()
    for (i <- 0 until initialContent.size())
      lines ::= Line("initial-L" + i, initialContent.getString(i), List(i))
  }



  def update(id: GitCommitId, parentid: GitCommitId, edits: EditList, newText: RawText) = {
    val parentIdx = commitIds.indexOf(parentid)
    commitIds ::= id

//    def makeLines (input: List[Line]): List[Line] = {

//      @tailrec
//      def loop(x: List[Line], y: List[Line]): List[Line] = x match{
//        case h :: t => loop(t, Line(h.id, h.text, h.revisionPositions(parentIdx) :: h.revisionPositions) :: y)
//        case Nil => y
//      }
//      loop(input, List())
//    }
//    lines = makeLines(lines)
//    println(edits.reverse)
//    println(lines)
    lines = for (l <- lines) yield {
      Line(l.id, l.text, l.revisionPositions(parentIdx) :: l.revisionPositions)
    }
//    println(lines)
    for (edit: Edit <- edits.asScala.toList.reverse) {
      var newLines = List[Line]()
      var inserted = false
      val linesSize = lines.size
      var j = 0
      while(j < linesSize){
        val l = lines.head
        if (l.revisionPositions.head < edit.getBeginA) // checkBySameHash if line position is later than the beginning of the edit
          newLines ::= l
        else if (l.revisionPositions.head >= edit.getEndA)
          newLines ::= l.incHeadPos(edit.getLengthB - edit.getLengthA)
        else
          newLines ::= l.setHeadPos(-1) //this did not exist?

        // this I do not get
        if (l.revisionPositions.head == edit.getEndA) {
          var s = edit.getLengthB
          while(s > 0){
            newLines ::= Line(id + "-L" + (edit.getBeginB + s), newText.getString(edit.getBeginB + s), List.fill(commitIds.size)(-1)).setHeadPos(edit.getBeginA + s)
            s = s-1
          }

          inserted = true
        }
        lines = lines.tail
        j += 1
      }
      if (!inserted) {
        val s = edit.getLengthB
        var i = 0
        while (i < s) {
          newLines ::= Line(id + "-L" + (edit.getBeginB + i), newText.getString(edit.getBeginB + i), List.fill(commitIds.size)(-1)).setHeadPos(edit.getBeginA + i)
          i +=1
        }
      }

      lines = newLines.reverse
    }
  }

  override def toString() = {
    (for (l <- lines) yield
      l.id + ": " + l.revisionPositions.mkString(" ")).mkString("\n")
  }

  def getCondition(l: Line): String =
    (l.revisionPositions zip commitIds).filter(_._1 >= 0).map(_._2).mkString("", " || ", "")

  def toIfdef(): String = {
    val out = new StringBuffer()
    var lastLineCondition: String = ""

    for (l <- lines.reverse) {
      val condition: String = getCondition(l)

      if (condition != lastLineCondition) {
        if (lastLineCondition != "")
          out.append("#endif\n")
        if (condition != "")
          out.append("#if ").append(condition).append("\n")
      }
      lastLineCondition = condition

      out.append(l.text).append("\n")
    }
    if (lastLineCondition != "")
      out.append("#endif\n")
    out.toString
  }

  def getColoredCondition(i: Int, l: Line): ColoredCondition = {
    val z = (l.revisionPositions zip commitIds).filter(_._1 >= 0)
    //    println(z)
    if (z.size == commitIds.size) { //does the line appear in all forks?
      // common code
      ColoredCondition(COMMON_CODE, i, l.text, "")
    }
    //
    else if (z.size == 1 && z.head._2 == commitIds.last) {
      // if it has the original repo only, then it was removed from the forks
      val forks = commitIds.init.mkString(",")
      ColoredCondition(DELETED_CODE_FROM_UPSTREAM, i, l.text,forks)
    }

    else { // code that was added in some forks
      val forks = commitIds.init.mkString(",")
      ColoredCondition(CODE, i, l.text,forks)
    }
  }

  def toColouredHTMLTable(): String = {
    val out = new StringBuffer()
//    out.append("<table class=\"highlight tab-size js-file-line-container\" data-tab-size=\"16\">" + "\n")
    out.append(
      """<dialog id="myDialog"></dialog>
        |<table class="highlight tab-size js-file-line-container" data-tab-size="8">
        |                  <colgroup>
        |                    <col width="80">
        |                    <col>
        |                    <col width="60">
        |                  </colgroup>
        |""".stripMargin + "\n")
    var i = 1
    for (l <- lines.reverse) {
      val condition = getColoredCondition(i, l)
      i = i + 1
      out.append(condition.toString).append("\n")
    }
    out.append("</table>")
    out.toString
  }

  def toColouredSplitHTMLTable(): String = {
    val out = new StringBuffer()
    out.append(
      """<table class="diff-table tab-size  file-diff-split" data-tab-size="8">
        |                  <colgroup>
        |                    <col width="52">
        |                    <col>
        |                    <col width="52">
        |                    <col>
        |                  </colgroup>
        | """.stripMargin + "\n")
    var i = 1
    for (l <- lines.reverse) {
      val condition = getColoredCondition(i, l)
      i = i + 1
      val color = condition.color
      val text = condition.text
      val lineNr = condition.lineNr
      if (condition.color == COMMON_CODE) {
        val x =
          s"""<tr>
              |<td class="blob-num js-line-number" data-line-number="$lineNr"></td>
              |<td id="LC$lineNr" class="blob-code blob-code-inner js-file-line" >$text </td>
              |<td class="blob-num js-line-number" data-line-number="$lineNr"></td>
              |<td id="LC$lineNr" class="blob-code blob-code-inner js-file-line" ></td>
              |</tr>
          """.stripMargin
        out.append(x).append("\n")
      }
      else if (condition.color == DELETED_CODE_FROM_UPSTREAM) {
        val x =
          s"""<tr>
              |<td class="blob-num js-line-number" data-line-number="$lineNr"></td>
              |<td id="LC$lineNr" class="blob-code blob-code-inner js-file-line" ></td>
              |<td class="blob-num js-line-number" data-line-number="$lineNr"></td>
              |<td id="LC$lineNr" class="blob-code blob-code-inner js-file-line" bgcolor="$DELETED_SPLIT">-$text </td>
              |</tr>
          """.stripMargin
        out.append(x).append("\n")
      }
      else if (condition.color == CODE) {
        val x =
          s"""<tr>
              |<td class="blob-num js-line-number" data-line-number="$lineNr"></td>
              |<td id="LC$lineNr" class="blob-code blob-code-inner js-file-line"> </td>
              |<td class="blob-num js-line-number" data-line-number="$lineNr"></td>
              |<td id="LC$lineNr" class="blob-code blob-code-inner js-file-line" bgcolor="$ADDED_SPLIT">+$text </td>
              |</tr>
          """.stripMargin
        out.append(x).append("\n")
      }
      else {

      }
    }
    out.append("</table>")
    out.toString
  }


  case class ColoredCondition(color: String, lineNr: Int, text: String, link: String) {
    override def toString = {
      if(color == COMMON_CODE){
        s"""<tr>
            |<td id="L$lineNr" class="blob-num js-line-number" data-line-number="$lineNr"></td>
            |<td id="LC$lineNr" class="blob-code blob-code-inner js-file-line" bgcolor="$color">$text </td>
            |<td id="LL$lineNr" class="link" hidden="true"></td>
            |</tr>""".stripMargin
      }
      else {
        s"""<tr>
            |<td id="L$lineNr" class="blob-num js-line-number" data-line-number="$lineNr"></td>
            |<td id="LC$lineNr" class="blob-code blob-code-inner js-file-line" bgcolor="$color">$text </td>
            |<td id="LL$lineNr" class="link" title="$link">Link</td>
            |</tr>""".stripMargin
      }
    }

  }

}